//document.getElementById("app").innerHTML="<p>Rendered webpack bundle</p>";
const angular = require('angular');
"use strict";


require('angular-resource');
require('angular-ui-router');
require('angular-ui-bootstrap');
require('angular-animate');
require('angular-messages');
require('angular-sanitize');
require('angular-xeditable');
require('angular-moment');
require('moment');
require('moment/locale/tr');
// require('jquery');
//require('xlsx/xlsx');
//const jsPDF = require('jspdf/dist/jspdf.min');
//require('jspdf-autotable/dist/jspdf.plugin.autotable.min');
// require('tableexport.jquery.plugin/libs/js-xlsx/xlsx.core.min');
// require('tableexport.jquery.plugin/libs/jsPDF/jspdf.min');
// require('tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable');
// require('tableexport.jquery.plugin/tableExport.min');

// var $ = require('jquery');

/*var FileSaver = */require('file-saver');
//portos framework modules
require('./modules/portos/core/portos.core');
require('./modules/portos/core/services/DomainServiceFactory');
require('./modules/portos/core/services/ApplicationDataFactory');
require('./modules/portos/navigation/portos.navigation');
require('./modules/portos/formInput/portos.formInput');
require('./modules/portos/formInput/directives/formField');
require('./modules/portos/formInput/directives/formFieldMessages');
require('./modules/portos/formInput/directives/formGroup');
// require('./modules/portos/menu/portos.menu');
// require('./modules/portos/menu/directives/portosMenu');
// require('./modules/portos/menu/domain/Menu');
// require('./modules/portos/menu/services/menuService');
require('./modules/portos/security/portos.security');
require('./modules/portos/spinner/portos.spinner');

require('./modules/portos/security/controllers/securityController');
require('./modules/portos/security/services/securityService');

//application modules
require('./app/yevmiye/yevmiye');
require('./app/logyev/logyev');
require('./app/hesplan/hesplan');
require('./app/gunlukFinans/gunlukFinans');
require('./app/detayliTahsilat/detayliTahsilat');
// import 'modules/portos/configmanager.core'


// import angular from 'angular'; //ES6 syntax check if it is supported by grails-webpack-profile
angular.module('app', [
    'portos.core',
    'portos.navigation',
    'portos.formInput',
    // 'portos.menu',
    'portos.security',
    'portos.spinner',
    'app.yevmiye',
    'app.hesplan',
    'app.logyev',
    'app.gunlukFinans',
    'app.detayliTahsilat',
    'ui.router',
    'angularMoment',
])
    .run(function(amMoment){
        amMoment.changeLocale('tr');
    })
    .config(config)
    .controller('AppController', function($scope){
       $scope.version = "1.6.4 oolum";
    })
    // .factory('FileSaver', function(){
    //     return function FileSaver //angular.extend(this, FileSaver);
    //     // this.saveAs = FileSaver.saveAs;
    // })

;



function config($stateProvider, $urlRouterProvider){
    $stateProvider.state('app',{
        url: '/app',
        template: require('./index.html')
    });
    $urlRouterProvider.otherwise('/app/yevmiye/list');
}
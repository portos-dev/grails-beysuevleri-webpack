/**
 * Created by birkan on 05.05.2017.
 */
angular.module('portos.menu')
    .controller('PortosMenuController',  ['SecurityService', 'MenuService','$scope', 
    function(SecurityService, MenuService, $scope){
        //TODO watch for authentication event and get menu config from url/file
        //console.log('PortosMenuController INIT');

        var vm = this;
        //vm.user = SecurityService.user
        //vm.userRoles = SecurityService.userRoles;

        $scope.$on('event.portos.security.auth.login.success', function(evt, user){
            vm.user = user;
            vm.loadMenu();
        });
        $scope.$on('event.portos.security.auth.logout.success', function(evt, user){
            vm.user = null;
        });

//     SecurityService.currentUserRoles().then(function(roles){
//         vm.userRoles = roles;
//         console.log('vm.userRoles: ' + vm.userRoles);
//     });

//     $scope.$watch('vm.userRoles.$resolved', function(val){
//         console.log('vm.userRoles.$resolved: ' + vm.userRoles.$resolved);
//     });

        //$scope.$watchCollection('vm.userRoles', function(val){
        //console.log('vm.userRoles: ');
        //console.log(vm.userRoles);
        //});

        vm.loadMenu = function(){
          vm.menus = MenuService.getMenus();  
        };

        vm.menuFilter = function(menu){
            if(vm.user) {
                return vm.user.roles.indexOf(menu.role) >= 0;
            }else{
                return false;
            }
        };

//         vm.menus = [
//             {
//                 name: 'menu1',
//                 displayName: 'Menu1',
//                 role: 'ROLE_USER_MENU',
//                 menuItems: [
//                     {
//                         displayName: 'Config Manager',
//                         state: 'configmanager.home'
//                     },
//                     {
//                         displayName: 'InputType',
//                         state: 'inputType.list'
//                     },
//                     {
//                         displayName: 'OutputType',
//                         state: 'outputType.list'
//                     },
//                     {
//                         displayName: 'Input (dev)',
//                         state: 'input.list'
//                     },
//                     {
//                         displayName: 'Output (dev)',
//                         state: 'output.list'
//                     }
//                 ]
//             },
//             {
//                 name: 'menu2',
//                 displayName: 'Menu2',
//                 role: 'ROLE_ADMIN_MENU',
//                 menuItems: [
//                     {
//                         displayName: 'Config Manager',
//                         state: 'configmanager.home'
//                     },
//                     {
//                         displayName: 'Input (dev)',
//                         state: 'input.list'
//                     },
//                     {
//                         displayName: 'Output (dev)',
//                         state: 'output.list'
//                     }
//                 ]
//             }
//         ]
    }])
    .directive('portosMenu',function($templateCache){
        return {
            scope: {
                style: '@',
                // previousText: '@',
                // nextText: '@',
                // lastText: '@',
            },
            replace: true,
            controller: 'PortosMenuController',
            controllerAs: 'menuCtrl',
            // templateUrl: function(element, attrs) {
            //     return attrs.templateUrl || '../../template/menu/menu.html' //|| 'configmanager/template/menu/menu.html';
            // }
            template: require('../menu.html')
            // templateUrl: '/portos/menu/menu.html'
            //templateUrl: 'portos-menu.html'
        }
    });

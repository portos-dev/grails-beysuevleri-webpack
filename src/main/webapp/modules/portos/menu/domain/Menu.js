/**
 * Created by birkan on 05.05.2017.
 */

Menu.$inject = ['domainServiceFactory'];
angular
    .module("portos.menu")
    .factory("Menu", Menu);


function Menu(domainServiceFactory) {
    return domainServiceFactory("api/menu/:id", {"id": "@id"},
        {
//             userMenus: {url: 'api/userMenu', isArray: true, method: 'get'},
            //erroneous: {url:'api/erroneousInputs',isArray: true, method:'get'},
            list: {
                isArray: true, method: 'get'/*,
                 transformResponse: function (data, headers) {
                 // console.log(data);
                 return JSON.parse(data); //.results;
                 }*/
            },
            // props: {url:'api/input/:id/prop',isArray: true, method:'get'}
        });
}

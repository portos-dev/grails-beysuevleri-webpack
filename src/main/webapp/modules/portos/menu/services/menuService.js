/**
 * Created by birkan on 05.05.2017.
 */
angular.module('portos.menu')
    .service('MenuService', function(Menu){
        this.getMenus = function(){
            return Menu.list();
        } ;
        //TODO use Menu resource, get menu items
    });

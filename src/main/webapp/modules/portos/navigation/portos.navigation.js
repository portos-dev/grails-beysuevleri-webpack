/**
 * Created by birkan on 25.05.2017.
 */
angular.module('portos.navigation', [])

.directive('navigation', function(){



    return {
        restrict: 'E',
        scope: {
            max : '=',
            //TODO navigateAction min: '=',
            count: '=',
            offset: '=',
            pageAction: '&',
        },
        template: require('./navigation.html'),
        /*'<ul class="pagination" ng-show="vm.list.count > vm.list.max">\
                    <li ng-repeat="page in vm.pageList">\
                    <a ng-disabled="vm.currentPage == page.index"\
                        ui-sref=".({max:page.max, offset: page.offset})">{{page.index}}</a>\
                    </li>\
                    </ul>'*/
        link: function(scope, iElem, iAttr){
            console.log('count: ' + scope.count);
            console.log('max: ' + scope.max);
            console.log('offset: ' + scope.offset);

            if (scope.count > scope.max){
                console.log('pagination necessary')
            }

            scope.$watch('count', function (val){
                console.log('count: ' + scope.count);
                updateNavigator();
            });
            scope.$watch('max', function (val){
                console.log('max: ' + scope.max);
                updateNavigator();
            });
            scope.$watch('offset', function (val){
                console.log('offset: ' + scope.offset);
                updateNavigator();
            });

            scope.navigateTo = navigateTo;

            function navigateTo(page){
                scope.pageAction({offset:page.offset, max: scope.max});
            }

            // precision is 10 for 10ths, 100 for 100ths, etc.
            function roundUp(num, precision) {
                return Math.ceil(num * precision) / precision
            }

            function updateNavigator() {
                if (scope.max && scope.offset >= 0 && scope.count) {
                    var farthest = 5;
                    var pageCount = roundUp(scope.count / scope.max, 1);
                    var currentPage = (scope.offset / scope.max) + 1;
                    var pages = [];

                    console.log('pageCount: ' + pageCount);
                    console.log('currentPage: ' + currentPage);

                    for (var i = 0; i < pageCount; i++) {
                        var page = i + 1;
                        if(page == 1 || page == pageCount){
                            pages.push({text: i + 1, offset: i * scope.max, index: i + 1})
                        }
                        else if ((currentPage - page > farthest || page - currentPage > farthest)) {
                            // ortalamayi bul ve o offsete ortalayan tek bir ... textli buton koy
                            // ve i yi sona veya basa kadar olan tum indexler i atla

                            var jumpToPage;
                            var skipToIndex;

                            if (currentPage - page > farthest) { //sayfa geride kalmis
                                // jumpToPage = roundUp((currentPage + page) / 2, 1);// : roundUp((page + pageCount) /2, 1);
                                jumpToPage = Math.max(2, currentPage - farthest*2);// : roundUp((page + pageCount) /2, 1);
                                skipToIndex = currentPage - farthest - 2; //: pageCount-2;
                            }
                            else if (page - currentPage > farthest) { //sayfa ileride
                                // jumpToPage = roundUp((page + pageCount) / 2, 1);
                                jumpToPage = Math.min(pageCount, currentPage + farthest*2); //roundUp((page + pageCount) / 2, 1);
                                skipToIndex = pageCount-2;
                            }
                            else {
                                console.log(i + " : indexi beklentiye uymuyor")
                            }
                            console.log(i)
                            console.log('jumpToPage: ' + jumpToPage + ', skipToIndex: ' + skipToIndex);

                            // pages.push({text: '...' + jumpToPage + '...', offset: jumpToPage * scope.max, index: jumpToPage})
                            // pages.push({text: jumpToPage, offset: (jumpToPage-1) * scope.max, index: jumpToPage})
                            pages.push({text: '...', offset: (jumpToPage-1) * scope.max, index: jumpToPage})
                            i = skipToIndex;
                        }
                        else {
                            pages.push({text: i + 1, offset: i * scope.max, index: i + 1})
                        }
                    }
                    scope.currentPage = currentPage;
                    scope.pages = pages;
                }
            }
        }
    }
})

.filter('activePages', function(){
    return function(pages, currentPage) {
        console.log(pages);
        console.log(currentPage);


    }
});

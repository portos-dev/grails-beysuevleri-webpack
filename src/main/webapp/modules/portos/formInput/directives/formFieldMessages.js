angular.module('portos.formInput')
    /*.directive('integer', function(){
        return {
            restrict: 'A',
            require: ['^form','ngModel'],
            link: function(scope, element, attrs, ctrls){
                var formController = ctrls[0];
                var ngModelController = ctrls[1];
                console.log('ngModelController: ' , ngModelController.$name)

                ngModelController.$render = function(){
                    console.log('render: ' + ngModelController.$name);
                };
            }
        }
    })*/
    .directive('hasMessages', function() {
        var messagesTpl = '\
                <div ng-messages="form.fieldModelName.$error" ng-show="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
                  <p ng-message="required" class="help-block">Zorunlu alan</p>\
                  <p ng-message="minlength"  class="help-block">Cok kisa</p>\
                  <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
                </div>\
                ';

        var postLink = function(scope, iElem, attrs, ctrls) {//instance element
            var formController = ctrls[0];
            var ngModelController = ctrls[1];
            // iElem.after(
            //     messagesTpl
            //         .replace(/fieldModelName/g, attrs.name)
            // );
            console.log('hasMessages.postLink');
            console.log('ngModelController: ', ngModelController.$name)

        };
        return {
            restrict: 'A',
            // restrict: 'E',
            replace: true,
            // template: '<div></div>',
            // scope:{},
            proirity: 1000,
            require: ['^form','ngModel'],
            compile: function(tElem, attrs){//template element
                console.log('hasMessages.compile');
                var messages = messagesTpl
                    .replace(/fieldModelName/g, attrs.name);
                // element.wrap()

                tElem.after(
                    messagesTpl
                        .replace(/fieldModelName/g, attrs.name)
                );

                return postLink

                /*console.log(name + ': compile => ' + tElem.html());
                return {
                    pre: function(scope, iElem, iAttrs){
                        console.log(name + ': pre link => ' + iElem.html());
                        // iElem.prepend("prepend some prelink")
                        // iElem.append("append some prelink")
                    },
                    post: function(scope, iElem, iAttrs){
                        console.log(name + ': post link => ' + iElem.html());
                    }
                }*/
            },
            link: postLink
        }
    })


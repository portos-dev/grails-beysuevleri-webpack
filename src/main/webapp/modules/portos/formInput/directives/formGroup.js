/**
 * Created by birkan on 02.05.2017.
 */
angular.module('portos.formInput')
    /*.directive('formGroup', function() {
        var templateFn = function (element, attrs, didi, bibi) {
            var templateWithVars = '\
      <div class="form-group" ng-class="{errorClassName: hasError}">\
      <label class="col-md-2 control-label">\
        labelName\
        <span ng-show="required">*</span>\
      </label>\
      <div class="col-md-10">\
        <div ng-transclude></div>\
        <div ng-messages="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
          <p ng-message="required" class="help-block">Zorunlu alan</p>\
          <p ng-message="minlength"  class="help-block">Cok kisa</p>\
          <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
        </div>\
      </div>\
      </div>\
      ';

            var errorClassName = "'has-error'";
            var labelName = attrs.label;
            var inputType = attrs.type;
            var fieldModelName = attrs.name;//attrs.ngModelName; //getFieldModelName(attrs);

            console.log(fieldModelName);

            var template = templateWithVars
                .replace("errorClassName", errorClassName)
                .replace("labelName", labelName)
                //  .replace("inputType", inputType)
                .replace(/fieldModelName/g, fieldModelName);

            return template;

        };

        function getFieldModelName(attrs) {
            var objectAndField = attrs.ngModel;
            var names = objectAndField.split('.');
            var fieldModelName = names[names.length-1];
            return fieldModelName;
        }

        return {
            //scope: {
                // name: '='
            //},
            restrict: 'E',
            replace: true,
            //template: templateFn,
            // controller: function($scope){
            //     this.setError = function(boolean){
            //         $scope.hasError = boolean;
            //     }
            // },
            template: templateFn,
            //transclude: 'element',
            transclude: true,
            link: function(scope, element, attrs, ctrl, transclude) {
                // if (attrs.required !== undefined) {
                //if ('required' in attrs) {
                // If attribute required exists
                // ng-required takes a boolean
                //scope.required = true;
                //}
                // transclude(function(clone)
                // {
                //     element.append(clone);
                // });
            }
        }
    });*/

    .directive('formGroup', function(){
        var messagesTpl = '\
                <div ng-messages="form.fieldModelName.$error" ng-show="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
                  <p ng-message="required" class="help-block">Zorunlu alan</p>\
                  <p ng-message="minlength"  class="help-block">Cok kisa</p>\
                  <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
                </div>\
                ';
        var labelTpl = '<label class="col-md-2 control-label">\
        fieldLabel<span>fieldRequired</span>\
        </label>\
        ';

        var inputTpl = ''
        return {
            // restrict: 'A',
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            scope:{},
            require: ['^form','ngModel'],
            compile: function(tElem, attrs){//template element
                /*console.log(attrs);
                 if(attrs.type == 'submit'){
                 element.addClass('btn btn-primary');
                 }
                 else if(attrs.type == 'text'){
                 element.addClass('form-control');
                 }
                 else if(attrs.type == 'checkbox'){
                 element.addClass('form-control');
                 }*/
                //element.addClass('form-control');

                var messages = messagesTpl
                //.replace('/fieldModelName/g', attrs.name);
                    .replace(/fieldModelName/g, attrs.name);
                // element.wrap()
                if(attrs.label) {
                    tElem.prepend(labelTpl
                        .replace(/fieldLabel/, attrs.label || attrs.name || 'undefined')
                        .replace(/fieldRequired/, attrs.required ? '*' : ''));
                }

                tElem.append(
                    '<div class="col-md-10">\
                        <input type="text" ng-model="ngModelName"\
                         name="fieldModelName" class="form-control">\
                     </div>'
                        .replace(/fieldModelName/g, attrs.name)
                        .replace(/ngModelName/g, attrs.ngModel)
                        .replace(/<input/, angular.isDefined(attrs.required) ? '<input required': '<input')
                    +
                    messagesTpl
                        .replace(/fieldModelName/g, attrs.name)
                );

                // tElem.append(messagesTpl
                //     .replace(/fieldModelName/g, attrs.name));
                // tElem.append('</div>')
                tElem.addClass('form-group');

                console.log(name + ': compile => ' + tElem.html());
                return {
                    pre: function(scope, iElem, iAttrs){
                        console.log(name + ': pre link => ' + iElem.html());
                        // iElem.prepend("prepend some prelink")
                        // iElem.append("append some prelink")
                    },
                    post: function(scope, iElem, iAttrs){
                        console.log(name + ': post link => ' + iElem.html());
                    }
                }
            },
            link: function(scope, element, attrs, ctrls){
                // element[0].prepend(<div>my before div</div>)
                // element.wrap(function(){
                //     '<div class="col-md-10">\
                //     \
                //     ' + this +
                //         '</div>'
                // });
                // element.addClass('form-control');
                // element.wrap('<div class="form-group">\
                //     <label class="col-md-2 control-label">Active<span>*</span></label>\
                //     <div class="col-md-10">\
                //     </div>');
                // element.after('</div>')
                var formController = ctrls[0];
                var ngModelController = ctrls[1];

                scope.form = formController;

                console.log('ngModelController: ' , ngModelController.$name)

                /*ngModelController.$render = function(){
                 console.log('render: ' + ngModelController.$name);
                 };*/

                // <div ng-messages="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">
                /* var messagesTpl = '\
                 <div ng-messages="form.fieldModelName.$error" ng-show="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
                 <p ng-message="required" class="help-block">Zorunlu alan</p>\
                 <p ng-message="minlength"  class="help-block">Cok kisa</p>\
                 <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
                 </div>\
                 ';
                 var messages = messagesTpl
                 //.replace('/fieldModelName/g', attrs.name);
                 .replace(/fieldModelName/g, attrs.name);
                 element.after(messages);*/
            }
        }
    });

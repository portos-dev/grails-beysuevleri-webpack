/**
 * Created by birkan on 02.05.2017.
 */
angular.module('portos.formInput')
    .directive('formField', function(/*$compile*/){
        return {
            restrict: 'A',
            priority: 1000.1,
            scope: {},
            compile: function (tElement, tAttrs) {
                // var input = tElement.find('input');
                // var input = angular.element(tElement.querySelectorAll('[ng-model]')[0]);
                var input = angular.element(tElement.find('input')[0]);
                if(!angular.isDefined(tAttrs.noFeedback)) {
                    tElement.addClass('has-feedback');
                    var feedback = angular.element('<span class="glyphicon form-control-feedback"></span>');
                    input.after(feedback);
                }
                var inputName = input.attr('name');
                var name = inputName || 'input-name';
                var required = input.attr('required') || input.attr('ng-required') || false;//TODO check when both required and ng-required exist

                tAttrs.name = name;
                tAttrs.required = required;

                //tAttrs.ngClass= "{'has-error': ((form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error)}"
                //.replace(/fieldModelName/g, name); //TODO does not compile properly, see if any alternative exists?
                return {
                     post: function(scope, element) {
                        //$compile(element)(scope); infinite loop :)
                     }
                }
            }
        }
    })
    .directive('formField', function(){
        var messagesTpl = '\
                <div ng-messages="form.fieldModelName.$error" ng-show="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
                  <p ng-message="required" class="help-block">Zorunlu alan</p>\
                  <p ng-message="minlength"  class="help-block">Cok kisa</p>\
                  <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
                </div>\
                ';
        var labelTpl = '<label class="col-md-3 control-label" for="fieldModelName">\
        fieldLabel<span>fieldRequired</span>\
        </label>\
        ';

        var inputTpl = ''

        var link = function(scope, iElem, attrs, ctrls, transclude){
            console.log('formField.postLink');

             transclude(function(clone){
                angular.element(iElem.querySelectorAll('.form-field-transclude')[0])
                   .prepend(clone);
                  // iElem.append('<div ng-hide="true">My messages</div>')
                });

            var checkForm = function(){
                 var invalid = formController[attrs.name].$invalid;
                 var hasError = invalid && (formController.$submitted || formController[attrs.name].$touched);
                 var hasSuccess = !invalid && formController[attrs.name].$touched;
                 console.log(formController.$name + '.' + attrs.name + '.$invalid: ' + formController[attrs.name].$invalid);
                 iElem.toggleClass('has-error', hasError);
                 iElem.toggleClass('has-success', hasSuccess);

                 angular.element(iElem.querySelectorAll('.glyphicon.form-control-feedback')).toggleClass('glyphicon-remove', hasError);
                 angular.element(iElem.querySelectorAll('.glyphicon.form-control-feedback')).toggleClass('glyphicon-ok', hasSuccess);
                 //TODO show error messages in detail on icon hover, i.e title, tooltip, popover etc.
            }

            var formController = ctrls[0];

             var formVariable = formController.$name + '.' + attrs.name;
             scope.$watch(formVariable + '.$invalid'  , function(val){
                    checkForm();
             });
             scope.$watch(formVariable + '.$touched'  , function(val){
                      checkForm();
             });
             scope.$watch(formController.$name + '.$submitted'  , function(val){
                      checkForm();
             });

        };

        return {
            restrict: 'A',
            priority: 1000,
            require: ['^form'/*,'ngModel'*/],
            transclude: true,//'element',
            compile: function(tElem, attrs, transclude){//template element
                console.log('formField.compile');
                console.log(transclude);
                /*console.log(attrs);
                 if(attrs.type == 'submit'){
                 element.addClass('btn btn-primary');
                 }
                 else if(attrs.type == 'text'){
                 element.addClass('form-control');
                 }
                 else if(attrs.type == 'checkbox'){
                 element.addClass('form-control');
                 }*/
                //element.addClass('form-control');

                //var children = tElem.querySelectorAll('.ng-transclude') //.children()[0];
               // console.log(tElem.children());


                    tElem.prepend(labelTpl
                        .replace(/fieldLabel/, attrs.label || attrs.name || 'undefined')
                        .replace(/fieldRequired/, attrs.required ? '*' : '')
                        .replace(/fieldModelName/g, attrs.name));


                tElem.append(
                     '<div class="col-md-9 form-field-transclude">'
                   +
                    (angular.isDefined(attrs.noMessages) ?  '': messagesTpl
                        .replace(/fieldModelName/g, attrs.name))
                    +
                    '</div>'
                );

                tElem.addClass('form-group');

//               TODO:  attrs.ngClass= "{'has-error': ((form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error)}"
//                     .replace(/fieldModelName/g, name);

                console.log(name + ': compile => ' + tElem.html());
                return {
                    pre: function(scope, iElem, iAttrs){
                        console.log(name + ': pre link => ' + iElem.html());
                        // iElem.prepend("prepend some prelink")
                        // iElem.append("append some prelink")
                    },
                    post: link
                }
            },
            link: link
        }
    });

require('angular-messages')

angular.module('portos.formInput', ['ngMessages'])
  .directive('formInput', function(){
    return {
      scope: {
        model: '=',//expression
        name: '@',
        form: '=',
        required: '@'
      },
      require: '^form',
      replace: true,
      restrict: 'E',
      templateUrl: 'configmanager/template/formInput/inputText.html'
    }
  })
  .directive('inputName', function() {
//         <span ng-show="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error.required" class="help-block">Cant be blank</span>\

    var templateFn = function(element, attrs) {
        //ng-required="required" in tag
        var templateWithVars = '\
        <input  name="fieldModelName"  ng-model="value" class="form-control" >\
        ';
      // var templateWithVars = '\
      // <div class="form-group" ng-class="{errorClassName: ((form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$invalid)}">\
      // <label class="col-md-2 control-label">\
      //   labelName\
      //   <span ng-show="required">*</span>\
      // </label>\
      // <div class="col-md-10">\
      //   <input  name="fieldModelName"  ng-model="value" class="form-control" ng-required="required">\
      //   <div ng-messages="(form.fieldModelName.$touched || form.$submitted) && form.fieldModelName.$error">\
      //     <p ng-message="required" class="help-block">Zorunlu alan</p>\
      //     <p ng-message="minlength"  class="help-block">Cok kisa</p>\
      //     <p ng-message="maxlength"  class="help-block">Cok uzun</p>\
      //   </div>\
      // </div>\
      // </div>\
      // ';

      var errorClassName = "'has-error'";
      var labelName = attrs.label;
      var inputType = attrs.type;
      var fieldModelName = getFieldModelName(attrs);

      console.log(fieldModelName);

      var template = templateWithVars
        // .replace("errorClassName", errorClassName)
        // .replace("labelName", labelName)
      //  .replace("inputType", inputType)
        .replace(/fieldModelName/g, fieldModelName);

      return template;
    };

    function getFieldModelName(attrs) {
      var objectAndField = attrs.ngModel;
      var names = objectAndField.split('.');
      var fieldModelName = names[names.length-1];
      return fieldModelName;
    }

    return {
      restrict: 'E',
      // template: templateFn,
      template: templateFn,
      replace: false,
      // controller: ['$scope', function($scope) {}],
      require: ['^form', 'ngModel','^formDiv'],

      // See Isolating the Scope of a Directive http://docs.angularjs.org/guide/directive#isolating-the-scope-of-a-directive
      scope: {
        ngModel: '='
      },
      // scope: true,
        //scope: true - the directive creates a new child scope that prototypically inherits from the parent scope. Properties that are defined on the parent scope are available to the directive scope (because of prototypal inheritance). Just beware of writing to a primitive scope property -- that will create a new property on the directive scope (that hides/shadows the parent scope property of the same name).

      link: function(scope, element, attrs, ctrls) {
      // link: function(scope, element, attrs, ngModelController) {
        //scope.form = ctrls[0];
        var ngModelController = ctrls[1];
        var formDivController = ctrls[2];

        // if (attrs.required !== undefined) {
        if ('required' in attrs) {
          // If attribute required exists
          // ng-required takes a boolean
          scope.required = true;
        }

        attrs.name = attrs.name || getFieldModelName(attrs);

        //var fieldModelName = getFieldModelName(attrs);

        //var ngModelExp = ngModelController.$$attr.ngModel;

//         scope.$watch(ngModelController.$$attr.ngModel, function(val){
//           console.log(val);
//           //ngModelController.$setViewValue(scope.$parent.$eval(ngModelExp));
//         });

        //when model change, update our view
        ngModelController.$render = function(){
            //element.find('input').value = ngModelController.$viewValue;
            scope.value = ngModelController.$viewValue;
        };

        scope.$watch('value', function(value){
          console.log('scope.value changed: ' + value);

          formDivController.setError(value ? false: true);

          if(attrs.required){
              if(!value){
                  ngModelController.$setValidity('required', true);
                  ngModelController.$setViewValue(value);
                  ngModelController.$render();

              }
              else {

            ngModelController.$setViewValue(value);
              }
          }
          else {

            ngModelController.$setViewValue(value);
          }

            //update the local view
            //ngModelController.$render() this is not needed since source of change is input field itself, otherwise we should call this method to reflect changes to the view
        });

        /*scope.$watch(fieldModelName, function() {
          ngModel.$setViewValue(scope[fieldModelName]);
        });*/
      }
    };
  })
// see http://blog.revolunet.com/blog/2013/11/28/create-resusable-angularjs-input-component/
    .directive('editor', function(){
        var toogleType = function(attrs){
          attrs.type = (attrs.type === 'text') ?  'submit' : 'text';
        };
        //var templateTemp =
        return {
            require: 'ngModel',
            replace: true,
           /* scope: {
              value: '=ngModel'
                /!*
                 This will automagically? bind our internal value variable to the external one declared in the ngModel attribute.
                  The = means “double data-binding” which means if ngModel is updated externally then the internal value will be updated, and vice-versa.
                 *!/
            },*/
            // template: "<input  ng-model='value' ng-change='onChange()'>",
            scope: {},
            template: '<input>',
            link: function(scope, element, attrs, ngModelController){
              element.bind('click', function(){
                  toogleType(attrs);
              });

              /*if(!ngModel) return;



                scope.onChange = function(){
                    ngModel.$setViewValue(scope.value);
                };

                ngModel.$render = function(){
                    scope.value = ngModel.$modelValue;
                }*/
            }
        }
        /*return {
            require: '?ngModel',
            scope: true,
            template: "<input  ng-model='value' ng-change='onChange()'>",
            link: function(scope, element, attrs, ngModel){
                if(!ngModel) return;

                scope.onChange = function(){
                    ngModel.$setViewValue(scope.value);
                };

                ngModel.$render = function(){
                    scope.value = ngModel.$modelValue;
                }
            }
        }*/
    })




/*

 default (scope: false) - the directive does not create a new scope, so there is no inheritance here. The directive's scope is the same scope as the parent/container. In the link function, use the first parameter (typically scope).
 scope: true - the directive creates a new child scope that prototypically inherits from the parent scope. Properties that are defined on the parent scope are available to the directive scope (because of prototypal inheritance). Just beware of writing to a primitive scope property -- that will create a new property on the directive scope (that hides/shadows the parent scope property of the same name).
 scope: { ... } - the directive creates a new isolate/isolated scope. It does not prototypically inherit the parent scope. You can still access the parent scope using $parent, but this is not normally recommended. Instead, you should specify which parent scope properties (and/or function) the directive needs via additional attributes on the same element where the directive is used, using the =, @, and & notation.
 transclude: true - the directive creates a new "transcluded" child scope, which prototypically inherits from the parent scope. If the directive also creates an isolate scope, the transcluded and the isolate scopes are siblings. The $parent property of each scope references the same parent scope.
 Angular v1.3 update: If the directive also creates an isolate scope, the transcluded scope is now a child of the isolate scope. The transcluded and isolate scopes are no longer siblings. The $parent property of the transcluded scope now references the isolate scope.
 */

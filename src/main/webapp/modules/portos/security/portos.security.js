// require(['./controllers/securityController'])
//console.log("portos.security")
angular.module("portos.security", ["portos.core"])
  .directive('portosLoginMenu', function() {
      return {
          restrict: 'E',
          replace: true,
          controller: 'SecurityController',
          controllerAs: 'vm',
          // templateUrl: function (element, attrs) {
          //     return attrs.templateUrl //|| require('../template/security/loginmenu.html')// || 'configmanager/template/security/loginmenu.html';
          // },
          template: require('./loginmenu.html'),
          link: function (scope, element, attrs) {
              //TODO event.stopPropagation for #login-dp ?
              var ul = element[0].querySelector('#login-dp');
              ul.onclick = function (evt) {
                  evt.stopPropagation();
              }
          }

      }
  });




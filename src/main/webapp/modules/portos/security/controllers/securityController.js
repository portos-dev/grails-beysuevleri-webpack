
SecurityController.$inject = ['applicationDataFactory', 'contextPath', '$state', '$scope', '$location', 'SecurityService'];angular
    .module("portos.security")
    .factory('authInterceptor', ['$rootScope', '$window', '$q', '$location', function ($rootScope, $window, $q, $location) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                }
                else{
                    //$rootScope.$broadcast('event.logout');
                }
                return config;
            },
           /* TODO response: function (response) {
                // do something on success
                if(response.headers()['content-type'] === "application/json; charset=utf-8"){
                    // Validate response, if not ok reject
                    var data = examineJSONResponse(response); // assumes this function is available

                    if(!data)
                        return $q.reject(response);
                }
                return response;
            },*/
            responseError: function (response) {
                // do something on error
                var statusCode = response.status;
                console.log("ERROR STATUS: " + statusCode);
                if(statusCode === 401){
                    console.log("NEED TO LOGIN!!: " + response.data.path);
                    //$location.path('/');
                    $rootScope.$broadcast('event.portos.security.http.response.status.401', response);
                }
                else if(statusCode === 403){
                    console.log("YETKISIZ ISLEM!!: " + response.data.path);
                    $rootScope.$broadcast('event.portos.security.http.response.status.403', response);
                }
                else if(statusCode === 422){
                    console.log("VALIDATION HATASI!!: "  + response.data.path);
                    $rootScope.$broadcast('event.portos.security.http.response.status.422', response);
                }
                else {
                    console.log('response error: '   + response.data.path);
                    console.log(response);
                    console.log('statusCode ' + statusCode);
                    if ($window.sessionStorage.token) {
                        console.log('token var!?');
                    }
                    else {
                        console.log('token yok!?');
                    }
                }
                return $q.reject(response);
            }
        };
    }])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('authInterceptor');
    }])
    .controller("SecurityController", SecurityController);

function SecurityController(applicationDataFactory, contextPath, $state, $scope, $location, SecurityService) {
    var vm = this;

    vm.init = function(){
        //console.log('SecurityController INIT');
        $scope.$on('event.portos.security.auth.login.success', function(evt, user){
            vm.authenticated = user ? true : false; //TODO what if I already have $window.sessionStorage.token
            vm.user = user;
        });
        $scope.$on('event.portos.security.auth.logout.success', function(evt, user){
            vm.authenticated = false;//user ? true : false; //TODO what if I already have $window.sessionStorage.token
            vm.user = null;
            $location.path('/');
        });

        $scope.$on('event.portos.security.http.response.status.401', function(evt, resp){
            if(vm.authenticated || vm.user) {
                vm.authenticated = false;//user ? true : false; //TODO what if I already have $window.sessionStorage.token
                vm.user = null;
            }
            if($location.path() !== "/"){
                console.log($location.path());
                $location.path('/');
            }
        });

//         $rootScope.$watch('SecurityService.user', function(user){
//             vm.authenticated = user ? true : false; //TODO what if I already have $window.sessionStorage.token
//             vm.user = user;
//             //$state.reload();
//         });


        vm.contextPath = contextPath;

        applicationDataFactory.get().then(function(response) {
            vm.applicationData = response.data;
        });
        if(!vm.user) {
            SecurityService.currentUser().then(
                function (user) {
                    if (user) {
                        vm.user = user;
                        vm.authenticated = true;
                        //$state.reload();
                    }
                    else {
                        vm.logout();
                        // $state.reload();
                    }
                }, function () {
                    //vm.logout();
                });
        }
    };
    vm.stateExists = function(name) {
        return $state.get(name) != null;
    };
    vm.login = function(){
        SecurityService.login(vm.username, vm.password).then(function(response){
            //vm.authenticated = true;
            //$rootScope.$broadcast('event.authentication.success');
            // $window.sessionStorage.token = response.data.access_token;
            console.log($state.current.name + ' is reloading');
            //debugger;
            //$state.reload();
        });
    };
    vm.logout = function() {
        SecurityService.logout();
        //vm.authenticated = false;
        //vm.user = null;
        //$window.sessionStorage.token = null;
        //$rootScope.$broadcast('event.authentication.success');
        //$location.path('/');
        console.log($state.current.name + ' is reloading');
        //debugger;
        //$state.reload();
        /*  });*/
    };

    vm.init();
}

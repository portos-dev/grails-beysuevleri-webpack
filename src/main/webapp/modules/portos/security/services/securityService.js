angular.module('portos.security')
.service('SecurityService', ['$http', '$window', '$q', '$rootScope', function($http, $window, $q, $rootScope){
    
   // this.userRoles = null;
   var vm = this;
   vm.user = null;

    vm.login = function(user, pass) {
        return $http.post('api/login', {
            username: user,
            password: pass
        }).then(function (response) {
            //$rootScope.$broadcast('event.authentication.success');
            $window.sessionStorage.token = response.data.access_token;
            //this.user = { username: response.data.username, roles: response.data.roles/*, permissions: response.data.permissions*/};
            //$rootScope.$broadcast('event.portos.security.auth.login.success', this.user);

            vm.currentUser();
            //this.currentUserRoles();
            return response;
        });
    };
    vm.logout = function(){
        //var defer = $q.defer();
        $window.sessionStorage.token = null;
        $rootScope.$broadcast('event.portos.security.auth.logout.success', this.user);
        this.user = null;
        //this.userRoles = null;
        //$q.resolve();
        //return defer.promise;
    };

    vm.currentUser = function(){
        
        return $http.get('api/user').then(
            function(resp){
                var user = resp.data;
                if(!vm.user && user) {//then mimic login, we do not set user immediately after login, this makes getting permissions for user possible
                    vm.user = user;
                    $rootScope.$broadcast('event.portos.security.auth.login.success', vm.user);
                }
                return user;
            },
            function(err){
                return;
            }
        );
    };

   /* this.currentUserRoles = function(){
        return $http.get('user').then(
            function(resp){
                var user = resp.data;
                //console.log('RESOLVED USER');
                //console.log(user.roles);
                this.userRoles = user.roles;
                return user.roles;//Promise.resolve(user.roles);
            },
            function(err){
                return null;//err;//Promise.reject(err);
            }
        );
    };*/
}]);

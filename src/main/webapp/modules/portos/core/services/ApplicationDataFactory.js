angular.module('portos.core')
    .factory("applicationDataFactory", applicationDataFactory);

applicationDataFactory.$inject = ['$http'];

function applicationDataFactory($http) {
    return {
        get: function() {
            return $http({method: "GET", url: "api/application"});
        }
    }
}

function IndexController( $scope, applicationDataFactory, contextPath, $state, $rootScope, $window) {
    var vm = this;
    //console.log('IndexController INIT');
    //console.log('ALL STATES');
    //console.log($state.get());

    vm.allStates =  $state.get();

    vm.contextPath = contextPath;

    applicationDataFactory.get().then(function(response) {
        vm.applicationData = response.data;
    });

    vm.stateExists = function(name) {
        return $state.get(name) != null;
    };

    /*  $scope.$on('event.authentication.success', function(){
     console.log('received event -> ' + 'event.authentication.success');
     console.log('reloading state');
     $state.reload();
     });*/

    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            //$scope.showSpinner();
            // $scope.$apply(function() {
            $scope.loading = true;
            // $rootScope.loading = true;
            // });
        }
    });
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if (toState.resolve) {
            //$scope.hideSpinner();
            // $scope.$apply(function() {
            $scope.loading = false;
            // $rootScope.loading = false;
            // });
        }
    });

    $rootScope.goBack = function(){
        $window.history.back();
    }

}





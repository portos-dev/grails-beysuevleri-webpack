angular.module("portos.core", ['ngResource'])
    .constant("contextPath", window.contextPath)
    .constant("apiPath", window.apiPath)
    .config(['$httpProvider', function config($httpProvider) {
        $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        $httpProvider.interceptors.push(['contextPath', 'apiPath', function (contextPath, apiPath) {
            return {
                request: function (config) {
                    if (!config.url.indexOf("/") == 0 && contextPath && config.url.indexOf("uib/template") == -1) {
                        config.url = contextPath + "/" + config.url;
                    }
                    /*if (!config.url.indexOf("/") == 0 && config.url.indexOf("configmanager/template") == 0) {
                     config.url = "/" + config.url;
                     }*/

                    //console.log(config.url);
                    if (!config.url.indexOf("/") == 0 && apiPath && config.url.indexOf(".html") == -1) {
                        config.url = apiPath + "/" + config.url;
                        //console.log(config.url + " after apiPath " + apiPath );
                    }
                    return config;
                },
                response: function (response) {
                    convertToDate(response.data);
                    return response;
                }
            };
        }]);
    }])









function isIso8601(value) {
    return angular.isString(value) && /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d+)?Z$/.test(value);
}

function convertToDate(input) {
    if (!angular.isObject(input)) {
        return input;
    }

    angular.forEach(input, function (value, key) {
        if (isIso8601(value)) {
            input[key] = new Date(value);
        } else if (angular.isObject(value)) {
            convertToDate(value);
        }
    });
}




/**
 * Created by birkan on 17.04.2017.
 */
angular.module('configmanager.notifications', [])
  .config(['$httpProvider', function($httpProvider){
      //$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
      $httpProvider.interceptors.push(['apiPath', 'NotificationService', '$q', function httpRequestInterceptor(apiPath, NotificationService, $q) {
        return {
          'request': function(config){
            // debugger;
            // if(config.url.indexOf('html') >= 0){
            //   //console.log("HTML");
            // }
            // else if(config.url.indexOf('auth/') >= 0 || config.url.indexOf('user/') >= 0){
            //   //console.log("AUTH REQUEST");
            // }
            // else if(config.url.indexOf('personelIzin/') >= 0 || config.url.indexOf('personelIzinForm/') >= 0){
            //   //console.log("AUTH REQUEST");
            // }
            // else{
            //   config.headers['Authorization']= 'Bearer ' + $rootScope.token;
            // }
            return config;
          },
          'response': function(response) {

            // if(response.config.url.indexOf('.html') < 0){
            //   //console.log("HTML");
            //   response.config.responseTimestamp = new Date().getTime();
            //   //$log.debug(response.status);
            //   NotificationService.success(response);
            // }

            return response;
          },
          'responseError': function(rejection) {
              rejection.eventDate = new Date();

              var statusCode = rejection.status;
              console.log("ERROR STATUS: " + statusCode);
              if (statusCode === 401) {
                  console.log("NEED TO LOGIN!!");
              }
              else if (statusCode === 403) {
                  console.log("YETKISIZ ISLEM!!");
              }
              // do something on error
              NotificationService.alert(rejection);
              console.log('rejection');
              console.log(rejection);
              if (rejection.status === 404) {
                  //    $location.path('/404/');
              }

              else if (rejection.status === 422) {
                  console.log('validation error');
                  //   $location.path('/auth/login');
              }

              return $q.reject(rejection);
          }
        };
      }]);
  }])
  .factory('NotificationService', [ '$rootScope', function ( $rootScope) {
  return {
    alert : function (data) {

      /*$scope.modalInstance = $modal.open({
        //angular.js:68 Uncaught Error: [$injector:cdep] Circular dependency found: $templateRequest <- $uibModal <- NotificationService <- $http <- $templateRequest <- $compile
        //templateUrl: "configmanager.alert/alert.html",
        template: 'TODO: ALERT MODAL <button ng-click="close()"> </button>',
        controller: function () {
          return {
            close: function () {
              $scope.modalInstance.close();
            }
          }
        }

      });*/
      $rootScope.$broadcast('evt.configmanager.notification', data);
    }
  }
}])
  //hold a singleton for notifications
  .service('NotificationManager', ['$uibModal', function($uibModal){
      this.notifications = [];
      // this.modals = [];
      // this.modalInstance;
      this.add = function(data){
        this.notifications.push(data);
        //this.modals.push(this.show(data));
        console.log(this.notifications.length);
        if(!this.modalInstance){
          this.modalInstance = this.show();
        }
      };
      this.close = function(data){
        if(this.modalInstance) {
          this.modalInstance.close();
          this.modalInstance = null; //TODO need to reuse existing modal?
        }
        /*var index = this.notifications.indexOf(data);
        if(index >= 0){
          this.modals[index].close();
        }*/
      };


      this.show = function(notificationData){
        var nm = this;
        var modalInstance =  $uibModal.open({
          //angular.js:68 Uncaught Error: [$injector:cdep] Circular dependency found: $templateRequest <- $uibModal <- NotificationService <- $http <- $templateRequest <- $compile
          templateUrl: "configmanager/template/notifications/list.html",
          //template: 'TODO: ALERT MODAL <p>{{notificationData}}</p> <button ng-click="vm.close()">Close </button>',
          //scope: $scope,
          controllerAs: 'vm',
          controller: ['$rootScope', function ($rootScope) {
            var vm = this;
            // vm.notificationData = notificationData;
            vm.nm = nm;
            vm.close = function () {
              // nm.close(notificationData);
              nm.close();
            };
          }]
        });
          modalInstance.closed.then(
          function(){
            nm.close();
          },
          function () {
            nm.close();
          });
          return modalInstance;
      }

}])
  .run(['$rootScope', '$uibModal', 'NotificationManager', function($rootScope, $uibModal, NotificationManager){
    $rootScope.$on('evt.configmanager.notification', function(evt, data){
      NotificationManager.add(data);
      /*$rootScope.notificationData = data;
      if($rootScope)
      $rootScope.modalInstance = $uibModal.open({
        //angular.js:68 Uncaught Error: [$injector:cdep] Circular dependency found: $templateRequest <- $uibModal <- NotificationService <- $http <- $templateRequest <- $compile
        //templateUrl: "configmanager.alert/alert.html",
        template: 'TODO: ALERT MODAL <p>{{notificationData}}</p> <button ng-click="vm.close()">Close </button>',
        //scope: $scope,
        controllerAs: 'vm',
        controller: function ($rootScope) {
            var vm = this;
            vm.close = function () {
              $rootScope.modalInstance.close();
            }

        }
      });*/
    })
}])




/**
 * Created by birkan on 28.04.2017.
 */
angular.module('portos.dialogs', ['ui.bootstrap'])
    /*.config(function($provide){
        $provide.decorator('ngClickDirective', ['$delegate', function($delegate){
            //$delegate is array of all ng-click directives
            //in this case first one is angular built-in ng-click
            //so we remove it.
            $delegate.shift();
            return $delegate;
        }])
    })*/

    .config(function($provide){
        //Create a decoration for ngClickDirective
        $provide.decorator('ngClickDirective', ['$delegate','$parse','ConfirmDialog', function($delegate, $parse, ConfirmDialog) {
            //Get the original compile function by ngClick
            var origValue = $delegate[0].compile;
            //Get set the compiler
            $delegate[0].compile = compiler;
            //return augmented ngClick
            return $delegate;

            /*Compiler Implementation*/
            function compiler(element, attrs, transclude){
                //Look for "confirmation" attribute, if present return confirmation event,
                //else return original ngclick event
                if(angular.isDefined(attrs.confirmation)) {

                    var fn = $parse(attrs["ngClick"]);
                    return function handler(scope, element) {
                            element.on('click', function (event) {
                                var confirmationEnabled = attrs.confirmationEnabled ? scope.$eval(attrs.confirmationEnabled) : true;

                                if (confirmationEnabled) {
                                    var question = attrs.confirmation || '';
                                    ConfirmDialog.open(question).then(function (result) {
                                        if (result) {
                                            fn(scope, {$event: event});
                                        }
                                    });
                                }
                                else {
                                    fn(scope, {$event: event});
                                }
                            });
                    };
                }
                //return original ngCLick implementation
                return origValue(element, attrs, transclude);
            }
        }]);
    })

    .directive('confirmation', function($uibModal, $parse, ConfirmDialog){
        return {
            restrict: 'A',
            scope: {
                confirmationEnabled: '=',
                confirmation: '@',
            },
            link: function(scope, element, attrs){
                if(!attrs.ngClick) { //ngClick kendi confirmation dialogunu kullaniyor
                    var question = scope.confirmation || 'Are you sure?';

                    scope.element = element;

                    element.bind('click', function (event) {
                        var confirmationEnabled = scope.confirmationEnabled == false ? scope.confirmationEnabled : true;
                        if (!attrs.confirmed && confirmationEnabled) {
                            //FIXME assuming the element will be gone from view after click is triggered, i.e state change etc. Otherwise this will only confirm once
                            event.preventDefault();
                            ConfirmDialog.open(question).then(function (result) {
                                console.log(result);
                                attrs.confirmed = result;
                                //FIXME assuming the element will be gone from view after click is triggered, i.e state change etc. Otherwise this will only confirm once
                                if (result) {
                                    /*if (attrs.ngClick) {
                                        scope.$eval(attrs.ngClick);
                                    } else {
                                        scope.element.triggerHandler('click');
                                    }*/
                                    scope.element.triggerHandler('click');
                                }
                            })
                        }
                    });
                }
            }
        }
    })
.service('ConfirmDialog',function ConfirmDialog($uibModal){
    this.open = function( question) {
        var message = question || 'Are you sure?';
        var modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'configmanager/template/dialogs/confirmation-dialog.html',
            size: 'sm',
            controllerAs: 'vm',
            controller: [function () {
                var vm = this;

                vm.message = message;

                // vm.notificationData = notificationData;
                vm.ok = function () {
                    // nm.close(notificationData);
                    modalInstance.close(true);
                };
                vm.cancel = function () {
                    // nm.close(notificationData);
                    modalInstance.close(false);
                };
            }]
        });
        return modalInstance.result;
    }
});

function confirmedNgClick(event, fn,ConfirmDialog){
        ConfirmDialog.open(question).then(function (result) {
            console.log(result);
            if (result) {
                //scope.$eval(attrs.ngClick);
                scope.$apply(function () {
                    fn(scope, {$event: event});
                });
            }
        })

}

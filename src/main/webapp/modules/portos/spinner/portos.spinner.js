angular.module('portos.spinner', [])
  .directive('loading',   ['$http', function ($http)
  {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs)
      {
        // var urls = attrs.loading.split(',');//[ "puantajHesapla/getQueueStats"]
        var urlRegex = new RegExp(attrs.loading || "*");//[ "puantajHesapla/getQueueStats"]
        //console.log('urls:');
        //console.log(urlRegex);
        scope.isLoading = function () {

          var reqCount = $http.pendingRequests
            .filter(function (req) {
              return urlRegex.test(req.url);
            })
            .length;
          // return $http.pendingRequests.length > 0;
          //console.log('loading --> ' + reqCount + ' requests');
          //console.log(attrs);
          return reqCount > 0;
        };

        scope.$watch(scope.isLoading, function (v)
        {
          var refreshEvent   = 'panel-refresh',
            whirlClass     = 'whirl',
            defaultSpinner = 'standard';
          //var $this = $(this);
          var spinner = attrs.spinner/*elm.data('spinner')*/ || defaultSpinner ;

         // console.log('spinner: ' +  spinner);

          if(v){
            //elm.show();


            // var panel =  angular.element('.panelDiv');

            elm.addClass(whirlClass + ' ' + spinner);
          }else{
            // elm.hide();
            elm.removeClass(whirlClass);
          }
          // var refreshEvent   = 'panel-refresh',
          //     whirlClass     = 'whirl',
          //     defaultSpinner = 'standard';
          // var $this = $(this);
          // var spinner = elm.data('spinner') || defaultSpinner ;
          //
          // // var panel =  angular.element('.panelDiv');
          //
          // elm.addClass(whirlClass + ' ' + spinner);
        });
      }
    };
    // }]);

  }]);


OutputPropertyConfig.$inject = ['domainServiceFactory'];angular
    .module("configmanager.outputPropertyConfig")
    .factory("OutputPropertyConfig", OutputPropertyConfig);

function OutputPropertyConfig(domainServiceFactory) {
  /*return domainServiceFactory("api/output/:outputId/prop/:id", {"id": "@id"},
    {

    });*/
    return domainServiceFactory("api/outputType/:typeId/config/:id", {"id": "@id"},
        {
            "findAllByOutputType": {url:"api/outputPropertyConfig/findAllByOutputType/:typeId",method:"GET", isArray: true}
        });
}



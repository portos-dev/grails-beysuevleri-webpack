
OutputPropertyConfigController.$inject = ['_instance', 'OutputPropertyConfig', '$state', '$stateParams'];angular
    .module("configmanager.outputPropertyConfig")
    .controller("OutputPropertyConfigController", OutputPropertyConfigController);

function OutputPropertyConfigController(_instance,  OutputPropertyConfig, $state, $stateParams) {
    var vm = this;
    vm.instance = _instance;
    //debugger
    // vm.output_types = [1,2];//_output_types;
    // vm.output_types = _output_types;
    vm.newInstance = new OutputPropertyConfig();
    vm.typeId = $stateParams.typeId;
    console.log($stateParams);
    vm.update = function(){
        vm.instance.$update({typeId:$stateParams.typeId}, function(){
            $state.go('^.list', {},{reload:true});
        });
    };
    vm.save = function(){
        vm.newInstance.outputType = {id:$stateParams.typeId};
        vm.newInstance.$save({typeId:$stateParams.typeId}, function(){
            $state.go('^.list', {},{reload:true});
        });
    }
}

OutputPropertyConfigController.resolve = {
    _output: function(OutputType, $stateParams){
        return OutputPropertyConfig.get({id: $stateParams.id});
    }
};

angular.module("configmanager.outputPropertyConfig",
	[
    "configmanager.core",
    "configmanager.outputType",
    "ngResource","ui.router"])
	.config(['$stateProvider', function($stateProvider){
        $stateProvider
            .state('outputType.show.config', {
                // url: "/config?offset&max&sort&order", //pagination enabled!
                url: "/config",
                abstract: true,
                templateUrl: "configmanager/template/outputPropertyConfig/index.html",
                redirectTo: 'outputType.show.config.list'
            })
          .state('outputType.show.config.list', {
            // url: "/config?offset&max&sort&order", //pagination enabled!
            url: "",
            controller: 'OutputPropertyConfigIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/outputPropertyConfig/list.html",
            resolve: {
              _list: ['OutputPropertyConfig', '$stateParams', function(OutputPropertyConfig, $stateParams){
                // debugger
                return OutputPropertyConfig.findAllByOutputType({typeId: $stateParams.typeId});
              }]
            }
          })
            .state('outputType.show.config.new', {
                url: "/new",
                controller: 'OutputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/outputPropertyConfig/new.html",
                // component:'outputshow',
                //TODO resolve: OutputController.resolve
                resolve: {
                    _instance: ['OutputPropertyConfig', '$stateParams', function(OutputPropertyConfig, $stateParams){
                        return {};//OutputPropertyConfig.get({id: $stateParams.configId});
                    }]
                }
            })
            .state('outputType.show.config.show', {
                url: "/{configId}",
                controller: 'OutputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/outputPropertyConfig/show.html",
                // component:'outputshow',
                //TODO resolve: OutputController.resolve
                resolve: {
                    _instance: ['OutputPropertyConfig', '$stateParams', function(OutputPropertyConfig, $stateParams){
                        // debugger
                        return OutputPropertyConfig.get({typeId: $stateParams.typeId, id: $stateParams.configId});
                    }]
                }
            })
            .state('outputType.show.config.edit', {
                url: "/{configId}/edit",
                controller: 'OutputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/outputPropertyConfig/edit.html",
                // component:'outputshow',
                //TODO resolve: OutputController.resolve
                resolve: {
                    _instance: ['OutputPropertyConfig', '$stateParams', function(OutputPropertyConfig, $stateParams){
                        return OutputPropertyConfig.get({ typeId: $stateParams.typeId,id: $stateParams.configId});
                    }]
                }
            });
	}]);


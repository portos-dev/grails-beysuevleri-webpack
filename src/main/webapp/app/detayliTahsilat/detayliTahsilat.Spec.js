describe('DetayliTahsilatController', function(){
    var $controller, DetayliTahsilatController, YevmiyeFactory, HesplanFactory, $http;

    // Before each test load our api.users module
    // beforeEach(module('$http'));
    beforeEach(module('app.detayliTahsilat'));
    // beforeEach(angular.mock.module('app.yevmiye'));
    beforeEach(angular.mock.module('app.logyev'));
    beforeEach(angular.mock.module('app.hesplan'));

    // Before each test set our injected Input factory (_Input_) to our local Input variable
    beforeEach(inject(function(_$controller_, _Yevmiye_, _Hesplan_, _$http_/*, _LogYev_*/, _$http_) {
        $controller = _$controller_;
        YevmiyeFactory = _Yevmiye_;
        HesplanFactory = _Hesplan_;
        $http = _$http_;


        DetayliTahsilatController = $controller('DetayliTahsilatController', {Yevmiye: YevmiyeFactory, Hesplan: HesplanFactory, $http: $http})
    }));

    // A simple test to verify the Input factory exists
    it('should exist', function() {
        expect(DetayliTahsilatController).toBeDefined();
    });


    // A set of tests for our Input.list() method
    // describe('.list()', function() {
    //     // A simple test to verify the method all exists
    //     it('should exist', function() {
    //         expect(Input.list).toBeDefined();
    //     });
    // });
    //
    // // A set of tests for our Input.pending() method
    // describe('.pending()', function() {
    //     // A simple test to verify the method all exists
    //     it('should exist', function() {
    //         expect(Input.pending).toBeDefined();
    //     });
    // });
    //
    // // A set of tests for our Input.erroneous() method
    // describe('.erroneous()', function() {
    //     // A simple test to verify the method all exists
    //     it('should exist', function() {
    //         expect(Input.erroneous).toBeDefined();
    //     });
    // });

});

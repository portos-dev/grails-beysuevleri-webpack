/**
 * Created by birkan on 22.05.2017.
 */

var FileSaver = require('file-saver');
angular.module('app.detayliTahsilat', ['app.yevmiye', 'app.hesplan'])
    .config(myconfig)
    // .factory('Yevmiye', Yevmiye)
    // .factory('Yevmiye2', Yevmiye2)
    .controller('DetayliTahsilatController',['Yevmiye', 'Hesplan', '$http', '$log','moment',
        function(Yevmiye, Hesplan, $http, $log, moment/*, FileSaver*/){
        var vm = this;


        function pdf(){
            // PDF format using jsPDF and jsPDF Autotable
            $('#myTable').tableExport({type:'pdf',
                jspdf: {orientation: 'l',
                    format: 'a3',
                    margins: {left:10, right:10, top:20, bottom:20},
                    autotable: {styles: {fillColor: 'inherit',
                        textColor: 'inherit'},
                        tableWidth: 'auto'}
                }
            });
        }

        function xls(){
            $('#myTable').tableExport({fileName:'ESKI_TAHSILAT', type:'excel'});
        }
        function png(){
            $('#myTable').tableExport({type:'png'});
        }

        function excelPlus(){
            $log.debug('excelPlus');
            var ep = new ExcelPlus();

            var file = ep.createFile('Book1');

            angular.forEach(vm.data.list, function(musteri, idx){
                $log.debug(musteri);
                // var headerRow = [];
                // headerRow.push('MÜŞTERİ');
                // headerRow.push(musteri.hesplan.aciklama);
                // file.writeNextRow(headerRow);
                file.writeCell('A'+(idx+1),'MÜŞTERİ', {fgColor: {rgb:'DDDDDD'}});
                file.writeCell('B'+(idx+1),musteri.hesplan.aciklama);
            });

            file.write({ "content":[ ["A1","B1","C1"] ] })
                .createSheet("Book2")
                .write({ "cell":"A1", "content":"A1" })
                .write({ "sheet":"Book1", "cell":"D1", "content":new Date() })
                .saveAs("demo.xlsx");
        }

        vm.excelPlus = excelPlus;

        vm.poi = function(){
            $http.post('detayliTahsilat/poi', vm.query, {responseType: 'arraybuffer'}).then(
                function onResponse(resp){
                    $log.info(resp.data);
                    var xls = new Blob([resp.data], {
                        type: "application/vnd.ms-excel"
                    });
                    FileSaver.saveAs(xls, 'detayli_tahsilat.xlsx');
                },
                function onError(data){
                    $log.error(data);
                }
            )
        };

        vm.export = function(_type){
            var type = _type || 'excel';

            if(type === 'excel'){
                xls();
            }
            else if(type === 'pdf'){
                pdf();
            }
            else if(type === 'png'){
                png();
            }

            // $(function () {
            //     // wait till load event fires so all resources are available
            //     $('#myTable').tableExport({type: type});
            // });

        };


        vm.query = {max:50};
        load();

            vm.reload = load;
        function load(){
            $http.post('detayliTahsilat', vm.query).then(
                function(resp){
                    var data = {};
                    //TODO: basliklar icin yilin kalan aylarini ve sonrasindaki yillari odeme planlarindan uret
                    //TODO: ay ve yillar icin alacaklari grupla
                    $log.info(resp.data);
                    var items = resp.data;
                    var donemlerAy = [];
                    var donemlerYil = [];
                    var currentAy = moment().format('MMMM YYYY');
                    var currentYil = moment().format('YYYY');
                    var satisBedeliToplam = 0;
                    var kalanAlacakToplam = 0;
                    var kalanAlacakToplam2 = 0;
                    var tahsilEdilenToplam = 0;
                    var donemAlacakToplamlari = {};
                    var musteriRegex = /\((.*)M2---(.*)\)/;
                    angular.forEach(items, function(musteri){
                        $log.debug('alici: ' + musteri.hesplan.aciklama);
                        var match = musteri.hesplan.aciklama.match(musteriRegex);
                        console.log(match);

                        musteri.satisBedeli = musteri.hesplan.topa;
                        musteri.tahsilEdilen = musteri.hesplan.topb;
                        musteri.kalanAlacak = musteri.hesplan.topa - (musteri.hesplan.topb || 0);
                        musteri.kalanToplamBakiye;
                        musteri.kalanDonemAlacaklari = {};
                        angular.forEach(musteri.islenmisOdemePlanlari, function(odemePlani){
                            var ay = moment(odemePlani.sonOdemeTarihi).format('MMMM YYYY');
                            var yil = moment(odemePlani.sonOdemeTarihi).format('YYYY');
                            if(odemePlani.kalanToplamBakiye) {
                                $log.debug('odemePlani.kalanToplamBakiye: ' + odemePlani.kalanToplamBakiye);
                                musteri.kalanToplamBakiye = odemePlani.kalanToplamBakiye;
                            }
                            if(!odemePlani.odendi) {
                                $log.debug('odemePlani.beklenenTahsilat ve tarihi: ' + odemePlani.kalanTutar + ' ' + odemePlani.sonOdemeTarihi);
                                $log.debug('kalan alacak ve donem: ' + moment(odemePlani.sonOdemeTarihi).format('MMMM YYYY') + ' ' + odemePlani.kalanTutar);


                                musteri.kalanDonemAlacaklari[ay] ? musteri.kalanDonemAlacaklari[ay]  +=  odemePlani.kalanTutar : musteri.kalanDonemAlacaklari[ay] =  odemePlani.kalanTutar;
                                musteri.kalanDonemAlacaklari[yil] ? musteri.kalanDonemAlacaklari[yil] +=  odemePlani.kalanTutar : musteri.kalanDonemAlacaklari[yil] =  odemePlani.kalanTutar;

                                if (yil === currentYil) {
                                    if (donemlerAy.indexOf(ay) < 0) {
                                        donemlerAy.push(ay);
                                    }
                                }
                                else if (donemlerYil.indexOf(yil) < 0) {
                                    donemlerYil.push(yil);
                                }
                            }
                        });

                        for(donem in musteri.kalanDonemAlacaklari){
                            donemAlacakToplamlari[donem] = (donemAlacakToplamlari[donem] || 0) + (musteri.kalanDonemAlacaklari[donem] || 0);
                            // donemAlacakToplamlari[yil] = (donemAlacakToplamlari[yil] || 0) + (musteri.kalanDonemAlacaklari[yil] || 0);
                        }

                        $log.info('islendikten sonra musteri: ');
                        $log.info(musteri);
                        satisBedeliToplam += musteri.satisBedeli;
                        kalanAlacakToplam += musteri.kalanToplamBakiye || 0;
                        kalanAlacakToplam2 += musteri.kalanAlacak;
                        tahsilEdilenToplam += musteri.tahsilEdilen || 0;
                    });

                    data.list = resp.data;
                    data.donemlerAy = donemlerAy.sort(function(a,b){
                        var _a = moment(a,'MMMM YYYY');
                        var _b = moment(b,'MMMM YYYY');
                        return _a>_b ? 1 : _a<_b ? -1 : 0;
                    });
                    data.donemlerYil = donemlerYil.sort();
                    data.satisBedeliToplam = satisBedeliToplam;
                    data.kalanAlacakToplam2 = kalanAlacakToplam2;
                    data.kalanAlacakToplam = kalanAlacakToplam;
                    data.tahsilEdilenToplam = tahsilEdilenToplam;
                    data.donemAlacakToplamlari = donemAlacakToplamlari;
                    $log.debug('donemler: ');
                    $log.debug(donemlerAy.sort(function(a,b){
                        var _a = moment(a,'MMMM YYYY');
                        var _b = moment(b,'MMMM YYYY');
                        return _a>_b ? 1 : _a<_b ? -1 : 0;
                    }));
                    $log.debug(donemlerYil.sort());

                    vm.data = data;
                },
                function(err){
                    $log.error(err);
                }
            );
        }

        // $http.post('/detayliTahsilat', vm.query).then(
        //     function(resp){
        //         $log.info(resp.data);
        //         vm.list = resp.data;
        //     },
        //     function(err){
        //         $log.error(err);
        //     }
        // );

        //vm.Yevmiye = new Yevmiye();

        // vm.list = Yevmiye.list({max:50});

        //TODO send paging params returned from query

        // vm.hesplanMap = Hesplan.map({max : -1});
        vm.search = function(offset, max){
            if(vm.list.$cancelRequest){
                // We don't care about any pending request for hotels
                // in a different destination any more
                vm.list.$cancelRequest();
            }
            vm.query.offset = offset;
            vm.query.max = max;
            vm.list = Yevmiye.query(vm.query);
        };
        vm.report = function(){
           vm.query.offset = 0;
           vm.query.max = -1;
           Yevmiye.gunlukTablo(vm.query, function(data){
               FileSaver.saveAs(data.response, 'gunlukTablo.pdf');
           });
        };
        vm.detayliTahsilat = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilat(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilat.pdf');
            });
        };
        vm.detayliTahsilatDefault = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatDefault(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
        vm.detayliTahsilatYeni = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatYeni(vm.query, function(data){
                //TODO: FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
    }])
    .controller('EskiTahsilatController',['Yevmiye', 'Hesplan', '$http', '$log', 'moment', function(Yevmiye, Hesplan, $http, $log, moment){
        var vm = this;
        vm.query = {max:50};

        function load(){
            $http.post('detayliTahsilat', vm.query).then(
                function(resp){
                    var data = {};
                    //TODO: basliklar icin yilin kalan aylarini ve sonrasindaki yillari odeme planlarindan uret
                    //TODO: ay ve yillar icin alacaklari grupla
                    $log.info(resp.data);
                    var items = resp.data;
                    var donemlerAy = [];
                    var donemlerYil = [];
                    var currentAy = moment().format('MMMM YYYY');
                    var currentYil = moment().format('YYYY');
                    var satisBedeliToplam = 0;
                    var kalanAlacakToplam = 0;
                    var kalanAlacakToplam2 = 0;
                    var tahsilEdilenToplam = 0;
                    var donemAlacakToplamlari = {};
                    var musteriRegex = /\((.*)M2---(.*)\)/;
                    angular.forEach(items, function(musteri){
                        $log.debug('alici: ' + musteri.hesplan.aciklama);
                        var match = musteri.hesplan.aciklama.match(musteriRegex);
                        console.log(match);

                        musteri.satisBedeli = musteri.hesplan.topa;
                        musteri.tahsilEdilen = musteri.hesplan.topb;
                        musteri.kalanAlacak = musteri.hesplan.topa - (musteri.hesplan.topb || 0);
                        musteri.kalanToplamBakiye;
                        musteri.kalanDonemAlacaklari = {};
                        angular.forEach(musteri.islenmisOdemePlanlari, function(odemePlani){
                            var ay = moment(odemePlani.sonOdemeTarihi).format('MMMM YYYY');
                            var yil = moment(odemePlani.sonOdemeTarihi).format('YYYY');
                            if(odemePlani.kalanToplamBakiye) {
                                $log.debug('odemePlani.kalanToplamBakiye: ' + odemePlani.kalanToplamBakiye);
                                musteri.kalanToplamBakiye = odemePlani.kalanToplamBakiye;
                            }
                            if(!odemePlani.odendi) {
                                $log.debug('odemePlani.beklenenTahsilat ve tarihi: ' + odemePlani.kalanTutar + ' ' + odemePlani.sonOdemeTarihi);
                                $log.debug('kalan alacak ve donem: ' + moment(odemePlani.sonOdemeTarihi).format('MMMM YYYY') + ' ' + odemePlani.kalanTutar);


                                musteri.kalanDonemAlacaklari[ay] ? musteri.kalanDonemAlacaklari[ay]  +=  odemePlani.kalanTutar : musteri.kalanDonemAlacaklari[ay] =  odemePlani.kalanTutar;
                                musteri.kalanDonemAlacaklari[yil] ? musteri.kalanDonemAlacaklari[yil] +=  odemePlani.kalanTutar : musteri.kalanDonemAlacaklari[yil] =  odemePlani.kalanTutar;

                                if (yil === currentYil) {
                                    if (donemlerAy.indexOf(ay) < 0) {
                                        donemlerAy.push(ay);
                                    }
                                }
                                else if (donemlerYil.indexOf(yil) < 0) {
                                    donemlerYil.push(yil);
                                }
                            }
                        });

                        for(donem in musteri.kalanDonemAlacaklari){
                            donemAlacakToplamlari[donem] = (donemAlacakToplamlari[donem] || 0) + (musteri.kalanDonemAlacaklari[donem] || 0);
                            // donemAlacakToplamlari[yil] = (donemAlacakToplamlari[yil] || 0) + (musteri.kalanDonemAlacaklari[yil] || 0);
                        }

                        $log.info('islendikten sonra musteri: ');
                        $log.info(musteri);
                        satisBedeliToplam += musteri.satisBedeli;
                        kalanAlacakToplam += musteri.kalanToplamBakiye || 0;
                        kalanAlacakToplam2 += musteri.kalanAlacak;
                        tahsilEdilenToplam += musteri.tahsilEdilen || 0;
                    });

                    data.list = resp.data;
                    data.donemlerAy = donemlerAy.sort(function(a,b){
                        var _a = moment(a,'MMMM YYYY');
                        var _b = moment(b,'MMMM YYYY');
                        return _a>_b ? 1 : _a<_b ? -1 : 0;
                    });
                    data.donemlerYil = donemlerYil.sort();
                    data.satisBedeliToplam = satisBedeliToplam;
                    data.kalanAlacakToplam2 = kalanAlacakToplam2;
                    data.kalanAlacakToplam = kalanAlacakToplam;
                    data.tahsilEdilenToplam = tahsilEdilenToplam;
                    data.donemAlacakToplamlari = donemAlacakToplamlari;
                    $log.debug('donemler: ');
                    $log.debug(donemlerAy.sort(function(a,b){
                        var _a = moment(a,'MMMM YYYY');
                        var _b = moment(b,'MMMM YYYY');
                        return _a>_b ? 1 : _a<_b ? -1 : 0;
                    }));
                    $log.debug(donemlerYil.sort());

                    vm.data = data;
                },
                function(err){
                    $log.error(err);
                }
            );
        }

        function pdf(){
            // PDF format using jsPDF and jsPDF Autotable
            $('#myTable').tableExport({type:'pdf',
                jspdf: {orientation: 'l',
                    format: 'a3',
                    margins: {left:10, right:10, top:20, bottom:20},
                    autotable: {styles: {fillColor: 'inherit',
                        textColor: 'inherit'},
                        tableWidth: 'auto'}
                }
            });
        }

        function xls(){
            $('#myTable').tableExport({fileName:'ESKI_TAHSILAT', type:'excel'});
        }
        function png(){
                    $('#myTable').tableExport({type:'png'});
        }

        vm.poi = function(){
            $http.post('eskiTahsilat/poi', vm.query, {responseType: 'arraybuffer'}).then(
                function onResponse(resp){
                    $log.info(resp.data);
                    var xls = new Blob([resp.data], {
                        type: "application/vnd.ms-excel"
                    });
                    FileSaver.saveAs(xls, 'eski_tahsilat.xlsx');
                },
                function onError(data){
                    $log.error(data);
                }
            )
        };


        load();

        vm.export = function(_type){
            var type = _type || 'excel';

            if(type === 'excel'){
                xls();
            }
            else if(type === 'pdf'){
                pdf();
            }
            else if(type === 'png'){
                png();
            }

            // $(function () {
            //     // wait till load event fires so all resources are available
            //     $('#myTable').tableExport({type: type});
            // });

        };

        vm.reload = load;

        //vm.Yevmiye = new Yevmiye();

        // vm.list = Yevmiye.list({max:50});

        //TODO send paging params returned from query

        // vm.hesplanMap = Hesplan.map({max : -1});
        vm.search = function(offset, max){
            if(vm.list.$cancelRequest){
                // We don't care about any pending request for hotels
                // in a different destination any more
                vm.list.$cancelRequest();
            }
            vm.query.offset = offset;
            vm.query.max = max;
            vm.list = Yevmiye.query(vm.query);
        };
        vm.report = function(){
           vm.query.offset = 0;
           vm.query.max = -1;
           Yevmiye.gunlukTablo(vm.query, function(data){
               FileSaver.saveAs(data.response, 'gunlukTablo.pdf');
           });
        };
        vm.detayliTahsilat = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilat(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilat.pdf');
            });
        };
        vm.detayliTahsilatDefault = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatDefault(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
        vm.detayliTahsilatYeni = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatYeni(vm.query, function(data){
                //TODO: FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
    }]);

function myconfig($stateProvider){
    console.log('DetayliTahsilat states loading...');
    var indexState = {
        name: 'app.detayliTahsilat',
        url: '/detayliTahsilat',
        template: require('./index.html')
    };

    var listState = {
        name: 'app.detayliTahsilat.list',
        url: '/list',
        controller: 'DetayliTahsilatController',
        controllerAs: 'vm',
        template: require('./list.html')
    };

    var eskiTahsilatState = {
        name: 'app.eskiTahsilat',
        url: '/eskiTahsilat',
        controllerAs: 'vm',
        template: require('./index.html')
    };

    var eskiTahsilatListState = {
        name: 'app.eskiTahsilat.list',
        url: '/list',
        controller: 'EskiTahsilatController',
        controllerAs: 'vm',
        template: require('./eskiTahsilatList.html')
    };

    $stateProvider.state(indexState);
    $stateProvider.state(listState);
    $stateProvider.state(eskiTahsilatState);
    $stateProvider.state(eskiTahsilatListState);

    console.log('DetayliTahsilat states loaded');
}



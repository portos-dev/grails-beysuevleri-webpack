/**
 * Created by birkan on 22.05.2017.
 */
angular.module('app.logyev', ['portos.core'])
    .config(config)
    .factory('Logyev', Logyev)
   // .factory('Logyev2', Logyev2)
    .controller('LogyevController', function(Logyev, Hesplan){
        var vm = this;

        //vm.Yevmiye = new Yevmiye();

        // vm.list = Yevmiye.list({max:50});
        vm.query = {max:50};
        vm.list = Logyev.query(vm.query);
        vm.hesplanMap = Hesplan.map({max : -1});
        // vm.list = Yevmiye.query({
        //     'baslangicTarihi': '01-01-2017',
        //     'gmkod': '100 01'
        // });


        //TODO send paging params returned from query
        vm.search = function(offset, max){
            if(vm.list.$cancelRequest){
                // We don't care about any pending request for hotels
                // in a different destination any more
                vm.list.$cancelRequest();
            }
            vm.query.offset = offset;
            vm.query.max = max;
            vm.list = Logyev.query({filterQuery: vm.query/*, offset:offset*/});
        }
    });

Logyev.$inject = ['domainServiceFactory'];
// Logyev2.$inject = ['domainServiceFactory'];

function Logyev(domainServiceFactory) {
    return domainServiceFactory("logyev/:refno:refno2", {"refno": "@refno", "refno2": "@refno2"},
        {
            "get":{
                method: "GET",
                url:'logyev/show',
                params: {
                    refno: "@refno",
                    refno2: "@refno2"
                }
            },
            /*"children":{
                method: "POST",
                url:'logyev/children',
                /!*params: {
                 refno: "@refno",
                 refno2: "@refno2"
                 }*!/
                isArray: false,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Logyev(item));
                    });
                    return wrapped;
                }
            },*/
            "list": {
                method: "GET",
                isArray: false,
                // transformResponse: function (json) {
                //     // var wrapped = {
                //     //     items: []
                //     // };
                //     var data = angular.fromJson(json)
                //     var wrapped = angular.copy(data);
                //     wrapped.items = [];
                //     angular.forEach(data.items, function (item) {
                //         wrapped.items.push(new Logyev2(item));
                //     });
                //     return wrapped;
                // }
            },
            "query": {
                url: "logyev/query",
                method: "POST",
                isArray: false,
                // transformResponse: function (json) {
                //     var data = angular.fromJson(json)
                //     var wrapped = angular.copy(data);
                //     wrapped.items = [];
                //     angular.forEach(data.items, function (item) {
                //         wrapped.items.push(new Logyev2(item));
                //     });
                //     return wrapped;
                // }
            }
        });
}



function config($stateProvider){
    var indexState = {
        name: 'app.logyev',
        url: '/logyev',
        template: require('./index.html')
    };

    var listState = {
        name: 'app.logyev.list',
        url: '/list',
        controller: 'LogyevController',
        controllerAs: 'vm',
        template: require('./list.html')
    };

    $stateProvider.state(indexState);
    $stateProvider.state(listState);

    console.log('logyev states loaded');
}




InputPropertiesController.$inject = ['_list', '_instance', '_propertyTypes', '$state', '$stateParams', 'InputProperty'];

angular
    .module("configmanager.input")
    .controller("InputPropertiesController", InputPropertiesController);

function InputPropertiesController(_list, _instance, _propertyTypes, $state, $stateParams, InputProperty) {
    var vm = this;
    vm.instance = _instance;
    // vm.list = _input.properties;
    vm.list = _list;
    vm.inputId = $stateParams.inputId;
    vm.propertyTypes = _propertyTypes;
    // vm.newInstance = new Input();

    vm.update = function(inputId){
        vm.instance.$update({inputId: inputId}, function(data){
            console.log('update OK');
            //TODO route to show
            //$state.reload();
            // $state.go('^');
            // $state.go('^.list', {},{reload:true});
            //debugger;
            // $state.go('^.list', $stateParams,{reload:true});
            debugger;
            $state.go('^.show', {inputId: data.id},{reload:true});
        });
    };
    vm.saveOrUpdate = function(data,row){
      if(row.id) {
          angular.extend(row, data);
          row.$update({},function (resp) {
//         new InputProperty(data).$save({inputId: vm.inputId},function (resp) {
              console.log(resp);
              $state.go('.', {inputId: vm.inputId}, {reload: true});
          });
        // InputProperty.get({inputId: vm.inputId, id: row.id}, function (resp) {
        //   console.log(resp);
        //   angular.extend(resp, data);
        //   resp.$update({inputId: vm.inputId}, function () {
        //     console.log(resp);
        //     $state.go('.', {inputId: vm.inputId}, {reload: true});
        //   });
        // });
      }
      else{
          angular.extend(row, data);
//         data.input = input;
          row.$save({},function (resp) {
//         new InputProperty(data).$save({inputId: vm.inputId},function (resp) {
              console.log(resp);
              $state.go('.', {inputId: vm.inputId}, {reload: true});
          });
      }
        // data.id = inputPropertyId;
        // data.$save({}, function (data) {
        //   console.log(data);
        //
        // })
        /*vm.newInstance.$save({inputId: inputId}, function(data){
            console.log('save OK');
            console.log('save OK');
            //TODO route to show
            // $state.reload('^');
            //this works  $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            //debugger;
            $state.go('^.show', {id: data.id},{reload:true});
        });*/
    }
}

InputPropertiesController.resolve = {
    _input: function(Input, $stateParams){
        return Input.get({id: $stateParams.inputId});
    }
};

angular
    .module("configmanager.input")
    .controller("InputController", ['_input', '_input_types', 'Input', '$state', '$stateParams', function (_input, _input_types, Input, $state, $stateParams) {
        var vm = this;
        vm.instance = _input;
        vm.input_types = _input_types;
        vm.mode = $stateParams.mode;
        // vm.properties = _properties;
        //vm.newInstance = new Input();

        vm.update = function(){
            vm.instance.$update({}, function(data){
                console.log('update OK');
                //TODO route to show
                //$state.reload();
                // $state.go('^');
                // $state.go('^.list', {},{reload:true});
                //debugger;
                // $state.go('^.list', $stateParams,{reload:true});
                $state.go('.', {inputId: data.id, mode:null},{reload:true});
            });
        };
        vm.save = function(valid, form){
            // if(valid) {


            vm.instance.$save({}, function (data) {
                console.log('save OK');
                console.log('save OK');
                //TODO route to show
                // $state.reload('^');
                //this works  $state.go('^', {},{reload:true});
                // $state.go('^.list', {},{reload:true});
                //debugger;
                $state.go('^.show.main', {inputId: data.id}, {reload: true});
            });
            // }
            // else {
            //   console.log(form);
            // }
        }
    }]);

;

angular.module("configmanager.input",
	[
    "configmanager.core",
    "configmanager.inputType",
    "ngResource",
    "ui.router",
    "ui.bootstrap"
    ])
    // .config(['$stateProvider', 'InputStatesProvider', function($stateProvider, InputStatesProvider) {
    .config(['$stateProvider', function($stateProvider) {
    var rootPath = '';
    // var states = InputStatesProvider.getStates();
    var states = InputStates();
    for (var state in states) {
        var stateName = (rootPath + state);
        var stateOptions = states[state];
        $stateProvider.state(stateName, stateOptions);
        //console.log('configmanager.input.config -> state: ' + stateName);
    }
}]);


function InputStates(){
    var states = {
        'input': {
            abstract: true,
            url: "/input", //pagination enabled!
            // controller: 'InputIndexController',
            // controllerAs: 'vm',
            templateUrl: "configmanager/template/input/index.html"
        },
        'input.list': {
            url: "/list?offset&max&sort&order&page", //pagination enabled!
            params: {
                offset: {value: '0', squash: true},
                max: {value: '10', squash: true},
                page: {value: '1', squash: true}
            },
            controller: 'InputIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/input/list.html",
            resolve: {
                _list: ['Input', '$stateParams', function (Input, $stateParams) {
                    return Input.list($stateParams).$promise;
                }],
                _page: ['$stateParams', function ($stateParams) {
                    var offset = $stateParams.offset || 0;
                    var max = $stateParams.max || 10;
                    // var page = $stateParams.page; //offset ? (offset/max) + 1 : 1;
                    var page = offset ? (offset / max) + 1 : 1;
                    return page;
                }]
            }
        },
        'input.new': {
            url: "/new",
            //parent: 'input',
            // controller: 'InputController as vm',
            controller: 'InputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/input/new.html",
            // component:'inputshow',
            //TODO resolve: InputController.resolve
            resolve: {
                _input: ['Input',function (Input) {
                    return new Input();
                    //return Input.get({id: $stateParams.id});
                    //return {};
                }],
                _input_types: ['InputType',function (InputType) {
                    return InputType.list();
                }]
            }
        },
        'input.show': {
            url: '/{inputId}',
            // abstract: true,
            controller: 'InputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/input/show-tabbed.html",
            //redirectTo: 'input.show.main'
            resolve: {
                _input: ['Input', '$stateParams',function (Input, $stateParams) {
                    //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                    return Input.get({id: $stateParams.inputId}).$promise;
                }],
                _input_types: ['InputType',function (InputType) {
                    return InputType.list();
                }]
            }
        },
        'input.show.main': {
            url: "/main?mode",
            controller: 'InputController',
            controllerAs: 'vm',
            /*
             cannot use array for DI here
             templateUrl: function ($stateParams) {
             return $stateParams.mode === 'edit' ? "configmanager/template/input/edit.html" :
             "configmanager/template/input/show.html"
             },*/
            templateProvider: ['$templateCache','$stateParams',function ($templateCache,$stateParams) {
                return $templateCache.get($stateParams.mode === 'edit' ?
                    "configmanager/template/input/edit.html" :
                    "configmanager/template/input/show.html")
            }],
            resolve: {
                _input: ['Input', '$stateParams',function (Input, $stateParams) {
                    //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                    return Input.get({id: $stateParams.inputId}).$promise;
                }],
                _input_types: ['InputType',function (InputType) {
                    return InputType.list();
                }]
            }
        },
        'input.show.props': {
            url: "/props",
            controller: 'InputPropertiesController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/input/props-list.html",
            resolve: {
                _list: ['InputProperty', '$stateParams',function (InputProperty, $stateParams) {
                    // debugger;
                    // return InputPropertyConfig.findAllByInputType({id: $stateParams.id});
                    return InputProperty.list({inputId: $stateParams.inputId});
                }],
                _instance: ['InputProperty',function (InputProperty) {
                    return new InputProperty();//InputProperty.get({inputId: $stateParams.id, id: $stateParams.propId})
                }],
                _propertyTypes: ['InputPropertyConfig','Input', '$stateParams',function (InputPropertyConfig, Input, $stateParams) {
                    Input.get({id: $stateParams.inputId}, function (input) {
                        return InputPropertyConfig.findAllByInputType({typeId: input.type.id}).$promise;
                    });
                }]
            }
        },
        'input.show.main.edit': {
            url: "/{inputId}/edit",
            //parent: 'input',
            // controller: 'InputController as vm',
            controller: 'InputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/input/edit.html",
            // component:'inputshow',
            //TODO resolve: InputController.resolve
            resolve: {
                _input: ['Input', '$stateParams',function (Input, $stateParams) {
                    return Input.get({id: $stateParams.inputId});
                }],
                _input_types: ['InputType',function (InputType) {
                    return InputType.list();
                }]
            }
        }
    };
    return states;
}

  /*  .provider('InputStates', function () {
        var prv = this;
        prv.getStates = function(){
            var states = {
                'input': {
                    abstract: true,
                    url: "/input", //pagination enabled!
                    // controller: 'InputIndexController',
                    // controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/index.html"
                },
                'input.list': {
                    url: "/list?offset&max&sort&order&page", //pagination enabled!
                    params: {
                        offset: {value: '0', squash: true},
                        max: {value: '10', squash: true},
                        page: {value: '1', squash: true}
                    },
                    controller: 'InputIndexController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/list.html",
                    resolve: {
                        _list: function (Input, $stateParams) {
                            return Input.list($stateParams).$promise;
                        },
                        _page: function ($stateParams) {
                            var offset = $stateParams.offset || 0;
                            var max = $stateParams.max || 10;
                            // var page = $stateParams.page; //offset ? (offset/max) + 1 : 1;
                            var page = offset ? (offset / max) + 1 : 1;
                            return page;
                        }
                    }
                },
                'input.new': {
                    url: "/new",
                    //parent: 'input',
                    // controller: 'InputController as vm',
                    controller: 'InputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/new.html",
                    // component:'inputshow',
                    //TODO resolve: InputController.resolve
                    resolve: {
                        _input: function ($stateParams, Input) {
                            return new Input();
                            //return Input.get({id: $stateParams.id});
                            //return {};
                        },
                        _input_types: function (InputType) {
                            return InputType.list();
                        }
                    }
                },
                'input.show': {
                    url: '/{inputId}',
                    // abstract: true,
                    controller: 'InputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/show-tabbed.html",
                    //redirectTo: 'input.show.main'
                    resolve: {
                        _input: function (Input, $stateParams) {
                            //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                            return Input.get({id: $stateParams.inputId}).$promise;
                        },
                        _input_types: function (InputType) {
                            return InputType.list();
                        }
                    }
                },
                'input.show.main': {
                    url: "/main?mode",
                    controller: 'InputController',
                    controllerAs: 'vm',
                    templateUrl: function ($stateParams) {
                        return $stateParams.mode === 'edit' ? "configmanager/template/input/edit.html" :
                            "configmanager/template/input/show.html"
                    },
                    resolve: {
                        _input: function (Input, $stateParams) {
                            //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                            return Input.get({id: $stateParams.inputId}).$promise;
                        },
                        _input_types: function (InputType) {
                            return InputType.list();
                        }
                    }
                },
                'input.show.props': {
                    url: "/props",
                    controller: 'InputPropertiesController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/props-list.html",
                    resolve: {
                        _list: function (InputProperty, $stateParams) {
                            // debugger;
                            // return InputPropertyConfig.findAllByInputType({id: $stateParams.id});
                            return InputProperty.list({inputId: $stateParams.inputId});
                        },
                        _instance: function (InputProperty, $stateParams) {
                            return new InputProperty();//InputProperty.get({inputId: $stateParams.id, id: $stateParams.propId})
                        },
                        _propertyTypes: function (InputPropertyConfig, Input, $stateParams) {
                            Input.get({id: $stateParams.inputId}, function (input) {
                                return InputPropertyConfig.findAllByInputType({typeId: input.type.id}).$promise;
                            });
                        }
                    }
                },
                'input.show.main.edit': {
                    url: "/{inputId}/edit",
                    //parent: 'input',
                    // controller: 'InputController as vm',
                    controller: 'InputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/input/edit.html",
                    // component:'inputshow',
                    //TODO resolve: InputController.resolve
                    resolve: {
                        _input: function (Input, $stateParams) {
                            return Input.get({id: $stateParams.inputId});
                        },
                        _input_types: function (InputType) {
                            return InputType.list();
                        }
                    }
                }
            };//create states inline to avoid state objects be reused in stateProvider, nasty bugs
            return states;
        };
        prv.$get = function() {
            return prv;
        };
    });*/


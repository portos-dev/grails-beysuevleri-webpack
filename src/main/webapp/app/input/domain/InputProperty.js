
InputProperty.$inject = ['domainServiceFactory'];angular
    .module("configmanager.input")
    .factory("InputProperty", InputProperty);


function InputProperty(domainServiceFactory) {
    return domainServiceFactory("api/input/:inputId/prop/:id", {"id": "@id", "inputId": "@configItem.id"},
        {
            //props: {url:'api/input/:id/props',isArray: true, method:'get'}
        });
}

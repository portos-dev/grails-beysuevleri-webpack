
Input.$inject = ['domainServiceFactory'];
angular
    .module("configmanager.input")
    .factory("Input", Input);


function Input(domainServiceFactory) {
    return domainServiceFactory("api/input/:id", {"id": "@id"},
        {
            pending: {url:'api/pendingInputs',isArray: true, method:'get'},
            erroneous: {url:'api/erroneousInputs',isArray: true, method:'get'},
            list:{isArray:false,method:'get'/*,
                transformResponse: function (data, headers) {
                    // console.log(data);
                    return JSON.parse(data); //.results;
                }*/},
           // props: {url:'api/input/:id/prop',isArray: true, method:'get'}
        });
}

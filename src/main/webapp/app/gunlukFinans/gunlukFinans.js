/**
 * Created by birkan on 24.05.2017.
 */

angular.module('app.gunlukFinans', [])
    .config(config)
    .controller('GunlukFinansController', GunlukFinansController)
    .factory('GunlukFinans', GunlukFinans);


GunlukFinans.$inject = ['domainServiceFactory'/*, 'GunlukFinans'*/]

function GunlukFinans(domainServiceFactory/*, GunlukFinansim*/){
    return domainServiceFactory('gunlukFinans', {refno: '@refno', refno2: '@refno2'},{
        'list': {
            method: 'GET',
            isArray: false,
            transformResponse: function (data) {
                var wrapped = {
                    items: []
                };
                /*angular.forEach(angular.fromJson(data), function (item) {
                 wrapped.items.push(new GunlukFinansim(item));
                 });*/
                wrapped.items = angular.fromJson(data);
                return wrapped;
            }
        },
        "query": {
            url: "gunlukFinans/query",
            method: "POST",
            // isArray: true,
            isArray: false,
            transformResponse: function (data) {
                var wrapped = {
                    items: []
                };
                /*angular.forEach(angular.fromJson(data), function (item) {
                    wrapped.items.push(new GunlukFinansim(item));
                });*/
                wrapped.items = angular.fromJson(data);
                return wrapped;
            }
        }
    })
}

function GunlukFinansController(GunlukFinans){
    var vm = this;

    vm.list = GunlukFinans.list({max:20 });

    vm.search = function(){
        if(vm.list.$cancelRequest){
            vm.list.$cancelRequest();
        }
        vm.list = GunlukFinans.query(vm.query);
    }

    // Hesplan.query

//
//     List<Hesplan> hesplans = [
//         [kod: '100 01'], // MERKEZ KASA
//
// //            [kod: '102'], //BANKALAR
//
// //            [kod:'102 01'], //VADESIZ HESAPLAR
// //            [kod:'102 01 001'] //AKBANK ignore
// //            [kod:'102 01 002'] //TEB ignore
//     [kod: '102 01 003'], //ISBANK
//     [kod:'102 01 004'], // ŞEKERBANK
//     [kod:'102 01 005'], // ŞEKERBANK EK HESAP
//
// //            [kod:'102 02'], //VADELI HESAPLAR
//     [kod:'102 02 001'], //ŞEKERBANK VADELİ HESAP
}


function config($stateProvider){
    var indexState = {
        name: 'app.gunlukFinans',
        url: '/gunlukFinans',
        template: require('./index.html')
    };

    var listState = {
        name: 'app.gunlukFinans.list',
        url: '/list',
        controller: 'GunlukFinansController',
        controllerAs: 'vm',
        template: require('./list.html')
    };

    $stateProvider.state(indexState);
    $stateProvider.state(listState);

    console.log('gunlukFinans states loaded');
}
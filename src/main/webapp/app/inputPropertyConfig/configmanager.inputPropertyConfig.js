angular.module("configmanager.inputPropertyConfig",
	[
    "configmanager.core",
    "configmanager.inputType",
    "ngResource","ui.router"])
	.config(['$stateProvider', function($stateProvider){
        $stateProvider
            .state('inputType.show.config', {
                // url: "/config?offset&max&sort&order", //pagination enabled!
                url: "/config",
                abstract: true,
                templateUrl: "configmanager/template/inputPropertyConfig/index.html",
                redirectTo: 'inputType.show.config.list'
            })
          .state('inputType.show.config.list', {
            // url: "/config?offset&max&sort&order", //pagination enabled!
            url: "",
            controller: 'InputPropertyConfigIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/inputPropertyConfig/list.html",
            resolve: {
              _list: ['InputPropertyConfig', '$stateParams', function(InputPropertyConfig, $stateParams){
                // debugger
                return InputPropertyConfig.findAllByInputType({typeId: $stateParams.typeId});
              }]
            }
          })
            .state('inputType.show.config.new', {
                url: "/new",
                controller: 'InputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/inputPropertyConfig/new.html",
                // component:'inputshow',
                //TODO resolve: InputController.resolve
                resolve: {
                    _instance: ['InputPropertyConfig', '$stateParams', function(InputPropertyConfig, $stateParams){
                        return {};//InputPropertyConfig.get({id: $stateParams.configId});
                    }]
                }
            })
            .state('inputType.show.config.show', {
                url: "/{configId}",
                controller: 'InputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/inputPropertyConfig/show.html",
                // component:'inputshow',
                //TODO resolve: InputController.resolve
                resolve: {
                    _instance: ['InputPropertyConfig', '$stateParams', function(InputPropertyConfig, $stateParams){
                        // debugger
                        return InputPropertyConfig.get({typeId: $stateParams.typeId, id: $stateParams.configId});
                    }]
                }
            })
            .state('inputType.show.config.edit', {
                url: "/{configId}/edit",
                controller: 'InputPropertyConfigController',
                controllerAs: 'vm',
                templateUrl: "configmanager/template/inputPropertyConfig/edit.html",
                // component:'inputshow',
                //TODO resolve: InputController.resolve
                resolve: {
                    _instance: ['InputPropertyConfig', '$stateParams', function(InputPropertyConfig, $stateParams){
                        return InputPropertyConfig.get({ typeId: $stateParams.typeId,id: $stateParams.configId});
                    }]
                }
            });
	}]);


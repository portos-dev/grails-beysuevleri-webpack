
InputPropertyConfigController.$inject = ['_instance', 'InputPropertyConfig', '$state', '$stateParams'];angular
    .module("configmanager.inputPropertyConfig")
    .controller("InputPropertyConfigController", InputPropertyConfigController);

function InputPropertyConfigController(_instance,  InputPropertyConfig, $state, $stateParams) {
    var vm = this;
    vm.instance = _instance;
    //debugger
    // vm.input_types = [1,2];//_input_types;
    // vm.input_types = _input_types;
    vm.newInstance = new InputPropertyConfig();
    vm.typeId = $stateParams.typeId;
    console.log($stateParams);
    vm.update = function(){
        vm.instance.$update({typeId:$stateParams.typeId}, function(){
            $state.go('^.list', {},{reload:true});
        });
    };
    vm.save = function(){
        vm.newInstance.inputType = {id:$stateParams.typeId};
        vm.newInstance.$save({typeId:$stateParams.typeId}, function(){
            $state.go('^.list', {},{reload:true});
        });
    }
}

InputPropertyConfigController.resolve = {
    _input: function(InputType, $stateParams){
        return InputPropertyConfig.get({id: $stateParams.id});
    }
};

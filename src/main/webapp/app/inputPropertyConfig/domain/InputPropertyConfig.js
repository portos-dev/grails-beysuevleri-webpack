
InputPropertyConfig.$inject = ['domainServiceFactory'];angular
    .module("configmanager.inputPropertyConfig")
    .factory("InputPropertyConfig", InputPropertyConfig);

function InputPropertyConfig(domainServiceFactory) {
  /*return domainServiceFactory("api/input/:inputId/prop/:id", {"id": "@id"},
    {

    });*/
    return domainServiceFactory("api/inputType/:typeId/config/:id", {"id": "@id"},
        {
            "findAllByInputType": {url:"api/inputPropertyConfig/findAllByInputType/:typeId",method:"GET", isArray: true}
        });
}



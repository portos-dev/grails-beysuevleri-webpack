/**
 * Created by birkan on 28.04.2017.
 */
angular.module('configmanager.outputText', [])
    .directive('outputBoolean', function(){
       /* var templateFn = function(elt,attrs){
            var templateWithParams = "<span class='classes'></span>";

        };*/
       return {
           scope: {
               value: '=',
               //size: '@',
               //colored: '='
               // expression: '='
           },
           // template: "<span class='{{classes}}' style='{{style}}'></span>",
           template: "<span></span>",
           link: function(scope,elt,attrs){
               var colors = { true: attrs.trueColor || 'green',
                false: attrs.falseColor || 'red' };
               var icons = {true: 'check', false: 'remove'};
               attrs.$addClass('fa fa-'+ icons[scope.value]);
               if('colored' in attrs){
//                        elt.setAttribute('style', 'color:green')
                       attrs.$set('style', 'color:'+colors[scope.value]);
               }
               if(attrs.size){
                   attrs.$addClass('fa-'+ attrs.size);
               }
           },
       }
    });

/**
 * Created by birkan on 22.05.2017.
 */
var FileSaver = require('file-saver');
angular.module('app.yevmiye', ['portos.core', 'ui.router'])
    .config(config)
    .factory('Yevmiye', Yevmiye)
    .factory('Yevmiye2', Yevmiye2)
    .controller('YevmiyeController', function(Yevmiye, Hesplan, moment, $http, $log/*, FileSaver*/){
        var vm = this;

        //vm.Yevmiye = new Yevmiye();

        // vm.list = Yevmiye.list({max:50});
        vm.query = {max:-1};




        // vm.hesplanMap = Hesplan.map({max : -1});
        vm.kodMap = {
            '100 01': 'KASA',
            '102 01 003': 'İŞBANK',
            '102 01 004': 'ŞEKERBANK',
            '102 01 005': 'ŞEKERBANK EK HES',
        };

        vm.turMap = {
            '100 01': '',
            '102 01 003': 'EFT',
            '102 01 004': 'EFT',
            '102 01 005': 'EFT',
        };

        vm.firma = function(masrafmerkezi) {
            var val = 'ORTAKLIK';
            if (masrafmerkezi === '4') {
                val = 'EMİR';
            }
            else if (masrafmerkezi === '5') {
                val = 'TEKİN';
            }else if(!masrafmerkezi){
                val = '';
            }
            return val;
        };

        vm.poi = function(){
            $http.post('gunlukOdemeTahsilat/poi', vm.query, {responseType: 'arraybuffer'}).then(
                function onResponse(resp){
                    $log.info(resp.data);
                    var xls = new Blob([resp.data], {
                        type: "application/vnd.ms-excel"
                    });
                    FileSaver.saveAs(xls, 'gunluk_odeme_tahsilat.xlsx');
                },
                function onError(data){
                    $log.error(data);
                }
            )
        };



        load();


        function pdf(){
            // PDF format using jsPDF and jsPDF Autotable
            $('#myTable').tableExport({type:'pdf',
                jspdf: {orientation: 'l',
                    format: 'a3',
                    margins: {left:10, right:10, top:20, bottom:20},
                    autotable: {styles: {fillColor: 'inherit',
                        textColor: 'inherit'},
                        tableWidth: 'auto'}
                }
            });
        }

        function xls(){
            $('#myTable').tableExport({fileName:'ESKI_TAHSILAT', type:'excel'});
        }
        function png(){
            $('#myTable').tableExport({type:'png'});
        }

        vm.export = function(_type){
            var type = _type || 'excel';

            if(type === 'excel'){
                xls();
            }
            else if(type === 'pdf'){
                pdf();
            }
            else if(type === 'png'){
                png();
            }

            // $(function () {
            //     // wait till load event fires so all resources are available
            //     $('#myTable').tableExport({type: type});
            // });

        };

        function load(){
            Yevmiye.query(vm.query, function(data){
                vm.list = data;
                var bakiye = 0;
                var borcluBakiye = 0;
                var alacakBakiye = 0;
                var donemToplamTahsilat = 0;
                var donemToplamHarcama = 0;
                angular.forEach(data.items, function(yevmiye, idx){
                    //TODO: ay sonlarinda toplam bakiyeyi bul, ay sonundaki son kayittan sonra ozet data yi ozet flagi ile ekle, html de bu flag var ise ona gore goruntulre
                    // if(val.borclu){
                    if(!yevmiye.donemSonu) {

                        bakiye += (yevmiye.borclu || 0);
                        bakiye -= (yevmiye.alacakli || 0);
                        donemToplamTahsilat += (yevmiye.borclu || 0);
                        donemToplamHarcama += (yevmiye.alacakli || 0);
                        // console.log('bakiye: ' + bakiye);
                        yevmiye.toplamBakiye = bakiye;

                        var ay = moment(yevmiye.evraktarihi).format('MMYYYY');

                        // console.log(moment(yevmiye.evraktarihi));
                        if (!data.items[idx + 1] || moment(data.items[idx + 1].evraktarihi).format('MMYYYY') !== ay) {
                            //console.log(moment(data.items[idx+1].evraktarihi));
                            // console.log('son kayit');
                            var donem = moment(yevmiye.evraktarihi).format('MMMM YYYY');
                            var donemOzet = {
                                aciklama: donem + ' DEVİR',
                                evraktarihi: yevmiye.evraktarihi,
                                donem: donem,
                                borclu: donemToplamTahsilat,
                                alacakli: donemToplamHarcama,
                                toplamBakiye: donemToplamTahsilat - donemToplamHarcama,
                                donemSonu: true
                            };
                            data.items.splice(idx+1, 0, donemOzet);// yevmiyeden sonraki indexe donem ozet ekle
                            donemToplamTahsilat = 0;
                            donemToplamHarcama = 0;
                        }
                    }
                    // }
                })
            });
        }

        //TODO send paging params returned from query
        vm.search = function(offset, max){
            if(vm.list.$cancelRequest){
                // We don't care about any pending request for hotels
                // in a different destination any more
                vm.list.$cancelRequest();
            }
            vm.query.offset = offset;
            vm.query.max = max;
            // vm.list = Yevmiye.query(vm.query);
            load();
        };
        vm.report = function(){
           vm.query.offset = 0;
           vm.query.max = -1;
           Yevmiye.gunlukTablo(vm.query, function(data){
               FileSaver.saveAs(data.response, 'gunlukTablo.pdf');
           });
        };
        vm.detayliTahsilat = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilat(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilat.pdf');
            });
        };
        vm.detayliTahsilatDefault = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatDefault(vm.query, function(data){
                FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
        vm.detayliTahsilatYeni = function(){
            vm.query.offset = 0;
            vm.query.max = -1;
            Yevmiye.detayliTahsilatYeni(vm.query, function(data){
                //TODO: FileSaver.saveAs(data.response, 'detayliTahsilatDefault.pdf');
            });
        };
    });

function Yevmiye(domainServiceFactory, Yevmiye2, Logyev) {
    return domainServiceFactory("yevmiye/:refno:refno2", {"refno": "@refno", "refno2": "@refno2"},
        {
            "get":{
                method: "GET",
                url:'yevmiye/show',
                params: {
                    refno: "@refno",
                    refno2: "@refno2"
                }
            },
            "children":{
                method: "POST",
                url:'yevmiye/children',
                /*params: {
                 refno: "@refno",
                 refno2: "@refno2"
                 }*/
                isArray: false,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Logyev(item));
                    });
                    return wrapped;
                }
            },
            "list": {
                method: "GET",
                isArray: false,
                transformResponse: function (json) {
                    // var wrapped = {
                    //     items: []
                    // };
                    var data = angular.fromJson(json)
                    var wrapped = angular.copy(data);
                    wrapped.items = [];
                    angular.forEach(data.items, function (item) {
                        wrapped.items.push(new Yevmiye2(item));
                    });
                    return wrapped;
                }
            },
            "query": {
                url: "yevmiye/query",
                method: "POST",
                isArray: false,
                transformResponse: function (json) {
                    var data = angular.fromJson(json)
                    var wrapped = angular.copy(data);
                    wrapped.items = [];
                    angular.forEach(data.items, function (item) {
                        wrapped.items.push(new Yevmiye2(item));
                    });
                    return wrapped;
                }
            },
            "gunlukTablo": {
                url: "report/gunlukTablo",
                method: "POST",
                headers: {
                    accept: 'application/pdf'
                },
                responseType: 'arraybuffer',
                cache: true,
                transformResponse: function (data) {
                    var pdf;
                    if (data) {
                        pdf = new Blob([data], {
                            type: 'application/pdf'
                        });
                    }
                    return {
                        response: pdf
                    };
                }
            },
            "detayliTahsilatYeni": {
                url: "detayliTahsilat",
                method: "POST",
                headers: {
                    accept: 'application/pdf'
                },
                responseType: 'arraybuffer',
                cache: true,
                transformResponse: function (data) {
                    var pdf;
                    if (data) {
                        pdf = new Blob([data], {
                            type: 'application/pdf'
                        });
                    }
                    return {
                        response: pdf
                    };
                }
            },
            "detayliTahsilat": {
                url: "report/detayliTahsilat",
                method: "POST",
                headers: {
                    accept: 'application/pdf'
                },
                responseType: 'arraybuffer',
                cache: true,
                transformResponse: function (data) {
                    var pdf;
                    if (data) {
                        pdf = new Blob([data], {
                            type: 'application/pdf'
                        });
                    }
                    return {
                        response: pdf
                    };
                }
            },
            "detayliTahsilatDefault": {
                url: "report/detayliTahsilatDefault",
                method: "POST",
                headers: {
                    accept: 'application/pdf'
                },
                responseType: 'arraybuffer',
                cache: true,
                transformResponse: function (data) {
                    var pdf;
                    if (data) {
                        pdf = new Blob([data], {
                            type: 'application/pdf'
                        });
                    }
                    return {
                        response: pdf
                    };
                }
            }
        });
}

function Yevmiye2(domainServiceFactory) {
    // return domainServiceFactory("yevmiye/:refno:refno2", {"refno": "@refno", "refno2": "@refno2"},
    return domainServiceFactory("yevmiye", {"refno": "@refno", "refno2": "@refno2"},
        {

            "get":{
                method: "GET",
                url:'yevmiye/show',
                params: {
                    refno: "@refno",
                    refno2: "@refno2"
                }
            },
            "children":{
                method: "POST",
                url:'yevmiye/children',
                /*params: {
                    refno: "@refno",
                    refno2: "@refno2"
                }*/
                isArray: true,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Logyev(item));
                    });
                    return wrapped.items;
                }
            },
            /*"list": {
                method: "GET",
                isArray: false,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Yevmiye(item));
                    });
                    return wrapped;
                }
            }*/
        });
}


function config($stateProvider){
    var indexState = {
        name: 'app.yevmiye',
        url: '/yevmiye',
        template: require('./index.html')
    };

    var listState = {
        name: 'app.yevmiye.list',
        url: '/list',
        controller: 'YevmiyeController',
        controllerAs: 'vm',
        template: require('./list.html')
    };

    $stateProvider.state(indexState);
    $stateProvider.state(listState);

    console.log('yevmiye states loaded');
}




InputType.$inject = ['domainServiceFactory'];angular
    .module("configmanager.inputType")
    .factory("InputType", InputType);

function InputType(domainServiceFactory) {
    /*return domainServiceFactory("api/inputType/:typeId", {"typeId": "@id"},
        {
            list:{isArray:false,method:'get'}
        });*/
    return domainServiceFactory("api/inputType/:id", {"id": "@id"},
        {
            "list": {method: "GET", isArray: false},
            "config": {url: "api/inputType/:id/config", method: "GET", isArray: true},
        });
}



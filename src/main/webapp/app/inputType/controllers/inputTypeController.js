
InputTypeController.$inject = ['_instance', 'InputType', '$state', '$stateParams'];angular
    .module("configmanager.inputType")
    .controller("InputTypeController", InputTypeController);

function InputTypeController(_instance,  InputType, $state, $stateParams) {
    var vm = this;
    vm.instance = _instance;
    // vm.input_types = [1,2];//_input_types;
    // vm.input_types = _input_types;
    vm.newInstance = new InputType();
    vm.update = function(){
        vm.instance.$update({}, function(data){
            // $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            // $state.go('^.list', $stateParams,{reload:true});
            $state.go('^.show', {typeId: data.id},{reload:true});
        });
    };
    vm.save = function(){
        vm.newInstance.$save({}, function(data){
            // $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            // debugger;
            $state.go('^.show', {typeId: data.id},{reload:true});
        });
    }
}

InputTypeController.resolve = {
    _input: function(InputType, $stateParams){
        return InputType.get({id: $stateParams.id});
    }
};

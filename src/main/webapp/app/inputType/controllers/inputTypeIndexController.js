
InputTypeIndexController.$inject = ['InputType', '$scope', '$stateParams', '$state'];angular
    .module("configmanager.inputType")
    .controller("InputTypeIndexController", InputTypeIndexController);

function InputTypeIndexController(InputType, $scope, $stateParams, $state) {
    var vm = this;
    vm.list = InputType.list($stateParams);

    vm.pageChanged = function(){
        console.log('pageChanged: ' + $scope.page);
        vm.page = $scope.page;
        //$stateParams.page = vm.currentPage;
        //$state.reload();
        // $state.go('.', {page:vm.currentPage,offset: (vm.currentPage-1) * max}, {reload:true})
        // $state.go('.', {offset: (vm.page-1) * vm.max}, {reload:false, notify:true})
        $state.go('.', {offset: (vm.page-1) * vm.list.max, max:vm.list.max})
    };

    $scope.$watch('vm.list.$resolved', function(val){
        console.log('resolved: ' + val);
        if(val){
            console.log(vm.list);
            vm.count = vm.list.count;
            vm.max = vm.list.max;
            vm.offset = vm.list.offset;
            vm.pages = vm.count/vm.max;
            vm.currentPage = vm.offset ? (vm.offset/vm.max)+1 : 1;
            vm.pageList = [];
            for(var i=0; i < vm.pages; i++){
                vm.pageList.push({index: i+1, offset: i*vm.max, max: vm.max});

            }
        }
    });

}

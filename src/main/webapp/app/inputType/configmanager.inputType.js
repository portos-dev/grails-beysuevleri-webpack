angular.module("configmanager.inputType",
	[
        "configmanager.core",
         "ngResource",
         "ui.router"
    ])
	.config(['$stateProvider', function($stateProvider){
        $stateProvider
        .state('inputType', {
            url:'/inputType',
            abstract:true,
            templateUrl: "configmanager/template/inputType/index.html"
        })
        .state('inputType.list', {
            url: "?offset&max&sort&order", //pagination enabled!
            controller: 'InputTypeIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/inputType/list.html"
        })
        .state('inputType.new', {
            url: "/new",
            //parent: 'inputType',
            // controller: 'InputController as vm',
            controller: 'InputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/inputType/new.html",
            // component:'inputshow',
            //TODO resolve: InputController.resolve
            resolve: {
                _instance: ['$stateParams', function($stateParams){
                    //return new Input();
                    //return Input.get({id: $stateParams.typeId});
                    return {};
                }]
        }})
        .state('inputType.show', {
            url: "/{typeId}",
            //parent: 'input',
            // controller: 'InputController as vm',
            controller: 'InputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/inputType/show.html",
            // component:'inputshow',
            //TODO resolve: InputController.resolve
            resolve: {
                _instance: ['InputType', '$stateParams', function(InputType, $stateParams){
                    return InputType.get({id: $stateParams.typeId});
                }]
            }
        })
        .state('inputType.edit', {
            url: "/{typeId}/edit",
            //parent: 'input',
            // controller: 'InputController as vm',
            controller: 'InputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/inputType/edit.html",
            // component:'inputshow',
            //TODO resolve: InputController.resolve
            resolve: {
                _instance: ['InputType', '$stateParams', function(InputType, $stateParams){
                    return InputType.get({id: $stateParams.typeId});
                }]
            }
        })
        ;
	}]);


/**
 * Created by birkan on 22.05.2017.
 */
Hesplan.$inject = ['domainServiceFactory', 'Hesplan2'];
angular.module('app.hesplan',['portos.core'])
    .config(config)
    .factory('Hesplan',Hesplan)
    .factory('Hesplan2',Hesplan2) //FIXME: workaround to avoid circular dependency in wrapping items with resources

    .controller('HesplanController', function(Hesplan){
        var vm = this;

        //vm.list = Hesplan.list({max:20});

        vm.search = function(){
            if(vm.list.$cancelRequest){
                vm.list.$cancelRequest();
            }
            vm.list = Hesplan.query({filterQuery: vm.query});
        }

    })
    .controller('HesplanTreeController', HesplanTreeController)
;



function Hesplan(domainServiceFactory, Hesplan2){
    return domainServiceFactory("hesplan/:id", {"id": "@id"},
        {
            "list": {
                method: "GET",
                isArray: false,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Hesplan2(item));
                    });
                    return wrapped;
                }
            },
            "map": {
                url: "hesplan/map",
              method: "POST",
              isArray: false,
            },
            "query": {
                url: "hesplan/query",
                method: "POST",
                isArray: false,
                transformResponse: function (json) {
                    var data = angular.fromJson(json)
                    var wrapped = angular.copy(data);
                    wrapped.items = [];
                    angular.forEach(data.items, function (item) {
                        wrapped.items.push(new Hesplan2(item));
                    });
                    return wrapped;
                }
            }
        });
}

function Hesplan2(domainServiceFactory){
    return domainServiceFactory("hesplan/:id", {"id": "@kod"},
        {
           /* "list": {
                method: "GET",
                isArray: false,
                transformResponse: function (data) {
                    var wrapped = {
                        items: []
                    };
                    angular.forEach(angular.fromJson(data), function (item) {
                        wrapped.items.push(new Hesplan(item));
                    });
                    return wrapped;
                }
            }*/
        });
}

function HesplanTreeController(Hesplan){
    var vm = this;

    vm.list = Hesplan.query({ kods: null,kodLikes: ['100%', '102%'], max:-1},function(data){
         console.log('hesplan loaded');
         console.log(data);
         vm.rootObjects = prepareHierarchy(data.items);
    });

    function prepareHierarchy(items){
        console.log('preparing hierarchy');
        var maxDepth = 3;
        // var finalHierarchy;
        //var depthKodLength = {0 : 3, 1: 6, 2: 10,  } ;
        if(items.length) {
            var levels = [];
            for (var depth = 0; depth <= maxDepth; depth++) {
                levels[depth] = [];
            }
            angular.forEach(items, function (val) {
                var depth = (val.kod.match(/ /g) || []).length;
                levels[depth].push(val);
            });
            console.log(levels);

            var root = levels[0];
            for (var depth = 0; depth <= maxDepth-1; depth++) {
                var rootObjects = levels[depth];
                var childObjects = levels[depth+1];
                angular.forEach(rootObjects, function(parent){
                    var childRegExp = new RegExp(parent.kod + "\\s\\S+$");
                    angular.forEach(childObjects, function(childCandid){
                        var isChild = childCandid.kod.match(childRegExp);
                        if(isChild){
                            if(parent.children){
                                parent.children.push(childCandid);
                            }
                            else{
                                parent.children = [childCandid];
                            }
                        }
                    })
                });
            }

            console.log(root);

            return root;

            //
            //
            //
            // for (var depth = 0; depth <= maxDepth-1; depth++) {
            //     var levelArr = levels[depth];
            //     var subLevelArr = levels[depth+1];
            //     angular.forEach(items, function (val) {
            //         if ((val.kod.match(/ /g) || []).length == depth) {
            //             levels[depth].push(val);
            //         }
            //     })
            // }

        }else{
            console.log('empty items array param');
        }
    }

    function findRoots(items){

    }




}

function HesplanTree(domainServiceFactory){

}

function config($stateProvider){
    var indexState = {
        name: 'app.hesplan',
        url: '/hesplan',
        template: require('./index.html')
    };

    var listState = {
        name: 'app.hesplan.list',
        url: '/list',
        controller: 'HesplanController',
        controllerAs: 'vm',
        template: require('./list.html')
    };

    $stateProvider.state(indexState);
    $stateProvider.state(listState);

    console.log('hesplan states loaded');
}




OutputProperty.$inject = ['domainServiceFactory'];angular
    .module("configmanager.output")
    .factory("OutputProperty", OutputProperty);


function OutputProperty(domainServiceFactory) {
    return domainServiceFactory("api/output/:outputId/prop/:id", {"id": "@id", "outputId": "@configItem.id"},
        {
            //props: {url:'api/output/:id/props',isArray: true, method:'get'}
        });
}

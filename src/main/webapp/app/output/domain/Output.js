
Output.$inject = ['domainServiceFactory'];
angular
    .module("configmanager.output")
    .factory("Output", Output);


function Output(domainServiceFactory) {
    return domainServiceFactory("api/output/:id", {"id": "@id"},
        {
            pending: {url:'api/pendingOutputs',isArray: true, method:'get'},
            erroneous: {url:'api/erroneousOutputs',isArray: true, method:'get'},
            list:{isArray:false,method:'get'},
           // props: {url:'api/output/:id/prop',isArray: true, method:'get'}
        });
}


angular.module("configmanager.output",
	[
    "configmanager.core",
    "configmanager.outputType",
    "ngResource",
    "ui.router",
    "ui.bootstrap"
    // ]).config(['$stateProvider', 'OutputStatesProvider', function($stateProvider, OutputStatesProvider) {
    ]).config(['$stateProvider',  function($stateProvider) {
    // $stateProvider
    // var states = OutputStatesProvider.getStates();
    var states = OutputStates();
    for (var state in states) {
        var stateName =  state;
        var stateOptions = states[state];
        $stateProvider.state(stateName, stateOptions);
        //console.log('configmanager.output.config -> state: ' + stateName);
    }

    //OutputStatesProvider.addStates();
    // OutputStateProvider($stateProvider).addStates();
}]);


function OutputStates(){
    var states = {
        'output': {
            abstract: true,
            url: "/output", //pagination enabled!
            // controller: 'OutputIndexController',
            // controllerAs: 'vm',
            templateUrl: "configmanager/template/output/index.html"
        },
        'output.list': {
            url: "/list?offset&max&sort&order&page", //pagination enabled!
            params: {
                offset: {value: '0', squash: true},
                max: {value: '10', squash: true},
                page: {value: '1', squash: true}
            },
            controller: 'OutputIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/output/list.html",
            resolve: {
                _list: ['Output', '$stateParams', function (Output, $stateParams) {
                    return Output.list($stateParams).$promise;
                }],
                _page: ['$stateParams', function ($stateParams) {
                    var offset = $stateParams.offset || 0;
                    var max = $stateParams.max || 10;
                    // var page = $stateParams.page; //offset ? (offset/max) + 1 : 1;
                    var page = offset ? (offset / max) + 1 : 1;
                    return page;
                }]
            }
        },
        'output.new': {
            url: "/new",
            //parent: 'output',
            // controller: 'OutputController as vm',
            controller: 'OutputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/output/new.html",
            // component:'outputshow',
            //TODO resolve: OutputController.resolve
            resolve: {
                _output: ['Output',function (Output) {
                    return new Output();
                    //return Output.get({id: $stateParams.id});
                    //return {};
                }],
                _output_types: ['OutputType',function (OutputType) {
                    return OutputType.list();
                }]
            }
        },
        'output.show': {
            url: '/{outputId}',
            // abstract: true,
            controller: 'OutputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/output/show-tabbed.html",
            //redirectTo: 'output.show.main'
            resolve: {
                _output: ['Output', '$stateParams',function (Output, $stateParams) {
                    //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                    return Output.get({id: $stateParams.outputId}).$promise;
                }],
                _output_types: ['OutputType',function (OutputType) {
                    return OutputType.list();
                }]
            }
        },
        'output.show.main': {
            url: "/main?mode",
            controller: 'OutputController',
            controllerAs: 'vm',
            /*
            cannot use array for DI here
            templateUrl: function ($stateParams) {
                return $stateParams.mode === 'edit' ? "configmanager/template/output/edit.html" :
                    "configmanager/template/output/show.html"
            },*/
            templateProvider: ['$templateCache','$stateParams',function ($templateCache,$stateParams) {
                return $templateCache.get($stateParams.mode === 'edit' ?
                    "configmanager/template/output/edit.html" :
                    "configmanager/template/output/show.html")
            }],
            resolve: {
                _output: ['Output', '$stateParams',function (Output, $stateParams) {
                    //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                    return Output.get({id: $stateParams.outputId}).$promise;
                }],
                _output_types: ['OutputType',function (OutputType) {
                    return OutputType.list();
                }]
            }
        },
        'output.show.props': {
            url: "/props",
            controller: 'OutputPropertiesController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/output/props-list.html",
            resolve: {
                _list: ['OutputProperty', '$stateParams',function (OutputProperty, $stateParams) {
                    // debugger;
                    // return OutputPropertyConfig.findAllByOutputType({id: $stateParams.id});
                    return OutputProperty.list({outputId: $stateParams.outputId});
                }],
                _instance: ['OutputProperty',function (OutputProperty) {
                    return new OutputProperty();//OutputProperty.get({outputId: $stateParams.id, id: $stateParams.propId})
                }],
                _propertyTypes: ['OutputPropertyConfig','Output', '$stateParams',function (OutputPropertyConfig, Output, $stateParams) {
                    Output.get({id: $stateParams.outputId}, function (output) {
                        return OutputPropertyConfig.findAllByOutputType({typeId: output.type.id}).$promise;
                    });
                }]
            }
        },
        'output.show.main.edit': {
            url: "/{outputId}/edit",
            //parent: 'output',
            // controller: 'OutputController as vm',
            controller: 'OutputController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/output/edit.html",
            // component:'outputshow',
            //TODO resolve: OutputController.resolve
            resolve: {
                _output: ['Output', '$stateParams',function (Output, $stateParams) {
                    return Output.get({id: $stateParams.outputId});
                }],
                _output_types: ['OutputType',function (OutputType) {
                    return OutputType.list();
                }]
            }
        }
    };
    return states;
}



  /*  .provider('OutputStates', ['$stateProvider', function providerConstructor($stateProvider) {
        this.getStates = function(){
            var states = {
                'output': {
                    abstract: true,
                    url: "/output", //pagination enabled!
                    // controller: 'OutputIndexController',
                    // controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/index.html"
                },
                'output.list': {
                    url: "/list?offset&max&sort&order&page", //pagination enabled!
                    params: {
                        offset: {value: '0', squash: true},
                        max: {value: '10', squash: true},
                        page: {value: '1', squash: true}
                    },
                    controller: 'OutputIndexController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/list.html",
                    resolve: {
                        _list: function (Output, $stateParams) {
                            return Output.list($stateParams).$promise;
                        },
                        _page: function ($stateParams) {
                            var offset = $stateParams.offset || 0;
                            var max = $stateParams.max || 10;
                            // var page = $stateParams.page; //offset ? (offset/max) + 1 : 1;
                            var page = offset ? (offset / max) + 1 : 1;
                            return page;
                        }
                    }
                },
                'output.new': {
                    url: "/new",
                    //parent: 'output',
                    // controller: 'OutputController as vm',
                    controller: 'OutputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/new.html",
                    // component:'outputshow',
                    //TODO resolve: OutputController.resolve
                    resolve: {
                        _output: function ($stateParams, Output) {
                            return new Output();
                            //return Output.get({id: $stateParams.id});
                            //return {};
                        },
                        _output_types: function (OutputType) {
                            return OutputType.list();
                        }
                    }
                },
                'output.show': {
                    url: '/{outputId}',
                    // abstract: true,
                    controller: 'OutputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/show-tabbed.html",
                    //redirectTo: 'output.show.main'
                    resolve: {
                        _output: function (Output, $stateParams) {
                            //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                            return Output.get({id: $stateParams.outputId}).$promise;
                        },
                        _output_types: function (OutputType) {
                            return OutputType.list();
                        }
                    }
                },
                'output.show.main': {
                    url: "/main?mode",
                    controller: 'OutputController',
                    controllerAs: 'vm',
                    templateUrl: function ($stateParams) {
                        return $stateParams.mode === 'edit' ? "configmanager/template/output/edit.html" :
                            "configmanager/template/output/show.html"
                    },
                    resolve: {
                        _output: function (Output, $stateParams) {
                            //http://www.jvandemo.com/how-to-resolve-angularjs-resources-with-ui-router/
                            return Output.get({id: $stateParams.outputId}).$promise;
                        },
                        _output_types: function (OutputType) {
                            return OutputType.list();
                        }
                    }
                },
                'output.show.props': {
                    url: "/props",
                    controller: 'OutputPropertiesController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/props-list.html",
                    resolve: {
                        _list: function (OutputProperty, $stateParams) {
                            // debugger;
                            // return OutputPropertyConfig.findAllByOutputType({id: $stateParams.id});
                            return OutputProperty.list({outputId: $stateParams.outputId});
                        },
                        _instance: function (OutputProperty, $stateParams) {
                            return new OutputProperty();//OutputProperty.get({outputId: $stateParams.id, id: $stateParams.propId})
                        },
                        _propertyTypes: function (OutputPropertyConfig, Output, $stateParams) {
                            Output.get({id: $stateParams.outputId}, function (output) {
                                return OutputPropertyConfig.findAllByOutputType({typeId: output.type.id}).$promise;
                            });
                        }
                    }
                },
                'output.show.main.edit': {
                    url: "/{outputId}/edit",
                    //parent: 'output',
                    // controller: 'OutputController as vm',
                    controller: 'OutputController',
                    controllerAs: 'vm',
                    templateUrl: "configmanager/template/output/edit.html",
                    // component:'outputshow',
                    //TODO resolve: OutputController.resolve
                    resolve: {
                        _output: function (Output, $stateParams) {
                            return Output.get({id: $stateParams.outputId});
                        },
                        _output_types: function (OutputType) {
                            return OutputType.list();
                        }
                    }
                }
            };
            return states;
        };
        this.$get = function() {
            return this;
        };
    }]);*/


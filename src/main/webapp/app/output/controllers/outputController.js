
OutputController.$inject = ['_output', '_output_types', 'Output', '$state', '$stateParams'];angular
    .module("configmanager.output")
    .controller("OutputController", OutputController);

function OutputController(_output, _output_types, Output, $state, $stateParams) {
    var vm = this;
    vm.instance = _output;
    vm.output_types = _output_types;
    vm.mode = $stateParams.mode;
    // vm.properties = _properties;
    //vm.newInstance = new Output();

    vm.update = function(){
        vm.instance.$update({}, function(data){
            console.log('update OK');
            //TODO route to show
            //$state.reload();
            // $state.go('^');
            // $state.go('^.list', {},{reload:true});
            //debugger;
            // $state.go('^.list', $stateParams,{reload:true});
            $state.go('.', {outputId: data.id, mode:null},{reload:true});
        });
    };
    vm.save = function(valid, form){
      // if(valid) {


        vm.instance.$save({}, function (data) {
          console.log('save OK');
          console.log('save OK');
          //TODO route to show
          // $state.reload('^');
          //this works  $state.go('^', {},{reload:true});
          // $state.go('^.list', {},{reload:true});
          //debugger;
          $state.go('^.show.main', {outputId: data.id}, {reload: true});
        });
      // }
      // else {
      //   console.log(form);
      // }
    }
}

OutputController.resolve = {
    _output: function(Output, $stateParams){
        return Output.get({id: $stateParams.outputId});
    }
};

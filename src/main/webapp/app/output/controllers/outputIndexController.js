
OutputIndexController.$inject = ['_list', '_page', '$scope', '$stateParams', '$state'];function OutputIndexController(_list, _page, $scope, $stateParams, $state) {
    var vm = this;
    vm.list = _list;//Output.list($stateParams);
    $scope.page = _page;
    console.log('$scope.page: ' + $scope.page);
    vm.pageChanged = function(){
        console.log('pageChanged: ' + $scope.page);
        vm.page = $scope.page;
        //$stateParams.page = vm.currentPage;
        //$state.reload();
        // $state.go('.', {page:vm.currentPage,offset: (vm.currentPage-1) * max}, {reload:true})
        // $state.go('.', {offset: (vm.page-1) * vm.max}, {reload:false, notify:true})
        $state.go('.', {offset: (vm.page-1) * vm.list.max, max:vm.list.max})
    };

/* return $promise in output.list state resolve body

$scope.$watch('vm.list.$resolved', function(val){
       console.log('resolved: ' + val);
       if(val){*/
           console.log(vm.list);
           vm.count = vm.list.count;
           vm.max = vm.list.max;
           vm.offset = vm.list.offset;
           vm.pages = vm.count/vm.max;
           vm.currentPage = vm.offset ? (vm.offset/vm.max)+1 : 1;
           vm.pageList = [];
           for(var i=0; i < vm.pages; i++){
               vm.pageList.push({index: i+1, offset: i*vm.max, max: vm.max});

           }
       /*}
    });*/

}

/*OutputIndexController.resolve = {
    _list: function(Output, $stateParams){
        return Output.list($stateParams);
    }
}*/
angular
    .module("configmanager.output")
    .controller("OutputIndexController", OutputIndexController);



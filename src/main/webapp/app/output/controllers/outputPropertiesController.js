
OutputPropertiesController.$inject = ['_list', '_instance', '_propertyTypes', '$state', '$stateParams', 'OutputProperty'];angular
    .module("configmanager.output")
    .controller("OutputPropertiesController", OutputPropertiesController);

function OutputPropertiesController(_list, _instance, _propertyTypes, $state, $stateParams, OutputProperty) {
    var vm = this;
    vm.instance = _instance;
    // vm.list = _output.properties;
    vm.list = _list;
    vm.outputId = $stateParams.outputId;
    vm.propertyTypes = _propertyTypes;
    // vm.newInstance = new Output();

    vm.update = function(outputId){
        vm.instance.$update({outputId: outputId}, function(data){
            console.log('update OK');
            //TODO route to show
            //$state.reload();
            // $state.go('^');
            // $state.go('^.list', {},{reload:true});
            //debugger;
            // $state.go('^.list', $stateParams,{reload:true});
            debugger;
            $state.go('^.show', {outputId: data.id},{reload:true});
        });
    };
    vm.saveOrUpdate = function(data,row){
      if(row.id) {
          angular.extend(row, data);
          row.$update({},function (resp) {
//         new OutputProperty(data).$save({outputId: vm.outputId},function (resp) {
              console.log(resp);
              $state.go('.', {outputId: vm.outputId}, {reload: true});
          });
        // OutputProperty.get({outputId: vm.outputId, id: row.id}, function (resp) {
        //   console.log(resp);
        //   angular.extend(resp, data);
        //   resp.$update({outputId: vm.outputId}, function () {
        //     console.log(resp);
        //     $state.go('.', {outputId: vm.outputId}, {reload: true});
        //   });
        // });
      }
      else{
          angular.extend(row, data);
//         data.output = output;
          row.$save({},function (resp) {
//         new OutputProperty(data).$save({outputId: vm.outputId},function (resp) {
              console.log(resp);
              $state.go('.', {outputId: vm.outputId}, {reload: true});
          });
      }
        // data.id = outputPropertyId;
        // data.$save({}, function (data) {
        //   console.log(data);
        //
        // })
        /*vm.newInstance.$save({outputId: outputId}, function(data){
            console.log('save OK');
            console.log('save OK');
            //TODO route to show
            // $state.reload('^');
            //this works  $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            //debugger;
            $state.go('^.show', {id: data.id},{reload:true});
        });*/
    }
}

OutputPropertiesController.resolve = {
    _output: function(Output, $stateParams){
        return Output.get({id: $stateParams.outputId});
    }
};

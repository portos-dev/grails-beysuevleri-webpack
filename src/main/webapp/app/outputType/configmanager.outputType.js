angular.module("configmanager.outputType",
	[
        "configmanager.core",
         "ngResource",
         "ui.router"
    ])
	.config(['$stateProvider', function($stateProvider){
        $stateProvider
        .state('outputType', {
            url:'/outputType',
            abstract:true,
            templateUrl: "configmanager/template/outputType/index.html"
        })
        .state('outputType.list', {
            url: "?offset&max&sort&order", //pagination enabled!
            controller: 'OutputTypeIndexController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/outputType/list.html"
        })
        .state('outputType.new', {
            url: "/new",
            //parent: 'outputType',
            // controller: 'OutputController as vm',
            controller: 'OutputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/outputType/new.html",
            // component:'outputshow',
            //TODO resolve: OutputController.resolve
            resolve: {
                _instance: ['$stateParams', function($stateParams){
                    //return new Output();
                    //return Output.get({id: $stateParams.typeId});
                    return {};
                }]
        }})
        .state('outputType.show', {
            url: "/{typeId}",
            //parent: 'output',
            // controller: 'OutputController as vm',
            controller: 'OutputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/outputType/show.html",
            // component:'outputshow',
            //TODO resolve: OutputController.resolve
            resolve: {
                _instance: ['OutputType', '$stateParams', function(OutputType, $stateParams){
                    return OutputType.get({id: $stateParams.typeId});
                }]
            }
        })
        .state('outputType.edit', {
            url: "/{typeId}/edit",
            //parent: 'output',
            // controller: 'OutputController as vm',
            controller: 'OutputTypeController',
            controllerAs: 'vm',
            templateUrl: "configmanager/template/outputType/edit.html",
            // component:'outputshow',
            //TODO resolve: OutputController.resolve
            resolve: {
                _instance: ['OutputType', '$stateParams', function(OutputType, $stateParams){
                    return OutputType.get({id: $stateParams.typeId});
                }]
            }
        })
        ;
	}]);


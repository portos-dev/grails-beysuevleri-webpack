
OutputType.$inject = ['domainServiceFactory'];angular
    .module("configmanager.outputType")
    .factory("OutputType", OutputType);

function OutputType(domainServiceFactory) {
    /*return domainServiceFactory("api/outputType/:typeId", {"typeId": "@id"},
        {
            list:{isArray:false,method:'get'}
        });*/
    return domainServiceFactory("api/outputType/:id", {"id": "@id"},
        {
            "list": {method: "GET", isArray: false},
            "config": {url: "api/outputType/:id/config", method: "GET", isArray: true},
        });
}



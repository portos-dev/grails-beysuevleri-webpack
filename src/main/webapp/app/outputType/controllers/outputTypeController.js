
OutputTypeController.$inject = ['_instance', 'OutputType', '$state', '$stateParams'];angular
    .module("configmanager.outputType")
    .controller("OutputTypeController", OutputTypeController);

function OutputTypeController(_instance,  OutputType, $state, $stateParams) {
    var vm = this;
    vm.instance = _instance;
    // vm.output_types = [1,2];//_output_types;
    // vm.output_types = _output_types;
    vm.newInstance = new OutputType();
    vm.update = function(){
        vm.instance.$update({}, function(data){
            // $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            // $state.go('^.list', $stateParams,{reload:true});
            $state.go('^.show', {typeId: data.id},{reload:true});
        });
    };
    vm.save = function(){
        vm.newInstance.$save({}, function(data){
            // $state.go('^', {},{reload:true});
            // $state.go('^.list', {},{reload:true});
            // debugger;
            $state.go('^.show', {typeId: data.id},{reload:true});
        });
    }
}

OutputTypeController.resolve = {
    _output: function(OutputType, $stateParams){
        return OutputType.get({id: $stateParams.id});
    }
};

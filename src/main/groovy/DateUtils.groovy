import java.text.SimpleDateFormat

/**
 * Created by birkan on 31.05.2017.
 */
class DateUtils {

    static  void main (String[] args){
        String dateStr = '2017-03-27T21:00:00Z'
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ssZ")
        println sdf.parse(dateStr)
        println sdf2.parse(dateStr)
        println sdf3.parse(dateStr)
    }
}

package tr.com.portos.grails.rest

import grails.artefact.Artefact
import grails.rest.RestfulController

/**
 * Created by birkan on 25.05.2017.
 */
@Artefact("Controller")
class PagingController<T> extends RestfulController<T>{
    PagingController(Class<T> resource){
        super(resource)
    }

    def index(Integer max){
        params.max = Math.min(max ?: 10, 100)
        def detail = params.detail ?: "complete"

        respond listAllResources(params),
                [detail: detail,
                 paging:[totalCount: resource.count(),
                         currentMax: params.max,
                         currentOffset: params.offset ?: 0]
                ]
    }
}

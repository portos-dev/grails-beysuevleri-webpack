package tr.com.portos.hibernate.mssql

import org.hibernate.dialect.function.VarArgsSQLFunction
import org.hibernate.type.Type

/**
 * Created by birkan on 30.05.2017.
 */
class FormatDateSQLFunction extends VarArgsSQLFunction {

    FormatDateSQLFunction(Type registeredType, String begin, String sep, String end) {
        super(registeredType, begin, sep, end)
    }
}

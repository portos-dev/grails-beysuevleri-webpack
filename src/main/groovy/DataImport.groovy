import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by birkan on 14.05.2017.
 */
class DataImport {

    static void main(String[] args){
        String database = args ? args[0]: 'TEKIN_EMIR_ORTAKLIK_2017'
        String url = "jdbc:jtds:sqlserver://192.168.0.102:49177;databaseName=${database};"
        String user = "birkan"
        String password = "asd"
        String urlWithAuth = "${url}user=${user};password=${password}"
        Class.forName("net.sourceforge.jtds.jdbc.Driver")
        Connection conn

        Pattern identityInsertRegEx = ~/in table '(.*)' when IDENTITY_INSERT is set to OFF/

        try {
            conn = DriverManager.getConnection(urlWithAuth)
            conn.autoCommit = false
            conn.createStatement().executeUpdate('EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"')

            File rootDir = new File("/ssd/projects/beysuevleri/zirve-data/full-wo-sys/${database}")
            rootDir.list(new FilenameFilter() {
                @Override
                boolean accept(File dir, String name) {
//                    return (/*dir.isFile() &&*/ name.startsWith("xa"))
                    return ( name.indexOf("_sys_") < 0)
                    return true
                }
            }).each {
                String fileName ->
                    def f = new File(rootDir, fileName)
                    f.readLines().each { String line ->
                        //print line
                        try {
                            Statement stmt = conn.createStatement()
                            stmt.executeUpdate(line)
                        }catch(SQLException se){
                            println se.message
                            Matcher matcher = identityInsertRegEx.matcher(se.message)
                            if(matcher.find()){
                                def tableName = matcher.group(1)
                                Statement stmt = conn.createStatement()
                                stmt.executeUpdate("SET IDENTITY_INSERT dbo.${tableName} ON;")
                                stmt.executeUpdate(line)
                                stmt.executeUpdate("SET IDENTITY_INSERT dbo.${tableName} OFF;")
                            }
                        }
                    }
                    conn.commit() //every file commits individually
            }
            //conn.commit()
        }catch(Exception e){
            println e.message
            conn?.rollback()
        }finally {
            conn?.createStatement().executeUpdate('EXEC sp_MSforeachtable @command1="print \'?\'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"')
            conn?.close()
        }
    }
}

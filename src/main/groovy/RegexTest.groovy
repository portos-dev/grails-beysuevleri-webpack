/**
 * Created by birkan on 22.05.2017.
 */
class RegexTest {

    static void main(String[] args){
        def textComp = "CREATE TABLE yevmiye\n" +
                "(\n" +
                "    Refno INT NOT NULL,\n" +
                "    Tip NVARCHAR(1),\n" +
                "    Gmkod NVARCHAR(20),\n" +
                "    Refno2 INT NOT NULL,\n" +
                "    Sube SMALLINT,\n" +
                "    Fistar SMALLDATETIME,\n" +
                "    Fistur NVARCHAR(1),\n" +
                "    Fisno NVARCHAR(9),\n" +
                "    Yevno INT,\n" +
                "    Dovizadi SMALLINT,\n" +
                "    Aciklama NVARCHAR(50),\n" +
                "    Miktar FLOAT,\n" +
                "    Doviz FLOAT,\n" +
                "    Borclu FLOAT,\n" +
                "    Alacakli FLOAT,\n" +
                "    Stk NVARCHAR(20),\n" +
                "    Islemtipi SMALLINT,\n" +
                "    Masrafmerkezi NVARCHAR(10),\n" +
                "    Evraktarihi SMALLDATETIME,\n" +
                "    Evrakno NVARCHAR(16),\n" +
                "    f_int INT,\n" +
                "    f_str NVARCHAR(80),\n" +
                "    f_islemkodu INT,\n" +
                "    Vergino NVARCHAR(11),\n" +
                "    fisbil_pid NVARCHAR(18),\n" +
                "    serino NVARCHAR(5),\n" +
                "    fis_guid UNIQUEIDENTIFIER,\n" +
                "    Belge_Turu SMALLINT,\n" +
                "    Odeme_Sekli SMALLINT,\n" +
                "    Belge_Turu_Aciklamasi NVARCHAR(50),\n" +
                "    Vade_Tarihi SMALLDATETIME,\n" +
                "    isDegisti TINYINT,\n" +
                "    sp_id NVARCHAR(50),\n" +
                "    CONSTRAINT PK_yevmiye PRIMARY KEY (Refno, Refno2),\n" +
                "    CONSTRAINT FK_yevmiye_fisbil FOREIGN KEY (Refno) REFERENCES fisbil (Refno)\n" +
                ");"

        def textSingle = "CREATE TABLE LogYev\n" +
                "(\n" +
                "    Arti INT PRIMARY KEY NOT NULL IDENTITY,\n" +
                "    Refno INT NOT NULL,\n" +
                "    Tip NVARCHAR(1),\n" +
                "    Gmkod NVARCHAR(20),\n" +
                "    Refno2 INT NOT NULL,\n" +
                "    Sube SMALLINT,\n" +
                "    Fistar SMALLDATETIME,\n" +
                "    Fistur NVARCHAR(1),\n" +
                "    Fisno NVARCHAR(9),\n" +
                "    Yevno INT,\n" +
                "    Dovizadi SMALLINT,\n" +
                "    Aciklama NVARCHAR(50),\n" +
                "    Miktar FLOAT,\n" +
                "    Doviz FLOAT,\n" +
                "    Borclu FLOAT,\n" +
                "    Alacakli FLOAT,\n" +
                "    Stk NVARCHAR(20),\n" +
                "    Islemtipi SMALLINT,\n" +
                "    Masrafmerkezi NVARCHAR(10),\n" +
                "    Evraktarihi SMALLDATETIME,\n" +
                "    Evrakno NVARCHAR(16),\n" +
                "    f_int INT,\n" +
                "    f_str NVARCHAR(80),\n" +
                "    f_islemkodu INT,\n" +
                "    Vergino NVARCHAR(11),\n" +
                "    fisbil_pid NVARCHAR(18),\n" +
                "    serino NVARCHAR(5),\n" +
                "    fis_guid UNIQUEIDENTIFIER,\n" +
                "    Belge_Turu SMALLINT,\n" +
                "    Odeme_Sekli SMALLINT,\n" +
                "    Belge_Turu_Aciklamasi NVARCHAR(50),\n" +
                "    Vade_Tarihi SMALLDATETIME,\n" +
                "    isDegisti TINYINT,\n" +
                "    sp_id NVARCHAR(50)\n" +
                ")"

        checkText(textComp)
        println ""
        checkText(textSingle)

    }

    def static checkText(text){
        def findComposite = (text =~ /(?m)CONSTRAINT \S* PRIMARY KEY \(([\w,\s]+)\)[,]?/)
        def hasCompositePkGroup = findComposite.count > 0

        def findSingle = (text =~ /(?m)(\S*) \S* PRIMARY KEY NOT NULL IDENTITY/)
        def hasSinglePkGroup = findSingle.count > 0

        if(hasCompositePkGroup) {
            println " * FOUND COMPOSITE PRIMARY KEY ${hasCompositePkGroup}"
            println " * COMPOSITE_KEY_SIZE: ${findComposite.count}"
            println " * FIRST MATCH: ${findComposite[0][1].split(',')}"
            //println " * PRIMARY_KEY: ${compositePkGroup[1]}"
        }


        if(hasSinglePkGroup) {
            println " * FOUND PRIMARY KEY ${hasSinglePkGroup}"
            println " * PRIMARY_KEY SIZE: ${findSingle.count}"
            println " * FIRST MATCH: ${findSingle[0][1]}"
            //println " * PRIMARY_KEY: ${singlePkGroup[1]}"
        }
    }
}

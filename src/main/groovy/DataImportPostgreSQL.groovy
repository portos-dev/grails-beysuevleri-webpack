

import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement
import java.util.regex.Pattern

/**
 * Created by birkan on 14.05.2017.
 */
class DataImportPostgreSQL {

    static void main(String[] args){
//        String database = args ? args[0]: 'BEYSU_ÖNGÖRÜ_2017'
        String database = args ? args[0]: 'TEKN_EMR_ORTAKLIK_2017'
        String url = "jdbc:postgresql://localhost:15432/${database}"
        String user = "beysu"
        String password = "ernesto1907"
//        String password = "asd"
       // String urlWithAuth = "${url}user=${user}&password=${password}"
        Class.forName("org.postgresql.Driver")
        Connection conn

        Pattern regex = Pattern.compile(database +'_dbo.*.sql')

//        Pattern identityInsertRegEx = ~/in table '(.*)' when IDENTITY_INSERT is set to OFF/

        try {
//            conn = DriverManager.getConnection(urlWithAuth)
            conn = DriverManager.getConnection(url, user, password)
            conn.autoCommit = false


//            File rootDir = new File("/ssd/projects/beysuevleri/zirve-data/full-wo-sys/${database}")
//            File rootDir = new File("/ssd/projects/beysuevleri/zirve-data/full-wo-sys/")
//            File rootDir = new File("/Users/birkan/Downloads/full-wo-sys/")
            File rootDir = new File("/ssd/projects/beysuevleri/zirve-data/2017-05-29-1715")
            def files =
                    rootDir.list(new FilenameFilter() {
                        @Override
                        boolean accept(File dir, String name) {
        //                    return (/*dir.isFile() &&*/ name.startsWith("xa"))
        //                    return ( name.indexOf("_sys_") < 0)
                            return regex.matcher(name).find()
        //                    return true
                        }
                    })
           /* files.each { String fileName ->
                String tableName = fileName.replaceAll(/.*dbo.|.sql/, '')
                conn.createStatement().executeUpdate("ALTER TABLE PUBLIC.${tableName} DISABLE TRIGGER ALL;")
            }*/
            files.each {
                String fileName ->

                    def f = new File(rootDir, fileName)
                    f.readLines().each { String line ->
                        print line
                        try {
                            Statement stmt = conn.createStatement()
                            stmt.executeUpdate(line.replace("dbo.", " "))
                        }catch(SQLException se){
                            println se.message
//                            Matcher matcher = identityInsertRegEx.matcher(se.message)
//                            if(matcher.find()){
//                                def tableName = matcher.group(1)
//                                Statement stmt = conn.createStatement()
//                                stmt.executeUpdate("SET IDENTITY_INSERT dbo.${tableName} ON;")
//                                stmt.executeUpdate(line)
//                                stmt.executeUpdate("SET IDENTITY_INSERT dbo.${tableName} OFF;")
//                            }
                        }
                    }

//                    conn.commit() //every file commits individually
            }
            /*files.each { String fileName ->
                String tableName = fileName.replaceAll(/.*dbo.|.sql/, '')
                conn.createStatement().executeUpdate("ALTER TABLE PUBLIC.${tableName} ENABLE TRIGGER ALL;")
            }*/
            conn.commit()
        }catch(Exception e){
            println e.message
            conn?.rollback()
        }finally {
            //conn?.createStatement()?.executeUpdate('SET FOREIGN_KEY_CHECKS=1')
            conn?.close()
        }
    }
}

// Karma configuration
// Generated on Fri Jun 02 2017 00:31:05 GMT+0300 (+03)

var path = require('path');
var webpackConfig = require('./webpack.config');
var entry = './src/main/webapp/index.js';//path.resolve(webpackConfig.context, webpackConfig.entry);
var preprocessors = {};
preprocessors[entry] = ['webpack'];


module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
        entry,
        // './node_modules/angular/angular.js',                 // loads our modules for tests
        // './node_modules/angular-resource/angular-resource.js',                 // loads our modules for tests
        './node_modules/angular-mocks/angular-mocks.js',                 // loads our modules for tests
        // './node_modules/angular-ui-router/release/angular-ui-router.js', // ui-router
        //'./src/main/webapp/app/**/*.js',
        './src/main/webapp/app/**/*.Spec.js',
    ] /*[
        './node_modules/angular/angular.js',                             // angular
        './node_modules/angular-ui-router/release/angular-ui-router.js', // ui-router
        './node_modules/angular-resource/angular-resource.js', // ng-resource
        './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js', // ui-bootstrap
        './node_modules/ui-select/dist/select.js', // ui-select
        './node_modules/angular-xeditable/dist/js/xeditable.js', // angular-xeditable
        './node_modules/angular-mocks/angular-mocks.js',                 // loads our modules for tests
        // './src/main/webapp/modules/!**!/!*.js', // portos modules
        './src/main/webapp/modules/portos/core/portos.core.js',
        './src/main/webapp/app/inputType/configmanager.inputType.js',
        './src/main/webapp/app/input/configmanager.input.js',
        './src/main/webapp/app/input/domain/Input.js',
        './src/main/webapp/app/input/domain/Input.Spec.js'
    ]*/,
     webpack: webpackConfig,


    // list of files to exclude
    exclude: [
        // './src/main/webapp/app/**/index.js',
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    // preprocessors: {
    // },
    preprocessors: preprocessors,


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // reporters: ['progress'],
    reporters: ['spec'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    logLevel: config.LOG_DEBUG,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    // browsers: ['Chrome'],
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

      // plugins:[
      //     require('karma-webpack'),
      //     ('karma-chai'),
      //     ('karma-jasmine'),
      //     // ('karma-mocha'),
      //     ('karma-chrome-launcher')
      // ]
  })
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE stokbf
(
    Tarih SMALLDATETIME,
    Ref INT,
    Arti INT NOT NULL IDENTITY,
    [Aly? Birim Fiyat] FLOAT,
    [Saty? Birim Fiyat] FLOAT,
    Tarihd SMALLDATETIME
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Stokbf implements Serializable{

  /**
   * Tarih SMALLDATETIME
   */
  Date tarih
  /**
   * Ref INT
   */
  Long ref
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * [Aly? Birim Fiyat] FLOAT
   */
  Double aly__Birim_Fiyat
  /**
   * [Saty? Birim Fiyat] FLOAT
   */
  Double saty__Birim_Fiyat
  /**
   * Tarihd SMALLDATETIME
   */
  Date tarihd

  static mapping = {
    table name: 'stokbf'
    //TODO: id name: '?' 
    version false
    tarih column: 'Tarih'
    ref column: 'Ref'
    arti column: 'Arti'
    aly__Birim_Fiyat column: 'Aly? Birim Fiyat'
    saty__Birim_Fiyat column: 'Saty? Birim Fiyat'
    tarihd column: 'Tarihd'
  }
}

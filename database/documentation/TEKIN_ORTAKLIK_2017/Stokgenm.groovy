/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE stokgenm
(
    Stk NVARCHAR(20) PRIMARY KEY NOT NULL,
    Ref INT NOT NULL IDENTITY,
    Sta NVARCHAR(40),
    Stb NVARCHAR(8),
    Stgm FLOAT,
    Stgt FLOAT,
    Stcm FLOAT,
    Stct FLOAT,
    Kar FLOAT,
    Ticari CHAR,
    Eyontemi CHAR
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Stokgenm implements Serializable{

  /**
   * Stk NVARCHAR(20) NOT NULL
   */
  String stk
  /**
   * Ref INT NOT NULL IDENTITY
   */
  Long ref
  /**
   * Sta NVARCHAR(40)
   */
  String sta
  /**
   * Stb NVARCHAR(8)
   */
  String stb
  /**
   * Stgm FLOAT
   */
  Double stgm
  /**
   * Stgt FLOAT
   */
  Double stgt
  /**
   * Stcm FLOAT
   */
  Double stcm
  /**
   * Stct FLOAT
   */
  Double stct
  /**
   * Kar FLOAT
   */
  Double kar
  /**
   * Ticari CHAR
   */
  String ticari
  /**
   * Eyontemi CHAR
   */
  String eyontemi

  static mapping = {
    table name: 'stokgenm'
    //TODO: id name: '?' 
    version false
    stk column: 'Stk'
    ref column: 'Ref'
    sta column: 'Sta'
    stb column: 'Stb'
    stgm column: 'Stgm'
    stgt column: 'Stgt'
    stcm column: 'Stcm'
    stct column: 'Stct'
    kar column: 'Kar'
    ticari column: 'Ticari'
    eyontemi column: 'Eyontemi'
  }
}

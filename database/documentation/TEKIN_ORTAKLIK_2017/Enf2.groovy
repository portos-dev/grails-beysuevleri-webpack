/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE enf2
(
    Ayi SMALLINT,
    Kod NVARCHAR(20),
    Yil SMALLINT,
    Ay SMALLINT,
    Sirano INT NOT NULL IDENTITY,
    Gsno SMALLINT,
    [Stok Düzeltme Yöntemi] NVARCHAR(1),
    [Düzeltmeye Esas Değer] FLOAT,
    [Düzeltilmiş Değer] FLOAT,
    [Enf#Düzeltme Hesap Kodu] NVARCHAR(20),
    [Düzeltme Katsayısı] FLOAT,
    [Elimine Edilecek Tutar] FLOAT,
    [Reel Olmayan Fin#Maliyeti] NVARCHAR(1),
    Yöntem NVARCHAR(1),
    [Borcun Kullanıldığı Yıl] SMALLINT,
    [Borcun Kullanıldığı Ay] SMALLINT,
    [Borcun Kapatılıdığı Yıl] SMALLINT,
    [Borcun Kapatılıdığı Ay] SMALLINT,
    [Finansman Maliyeti Tutarı] FLOAT,
    [Reel Olmayan Fin#Mali#] FLOAT,
    [Tefe Artış Oranı] FLOAT,
    [Ort#Tic#Kredi Faiz Oranı] FLOAT,
    [TEFE/OTKFO] FLOAT,
    [Amortismana Tabimi] NVARCHAR(1),
    [Birikmiş Amort#Hesap Kodu] NVARCHAR(20),
    [Birikmiş Amortisman Tutar] FLOAT,
    [Artış Oranı] FLOAT,
    [Düzeltilmiş Amortisman De] FLOAT,
    [Önc#Dön#Düzeltilmiş Değer] FLOAT,
    [Taşıma Katsayısı] FLOAT,
    [Yen#Değer#Demirbaş Değeri] FLOAT,
    [Düzeltilecek Değer] FLOAT,
    Önc#Dön#Düz#Amort#Değeri FLOAT,
    [ROFM İçin Oran] FLOAT,
    [Borcun Kul#Dön#Ait TEFE] FLOAT,
    [Borcun Kap#Dön#Ait TEFE] FLOAT,
    [Mali Tab#Ait Ol#Aya Ait T] FLOAT,
    [Tk/Dk] NVARCHAR(1),
    [Düzelt#Önc#Dönem Yılı] SMALLINT,
    [Düzelt#Önc#Dönem Ayı] SMALLINT,
    Enf#Düz#Heskod NVARCHAR(20),
    Birahk NVARCHAR(20)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Enf2 implements Serializable{

  /**
   * Ayi SMALLINT
   */
  Long ayi
  /**
   * Kod NVARCHAR(20)
   */
  String kod
  /**
   * Yil SMALLINT
   */
  Long yil
  /**
   * Ay SMALLINT
   */
  Long ay
  /**
   * Sirano INT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Gsno SMALLINT
   */
  Long gsno
  /**
   * [Stok Düzeltme Yöntemi] NVARCHAR(1)
   */
  String stok_Düzeltme_Yöntemi
  /**
   * [Düzeltmeye Esas Değer] FLOAT
   */
  Double düzeltmeye_Esas_Değer
  /**
   * [Düzeltilmiş Değer] FLOAT
   */
  Double düzeltilmiş_Değer
  /**
   * [Enf#Düzeltme Hesap Kodu] NVARCHAR(20)
   */
  String enf_Düzeltme_Hesap_Kodu
  /**
   * [Düzeltme Katsayısı] FLOAT
   */
  Double düzeltme_Katsayısı
  /**
   * [Elimine Edilecek Tutar] FLOAT
   */
  Double elimine_Edilecek_Tutar
  /**
   * [Reel Olmayan Fin#Maliyeti] NVARCHAR(1)
   */
  String reel_Olmayan_Fin_Maliyeti
  /**
   * Yöntem NVARCHAR(1)
   */
  String yöntem
  /**
   * [Borcun Kullanıldığı Yıl] SMALLINT
   */
  Long borcun_Kullanıldığı_Yıl
  /**
   * [Borcun Kullanıldığı Ay] SMALLINT
   */
  Long borcun_Kullanıldığı_Ay
  /**
   * [Borcun Kapatılıdığı Yıl] SMALLINT
   */
  Long borcun_Kapatılıdığı_Yıl
  /**
   * [Borcun Kapatılıdığı Ay] SMALLINT
   */
  Long borcun_Kapatılıdığı_Ay
  /**
   * [Finansman Maliyeti Tutarı] FLOAT
   */
  Double finansman_Maliyeti_Tutarı
  /**
   * [Reel Olmayan Fin#Mali#] FLOAT
   */
  Double reel_Olmayan_Fin_Mali_
  /**
   * [Tefe Artış Oranı] FLOAT
   */
  Double tefe_Artış_Oranı
  /**
   * [Ort#Tic#Kredi Faiz Oranı] FLOAT
   */
  Double ort_Tic_Kredi_Faiz_Oranı
  /**
   * [TEFE/OTKFO] FLOAT
   */
  Double tefe_Otkfo
  /**
   * [Amortismana Tabimi] NVARCHAR(1)
   */
  String amortismana_Tabimi
  /**
   * [Birikmiş Amort#Hesap Kodu] NVARCHAR(20)
   */
  String birikmiş_Amort_Hesap_Kodu
  /**
   * [Birikmiş Amortisman Tutar] FLOAT
   */
  Double birikmiş_Amortisman_Tutar
  /**
   * [Artış Oranı] FLOAT
   */
  Double artış_Oranı
  /**
   * [Düzeltilmiş Amortisman De] FLOAT
   */
  Double düzeltilmiş_Amortisman_De
  /**
   * [Önc#Dön#Düzeltilmiş Değer] FLOAT
   */
  Double önc_Dön_Düzeltilmiş_Değer
  /**
   * [Taşıma Katsayısı] FLOAT
   */
  Double taşıma_Katsayısı
  /**
   * [Yen#Değer#Demirbaş Değeri] FLOAT
   */
  Double yen_Değer_Demirbaş_Değeri
  /**
   * [Düzeltilecek Değer] FLOAT
   */
  Double düzeltilecek_Değer
  /**
   * Önc#Dön#Düz#Amort#Değeri FLOAT
   */
  Double önc_Dön_Düz_Amort_Değeri
  /**
   * [ROFM İçin Oran] FLOAT
   */
  Double rofm_İçin_Oran
  /**
   * [Borcun Kul#Dön#Ait TEFE] FLOAT
   */
  Double borcun_Kul_Dön_Ait_Tefe
  /**
   * [Borcun Kap#Dön#Ait TEFE] FLOAT
   */
  Double borcun_Kap_Dön_Ait_Tefe
  /**
   * [Mali Tab#Ait Ol#Aya Ait T] FLOAT
   */
  Double mali_Tab_Ait_Ol_Aya_Ait_T
  /**
   * [Tk/Dk] NVARCHAR(1)
   */
  String tk_Dk
  /**
   * [Düzelt#Önc#Dönem Yılı] SMALLINT
   */
  Long düzelt_Önc_Dönem_Yılı
  /**
   * [Düzelt#Önc#Dönem Ayı] SMALLINT
   */
  Long düzelt_Önc_Dönem_Ayı
  /**
   * Enf#Düz#Heskod NVARCHAR(20)
   */
  String enf_Düz_Heskod
  /**
   * Birahk NVARCHAR(20)
   */
  String birahk

  static mapping = {
    table name: 'enf2'
    //TODO: id name: '?' 
    version false
    ayi column: 'Ayi'
    kod column: 'Kod'
    yil column: 'Yil'
    ay column: 'Ay'
    sirano column: 'Sirano'
    gsno column: 'Gsno'
    stok_Düzeltme_Yöntemi column: 'Stok Düzeltme Yöntemi'
    düzeltmeye_Esas_Değer column: 'Düzeltmeye Esas Değer'
    düzeltilmiş_Değer column: 'Düzeltilmiş Değer'
    enf_Düzeltme_Hesap_Kodu column: 'Enf#Düzeltme Hesap Kodu'
    düzeltme_Katsayısı column: 'Düzeltme Katsayısı'
    elimine_Edilecek_Tutar column: 'Elimine Edilecek Tutar'
    reel_Olmayan_Fin_Maliyeti column: 'Reel Olmayan Fin#Maliyeti'
    yöntem column: 'Yöntem'
    borcun_Kullanıldığı_Yıl column: 'Borcun Kullanıldığı Yıl'
    borcun_Kullanıldığı_Ay column: 'Borcun Kullanıldığı Ay'
    borcun_Kapatılıdığı_Yıl column: 'Borcun Kapatılıdığı Yıl'
    borcun_Kapatılıdığı_Ay column: 'Borcun Kapatılıdığı Ay'
    finansman_Maliyeti_Tutarı column: 'Finansman Maliyeti Tutarı'
    reel_Olmayan_Fin_Mali_ column: 'Reel Olmayan Fin#Mali#'
    tefe_Artış_Oranı column: 'Tefe Artış Oranı'
    ort_Tic_Kredi_Faiz_Oranı column: 'Ort#Tic#Kredi Faiz Oranı'
    tefe_Otkfo column: 'TEFE/OTKFO'
    amortismana_Tabimi column: 'Amortismana Tabimi'
    birikmiş_Amort_Hesap_Kodu column: 'Birikmiş Amort#Hesap Kodu'
    birikmiş_Amortisman_Tutar column: 'Birikmiş Amortisman Tutar'
    artış_Oranı column: 'Artış Oranı'
    düzeltilmiş_Amortisman_De column: 'Düzeltilmiş Amortisman De'
    önc_Dön_Düzeltilmiş_Değer column: 'Önc#Dön#Düzeltilmiş Değer'
    taşıma_Katsayısı column: 'Taşıma Katsayısı'
    yen_Değer_Demirbaş_Değeri column: 'Yen#Değer#Demirbaş Değeri'
    düzeltilecek_Değer column: 'Düzeltilecek Değer'
    önc_Dön_Düz_Amort_Değeri column: 'Önc#Dön#Düz#Amort#Değeri'
    rofm_İçin_Oran column: 'ROFM İçin Oran'
    borcun_Kul_Dön_Ait_Tefe column: 'Borcun Kul#Dön#Ait TEFE'
    borcun_Kap_Dön_Ait_Tefe column: 'Borcun Kap#Dön#Ait TEFE'
    mali_Tab_Ait_Ol_Aya_Ait_T column: 'Mali Tab#Ait Ol#Aya Ait T'
    tk_Dk column: 'Tk/Dk'
    düzelt_Önc_Dönem_Yılı column: 'Düzelt#Önc#Dönem Yılı'
    düzelt_Önc_Dönem_Ayı column: 'Düzelt#Önc#Dönem Ayı'
    enf_Düz_Heskod column: 'Enf#Düz#Heskod'
    birahk column: 'Birahk'
  }
}

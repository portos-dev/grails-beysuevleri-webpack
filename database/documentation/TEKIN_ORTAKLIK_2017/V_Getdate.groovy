/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE v_GetDate
(
    GetDate DATETIME NOT NULL
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class V_Getdate implements Serializable{

  /**
   * GetDate DATETIME NOT NULL
   */
  Date getdate

  static mapping = {
    table name: 'v_GetDate'
    //TODO: id name: '?' 
    version false
    getdate column: 'GetDate'
  }
}

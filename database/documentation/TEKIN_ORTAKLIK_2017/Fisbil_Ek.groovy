/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE fisbil_ek
(
    Arti INT NOT NULL IDENTITY,
    fisbil_pid NVARCHAR(18) NOT NULL,
    Ad NVARCHAR(20),
    Deger NVARCHAR(200)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Fisbil_Ek implements Serializable{

  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * fisbil_pid NVARCHAR(18) NOT NULL
   */
  String fisbil_Pid
  /**
   * Ad NVARCHAR(20)
   */
  String ad
  /**
   * Deger NVARCHAR(200)
   */
  String deger

  static mapping = {
    table name: 'fisbil_ek'
    //TODO: id name: '?' 
    version false
    arti column: 'Arti'
    fisbil_Pid column: 'fisbil_pid'
    ad column: 'Ad'
    deger column: 'Deger'
  }
}

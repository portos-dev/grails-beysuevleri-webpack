/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_Ortulu_Sermaye
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Aciklama NVARCHAR(110),
    Miktar FLOAT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Ortulu_Sermaye implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Aciklama NVARCHAR(110)
   */
  String aciklama
  /**
   * Miktar FLOAT
   */
  Double miktar

  static mapping = {
    table name: 'Transfer_Ortulu_Sermaye'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    aciklama column: 'Aciklama'
    miktar column: 'Miktar'
  }
}

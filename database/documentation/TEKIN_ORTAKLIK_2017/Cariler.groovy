/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE cariler
(
    Arti INT NOT NULL IDENTITY,
    Cari_pid NVARCHAR(80),
    Tckimlikno NVARCHAR(11),
    Vergikimlikno NVARCHAR(11),
    Ulkekodu CHAR(10),
    Cariadi NVARCHAR(100),
    Yetkili NVARCHAR(50),
    Tel NVARCHAR(20),
    Vergidairesi NVARCHAR(40)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Cariler implements Serializable{

  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Cari_pid NVARCHAR(80)
   */
  String cari_Pid
  /**
   * Tckimlikno NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Vergikimlikno NVARCHAR(11)
   */
  String vergikimlikno
  /**
   * Ulkekodu CHAR(10)
   */
  String ulkekodu
  /**
   * Cariadi NVARCHAR(100)
   */
  String cariadi
  /**
   * Yetkili NVARCHAR(50)
   */
  String yetkili
  /**
   * Tel NVARCHAR(20)
   */
  String tel
  /**
   * Vergidairesi NVARCHAR(40)
   */
  String vergidairesi

  static mapping = {
    table name: 'cariler'
    //TODO: id name: '?' 
    version false
    arti column: 'Arti'
    cari_Pid column: 'Cari_pid'
    tckimlikno column: 'Tckimlikno'
    vergikimlikno column: 'Vergikimlikno'
    ulkekodu column: 'Ulkekodu'
    cariadi column: 'Cariadi'
    yetkili column: 'Yetkili'
    tel column: 'Tel'
    vergidairesi column: 'Vergidairesi'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE eDefter_Kayit_Tablosu
(
    Sirano INT NOT NULL IDENTITY,
    Defter_Turu NVARCHAR(20),
    Kontrol_Numarasi NVARCHAR(20),
    Dosya_Sira_No NVARCHAR(2),
    Donemi NVARCHAR(20),
    Defter_Baslangic_Tarihi DATETIME,
    Defter_Bitis_Tarihi DATETIME,
    Defter_Aciklamasi NVARCHAR(150),
    Olusturan_Kisi NVARCHAR(100),
    Olusturma_Tarihi DATETIME,
    Defter_Durumu NVARCHAR(30),
    Donem_Ayi SMALLINT,
    Kayit_Eden_Kullanici NVARCHAR(50),
    Kayit_Tarihi DATETIME,
    Silindimi SMALLINT,
    Silen_Kullanici NVARCHAR(50),
    Silinme_Tarihi DATETIME,
    Xml_Dosya_Yeri NVARCHAR(250),
    Berat_Dosya_Yeri NVARCHAR(250),
    Paket_Dosya_Yeri NVARCHAR(250),
    SubeliDefter NVARCHAR(5),
    SubeAdi NVARCHAR(250),
    SubeNo NVARCHAR(4),
    BasYevno INT,
    BitYevno INT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Edefter_Kayit_Tablosu implements Serializable{

  /**
   * Sirano INT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Defter_Turu NVARCHAR(20)
   */
  String defter_Turu
  /**
   * Kontrol_Numarasi NVARCHAR(20)
   */
  String kontrol_Numarasi
  /**
   * Dosya_Sira_No NVARCHAR(2)
   */
  String dosya_Sira_No
  /**
   * Donemi NVARCHAR(20)
   */
  String donemi
  /**
   * Defter_Baslangic_Tarihi DATETIME
   */
  Date defter_Baslangic_Tarihi
  /**
   * Defter_Bitis_Tarihi DATETIME
   */
  Date defter_Bitis_Tarihi
  /**
   * Defter_Aciklamasi NVARCHAR(150)
   */
  String defter_Aciklamasi
  /**
   * Olusturan_Kisi NVARCHAR(100)
   */
  String olusturan_Kisi
  /**
   * Olusturma_Tarihi DATETIME
   */
  Date olusturma_Tarihi
  /**
   * Defter_Durumu NVARCHAR(30)
   */
  String defter_Durumu
  /**
   * Donem_Ayi SMALLINT
   */
  Long donem_Ayi
  /**
   * Kayit_Eden_Kullanici NVARCHAR(50)
   */
  String kayit_Eden_Kullanici
  /**
   * Kayit_Tarihi DATETIME
   */
  Date kayit_Tarihi
  /**
   * Silindimi SMALLINT
   */
  Long silindimi
  /**
   * Silen_Kullanici NVARCHAR(50)
   */
  String silen_Kullanici
  /**
   * Silinme_Tarihi DATETIME
   */
  Date silinme_Tarihi
  /**
   * Xml_Dosya_Yeri NVARCHAR(250)
   */
  String xml_Dosya_Yeri
  /**
   * Berat_Dosya_Yeri NVARCHAR(250)
   */
  String berat_Dosya_Yeri
  /**
   * Paket_Dosya_Yeri NVARCHAR(250)
   */
  String paket_Dosya_Yeri
  /**
   * SubeliDefter NVARCHAR(5)
   */
  String subelidefter
  /**
   * SubeAdi NVARCHAR(250)
   */
  String subeadi
  /**
   * SubeNo NVARCHAR(4)
   */
  String subeno
  /**
   * BasYevno INT
   */
  Long basyevno
  /**
   * BitYevno INT
   */
  Long bityevno

  static mapping = {
    table name: 'eDefter_Kayit_Tablosu'
    //TODO: id name: '?' 
    version false
    sirano column: 'Sirano'
    defter_Turu column: 'Defter_Turu'
    kontrol_Numarasi column: 'Kontrol_Numarasi'
    dosya_Sira_No column: 'Dosya_Sira_No'
    donemi column: 'Donemi'
    defter_Baslangic_Tarihi column: 'Defter_Baslangic_Tarihi'
    defter_Bitis_Tarihi column: 'Defter_Bitis_Tarihi'
    defter_Aciklamasi column: 'Defter_Aciklamasi'
    olusturan_Kisi column: 'Olusturan_Kisi'
    olusturma_Tarihi column: 'Olusturma_Tarihi'
    defter_Durumu column: 'Defter_Durumu'
    donem_Ayi column: 'Donem_Ayi'
    kayit_Eden_Kullanici column: 'Kayit_Eden_Kullanici'
    kayit_Tarihi column: 'Kayit_Tarihi'
    silindimi column: 'Silindimi'
    silen_Kullanici column: 'Silen_Kullanici'
    silinme_Tarihi column: 'Silinme_Tarihi'
    xml_Dosya_Yeri column: 'Xml_Dosya_Yeri'
    berat_Dosya_Yeri column: 'Berat_Dosya_Yeri'
    paket_Dosya_Yeri column: 'Paket_Dosya_Yeri'
    subelidefter column: 'SubeliDefter'
    subeadi column: 'SubeAdi'
    subeno column: 'SubeNo'
    basyevno column: 'BasYevno'
    bityevno column: 'BitYevno'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Stopajlar
(
    Soyadi NVARCHAR(50),
    Adi NVARCHAR(50),
    Vkn NVARCHAR(11),
    Tk NVARCHAR(11),
    Gst FLOAT,
    Kt FLOAT,
    Adres NVARCHAR(250),
    Tarbas SMALLDATETIME,
    Tarbit SMALLDATETIME,
    Sube SMALLINT,
    Sirano SMALLINT NOT NULL IDENTITY,
    Tckimlikno NVARCHAR(11),
    Ehust CHAR,
    Ehalt CHAR,
    Evrakno NVARCHAR(30),
    KdvTevkifatKodu SMALLINT,
    Belgeturu SMALLINT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Stopajlar implements Serializable{

  /**
   * Soyadi NVARCHAR(50)
   */
  String soyadi
  /**
   * Adi NVARCHAR(50)
   */
  String adi
  /**
   * Vkn NVARCHAR(11)
   */
  String vkn
  /**
   * Tk NVARCHAR(11)
   */
  String tk
  /**
   * Gst FLOAT
   */
  Double gst
  /**
   * Kt FLOAT
   */
  Double kt
  /**
   * Adres NVARCHAR(250)
   */
  String adres
  /**
   * Tarbas SMALLDATETIME
   */
  Date tarbas
  /**
   * Tarbit SMALLDATETIME
   */
  Date tarbit
  /**
   * Sube SMALLINT
   */
  Long sube
  /**
   * Sirano SMALLINT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Tckimlikno NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Ehust CHAR
   */
  String ehust
  /**
   * Ehalt CHAR
   */
  String ehalt
  /**
   * Evrakno NVARCHAR(30)
   */
  String evrakno
  /**
   * KdvTevkifatKodu SMALLINT
   */
  Long kdvtevkifatkodu
  /**
   * Belgeturu SMALLINT
   */
  Long belgeturu

  static mapping = {
    table name: 'Stopajlar'
    //TODO: id name: '?' 
    version false
    soyadi column: 'Soyadi'
    adi column: 'Adi'
    vkn column: 'Vkn'
    tk column: 'Tk'
    gst column: 'Gst'
    kt column: 'Kt'
    adres column: 'Adres'
    tarbas column: 'Tarbas'
    tarbit column: 'Tarbit'
    sube column: 'Sube'
    sirano column: 'Sirano'
    tckimlikno column: 'Tckimlikno'
    ehust column: 'Ehust'
    ehalt column: 'Ehalt'
    evrakno column: 'Evrakno'
    kdvtevkifatkodu column: 'KdvTevkifatKodu'
    belgeturu column: 'Belgeturu'
  }
}

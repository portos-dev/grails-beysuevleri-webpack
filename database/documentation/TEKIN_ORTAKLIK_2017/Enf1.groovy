/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE enf1
(
    Ay SMALLINT,
    Kod NVARCHAR(20),
    Acik NVARCHAR(50),
    Detay NVARCHAR(1),
    Nsd NVARCHAR(1),
    Parmi NVARCHAR(1),
    Borc FLOAT,
    Alacak FLOAT,
    S1 FLOAT,
    S2 FLOAT,
    S3 FLOAT,
    S4 FLOAT,
    S5 FLOAT,
    S6 FLOAT,
    S7 FLOAT,
    S8 FLOAT,
    S9 FLOAT,
    S10 FLOAT,
    S11 FLOAT,
    S12 FLOAT,
    Bh NVARCHAR(1),
    Dss FLOAT,
    Bat NVARCHAR(1),
    Hek NVARCHAR(20),
    Arti INT NOT NULL IDENTITY
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Enf1 implements Serializable{

  /**
   * Ay SMALLINT
   */
  Long ay
  /**
   * Kod NVARCHAR(20)
   */
  String kod
  /**
   * Acik NVARCHAR(50)
   */
  String acik
  /**
   * Detay NVARCHAR(1)
   */
  String detay
  /**
   * Nsd NVARCHAR(1)
   */
  String nsd
  /**
   * Parmi NVARCHAR(1)
   */
  String parmi
  /**
   * Borc FLOAT
   */
  Double borc
  /**
   * Alacak FLOAT
   */
  Double alacak
  /**
   * S1 FLOAT
   */
  Double s1
  /**
   * S2 FLOAT
   */
  Double s2
  /**
   * S3 FLOAT
   */
  Double s3
  /**
   * S4 FLOAT
   */
  Double s4
  /**
   * S5 FLOAT
   */
  Double s5
  /**
   * S6 FLOAT
   */
  Double s6
  /**
   * S7 FLOAT
   */
  Double s7
  /**
   * S8 FLOAT
   */
  Double s8
  /**
   * S9 FLOAT
   */
  Double s9
  /**
   * S10 FLOAT
   */
  Double s10
  /**
   * S11 FLOAT
   */
  Double s11
  /**
   * S12 FLOAT
   */
  Double s12
  /**
   * Bh NVARCHAR(1)
   */
  String bh
  /**
   * Dss FLOAT
   */
  Double dss
  /**
   * Bat NVARCHAR(1)
   */
  String bat
  /**
   * Hek NVARCHAR(20)
   */
  String hek
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti

  static mapping = {
    table name: 'enf1'
    //TODO: id name: '?' 
    version false
    ay column: 'Ay'
    kod column: 'Kod'
    acik column: 'Acik'
    detay column: 'Detay'
    nsd column: 'Nsd'
    parmi column: 'Parmi'
    borc column: 'Borc'
    alacak column: 'Alacak'
    s1 column: 'S1'
    s2 column: 'S2'
    s3 column: 'S3'
    s4 column: 'S4'
    s5 column: 'S5'
    s6 column: 'S6'
    s7 column: 'S7'
    s8 column: 'S8'
    s9 column: 'S9'
    s10 column: 'S10'
    s11 column: 'S11'
    s12 column: 'S12'
    bh column: 'Bh'
    dss column: 'Dss'
    bat column: 'Bat'
    hek column: 'Hek'
    arti column: 'Arti'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE bformua
(
    Sirano INT NOT NULL IDENTITY,
    Vergino NVARCHAR(11),
    Vergidairesi NVARCHAR(30),
    Unvan NVARCHAR(60),
    Ad NVARCHAR(50),
    Donem NVARCHAR(2),
    Sno NVARCHAR(2),
    Mukunvan NVARCHAR(60),
    Ukodu NVARCHAR(3),
    Mukvergino NVARCHAR(11),
    Bsayisi NVARCHAR(4),
    Bedel FLOAT,
    Muhvergino NVARCHAR(11),
    Muhtcno NVARCHAR(11),
    Muhsoyadi NVARCHAR(50),
    Muhadi NVARCHAR(50),
    Muhtur NVARCHAR(5),
    Tcno NVARCHAR(11),
    Ayindex SMALLINT,
    Digertoplam FLOAT,
    Sirno NVARCHAR(5),
    Sayfasayisi SMALLINT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Bformua implements Serializable{

  /**
   * Sirano INT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Vergino NVARCHAR(11)
   */
  String vergino
  /**
   * Vergidairesi NVARCHAR(30)
   */
  String vergidairesi
  /**
   * Unvan NVARCHAR(60)
   */
  String unvan
  /**
   * Ad NVARCHAR(50)
   */
  String ad
  /**
   * Donem NVARCHAR(2)
   */
  String donem
  /**
   * Sno NVARCHAR(2)
   */
  String sno
  /**
   * Mukunvan NVARCHAR(60)
   */
  String mukunvan
  /**
   * Ukodu NVARCHAR(3)
   */
  String ukodu
  /**
   * Mukvergino NVARCHAR(11)
   */
  String mukvergino
  /**
   * Bsayisi NVARCHAR(4)
   */
  String bsayisi
  /**
   * Bedel FLOAT
   */
  Double bedel
  /**
   * Muhvergino NVARCHAR(11)
   */
  String muhvergino
  /**
   * Muhtcno NVARCHAR(11)
   */
  String muhtcno
  /**
   * Muhsoyadi NVARCHAR(50)
   */
  String muhsoyadi
  /**
   * Muhadi NVARCHAR(50)
   */
  String muhadi
  /**
   * Muhtur NVARCHAR(5)
   */
  String muhtur
  /**
   * Tcno NVARCHAR(11)
   */
  String tcno
  /**
   * Ayindex SMALLINT
   */
  Long ayindex
  /**
   * Digertoplam FLOAT
   */
  Double digertoplam
  /**
   * Sirno NVARCHAR(5)
   */
  String sirno
  /**
   * Sayfasayisi SMALLINT
   */
  Long sayfasayisi

  static mapping = {
    table name: 'bformua'
    //TODO: id name: '?' 
    version false
    sirano column: 'Sirano'
    vergino column: 'Vergino'
    vergidairesi column: 'Vergidairesi'
    unvan column: 'Unvan'
    ad column: 'Ad'
    donem column: 'Donem'
    sno column: 'Sno'
    mukunvan column: 'Mukunvan'
    ukodu column: 'Ukodu'
    mukvergino column: 'Mukvergino'
    bsayisi column: 'Bsayisi'
    bedel column: 'Bedel'
    muhvergino column: 'Muhvergino'
    muhtcno column: 'Muhtcno'
    muhsoyadi column: 'Muhsoyadi'
    muhadi column: 'Muhadi'
    muhtur column: 'Muhtur'
    tcno column: 'Tcno'
    ayindex column: 'Ayindex'
    digertoplam column: 'Digertoplam'
    sirno column: 'Sirno'
    sayfasayisi column: 'Sayfasayisi'
  }
}

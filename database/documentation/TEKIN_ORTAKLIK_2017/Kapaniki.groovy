/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE kapaniki
(
    Khk NVARCHAR(17),
    Yhk NVARCHAR(17),
    Tip NVARCHAR(1)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Kapaniki implements Serializable{

  /**
   * Khk NVARCHAR(17)
   */
  String khk
  /**
   * Yhk NVARCHAR(17)
   */
  String yhk
  /**
   * Tip NVARCHAR(1)
   */
  String tip

  static mapping = {
    table name: 'kapaniki'
    //TODO: id name: '?' 
    version false
    khk column: 'Khk'
    yhk column: 'Yhk'
    tip column: 'Tip'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE stoktar
(
    Stt SMALLDATETIME,
    Dbi NVARCHAR(2),
    Sno INT NOT NULL IDENTITY,
    Ref INT,
    Sta NVARCHAR(75),
    Stgm FLOAT,
    Stgt FLOAT,
    Stcm FLOAT,
    Stct FLOAT,
    Refno INT,
    Tip NVARCHAR(1),
    Tip2 NVARCHAR(2),
    Tip3 NVARCHAR(1),
    Evrakno NVARCHAR(16),
    Iademi CHAR
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Stoktar implements Serializable{

  /**
   * Stt SMALLDATETIME
   */
  Date stt
  /**
   * Dbi NVARCHAR(2)
   */
  String dbi
  /**
   * Sno INT NOT NULL IDENTITY
   */
  Long sno
  /**
   * Ref INT
   */
  Long ref
  /**
   * Sta NVARCHAR(75)
   */
  String sta
  /**
   * Stgm FLOAT
   */
  Double stgm
  /**
   * Stgt FLOAT
   */
  Double stgt
  /**
   * Stcm FLOAT
   */
  Double stcm
  /**
   * Stct FLOAT
   */
  Double stct
  /**
   * Refno INT
   */
  Long refno
  /**
   * Tip NVARCHAR(1)
   */
  String tip
  /**
   * Tip2 NVARCHAR(2)
   */
  String tip2
  /**
   * Tip3 NVARCHAR(1)
   */
  String tip3
  /**
   * Evrakno NVARCHAR(16)
   */
  String evrakno
  /**
   * Iademi CHAR
   */
  String iademi

  static mapping = {
    table name: 'stoktar'
    //TODO: id name: '?' 
    version false
    stt column: 'Stt'
    dbi column: 'Dbi'
    sno column: 'Sno'
    ref column: 'Ref'
    sta column: 'Sta'
    stgm column: 'Stgm'
    stgt column: 'Stgt'
    stcm column: 'Stcm'
    stct column: 'Stct'
    refno column: 'Refno'
    tip column: 'Tip'
    tip2 column: 'Tip2'
    tip3 column: 'Tip3'
    evrakno column: 'Evrakno'
    iademi column: 'Iademi'
  }
}

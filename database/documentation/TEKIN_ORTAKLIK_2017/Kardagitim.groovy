/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE KarDagitim
(
    Aciklama NVARCHAR(100),
    OdTutar FLOAT,
    CdTutar FLOAT,
    Kod SMALLINT,
    isaret CHAR
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Kardagitim implements Serializable{

  /**
   * Aciklama NVARCHAR(100)
   */
  String aciklama
  /**
   * OdTutar FLOAT
   */
  Double odtutar
  /**
   * CdTutar FLOAT
   */
  Double cdtutar
  /**
   * Kod SMALLINT
   */
  Long kod
  /**
   * isaret CHAR
   */
  String isaret

  static mapping = {
    table name: 'KarDagitim'
    //TODO: id name: '?' 
    version false
    aciklama column: 'Aciklama'
    odtutar column: 'OdTutar'
    cdtutar column: 'CdTutar'
    kod column: 'Kod'
    isaret column: 'isaret'
  }
}

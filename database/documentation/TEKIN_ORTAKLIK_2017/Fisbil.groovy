/***
 * FOUND PRIMARY KEY true
 * PRIMARY_KEY SIZE: 1
 * FIRST MATCH: Refno
 * 
 * 
 *CREATE TABLE fisbil
(
    Fistar SMALLDATETIME,
    Fistur NVARCHAR(1),
    Fisno NVARCHAR(9),
    Yevno INT,
    Refno INT PRIMARY KEY NOT NULL IDENTITY,
    Sube NVARCHAR(2),
    Btop FLOAT,
    Atop FLOAT,
    Nereden NVARCHAR(1),
    Ekaciklama NVARCHAR(500),
    Ozkod NVARCHAR(10),
    Islemtipi SMALLINT,
    Eynm INT,
    Eynmk SMALLINT,
    Cmiorbmi SMALLINT,
    Masrafmerkezi NVARCHAR(10),
    Onaylimi NVARCHAR(1),
    Fisekilit NVARCHAR(1),
    Kayittarihi DATETIME,
    Kontrolno BIGINT,
    Fissilmekodu SMALLINT,
    fisbil_pid NVARCHAR(18),
    fis_guid UNIQUEIDENTIFIER
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Fisbil implements Serializable{

  /**
   * Fistar SMALLDATETIME
   */
  Date fistar
  /**
   * Fistur NVARCHAR(1)
   */
  String fistur
  /**
   * Fisno NVARCHAR(9)
   */
  String fisno
  /**
   * Yevno INT
   */
  Long yevno
  /**
   * Refno INT NOT NULL IDENTITY
   */
  Long refno
  /**
   * Sube NVARCHAR(2)
   */
  String sube
  /**
   * Btop FLOAT
   */
  Double btop
  /**
   * Atop FLOAT
   */
  Double atop
  /**
   * Nereden NVARCHAR(1)
   */
  String nereden
  /**
   * Ekaciklama NVARCHAR(500)
   */
  String ekaciklama
  /**
   * Ozkod NVARCHAR(10)
   */
  String ozkod
  /**
   * Islemtipi SMALLINT
   */
  Long islemtipi
  /**
   * Eynm INT
   */
  Long eynm
  /**
   * Eynmk SMALLINT
   */
  Long eynmk
  /**
   * Cmiorbmi SMALLINT
   */
  Long cmiorbmi
  /**
   * Masrafmerkezi NVARCHAR(10)
   */
  String masrafmerkezi
  /**
   * Onaylimi NVARCHAR(1)
   */
  String onaylimi
  /**
   * Fisekilit NVARCHAR(1)
   */
  String fisekilit
  /**
   * Kayittarihi DATETIME
   */
  Date kayittarihi
  /**
   * Kontrolno BIGINT
   */
  Long kontrolno
  /**
   * Fissilmekodu SMALLINT
   */
  Long fissilmekodu
  /**
   * fisbil_pid NVARCHAR(18)
   */
  String fisbil_Pid
  /**
   * fis_guid UNIQUEIDENTIFIER
   */
  String fis_Guid

  static mapping = {
    table name: 'fisbil'
    //TODO: id name: '?' 
    id name: 'refno'
    version false
    fistar column: 'Fistar'
    fistur column: 'Fistur'
    fisno column: 'Fisno'
    yevno column: 'Yevno'
    refno column: 'Refno'
    sube column: 'Sube'
    btop column: 'Btop'
    atop column: 'Atop'
    nereden column: 'Nereden'
    ekaciklama column: 'Ekaciklama'
    ozkod column: 'Ozkod'
    islemtipi column: 'Islemtipi'
    eynm column: 'Eynm'
    eynmk column: 'Eynmk'
    cmiorbmi column: 'Cmiorbmi'
    masrafmerkezi column: 'Masrafmerkezi'
    onaylimi column: 'Onaylimi'
    fisekilit column: 'Fisekilit'
    kayittarihi column: 'Kayittarihi'
    kontrolno column: 'Kontrolno'
    fissilmekodu column: 'Fissilmekodu'
    fisbil_Pid column: 'fisbil_pid'
    fis_Guid column: 'fis_guid'
  }
}

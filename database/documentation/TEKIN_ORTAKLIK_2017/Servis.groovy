/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Servis
(
    Tarih SMALLDATETIME,
    Versiyon INT,
    ServisTarihi SMALLDATETIME
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Servis implements Serializable{

  /**
   * Tarih SMALLDATETIME
   */
  Date tarih
  /**
   * Versiyon INT
   */
  Long versiyon
  /**
   * ServisTarihi SMALLDATETIME
   */
  Date servistarihi

  static mapping = {
    table name: 'Servis'
    //TODO: id name: '?' 
    version false
    tarih column: 'Tarih'
    versiyon column: 'Versiyon'
    servistarihi column: 'ServisTarihi'
  }
}

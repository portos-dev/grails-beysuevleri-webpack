/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Onay
(
    Gelir_onay SMALLDATETIME,
    Gider_onay SMALLDATETIME,
    Gmuh_onay SMALLDATETIME
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Onay implements Serializable{

  /**
   * Gelir_onay SMALLDATETIME
   */
  Date gelir_Onay
  /**
   * Gider_onay SMALLDATETIME
   */
  Date gider_Onay
  /**
   * Gmuh_onay SMALLDATETIME
   */
  Date gmuh_Onay

  static mapping = {
    table name: 'Onay'
    //TODO: id name: '?' 
    version false
    gelir_Onay column: 'Gelir_onay'
    gider_Onay column: 'Gider_onay'
    gmuh_Onay column: 'Gmuh_onay'
  }
}

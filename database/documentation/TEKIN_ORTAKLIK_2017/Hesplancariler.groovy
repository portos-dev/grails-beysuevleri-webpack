/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE hesplancariler
(
    Arti INT NOT NULL IDENTITY,
    Kod NVARCHAR(20),
    Unvan NVARCHAR(150),
    Email NVARCHAR(150),
    Tel1 NVARCHAR(15),
    Tel2 NVARCHAR(15),
    Tel3 NVARCHAR(15),
    Tel4 NVARCHAR(15),
    Fax1 NVARCHAR(15),
    Yetkili NVARCHAR(30),
    Yetkiligorevi NVARCHAR(50),
    Adres1 NVARCHAR(100),
    Adres2 NVARCHAR(100),
    Adres3 NVARCHAR(100),
    Adres4 NVARCHAR(100),
    Postakodu NVARCHAR(15),
    Il NVARCHAR(30),
    Ilce NVARCHAR(30),
    Ulke NVARCHAR(30),
    aciklama NVARCHAR(200),
    Fax2 NVARCHAR(15),
    Cep1 NVARCHAR(15),
    Cep2 NVARCHAR(15),
    Email2 NVARCHAR(150),
    Webadresi NVARCHAR(150),
    Tckimlikno NVARCHAR(11),
    Vergikimlikno NVARCHAR(11),
    Cariadi NVARCHAR(70),
    Ulkekodu CHAR(10),
    Vergidairesi NVARCHAR(20),
    Nereden CHAR,
    Musavir_adi NVARCHAR(50),
    Musavir_sicilno NVARCHAR(20),
    Musavir_vergino NVARCHAR(11),
    Musavir_tcno NVARCHAR(11),
    Musavir_tel NVARCHAR(15),
    Heskod NVARCHAR(20),
    Heskod2 NVARCHAR(20),
    HesArti INT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Hesplancariler implements Serializable{

  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Kod NVARCHAR(20)
   */
  String kod
  /**
   * Unvan NVARCHAR(150)
   */
  String unvan
  /**
   * Email NVARCHAR(150)
   */
  String email
  /**
   * Tel1 NVARCHAR(15)
   */
  String tel1
  /**
   * Tel2 NVARCHAR(15)
   */
  String tel2
  /**
   * Tel3 NVARCHAR(15)
   */
  String tel3
  /**
   * Tel4 NVARCHAR(15)
   */
  String tel4
  /**
   * Fax1 NVARCHAR(15)
   */
  String fax1
  /**
   * Yetkili NVARCHAR(30)
   */
  String yetkili
  /**
   * Yetkiligorevi NVARCHAR(50)
   */
  String yetkiligorevi
  /**
   * Adres1 NVARCHAR(100)
   */
  String adres1
  /**
   * Adres2 NVARCHAR(100)
   */
  String adres2
  /**
   * Adres3 NVARCHAR(100)
   */
  String adres3
  /**
   * Adres4 NVARCHAR(100)
   */
  String adres4
  /**
   * Postakodu NVARCHAR(15)
   */
  String postakodu
  /**
   * Il NVARCHAR(30)
   */
  String il
  /**
   * Ilce NVARCHAR(30)
   */
  String ilce
  /**
   * Ulke NVARCHAR(30)
   */
  String ulke
  /**
   * aciklama NVARCHAR(200)
   */
  String aciklama
  /**
   * Fax2 NVARCHAR(15)
   */
  String fax2
  /**
   * Cep1 NVARCHAR(15)
   */
  String cep1
  /**
   * Cep2 NVARCHAR(15)
   */
  String cep2
  /**
   * Email2 NVARCHAR(150)
   */
  String email2
  /**
   * Webadresi NVARCHAR(150)
   */
  String webadresi
  /**
   * Tckimlikno NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Vergikimlikno NVARCHAR(11)
   */
  String vergikimlikno
  /**
   * Cariadi NVARCHAR(70)
   */
  String cariadi
  /**
   * Ulkekodu CHAR(10)
   */
  String ulkekodu
  /**
   * Vergidairesi NVARCHAR(20)
   */
  String vergidairesi
  /**
   * Nereden CHAR
   */
  String nereden
  /**
   * Musavir_adi NVARCHAR(50)
   */
  String musavir_Adi
  /**
   * Musavir_sicilno NVARCHAR(20)
   */
  String musavir_Sicilno
  /**
   * Musavir_vergino NVARCHAR(11)
   */
  String musavir_Vergino
  /**
   * Musavir_tcno NVARCHAR(11)
   */
  String musavir_Tcno
  /**
   * Musavir_tel NVARCHAR(15)
   */
  String musavir_Tel
  /**
   * Heskod NVARCHAR(20)
   */
  String heskod
  /**
   * Heskod2 NVARCHAR(20)
   */
  String heskod2
  /**
   * HesArti INT
   */
  Long hesarti

  static mapping = {
    table name: 'hesplancariler'
    //TODO: id name: '?' 
    version false
    arti column: 'Arti'
    kod column: 'Kod'
    unvan column: 'Unvan'
    email column: 'Email'
    tel1 column: 'Tel1'
    tel2 column: 'Tel2'
    tel3 column: 'Tel3'
    tel4 column: 'Tel4'
    fax1 column: 'Fax1'
    yetkili column: 'Yetkili'
    yetkiligorevi column: 'Yetkiligorevi'
    adres1 column: 'Adres1'
    adres2 column: 'Adres2'
    adres3 column: 'Adres3'
    adres4 column: 'Adres4'
    postakodu column: 'Postakodu'
    il column: 'Il'
    ilce column: 'Ilce'
    ulke column: 'Ulke'
    aciklama column: 'aciklama'
    fax2 column: 'Fax2'
    cep1 column: 'Cep1'
    cep2 column: 'Cep2'
    email2 column: 'Email2'
    webadresi column: 'Webadresi'
    tckimlikno column: 'Tckimlikno'
    vergikimlikno column: 'Vergikimlikno'
    cariadi column: 'Cariadi'
    ulkekodu column: 'Ulkekodu'
    vergidairesi column: 'Vergidairesi'
    nereden column: 'Nereden'
    musavir_Adi column: 'Musavir_adi'
    musavir_Sicilno column: 'Musavir_sicilno'
    musavir_Vergino column: 'Musavir_vergino'
    musavir_Tcno column: 'Musavir_tcno'
    musavir_Tel column: 'Musavir_tel'
    heskod column: 'Heskod'
    heskod2 column: 'Heskod2'
    hesarti column: 'HesArti'
  }
}

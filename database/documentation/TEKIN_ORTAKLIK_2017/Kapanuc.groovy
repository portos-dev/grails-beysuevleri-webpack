/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE kapanuc
(
    Sno SMALLINT,
    Khkba NVARCHAR(20),
    Khkbi NVARCHAR(20),
    Yhk NVARCHAR(20),
    Yahk NVARCHAR(20),
    Oran REAL
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Kapanuc implements Serializable{

  /**
   * Sno SMALLINT
   */
  Long sno
  /**
   * Khkba NVARCHAR(20)
   */
  String khkba
  /**
   * Khkbi NVARCHAR(20)
   */
  String khkbi
  /**
   * Yhk NVARCHAR(20)
   */
  String yhk
  /**
   * Yahk NVARCHAR(20)
   */
  String yahk
  /**
   * Oran REAL
   */
  Double oran

  static mapping = {
    table name: 'kapanuc'
    //TODO: id name: '?' 
    version false
    sno column: 'Sno'
    khkba column: 'Khkba'
    khkbi column: 'Khkbi'
    yhk column: 'Yhk'
    yahk column: 'Yahk'
    oran column: 'Oran'
  }
}

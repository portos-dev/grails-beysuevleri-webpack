/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_istirakler
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Unvani NVARCHAR(60),
    Ulke NVARCHAR(100),
    Sermaye_Orani FLOAT,
    Hasilat FLOAT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Istirakler implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Unvani NVARCHAR(60)
   */
  String unvani
  /**
   * Ulke NVARCHAR(100)
   */
  String ulke
  /**
   * Sermaye_Orani FLOAT
   */
  Double sermaye_Orani
  /**
   * Hasilat FLOAT
   */
  Double hasilat

  static mapping = {
    table name: 'Transfer_istirakler'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    unvani column: 'Unvani'
    ulke column: 'Ulke'
    sermaye_Orani column: 'Sermaye_Orani'
    hasilat column: 'Hasilat'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE kapandsm
(
    Hk NVARCHAR(20),
    Arti INT NOT NULL IDENTITY,
    Dsm FLOAT,
    Ay SMALLINT,
    Topb FLOAT,
    Topa FLOAT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Kapandsm implements Serializable{

  /**
   * Hk NVARCHAR(20)
   */
  String hk
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Dsm FLOAT
   */
  Double dsm
  /**
   * Ay SMALLINT
   */
  Long ay
  /**
   * Topb FLOAT
   */
  Double topb
  /**
   * Topa FLOAT
   */
  Double topa

  static mapping = {
    table name: 'kapandsm'
    //TODO: id name: '?' 
    version false
    hk column: 'Hk'
    arti column: 'Arti'
    dsm column: 'Dsm'
    ay column: 'Ay'
    topb column: 'Topb'
    topa column: 'Topa'
  }
}

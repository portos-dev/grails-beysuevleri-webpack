/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE envanay
(
    Stk NVARCHAR(20),
    Stt SMALLDATETIME,
    Sno INT NOT NULL IDENTITY,
    Sta NVARCHAR(40),
    Stb NVARCHAR(8),
    Bakm FLOAT,
    Bakt FLOAT,
    Ref INT,
    Ticari CHAR,
    Eyontemi CHAR
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Envanay implements Serializable{

  /**
   * Stk NVARCHAR(20)
   */
  String stk
  /**
   * Stt SMALLDATETIME
   */
  Date stt
  /**
   * Sno INT NOT NULL IDENTITY
   */
  Long sno
  /**
   * Sta NVARCHAR(40)
   */
  String sta
  /**
   * Stb NVARCHAR(8)
   */
  String stb
  /**
   * Bakm FLOAT
   */
  Double bakm
  /**
   * Bakt FLOAT
   */
  Double bakt
  /**
   * Ref INT
   */
  Long ref
  /**
   * Ticari CHAR
   */
  String ticari
  /**
   * Eyontemi CHAR
   */
  String eyontemi

  static mapping = {
    table name: 'envanay'
    //TODO: id name: '?' 
    version false
    stk column: 'Stk'
    stt column: 'Stt'
    sno column: 'Sno'
    sta column: 'Sta'
    stb column: 'Stb'
    bakm column: 'Bakm'
    bakt column: 'Bakt'
    ref column: 'Ref'
    ticari column: 'Ticari'
    eyontemi column: 'Eyontemi'
  }
}

/***
 * FOUND COMPOSITE PRIMARY KEY true
 * COMPOSITE_KEY_SIZE: 1
 * FIRST MATCH: [id,  property]
 * [id, property]
 * 
 * 
 *CREATE TABLE dtproperties
(
    id INT NOT NULL IDENTITY,
    objectid INT,
    property VARCHAR(64) NOT NULL,
    value VARCHAR(255),
    uvalue NVARCHAR(255),
    lvalue IMAGE,
    version INT DEFAULT 0 NOT NULL,
    CONSTRAINT pk_dtproperties PRIMARY KEY (id, property)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Dtproperties implements Serializable{

  /**
   * id INT NOT NULL IDENTITY
   */
  Long id
  /**
   * objectid INT
   */
  Long objectid
  /**
   * property VARCHAR(64) NOT NULL
   */
  String property
  /**
   * value VARCHAR(255)
   */
  String value
  /**
   * uvalue NVARCHAR(255)
   */
  String uvalue
  /**
   * lvalue IMAGE
   */
  String lvalue
  /**
   * version INT DEFAULT 0 NOT NULL
   */
  Long version

  static mapping = {
    table name: 'dtproperties'
    //TODO: id name: '?' 
    id composite: ['id','property']
    version false
    id column: 'id'
    objectid column: 'objectid'
    property column: 'property'
    value column: 'value'
    uvalue column: 'uvalue'
    lvalue column: 'lvalue'
    version column: 'version'
  }
}

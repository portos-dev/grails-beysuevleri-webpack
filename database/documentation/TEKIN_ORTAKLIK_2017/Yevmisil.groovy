/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE yevmisil
(
    Refno INT,
    Tip NVARCHAR(1),
    Gmkod NVARCHAR(20),
    Refno2 INT,
    Sube SMALLINT,
    Fistar SMALLDATETIME,
    Fistur NVARCHAR(1),
    Fisno NVARCHAR(9),
    Yevno INT,
    Dovizadi SMALLINT,
    Aciklama NVARCHAR(50),
    Miktar FLOAT,
    Doviz FLOAT,
    Borclu FLOAT,
    Alacakli FLOAT,
    Stk NVARCHAR(20),
    Islemtipi SMALLINT,
    Masrafmerkezi NVARCHAR(10),
    Evraktarihi SMALLDATETIME,
    Evrakno NVARCHAR(16),
    f_int INT,
    f_str NVARCHAR(80),
    f_islemkodu INT,
    Vergino NVARCHAR(11),
    fisbil_pid NVARCHAR(18),
    serino NVARCHAR(5),
    fis_guid UNIQUEIDENTIFIER,
    Belge_Turu SMALLINT,
    Odeme_Sekli SMALLINT,
    Belge_Turu_Aciklamasi NVARCHAR(50),
    Vade_Tarihi SMALLDATETIME,
    isDegisti TINYINT,
    sp_id NVARCHAR(50)
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Yevmisil implements Serializable{

  /**
   * Refno INT
   */
  Long refno
  /**
   * Tip NVARCHAR(1)
   */
  String tip
  /**
   * Gmkod NVARCHAR(20)
   */
  String gmkod
  /**
   * Refno2 INT
   */
  Long refno2
  /**
   * Sube SMALLINT
   */
  Long sube
  /**
   * Fistar SMALLDATETIME
   */
  Date fistar
  /**
   * Fistur NVARCHAR(1)
   */
  String fistur
  /**
   * Fisno NVARCHAR(9)
   */
  String fisno
  /**
   * Yevno INT
   */
  Long yevno
  /**
   * Dovizadi SMALLINT
   */
  Long dovizadi
  /**
   * Aciklama NVARCHAR(50)
   */
  String aciklama
  /**
   * Miktar FLOAT
   */
  Double miktar
  /**
   * Doviz FLOAT
   */
  Double doviz
  /**
   * Borclu FLOAT
   */
  Double borclu
  /**
   * Alacakli FLOAT
   */
  Double alacakli
  /**
   * Stk NVARCHAR(20)
   */
  String stk
  /**
   * Islemtipi SMALLINT
   */
  Long islemtipi
  /**
   * Masrafmerkezi NVARCHAR(10)
   */
  String masrafmerkezi
  /**
   * Evraktarihi SMALLDATETIME
   */
  Date evraktarihi
  /**
   * Evrakno NVARCHAR(16)
   */
  String evrakno
  /**
   * f_int INT
   */
  Long f_Int
  /**
   * f_str NVARCHAR(80)
   */
  String f_Str
  /**
   * f_islemkodu INT
   */
  Long f_Islemkodu
  /**
   * Vergino NVARCHAR(11)
   */
  String vergino
  /**
   * fisbil_pid NVARCHAR(18)
   */
  String fisbil_Pid
  /**
   * serino NVARCHAR(5)
   */
  String serino
  /**
   * fis_guid UNIQUEIDENTIFIER
   */
  String fis_Guid
  /**
   * Belge_Turu SMALLINT
   */
  Long belge_Turu
  /**
   * Odeme_Sekli SMALLINT
   */
  Long odeme_Sekli
  /**
   * Belge_Turu_Aciklamasi NVARCHAR(50)
   */
  String belge_Turu_Aciklamasi
  /**
   * Vade_Tarihi SMALLDATETIME
   */
  Date vade_Tarihi
  /**
   * isDegisti TINYINT
   */
  Long isdegisti
  /**
   * sp_id NVARCHAR(50)
   */
  String sp_Id

  static mapping = {
    table name: 'yevmisil'
    //TODO: id name: '?' 
    version false
    refno column: 'Refno'
    tip column: 'Tip'
    gmkod column: 'Gmkod'
    refno2 column: 'Refno2'
    sube column: 'Sube'
    fistar column: 'Fistar'
    fistur column: 'Fistur'
    fisno column: 'Fisno'
    yevno column: 'Yevno'
    dovizadi column: 'Dovizadi'
    aciklama column: 'Aciklama'
    miktar column: 'Miktar'
    doviz column: 'Doviz'
    borclu column: 'Borclu'
    alacakli column: 'Alacakli'
    stk column: 'Stk'
    islemtipi column: 'Islemtipi'
    masrafmerkezi column: 'Masrafmerkezi'
    evraktarihi column: 'Evraktarihi'
    evrakno column: 'Evrakno'
    f_Int column: 'f_int'
    f_Str column: 'f_str'
    f_Islemkodu column: 'f_islemkodu'
    vergino column: 'Vergino'
    fisbil_Pid column: 'fisbil_pid'
    serino column: 'serino'
    fis_Guid column: 'fis_guid'
    belge_Turu column: 'Belge_Turu'
    odeme_Sekli column: 'Odeme_Sekli'
    belge_Turu_Aciklamasi column: 'Belge_Turu_Aciklamasi'
    vade_Tarihi column: 'Vade_Tarihi'
    isdegisti column: 'isDegisti'
    sp_Id column: 'sp_id'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE gidertablosu
(
    Faturatarihi SMALLDATETIME,
    Sirano INT NOT NULL IDENTITY,
    Miktar FLOAT,
    Faturano NVARCHAR(16),
    Aciklama NVARCHAR(40),
    Masraflar FLOAT,
    Indirilebilecekkdv FLOAT,
    Toplam FLOAT,
    Kdvorani FLOAT,
    Dahilharic NVARCHAR(1),
    Stokkodu NVARCHAR(20),
    Ay SMALLINT,
    Kod NVARCHAR(2),
    Islemetarihi SMALLDATETIME,
    Heskod NVARCHAR(20),
    Kayitno INT,
    Eynmk INT,
    Cmiorbmi INT,
    Esno SMALLINT,
    Sino SMALLINT,
    Torani CHAR(6),
    Vergino NVARCHAR(11),
    Stopajtutari FLOAT,
    Onay SMALLINT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Gidertablosu implements Serializable{

  /**
   * Faturatarihi SMALLDATETIME
   */
  Date faturatarihi
  /**
   * Sirano INT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Miktar FLOAT
   */
  Double miktar
  /**
   * Faturano NVARCHAR(16)
   */
  String faturano
  /**
   * Aciklama NVARCHAR(40)
   */
  String aciklama
  /**
   * Masraflar FLOAT
   */
  Double masraflar
  /**
   * Indirilebilecekkdv FLOAT
   */
  Double indirilebilecekkdv
  /**
   * Toplam FLOAT
   */
  Double toplam
  /**
   * Kdvorani FLOAT
   */
  Double kdvorani
  /**
   * Dahilharic NVARCHAR(1)
   */
  String dahilharic
  /**
   * Stokkodu NVARCHAR(20)
   */
  String stokkodu
  /**
   * Ay SMALLINT
   */
  Long ay
  /**
   * Kod NVARCHAR(2)
   */
  String kod
  /**
   * Islemetarihi SMALLDATETIME
   */
  Date islemetarihi
  /**
   * Heskod NVARCHAR(20)
   */
  String heskod
  /**
   * Kayitno INT
   */
  Long kayitno
  /**
   * Eynmk INT
   */
  Long eynmk
  /**
   * Cmiorbmi INT
   */
  Long cmiorbmi
  /**
   * Esno SMALLINT
   */
  Long esno
  /**
   * Sino SMALLINT
   */
  Long sino
  /**
   * Torani CHAR(6)
   */
  String torani
  /**
   * Vergino NVARCHAR(11)
   */
  String vergino
  /**
   * Stopajtutari FLOAT
   */
  Double stopajtutari
  /**
   * Onay SMALLINT
   */
  Long onay

  static mapping = {
    table name: 'gidertablosu'
    //TODO: id name: '?' 
    version false
    faturatarihi column: 'Faturatarihi'
    sirano column: 'Sirano'
    miktar column: 'Miktar'
    faturano column: 'Faturano'
    aciklama column: 'Aciklama'
    masraflar column: 'Masraflar'
    indirilebilecekkdv column: 'Indirilebilecekkdv'
    toplam column: 'Toplam'
    kdvorani column: 'Kdvorani'
    dahilharic column: 'Dahilharic'
    stokkodu column: 'Stokkodu'
    ay column: 'Ay'
    kod column: 'Kod'
    islemetarihi column: 'Islemetarihi'
    heskod column: 'Heskod'
    kayitno column: 'Kayitno'
    eynmk column: 'Eynmk'
    cmiorbmi column: 'Cmiorbmi'
    esno column: 'Esno'
    sino column: 'Sino'
    torani column: 'Torani'
    vergino column: 'Vergino'
    stopajtutari column: 'Stopajtutari'
    onay column: 'Onay'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE gelirtablosu
(
    Faturatarihi SMALLDATETIME,
    Sirano INT NOT NULL IDENTITY,
    Miktar FLOAT,
    Faturano NVARCHAR(16),
    Aciklama NVARCHAR(40),
    Alinanucret FLOAT,
    Hesaplanankdv FLOAT,
    Toplam FLOAT,
    Kdvorani FLOAT,
    Dahilharic NVARCHAR(1),
    Stokkodu NVARCHAR(20),
    Ay SMALLINT,
    Kod NVARCHAR(2),
    Islemetarihi SMALLDATETIME,
    Tip NVARCHAR(1),
    Heskod NVARCHAR(20),
    Kayitno INT,
    Kredi FLOAT,
    Torani CHAR(6),
    Eynmk INT,
    Cmiorbmi INT,
    Esno SMALLINT,
    Sino SMALLINT,
    Vergino NVARCHAR(11),
    Stopajtutari FLOAT,
    Onay SMALLINT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Gelirtablosu implements Serializable{

  /**
   * Faturatarihi SMALLDATETIME
   */
  Date faturatarihi
  /**
   * Sirano INT NOT NULL IDENTITY
   */
  Long sirano
  /**
   * Miktar FLOAT
   */
  Double miktar
  /**
   * Faturano NVARCHAR(16)
   */
  String faturano
  /**
   * Aciklama NVARCHAR(40)
   */
  String aciklama
  /**
   * Alinanucret FLOAT
   */
  Double alinanucret
  /**
   * Hesaplanankdv FLOAT
   */
  Double hesaplanankdv
  /**
   * Toplam FLOAT
   */
  Double toplam
  /**
   * Kdvorani FLOAT
   */
  Double kdvorani
  /**
   * Dahilharic NVARCHAR(1)
   */
  String dahilharic
  /**
   * Stokkodu NVARCHAR(20)
   */
  String stokkodu
  /**
   * Ay SMALLINT
   */
  Long ay
  /**
   * Kod NVARCHAR(2)
   */
  String kod
  /**
   * Islemetarihi SMALLDATETIME
   */
  Date islemetarihi
  /**
   * Tip NVARCHAR(1)
   */
  String tip
  /**
   * Heskod NVARCHAR(20)
   */
  String heskod
  /**
   * Kayitno INT
   */
  Long kayitno
  /**
   * Kredi FLOAT
   */
  Double kredi
  /**
   * Torani CHAR(6)
   */
  String torani
  /**
   * Eynmk INT
   */
  Long eynmk
  /**
   * Cmiorbmi INT
   */
  Long cmiorbmi
  /**
   * Esno SMALLINT
   */
  Long esno
  /**
   * Sino SMALLINT
   */
  Long sino
  /**
   * Vergino NVARCHAR(11)
   */
  String vergino
  /**
   * Stopajtutari FLOAT
   */
  Double stopajtutari
  /**
   * Onay SMALLINT
   */
  Long onay

  static mapping = {
    table name: 'gelirtablosu'
    //TODO: id name: '?' 
    version false
    faturatarihi column: 'Faturatarihi'
    sirano column: 'Sirano'
    miktar column: 'Miktar'
    faturano column: 'Faturano'
    aciklama column: 'Aciklama'
    alinanucret column: 'Alinanucret'
    hesaplanankdv column: 'Hesaplanankdv'
    toplam column: 'Toplam'
    kdvorani column: 'Kdvorani'
    dahilharic column: 'Dahilharic'
    stokkodu column: 'Stokkodu'
    ay column: 'Ay'
    kod column: 'Kod'
    islemetarihi column: 'Islemetarihi'
    tip column: 'Tip'
    heskod column: 'Heskod'
    kayitno column: 'Kayitno'
    kredi column: 'Kredi'
    torani column: 'Torani'
    eynmk column: 'Eynmk'
    cmiorbmi column: 'Cmiorbmi'
    esno column: 'Esno'
    sino column: 'Sino'
    vergino column: 'Vergino'
    stopajtutari column: 'Stopajtutari'
    onay column: 'Onay'
  }
}

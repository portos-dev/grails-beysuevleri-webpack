/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_Yontemler
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Aciklama NVARCHAR(100),
    Alim FLOAT,
    Satim FLOAT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Yontemler implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Aciklama NVARCHAR(100)
   */
  String aciklama
  /**
   * Alim FLOAT
   */
  Double alim
  /**
   * Satim FLOAT
   */
  Double satim

  static mapping = {
    table name: 'Transfer_Yontemler'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    aciklama column: 'Aciklama'
    alim column: 'Alim'
    satim column: 'Satim'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE satmaliyeti
(
    Mad NVARCHAR(80),
    Kod NVARCHAR(3),
    Modb FLOAT,
    Moda FLOAT,
    Modbak FLOAT,
    Mcdb FLOAT,
    Mcda FLOAT,
    Mcdbak FLOAT
)
 */
package beysuevleri.vanilla


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Satmaliyeti implements Serializable{

  /**
   * Mad NVARCHAR(80)
   */
  String mad
  /**
   * Kod NVARCHAR(3)
   */
  String kod
  /**
   * Modb FLOAT
   */
  Double modb
  /**
   * Moda FLOAT
   */
  Double moda
  /**
   * Modbak FLOAT
   */
  Double modbak
  /**
   * Mcdb FLOAT
   */
  Double mcdb
  /**
   * Mcda FLOAT
   */
  Double mcda
  /**
   * Mcdbak FLOAT
   */
  Double mcdbak

  static mapping = {
    table name: 'satmaliyeti'
    //TODO: id name: '?' 
    version false
    mad column: 'Mad'
    kod column: 'Kod'
    modb column: 'Modb'
    moda column: 'Moda'
    modbak column: 'Modbak'
    mcdb column: 'Mcdb'
    mcda column: 'Mcda'
    mcdbak column: 'Mcdbak'
  }
}

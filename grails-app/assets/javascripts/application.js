// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require libs/js-xlsx/xlsx.core.min
//= require libs/jsPDF/jspdf.min
//= require libs/jsPDF-AutoTable/jspdf.plugin.autotable
//= require libs/html2canvas/html2canvas.min
//= require excelplus-2.5.min
//= require tableExport
//= require bootstrap
//DISABLED Loads bundle.js twice  = require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);
}

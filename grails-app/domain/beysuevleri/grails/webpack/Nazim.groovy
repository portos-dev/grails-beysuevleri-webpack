/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE nazim
(
    Kod NVARCHAR(20),
    Art NVARCHAR(1),
    Arti SMALLINT NOT NULL IDENTITY,
    Btop FLOAT,
    Atop FLOAT,
    Dsonu FLOAT,
    Ay SMALLINT
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Nazim implements Serializable{

  /**
   * Kod NVARCHAR(20)
   */
  String kod
  /**
   * Art NVARCHAR(1)
   */
  String art
  /**
   * Arti SMALLINT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Btop FLOAT
   */
  Double btop
  /**
   * Atop FLOAT
   */
  Double atop
  /**
   * Dsonu FLOAT
   */
  Double dsonu
  /**
   * Ay SMALLINT
   */
  Long ay

  static constraints = {
    kod nullable: true
    art nullable: true
    arti nullable: false
    btop nullable: true
    atop nullable: true
    dsonu nullable: true
    ay nullable: true
  }

  static mapping = {
    table name: 'nazim'
    //TODO: id name: '?' 
    version false
    kod column: 'Kod'
    art column: 'Art'
    arti column: 'Arti'
    btop column: 'Btop'
    atop column: 'Atop'
    dsonu column: 'Dsonu'
    ay column: 'Ay'
  }
}

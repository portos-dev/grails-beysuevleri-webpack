/***
 * FOUND PRIMARY KEY true
 * PRIMARY_KEY SIZE: 1
 * FIRST MATCH: Kod
 * 
 * 
 *CREATE TABLE hesplan
(
    Kod NVARCHAR(20) PRIMARY KEY NOT NULL,
    Kod2 NVARCHAR(20),
    Aciklama NVARCHAR(50),
    Detay NVARCHAR(1),
    Topb FLOAT,
    Topa FLOAT,
    Ba1 NVARCHAR(1),
    Ba2 NVARCHAR(1),
    Ba3 NVARCHAR(2),
    Tckimlikno NVARCHAR(11),
    Vergikimlikno NVARCHAR(11),
    Kamuorozel NVARCHAR(1),
    Yabanci NVARCHAR(50),
    Stokkodu NVARCHAR(20),
    Vergidairesi NVARCHAR(20),
    Blerdekullan CHAR,
    Ulkekodu CHAR(10),
    Grup NVARCHAR(20),
    Kurfarkindakullan NVARCHAR(1),
    Tev_hes_kodu NVARCHAR(20),
    Tev_orani NVARCHAR(5),
    Tev_tur_kodu NVARCHAR(3),
    Stopaj_orani REAL,
    Stopaj_tur_kodu NVARCHAR(3),
    Stopaj_hes_kodu NVARCHAR(20),
    Arti INT NOT NULL IDENTITY,
    Stopaj_evrak_turu SMALLINT,
    Ilave_Edilecek_KDVmi NVARCHAR(1),
    Iade_KDVmi NVARCHAR(1),
    Kredi_Karti_Satisimi NVARCHAR(1),
    Indirimmi NVARCHAR(1),
    Normal_KDVmi NVARCHAR(1),
    KDVislemTuru NVARCHAR(3),
    KDVislemTuru2 NVARCHAR(3),
    KDVislemTuru3 NVARCHAR(3),
    KDVislemTuruint SMALLINT,
    KDVislemTuruint2 SMALLINT,
    KDVislemTuruint3 SMALLINT,
    DigerEH NVARCHAR(1),
    Diger2EH NVARCHAR(1),
    Diger3EH NVARCHAR(1)
)
 */
package beysuevleri.grails.webpack

import grails.gorm.MultiTenant
import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Hesplan implements Serializable, MultiTenant<Hesplan>{

  Collection<Yevmiye> odemePlani = []

  static transients = ['odemePlani']

  /**
   * Kod NVARCHAR(20) NOT NULL
   */
  String kod
  /**
   * Kod2 NVARCHAR(20)
   */
  String kod2
  /**
   * Aciklama NVARCHAR(50)
   */
  String aciklama
  /**
   * Detay NVARCHAR(1)
   */
  String detay
  /**
   * Topb FLOAT
   */
  Double topb
  /**
   * Topa FLOAT
   */
  Double topa
  /**
   * Ba1 NVARCHAR(1)
   */
  String ba1
  /**
   * Ba2 NVARCHAR(1)
   */
  String ba2
  /**
   * Ba3 NVARCHAR(2)
   */
  String ba3
  /**
   * Tckimlikno NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Vergikimlikno NVARCHAR(11)
   */
  String vergikimlikno
  /**
   * Kamuorozel NVARCHAR(1)
   */
  String kamuorozel
  /**
   * Yabanci NVARCHAR(50)
   */
  String yabanci
  /**
   * Stokkodu NVARCHAR(20)
   */
  String stokkodu
  /**
   * Vergidairesi NVARCHAR(20)
   */
  String vergidairesi
  /**
   * Blerdekullan CHAR
   */
  String blerdekullan
  /**
   * Ulkekodu CHAR(10)
   */
  String ulkekodu
  /**
   * Grup NVARCHAR(20)
   */
  String grup
  /**
   * Kurfarkindakullan NVARCHAR(1)
   */
  String kurfarkindakullan
  /**
   * Tev_hes_kodu NVARCHAR(20)
   */
  String tev_Hes_Kodu
  /**
   * Tev_orani NVARCHAR(5)
   */
  String tev_Orani
  /**
   * Tev_tur_kodu NVARCHAR(3)
   */
  String tev_Tur_Kodu
  /**
   * Stopaj_orani REAL
   */
  Double stopaj_Orani
  /**
   * Stopaj_tur_kodu NVARCHAR(3)
   */
  String stopaj_Tur_Kodu
  /**
   * Stopaj_hes_kodu NVARCHAR(20)
   */
  String stopaj_Hes_Kodu
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Stopaj_evrak_turu SMALLINT
   */
  Long stopaj_Evrak_Turu
  /**
   * Ilave_Edilecek_KDVmi NVARCHAR(1)
   */
  String ilave_Edilecek_Kdvmi
  /**
   * Iade_KDVmi NVARCHAR(1)
   */
  String iade_Kdvmi
  /**
   * Kredi_Karti_Satisimi NVARCHAR(1)
   */
  String kredi_Karti_Satisimi
  /**
   * Indirimmi NVARCHAR(1)
   */
  String indirimmi
  /**
   * Normal_KDVmi NVARCHAR(1)
   */
  String normal_Kdvmi
  /**
   * KDVislemTuru NVARCHAR(3)
   */
  String kdvislemturu
  /**
   * KDVislemTuru2 NVARCHAR(3)
   */
  String kdvislemturu2
  /**
   * KDVislemTuru3 NVARCHAR(3)
   */
  String kdvislemturu3
  /**
   * KDVislemTuruint SMALLINT
   */
  Long kdvislemturuint
  /**
   * KDVislemTuruint2 SMALLINT
   */
  Long kdvislemturuint2
  /**
   * KDVislemTuruint3 SMALLINT
   */
  Long kdvislemturuint3
  /**
   * DigerEH NVARCHAR(1)
   */
  String digereh
  /**
   * Diger2EH NVARCHAR(1)
   */
  String diger2Eh
  /**
   * Diger3EH NVARCHAR(1)
   */
  String diger3Eh

  static constraints = {
    kod nullable: false
    kod2 nullable: true
    aciklama nullable: true
    detay nullable: true
    topb nullable: true
    topa nullable: true
    ba1 nullable: true
    ba2 nullable: true
    ba3 nullable: true
    tckimlikno nullable: true
    vergikimlikno nullable: true
    kamuorozel nullable: true
    yabanci nullable: true
    stokkodu nullable: true
    vergidairesi nullable: true
    blerdekullan nullable: true
    ulkekodu nullable: true
    grup nullable: true
    kurfarkindakullan nullable: true
    tev_Hes_Kodu nullable: true
    tev_Orani nullable: true
    tev_Tur_Kodu nullable: true
    stopaj_Orani nullable: true
    stopaj_Tur_Kodu nullable: true
    stopaj_Hes_Kodu nullable: true
    arti nullable: false
    stopaj_Evrak_Turu nullable: true
    ilave_Edilecek_Kdvmi nullable: true
    iade_Kdvmi nullable: true
    kredi_Karti_Satisimi nullable: true
    indirimmi nullable: true
    normal_Kdvmi nullable: true
    kdvislemturu nullable: true
    kdvislemturu2 nullable: true
    kdvislemturu3 nullable: true
    kdvislemturuint nullable: true
    kdvislemturuint2 nullable: true
    kdvislemturuint3 nullable: true
    digereh nullable: true
    diger2Eh nullable: true
    diger3Eh nullable: true
  }

  static mapping = {
    table name: 'hesplan'
    //TODO: id name: '?' 
    id name: 'kod', generator:'assigned'
    version false
    kod column: 'Kod'
    kod2 column: 'Kod2'
    aciklama column: 'Aciklama'
    detay column: 'Detay'
    topb column: 'Topb'
    topa column: 'Topa'
    ba1 column: 'Ba1'
    ba2 column: 'Ba2'
    ba3 column: 'Ba3'
    tckimlikno column: 'Tckimlikno'
    vergikimlikno column: 'Vergikimlikno'
    kamuorozel column: 'Kamuorozel'
    yabanci column: 'Yabanci'
    stokkodu column: 'Stokkodu'
    vergidairesi column: 'Vergidairesi'
    blerdekullan column: 'Blerdekullan'
    ulkekodu column: 'Ulkekodu'
    grup column: 'Grup'
    kurfarkindakullan column: 'Kurfarkindakullan'
    tev_Hes_Kodu column: 'Tev_hes_kodu'
    tev_Orani column: 'Tev_orani'
    tev_Tur_Kodu column: 'Tev_tur_kodu'
    stopaj_Orani column: 'Stopaj_orani'
    stopaj_Tur_Kodu column: 'Stopaj_tur_kodu'
    stopaj_Hes_Kodu column: 'Stopaj_hes_kodu'
    arti column: 'Arti'
    stopaj_Evrak_Turu column: 'Stopaj_evrak_turu'
    ilave_Edilecek_Kdvmi column: 'Ilave_Edilecek_KDVmi'
    iade_Kdvmi column: 'Iade_KDVmi'
    kredi_Karti_Satisimi column: 'Kredi_Karti_Satisimi'
    indirimmi column: 'Indirimmi'
    normal_Kdvmi column: 'Normal_KDVmi'
    kdvislemturu column: 'KDVislemTuru'
    kdvislemturu2 column: 'KDVislemTuru2'
    kdvislemturu3 column: 'KDVislemTuru3'
    kdvislemturuint column: 'KDVislemTuruint'
    kdvislemturuint2 column: 'KDVislemTuruint2'
    kdvislemturuint3 column: 'KDVislemTuruint3'
    digereh column: 'DigerEH'
    diger2Eh column: 'Diger2EH'
    diger3Eh column: 'Diger3EH'
  }
}

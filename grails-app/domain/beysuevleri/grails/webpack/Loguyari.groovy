/***
 * FOUND PRIMARY KEY true
 * PRIMARY_KEY SIZE: 1
 * FIRST MATCH: ID
 * 
 * 
 *CREATE TABLE LogUyari
(
    ID INT PRIMARY KEY NOT NULL IDENTITY,
    Mesaj NVARCHAR(250),
    Kullanici NVARCHAR(150),
    SQLTar DATETIME,
    Tarih DATETIME,
    Modul NVARCHAR(50),
    GUID UNIQUEIDENTIFIER
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Loguyari implements Serializable{

  /**
   * ID INT NOT NULL IDENTITY
   */
  Long id
  /**
   * Mesaj NVARCHAR(250)
   */
  String mesaj
  /**
   * Kullanici NVARCHAR(150)
   */
  String kullanici
  /**
   * SQLTar DATETIME
   */
  Date sqltar
  /**
   * Tarih DATETIME
   */
  Date tarih
  /**
   * Modul NVARCHAR(50)
   */
  String modul
  /**
   * GUID UNIQUEIDENTIFIER
   */
  String guid

  static constraints = {
    id nullable: false
    mesaj nullable: true
    kullanici nullable: true
    sqltar nullable: true
    tarih nullable: true
    modul nullable: true
    guid nullable: true
  }

  static mapping = {
    table name: 'LogUyari'
    //TODO: id name: '?' 
    id name: 'id'
    version false
    id column: 'ID'
    mesaj column: 'Mesaj'
    kullanici column: 'Kullanici'
    sqltar column: 'SQLTar'
    tarih column: 'Tarih'
    modul column: 'Modul'
    guid column: 'GUID'
  }
}

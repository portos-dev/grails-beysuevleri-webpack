/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_iliskin_islemler
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Tur NVARCHAR(100),
    Alim FLOAT,
    Satim FLOAT,
    Aciklama NVARCHAR(255)
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Iliskin_Islemler implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Tur NVARCHAR(100)
   */
  String tur
  /**
   * Alim FLOAT
   */
  Double alim
  /**
   * Satim FLOAT
   */
  Double satim
  /**
   * Aciklama NVARCHAR(255)
   */
  String aciklama

  static constraints = {
    cyili nullable: true
    arti nullable: false
    sirano nullable: true
    tur nullable: true
    alim nullable: true
    satim nullable: true
    aciklama nullable: true
  }

  static mapping = {
    table name: 'Transfer_iliskin_islemler'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    tur column: 'Tur'
    alim column: 'Alim'
    satim column: 'Satim'
    aciklama column: 'Aciklama'
  }
}

package beysuevleri.grails.webpack

class Report {

    String reportId
    String reportName
    /**
     * //ayni rapor icin birden fazla config tutulabilmesi icin temel rapor burada tutulur ,
     * detayli_tahsilat temel rapor olup birden fazla rapor ayarinin farkli isimlerle (reportId, reportName) ile bulundurulmasi vb.
     * Bu raporlar bir liste ile goruntulenip secilerek istenilen seceneklerle rapor alinabilmesi gibi
     */
    String baseReport

//    Map config // cannot extend db column size in grails, limited to VARCHAR(255)
    static hasMany = [config: ReportConfigItem]

    static constraints = {
    }

    static mapping = {
        datasource 'internal'
        id name: 'reportId', type:'string',generator:'assigned'
    }

    def config(String key){
        def confVal
        this.config.each { ReportConfigItem item ->
            if(item.key == key)
                if(item.val.contains(','))
                    confVal = item.val.split(',').collect{
                        it.trim()
                    }
                else{
                    confVal = item.val
                }
        }
        confVal
    }
}

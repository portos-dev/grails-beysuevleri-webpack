/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_iliskili_kisiler
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Yurt_icinde_iliskili_kisi NVARCHAR(60),
    Vergi_No CHAR(10),
    TCKimlikNo NVARCHAR(11),
    Yurt_Disinda_iliskili_kisi NVARCHAR(60),
    Bulundugu_Ulke NVARCHAR(100)
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Iliskili_Kisiler implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Yurt_icinde_iliskili_kisi NVARCHAR(60)
   */
  String yurt_Icinde_Iliskili_Kisi
  /**
   * Vergi_No CHAR(10)
   */
  String vergi_No
  /**
   * TCKimlikNo NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Yurt_Disinda_iliskili_kisi NVARCHAR(60)
   */
  String yurt_Disinda_Iliskili_Kisi
  /**
   * Bulundugu_Ulke NVARCHAR(100)
   */
  String bulundugu_Ulke

  static constraints = {
    cyili nullable: true
    arti nullable: false
    sirano nullable: true
    yurt_Icinde_Iliskili_Kisi nullable: true
    vergi_No nullable: true
    tckimlikno nullable: true
    yurt_Disinda_Iliskili_Kisi nullable: true
    bulundugu_Ulke nullable: true
  }

  static mapping = {
    table name: 'Transfer_iliskili_kisiler'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    yurt_Icinde_Iliskili_Kisi column: 'Yurt_icinde_iliskili_kisi'
    vergi_No column: 'Vergi_No'
    tckimlikno column: 'TCKimlikNo'
    yurt_Disinda_Iliskili_Kisi column: 'Yurt_Disinda_iliskili_kisi'
    bulundugu_Ulke column: 'Bulundugu_Ulke'
  }
}

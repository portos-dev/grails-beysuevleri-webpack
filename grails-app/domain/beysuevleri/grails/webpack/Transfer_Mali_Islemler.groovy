/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Transfer_Mali_islemler
(
    Cyili SMALLINT,
    Arti INT NOT NULL IDENTITY,
    SiraNo SMALLINT,
    Aciklama NVARCHAR(100),
    Alim_Ana_Para FLOAT,
    Alim_Faiz FLOAT,
    Satim_Ana_Para FLOAT,
    Satim_Faiz FLOAT
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Transfer_Mali_Islemler implements Serializable{

  /**
   * Cyili SMALLINT
   */
  Long cyili
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Aciklama NVARCHAR(100)
   */
  String aciklama
  /**
   * Alim_Ana_Para FLOAT
   */
  Double alim_Ana_Para
  /**
   * Alim_Faiz FLOAT
   */
  Double alim_Faiz
  /**
   * Satim_Ana_Para FLOAT
   */
  Double satim_Ana_Para
  /**
   * Satim_Faiz FLOAT
   */
  Double satim_Faiz

  static constraints = {
    cyili nullable: true
    arti nullable: false
    sirano nullable: true
    aciklama nullable: true
    alim_Ana_Para nullable: true
    alim_Faiz nullable: true
    satim_Ana_Para nullable: true
    satim_Faiz nullable: true
  }

  static mapping = {
    table name: 'Transfer_Mali_islemler'
    //TODO: id name: '?' 
    version false
    cyili column: 'Cyili'
    arti column: 'Arti'
    sirano column: 'SiraNo'
    aciklama column: 'Aciklama'
    alim_Ana_Para column: 'Alim_Ana_Para'
    alim_Faiz column: 'Alim_Faiz'
    satim_Ana_Para column: 'Satim_Ana_Para'
    satim_Faiz column: 'Satim_Faiz'
  }
}

/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE bankaexcel
(
    Arti SMALLINT NOT NULL IDENTITY,
    Bankaaciklamasi NVARCHAR(200),
    Muavinaciklamasi NVARCHAR(200),
    Hesapkodu NVARCHAR(20),
    Bankakodu SMALLINT,
    Tur SMALLINT,
    Islemkodu NVARCHAR(200),
    Bora NVARCHAR(8)
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Bankaexcel implements Serializable{

  /**
   * Arti SMALLINT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Bankaaciklamasi NVARCHAR(200)
   */
  String bankaaciklamasi
  /**
   * Muavinaciklamasi NVARCHAR(200)
   */
  String muavinaciklamasi
  /**
   * Hesapkodu NVARCHAR(20)
   */
  String hesapkodu
  /**
   * Bankakodu SMALLINT
   */
  Long bankakodu
  /**
   * Tur SMALLINT
   */
  Long tur
  /**
   * Islemkodu NVARCHAR(200)
   */
  String islemkodu
  /**
   * Bora NVARCHAR(8)
   */
  String bora

  static constraints = {
    arti nullable: false
    bankaaciklamasi nullable: true
    muavinaciklamasi nullable: true
    hesapkodu nullable: true
    bankakodu nullable: true
    tur nullable: true
    islemkodu nullable: true
    bora nullable: true
  }

  static mapping = {
    table name: 'bankaexcel'
    //TODO: id name: '?' 
    version false
    arti column: 'Arti'
    bankaaciklamasi column: 'Bankaaciklamasi'
    muavinaciklamasi column: 'Muavinaciklamasi'
    hesapkodu column: 'Hesapkodu'
    bankakodu column: 'Bankakodu'
    tur column: 'Tur'
    islemkodu column: 'Islemkodu'
    bora column: 'Bora'
  }
}

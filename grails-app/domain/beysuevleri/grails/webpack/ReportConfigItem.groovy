package beysuevleri.grails.webpack

class ReportConfigItem {

    String key
    String val

    static belongsTo = [report:Report]

    static constraints = {
        key nullable: false, blank: false
        val nullable: false, blank: false, maxSize: 4000
    }

    static mapping = {
        datasource 'internal'
    }
}

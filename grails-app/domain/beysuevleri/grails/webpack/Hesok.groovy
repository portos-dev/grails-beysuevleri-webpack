/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE hesok
(
    G1 FLOAT,
    G2 FLOAT,
    G3 FLOAT,
    G4 FLOAT,
    G5 FLOAT,
    G6 FLOAT,
    G7 FLOAT,
    G8 FLOAT,
    G9 FLOAT,
    G10 FLOAT,
    G11 FLOAT,
    G12 FLOAT,
    G13 NVARCHAR(25)
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Hesok implements Serializable{

  /**
   * G1 FLOAT
   */
  Double g1
  /**
   * G2 FLOAT
   */
  Double g2
  /**
   * G3 FLOAT
   */
  Double g3
  /**
   * G4 FLOAT
   */
  Double g4
  /**
   * G5 FLOAT
   */
  Double g5
  /**
   * G6 FLOAT
   */
  Double g6
  /**
   * G7 FLOAT
   */
  Double g7
  /**
   * G8 FLOAT
   */
  Double g8
  /**
   * G9 FLOAT
   */
  Double g9
  /**
   * G10 FLOAT
   */
  Double g10
  /**
   * G11 FLOAT
   */
  Double g11
  /**
   * G12 FLOAT
   */
  Double g12
  /**
   * G13 NVARCHAR(25)
   */
  String g13

  static constraints = {
    g1 nullable: true
    g2 nullable: true
    g3 nullable: true
    g4 nullable: true
    g5 nullable: true
    g6 nullable: true
    g7 nullable: true
    g8 nullable: true
    g9 nullable: true
    g10 nullable: true
    g11 nullable: true
    g12 nullable: true
    g13 nullable: true
  }

  static mapping = {
    table name: 'hesok'
    //TODO: id name: '?' 
    version false
    g1 column: 'G1'
    g2 column: 'G2'
    g3 column: 'G3'
    g4 column: 'G4'
    g5 column: 'G5'
    g6 column: 'G6'
    g7 column: 'G7'
    g8 column: 'G8'
    g9 column: 'G9'
    g10 column: 'G10'
    g11 column: 'G11'
    g12 column: 'G12'
    g13 column: 'G13'
  }
}

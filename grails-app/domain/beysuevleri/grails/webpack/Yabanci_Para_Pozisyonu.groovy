/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Yabanci_Para_Pozisyonu
(
    SiraNo SMALLINT,
    Aciklama NVARCHAR(100),
    Usd FLOAT,
    Euro FLOAT,
    Diger FLOAT,
    KodNo SMALLINT
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Yabanci_Para_Pozisyonu implements Serializable{

  /**
   * SiraNo SMALLINT
   */
  Long sirano
  /**
   * Aciklama NVARCHAR(100)
   */
  String aciklama
  /**
   * Usd FLOAT
   */
  Double usd
  /**
   * Euro FLOAT
   */
  Double euro
  /**
   * Diger FLOAT
   */
  Double diger
  /**
   * KodNo SMALLINT
   */
  Long kodno

  static constraints = {
    sirano nullable: true
    aciklama nullable: true
    usd nullable: true
    euro nullable: true
    diger nullable: true
    kodno nullable: true
  }

  static mapping = {
    table name: 'Yabanci_Para_Pozisyonu'
    //TODO: id name: '?' 
    version false
    sirano column: 'SiraNo'
    aciklama column: 'Aciklama'
    usd column: 'Usd'
    euro column: 'Euro'
    diger column: 'Diger'
    kodno column: 'KodNo'
  }
}

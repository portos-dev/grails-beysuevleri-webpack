/***
 * FOUND COMPOSITE PRIMARY KEY true
 * COMPOSITE_KEY_SIZE: 1
 * FIRST MATCH: [Kod,  Gelorgid]
 * [kod, gelorgid]
 * 
 * 
 *CREATE TABLE hesapisletgel
(
    Kod NVARCHAR(20) NOT NULL,
    Anakod CHAR(2),
    Aciklama NVARCHAR(50),
    Kdvor FLOAT,
    Stokkodu NVARCHAR(20),
    Detay CHAR,
    Gelorgid CHAR NOT NULL,
    Deg1 FLOAT,
    Deg2 FLOAT,
    Deg3 FLOAT,
    Deg4 FLOAT,
    CONSTRAINT PK_hesapisletgel PRIMARY KEY (Kod, Gelorgid)
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Hesapisletgel implements Serializable{

  /**
   * Kod NVARCHAR(20) NOT NULL
   */
  String kod
  /**
   * Anakod CHAR(2)
   */
  String anakod
  /**
   * Aciklama NVARCHAR(50)
   */
  String aciklama
  /**
   * Kdvor FLOAT
   */
  Double kdvor
  /**
   * Stokkodu NVARCHAR(20)
   */
  String stokkodu
  /**
   * Detay CHAR
   */
  String detay
  /**
   * Gelorgid CHAR NOT NULL
   */
  String gelorgid
  /**
   * Deg1 FLOAT
   */
  Double deg1
  /**
   * Deg2 FLOAT
   */
  Double deg2
  /**
   * Deg3 FLOAT
   */
  Double deg3
  /**
   * Deg4 FLOAT
   */
  Double deg4

  static constraints = {
    kod nullable: false
    anakod nullable: true
    aciklama nullable: true
    kdvor nullable: true
    stokkodu nullable: true
    detay nullable: true
    gelorgid nullable: false
    deg1 nullable: true
    deg2 nullable: true
    deg3 nullable: true
    deg4 nullable: true
  }

  static mapping = {
    table name: 'hesapisletgel'
    //TODO: id name: '?' 
    id composite: ['kod','gelorgid']
    version false
    kod column: 'Kod'
    anakod column: 'Anakod'
    aciklama column: 'Aciklama'
    kdvor column: 'Kdvor'
    stokkodu column: 'Stokkodu'
    detay column: 'Detay'
    gelorgid column: 'Gelorgid'
    deg1 column: 'Deg1'
    deg2 column: 'Deg2'
    deg3 column: 'Deg3'
    deg4 column: 'Deg4'
  }
}

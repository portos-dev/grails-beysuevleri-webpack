/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE Kdv_indirilecek
(
    Ayindex SMALLINT,
    Arti INT NOT NULL IDENTITY,
    Fatura_Tarihi DATETIME,
    Fatura_No NVARCHAR(20),
    Soyadi NVARCHAR(30),
    Adi NVARCHAR(30),
    Tckimlikno NVARCHAR(11),
    Vergino NVARCHAR(10),
    Cinsi NVARCHAR(90),
    Miktar FLOAT,
    Tutar FLOAT,
    KDV FLOAT
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Kdv_Indirilecek implements Serializable{

  /**
   * Ayindex SMALLINT
   */
  Long ayindex
  /**
   * Arti INT NOT NULL IDENTITY
   */
  Long arti
  /**
   * Fatura_Tarihi DATETIME
   */
  Date fatura_Tarihi
  /**
   * Fatura_No NVARCHAR(20)
   */
  String fatura_No
  /**
   * Soyadi NVARCHAR(30)
   */
  String soyadi
  /**
   * Adi NVARCHAR(30)
   */
  String adi
  /**
   * Tckimlikno NVARCHAR(11)
   */
  String tckimlikno
  /**
   * Vergino NVARCHAR(10)
   */
  String vergino
  /**
   * Cinsi NVARCHAR(90)
   */
  String cinsi
  /**
   * Miktar FLOAT
   */
  Double miktar
  /**
   * Tutar FLOAT
   */
  Double tutar
  /**
   * KDV FLOAT
   */
  Double kdv

  static constraints = {
    ayindex nullable: true
    arti nullable: false
    fatura_Tarihi nullable: true
    fatura_No nullable: true
    soyadi nullable: true
    adi nullable: true
    tckimlikno nullable: true
    vergino nullable: true
    cinsi nullable: true
    miktar nullable: true
    tutar nullable: true
    kdv nullable: true
  }

  static mapping = {
    table name: 'Kdv_indirilecek'
    //TODO: id name: '?' 
    version false
    ayindex column: 'Ayindex'
    arti column: 'Arti'
    fatura_Tarihi column: 'Fatura_Tarihi'
    fatura_No column: 'Fatura_No'
    soyadi column: 'Soyadi'
    adi column: 'Adi'
    tckimlikno column: 'Tckimlikno'
    vergino column: 'Vergino'
    cinsi column: 'Cinsi'
    miktar column: 'Miktar'
    tutar column: 'Tutar'
    kdv column: 'KDV'
  }
}

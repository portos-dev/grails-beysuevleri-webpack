/***
 * FIXME: NO key found
 * 
 * 
 *CREATE TABLE envanter
(
    Stk NVARCHAR(20),
    Sta NVARCHAR(40),
    Stb NVARCHAR(8),
    Bakm FLOAT,
    Bakt FLOAT,
    Ref INT,
    Kar FLOAT,
    Dbm FLOAT,
    Diam FLOAT,
    Dism FLOAT,
    Dbt FLOAT,
    Diat FLOAT,
    Dist FLOAT,
    Smm FLOAT,
    Ticari CHAR,
    Eyontemi CHAR
)
 */
package beysuevleri.grails.webpack


import groovy.transform.ToString


@ToString(includePackage = false, includeNames = true, includeFields = true, ignoreNulls = true)
class Envanter implements Serializable{

  /**
   * Stk NVARCHAR(20)
   */
  String stk
  /**
   * Sta NVARCHAR(40)
   */
  String sta
  /**
   * Stb NVARCHAR(8)
   */
  String stb
  /**
   * Bakm FLOAT
   */
  Double bakm
  /**
   * Bakt FLOAT
   */
  Double bakt
  /**
   * Ref INT
   */
  Long ref
  /**
   * Kar FLOAT
   */
  Double kar
  /**
   * Dbm FLOAT
   */
  Double dbm
  /**
   * Diam FLOAT
   */
  Double diam
  /**
   * Dism FLOAT
   */
  Double dism
  /**
   * Dbt FLOAT
   */
  Double dbt
  /**
   * Diat FLOAT
   */
  Double diat
  /**
   * Dist FLOAT
   */
  Double dist
  /**
   * Smm FLOAT
   */
  Double smm
  /**
   * Ticari CHAR
   */
  String ticari
  /**
   * Eyontemi CHAR
   */
  String eyontemi

  static constraints = {
    stk nullable: true
    sta nullable: true
    stb nullable: true
    bakm nullable: true
    bakt nullable: true
    ref nullable: true
    kar nullable: true
    dbm nullable: true
    diam nullable: true
    dism nullable: true
    dbt nullable: true
    diat nullable: true
    dist nullable: true
    smm nullable: true
    ticari nullable: true
    eyontemi nullable: true
  }

  static mapping = {
    table name: 'envanter'
    //TODO: id name: '?' 
    version false
    stk column: 'Stk'
    sta column: 'Sta'
    stb column: 'Stb'
    bakm column: 'Bakm'
    bakt column: 'Bakt'
    ref column: 'Ref'
    kar column: 'Kar'
    dbm column: 'Dbm'
    diam column: 'Diam'
    dism column: 'Dism'
    dbt column: 'Dbt'
    diat column: 'Diat'
    dist column: 'Dist'
    smm column: 'Smm'
    ticari column: 'Ticari'
    eyontemi column: 'Eyontemi'
  }
}

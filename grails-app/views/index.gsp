<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Beysu Evleri</title>

    %{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}%

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body >
%{--<content tag="nav">--}%
    %{--<li class="dropdown">--}%
        %{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Application Status <span class="caret"></span></a>--}%
        %{--<ul class="dropdown-menu">--}%
            %{--<li><a href="#">Environment: ${grails.util.Environment.current.name}</a></li>--}%
            %{--<li><a href="#">App profile: ${grailsApplication.config.grails?.profile}</a></li>--}%
            %{--<li><a href="#">App version:--}%
                %{--<g:meta name="info.app.version"/></a>--}%
            %{--</li>--}%
            %{--<li role="separator" class="divider"></li>--}%
            %{--<li><a href="#">Grails version:--}%
                %{--<g:meta name="info.app.grailsVersion"/></a>--}%
            %{--</li>--}%
            %{--<li><a href="#">Groovy version: ${GroovySystem.getVersion()}</a></li>--}%
            %{--<li><a href="#">JVM version: ${System.getProperty('java.version')}</a></li>--}%
            %{--<li role="separator" class="divider"></li>--}%
            %{--<li><a href="#">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</a></li>--}%
        %{--</ul>--}%
    %{--</li>--}%
    %{--<li class="dropdown">--}%
        %{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Artefacts <span class="caret"></span></a>--}%
        %{--<ul class="dropdown-menu">--}%
            %{--<li><a href="#">Controllers: ${grailsApplication.controllerClasses.size()}</a></li>--}%
            %{--<li><a href="#">Domains: ${grailsApplication.domainClasses.size()}</a></li>--}%
            %{--<li><a href="#">Services: ${grailsApplication.serviceClasses.size()}</a></li>--}%
            %{--<li><a href="#">Tag Libraries: ${grailsApplication.tagLibClasses.size()}</a></li>--}%
        %{--</ul>--}%
    %{--</li>--}%

    %{--<li class="dropdown">--}%
        %{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Raporlar <span class="caret"></span></a>--}%
        %{--<ul class="dropdown-menu">--}%
                %{--<li><a ui-sref="app.detayliTahsilat.list">Detayli Tahsilat</a></li>--}%
                %{--<li><a ui-sref="app.eskiTahsilat.list">Eski Tahsilat</a></li>--}%
        %{--</ul>--}%
    %{--</li>--}%
    %{--<portos-menu></portos-menu>--}%
    %{--<portos-login-menu></portos-login-menu>--}%
    %{--<portos-menu></portos-menu>--}%
%{--</content>--}%

%{--<div class="svg" role="presentation">
    <div class="grails-logo-container">
        <asset:image src="grails-cupsonly-logo-white.svg" class="grails-logo"/>
    </div>
</div>--}%

<div  id="content" role="main">
    <section class="row colset-2-its">
        %{--<h1>Beysu Evleri</h1>--}%
        <ul class="nav nav-pills">
            <li ui-sref-active="active"><a ui-sref="app.yevmiye.list"  >GÜNLÜK ÖDEME TAHSİLAT</a></li>
            <li ui-sref-active="active"><a ui-sref="app.detayliTahsilat.list" >DETAYLI TAHSİLAT</a></li>
            <li ui-sref-active="active"><a ui-sref="app.eskiTahsilat.list">ESKİ TAHSİLAT</a></li>
            %{--<li><a href="#">Menu 3</a></li>--}%
        </ul>

        %{--START UIVIEW--}%
        <ui-view></ui-view>
        %{--END UIVIEW--}%

        %{--<div >--}%
            %{--This is angular controlled div--}%
            %{--<div ng-controller="AppController">--}%
                %{--angular.version: {{version}}--}%
            %{--</div>--}%
        %{--<div data-ng-controller="HesplanTreeController as vm">--}%
            %{--Hesplan Tree Here--}%
        %{--</div>--}%

        <div id="app"></div>
        %{--<asset:javascript src="libs/js-xlsx/xlsx.core.min.js"/>--}%
        %{--<asset:javascript src="libs/jsPDF/jspdf.min.js"/>--}%
        %{--<asset:javascript src="libs/jsPDF-AutoTable/jspdf.plugin.autotable.js"/>--}%
        <asset:javascript src="bundle.js" />

        %{--<div id="controllers" role="navigation">--}%
            %{--<h2>Available Controllers:</h2>--}%
            %{--<ul>--}%
                %{--<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--}%
                    %{--<li class="controller">--}%
                        %{--<g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link>--}%
                    %{--</li>--}%
                %{--</g:each>--}%
            %{--</ul>--}%
        %{--</div>--}%
    </section>
</div>
<script>
    angular.bootstrap(document, ['app']);
</script>
</body>
</html>

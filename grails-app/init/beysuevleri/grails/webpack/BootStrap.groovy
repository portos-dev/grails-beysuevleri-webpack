package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.util.Environment

class BootStrap {

    def init = { servletContext ->
        def report = new Report(reportId: 'gunluk_odeme_tahsilat', baseReport: 'gunluk_odeme_tahsilat', reportName: 'Günlük Ödeme Tahsilat')
        report.addToConfig([key:'yevmiye.gmkod.includes',
                            val:
                                    '100 01,\
                                    102 01 003,\
                                    102 01 004,\
                                    102 01 005,\
                                    102 02 001' ])

        report.addToConfig([key:'yevmiye.aciklama.excludes',
                            val:
                                '%Temdit%,%TEMDİT%,%VİRMAN%,%Virman%,%İade%,\
                                %İADE%,%Nakit Çekilen%,%Çekilen%,\
                                NAKİT ÇEKİLEN,\
                                VİRMAN / HESAPLAR ARASI VİRMAN,\
                                TEMDİT / TEMDİT İŞLEMİ,\
                                İADE' ])

        //FIXME: 29-05-2017 yevmiye gunluk odeme tahsilat raporunda aciklama (yerine/birlikte) serino alani 0 degeri olanlarin hesaplamaya dahil edilmemesi istendi
        report.addToConfig([key:'yevmiye.serino.excludes',
                            val: '0,' ])

        report.save(failOnError:true)

        def ongoruReport = new Report(reportId: 'detayli_tahsilat', baseReport: 'detayli_tahsilat', reportName: 'Detaylı Tahsilat')


        //bu gmkod esleme ile  hesplan prefix '121 ' ile donemlere ait odeme planlarina ulasacagiz
        ongoruReport.addToConfig([key:'yevmiye.ongoru.hesplan.gmkod.likes',
                            val: '121 %,' //121 senet hesplan\ 121 17 06  -> 2017 06 doneminde beklenen tahsilatlar
//                            val: '121 % %,120 %' //121 senet hesplan\ 121 17 06  -> 2017 06 doneminde beklenen tahsilatlar
                            // 120 % musteri hesplanlari '120 A4-08'  -> Kemal Ufuk Mit
        ])

        //FIXME: yapilacak tahsilat donem bazinda 121 hesplan altinda 17 06 seklinde 06.2017 donemine ait tahsilat plani
        //FIXME: ve evrakno alaninda son odeme tarihi olarak isleniyor 20.06.2017 gibi
        //FIXME burada beklenen odemenin odeme yapacak musteri icin hangi musterinin hesap plani oldugu stokKodu(stk on db??)
        //FIXME:  alanina A4-08 olarak isleniyor bu deger musterinin ilgili hesap planina ulasmak icin "120 ${stk}" degeri olusturularak
        //FIXME: "120 A4-08" hesplanina ulasarak buna ait yapilan odemeler alacakli alanindaki tutar ve evrak tarihi alanindaki odeme yapildigi tarih
        //FIXME: belirlenerek musterinin beklenen odeme plani ve yaptigi odemeler eslestirilip mevcut odeme plani durumu elde ediliyor
        //FIXME:beklenen tarih ile odeme yapilan tarih karsilastirilip gecikme durumu, yapilan odeme tutari  ve kalan bakiye raporda yansitilabilir


        //TODO: ongoruler ayni veritabanindan alinacaksa? e o zaman datasource ayni veritabani url verilebilir
        //'121 ' ile baslayan hesplanlar tahsilat planlarini iceriyor
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.prefix", val:"121 "])
        // //'121 ' den sonraki degerler ay icin 'yy MM' formati ile formatlanip donem planlanan odemelerin hesap planina ulasiyor
        // 2017 07 ayina ait hesplan icin ay date belirtilen 'yy MM' formati ile str cevrilip '121 ' prefix e eklenerek '121 17 07' kodlu hesplanina ait yevmiyelere bakilacak
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.suffix.dateFormat", val:"yy MM"])
        //aylik hesplan altindaki tum yevmiyelerdeki 'stk' alanina bakilarak hangi musteri hesplani icin deger icerdigi belirleniyor
        // A4-08 gibi


        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.musteri.hesplan.ref.field", val:"stk"])
        //yukaridaki deger asagidaki prefix '120 ' ile birlestirilip musteriye ait hesplana '120 A4-08' e ulasiliyor
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix", val:"120 "])
        //yevmiye tablosunda plandaki beklenen odeme tarihi icin kullanilan alan
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.odeme.plan.tarih.field", val:"evrakno"])
        //dateFormat degeri yoksa zaten alan date kabul edilecek
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat", val:"dd.MM.yyyy"])


        //musteriye ait '120 A4-08' hesplaninda yapilan odemenin hangi tarihte yapildigini belirlemek icin asagidaki alan kullaniliyor
        ongoruReport.addToConfig([key:"yevmiye.tahsilat.hesplan.odeme.tarih.field", val:"evraktarihi"])
        //musteriye ait '120 A4-08' hesplaninda yapilan odemenin tutarinin belirlemek icin asagidaki alan kullaniliyor
        ongoruReport.addToConfig([key:"yevmiye.tahsilat.hesplan.odeme.tutar.field", val:"borclu"])
        ongoruReport.addToConfig([key:"yevmiye.ongoru.hesplan.borc.tutar.field", val:"alacakli"])
        //FIXME: yukaridaki bu degerler kullanilarak musterinin ongorulen plani ve yaptigi odemeler birbiri ile eslenip tamamlanan odemeler
        //FIXME: ve kalan odemeler cikarilip musteri bazinda raporlanacak

//        ongoruReport.addToConfig([key:'yevmiye.aciklama.excludes',
//                            val:
//                                    '%Temdit%,%TEMDİT%,%VİRMAN%,%Virman%,%İade%,\
//                                    %İADE%,%Nakit Çekilen%,%Çekilen%,\
//                                    NAKİT ÇEKİLEN,\
//                                    VİRMAN / HESAPLAR ARASI VİRMAN,\
//                                    TEMDİT / TEMDİT İŞLEMİ,\
//                                    İADE' ])

        //Deprecated yukaridaki config bunu iceriyor
        //gelecek tahsilatlar icin olusturulan yevmiyelerin evraktarihi kayit tarihini gosteriyor.
        // bu yuzden bir baska alan beklenen tahsilat tarihini belirliyor olmali
        //beysu evleri bu is icin evrakno alanini dd.MM.yyyy formatinda kullanmis
//        ongoruReport.addToConfig([key:'yevmiye.tahsilattarihi.alan',
//                            val: 'evrakno' ])
//        ongoruReport.addToConfig([key:'yevmiye.tahsilattarihi.pattern',
//                            val: 'dd.MM.yyyy' ])
        //FIXME: donemler aciklama alaninda belirtilmis bunlari parse etmek icin de bir regex config kullanilabilir

        ongoruReport.save(failOnError:true)


        def confVal = report.config('yevmiye.aciklama.excludes')
        println confVal

        def confVal2 = ongoruReport.config('yevmiye.gmkod.likes')
        println confVal2

        println "environment: ${Environment.current.name}"


        Yevmiye.list(max:10)/*.each {
            println it
        }*/

        Tenants.withId("ongoru") {
            Yevmiye.findAll("from Yevmiye as b where length(b.evrakno) = 10 order by b.evraktarihi desc", [max: 10])
            /*Yevmiye.where {
                evrakno.length == 12
            }.list(max: 10, sort: 'evraktarihi', order:'desc')*/.each {
                println it
            }
        }

        Logyev.list(max:10)/*.each {
            println it
        }*/

        Hesplan.list(max:10)/*.each {
            println it
        }*/
    }
    def destroy = {
    }
}

package beysuevleri.grails.webpack

import grails.databinding.BindingFormat
import groovy.transform.ToString

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class YevmiyeController {
    def yevmiyeService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max, Integer offset, YevmiyeQueryCommand query) {
        params.max = Math.min(max ?: 10, 100)

        //TODO search with baslangicTarihi, bitisTarihi ve diger alanlar
        respond ([items:Yevmiye.where{
        }.list(params), count: Yevmiye.count(),
                max: params.max,
                offset: offset ?: 0])
    }

    def query(YevmiyeQueryCommand filterQuery){

        def reportConfig = Report.findByReportId('gunluk_odeme_tahsilat')
        if(!filterQuery.gmkods){
            filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
        }
        if(!filterQuery.aciklamaExcludes){
            filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
        }

        if(!filterQuery.serinoExcludes){
            filterQuery.serinoExcludes = reportConfig.config('')
        }

        println filterQuery

        def queryParams = [:]
        def queryCriteria = []

        def dateField = 'evraktarihi' //'fistar'  ?


        params.max = filterQuery.max ?: 25
        params.offset = filterQuery.offset ?: 0
        params.sort = filterQuery.sort ?: dateField
        params.order = filterQuery.order ?: 'asc'


       /* def criteria = Yevmiye.where {*/
            if (filterQuery.gmkod) {
//                gmkod == filterQuery.gmkod
                queryParams['gmkod'] = filterQuery.gmkod
                queryCriteria << 'gmkod = :gmkod'
            }
//            if(cmd.baslangicTarihi && cmd.bitisTarihi){
//                //TODO between ?
//            }
            if(filterQuery.baslangicTarihi){
//                fistar >= filterQuery.baslangicTarihi
                queryParams['baslangicTarihi'] = filterQuery.baslangicTarihi
                queryCriteria << "${dateField} >= :baslangicTarihi"
            }
            if(filterQuery.bitisTarihi){
//                fistar <= filterQuery.baslangicTarihi
                queryParams['bitisTarihi'] = filterQuery.bitisTarihi
                queryCriteria << "${dateField}  <= :bitisTarihi"
            }

            if(filterQuery.gmkods){
//                gmkod in filterQuery.hesplans.kod
                queryParams['gmkods'] = filterQuery.gmkods
                queryCriteria << 'gmkod in :gmkods'
            }

            if(filterQuery.aciklamaExcludes){
                filterQuery.aciklamaExcludes.each {
                    String exc ->
//                        aciklama ==~ exc
//                        queryParams
                        queryCriteria << "aciklama not like '${exc}'"
                }
            }
        if(filterQuery.aciklamaLike){
            queryCriteria << "lower(aciklama) like lower('%$filterQuery.aciklamaLike%')"
        }

//            aciklama ==~ filterQuery.excludes.first()
//            aciklama =~ filterQuery.excludes.last()
     /*   }*/

        def query = " from Yevmiye yevmiye "
        def finalQuery = "select yevmiye ${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')} order by $params.sort $params.order"
        def countQuery = "select count (*) ${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')}"
        def results = Yevmiye.executeQuery(finalQuery, queryParams, params )
        Long count = Yevmiye.executeQuery(countQuery, queryParams)[0]

        respond ([items: results,
                  count: count.toInteger(),
                  max: params.max,
                  offset: params.offset,
                  sort: params.sort,
                  order: params.order,
        ])


//        respond yevmiyeService.search(cmd)
    }

    def show(Long refno, Long refno2) {
        respond Yevmiye.findByRefnoAndRefno2(refno, refno2)
    }

    def children(Yevmiye yevmiye){
        respond Logyev.findAllByRefnoAndRefno2(yevmiye.refno, yevmiye.refno2)
    }

    def create() {
        respond new Yevmiye(params)
    }

    @Transactional
    def save(Yevmiye yevmiye) {
        if (yevmiye == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (yevmiye.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond yevmiye.errors, view:'create'
            return
        }

        yevmiye.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'yevmiye.label', default: 'Yevmiye'), yevmiye.id])
                redirect yevmiye
            }
            '*' { respond yevmiye, [status: CREATED] }
        }
    }

    def edit(Yevmiye yevmiye) {
        respond yevmiye
    }

    @Transactional
    def update(Yevmiye yevmiye) {
        if (yevmiye == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (yevmiye.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond yevmiye.errors, view:'edit'
            return
        }

        yevmiye.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'yevmiye.label', default: 'Yevmiye'), yevmiye.id])
                redirect yevmiye
            }
            '*'{ respond yevmiye, [status: OK] }
        }
    }

    @Transactional
    def delete(Yevmiye yevmiye) {

        if (yevmiye == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        yevmiye.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'yevmiye.label', default: 'Yevmiye'), yevmiye.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'yevmiye.label', default: 'Yevmiye'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
@ToString(includePackage = false, includeNames = true, includeFields = true,
        ignoreNulls = true, includes = ['gmkod','baslangicTarihi','bitisTarihi'])
class YevmiyeQueryCommand{
    @BindingFormat('dd-MM-yyyy')
    Date baslangicTarihi
    @BindingFormat('dd-MM-yyyy')
    Date bitisTarihi
    String gmkod

    String gmkodLike
    List<String> gmkodLikes
    String aciklamaLike
    Long refno
    Long refno2
    Integer max
    Integer offset
    String sort
    String order
    List<String> serinoExcludes

    //TODO: read from applicatiın.yml
    def aciklamaExcludes /*= [
            '%Temdit%', '%TEMDİT%', '%VİRMAN%', '%Virman%', '%İade%',
            '%İADE%', '%Nakit Çekilen%', '%Çekilen%',
//            '%NAKİT ÇEKİLEN%',
'NAKİT ÇEKİLEN',

'VİRMAN / HESAPLAR ARASI VİRMAN',

'TEMDİT / TEMDİT İŞLEMİ',

'İADE',
    ]*/

    List<String> gmkods /*= [
            '100 01', // MERKEZ KASA

//            [kod: '102'], //BANKALAR

//            [kod:'102 01'], //VADESIZ HESAPLAR
//            [kod:'102 01 001'] //AKBANK ignore
//            [kod:'102 01 002'] //TEB ignore
            '102 01 003', //ISBANK
            '102 01 004', // ŞEKERBANK
            '102 01 005', // ŞEKERBANK EK HESAP

//            [kod:'102 02'], //VADELI HESAPLAR
            '102 02 001', //ŞEKERBANK VADELİ HESAP

    ]*/

    static boolean defaultNullable() {
        true
    }
    static constraints = {
        baslangicTarihi(nullable:true)
        bitisTarihi(nullable:true)
        gmkod(blank: true, minSize: 3)
        refno(nullable: true)
        refno2(nullable: true)
        max(nullable: true)
        offset(nullable: true)
        sort(nullable: true, blank: true)
        order(nullable: true, blank:true)
        aciklamaLike(nullable: true, blank: false)
    }
}

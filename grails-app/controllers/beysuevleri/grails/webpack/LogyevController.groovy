package beysuevleri.grails.webpack

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogyevController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def logyevService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Logyev.list(params), model:[logyevCount: Logyev.count()]
    }

    def query(YevmiyeQueryCommand filterQuery){
        println filterQuery

        def queryParams = [:]
        def queryCriteria = []


        params.max = filterQuery.max ?: 25
        params.offset = filterQuery.offset ?: 0
        params.sort = filterQuery.sort ?: 'evraktarihi'
        params.order = filterQuery.order ?: 'asc'


        /* def criteria = Yevmiye.where {*/
        if (filterQuery.gmkod) {
//                gmkod == filterQuery.gmkod
            queryParams['gmkod'] = filterQuery.gmkod
            queryCriteria << 'gmkod = :gmkod'
        }
//            if(cmd.baslangicTarihi && cmd.bitisTarihi){
//                //TODO between ?
//            }
        if(filterQuery.baslangicTarihi){
//                fistar >= filterQuery.baslangicTarihi
            queryParams['baslangicTarihi'] = filterQuery.baslangicTarihi
            queryCriteria << 'evraktarihi >= :baslangicTarihi'
        }
        if(filterQuery.bitisTarihi){
//                fistar <= filterQuery.baslangicTarihi
            queryParams['bitisTarihi'] = filterQuery.bitisTarihi
            queryCriteria << 'evraktarihi <= :bitisTarihi'
        }

        if(filterQuery.gmkods){
//                gmkod in filterQuery.hesplans.kod
            queryParams['gmkods'] = filterQuery.gmkods
            queryCriteria << 'gmkod in :gmkods'
        }

        if(filterQuery.aciklamaExcludes){
            filterQuery.aciklamaExcludes.each {
                String exc ->
//                        aciklama ==~ exc
//                        queryParams
                    queryCriteria << "aciklama not like '${exc}'"
            }
        }

//            aciklama ==~ filterQuery.excludes.first()
//            aciklama =~ filterQuery.excludes.last()
        /*   }*/

        def query = " from Logyev logyev "
        def maxCriteria = " logyev.arti = (select max(arti) from Logyev logyev2 where sp_id = logyev.sp_Id group by sp_Id ) "
        def finalQuery = "select logyev ${query} where $maxCriteria and ${queryCriteria.join(' and ')} order by $params.sort $params.order"
        def countQuery = "select count (*) ${query} where $maxCriteria and ${queryCriteria.join(' and ')}"
        def results = Logyev.executeQuery(finalQuery, queryParams, params )
        Long count = Logyev.executeQuery(countQuery, queryParams)[0]

        respond ([items: results,
                  count: count.toInteger(),
                  max: params.max,
                  offset: params.offset,
                  sort: params.sort,
                  order: params.order,
        ])


//        respond yevmiyeService.search(cmd)
    }

    def show(Logyev logyev) {
        respond logyev
    }

    def create() {
        respond new Logyev(params)
    }

    @Transactional
    def save(Logyev logyev) {
        if (logyev == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (logyev.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond logyev.errors, view:'create'
            return
        }

        logyev.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logyev.label', default: 'Logyev'), logyev.id])
                redirect logyev
            }
            '*' { respond logyev, [status: CREATED] }
        }
    }

    def edit(Logyev logyev) {
        respond logyev
    }

    @Transactional
    def update(Logyev logyev) {
        if (logyev == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (logyev.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond logyev.errors, view:'edit'
            return
        }

        logyev.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'logyev.label', default: 'Logyev'), logyev.id])
                redirect logyev
            }
            '*'{ respond logyev, [status: OK] }
        }
    }

    @Transactional
    def delete(Logyev logyev) {

        if (logyev == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        logyev.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'logyev.label', default: 'Logyev'), logyev.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logyev.label', default: 'Logyev'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

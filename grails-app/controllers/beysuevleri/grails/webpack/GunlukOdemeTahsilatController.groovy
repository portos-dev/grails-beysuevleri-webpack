package beysuevleri.grails.webpack


import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.BorderFormatting
import org.apache.poi.ss.usermodel.BuiltinFormats
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.ComparisonOperator
import org.apache.poi.ss.usermodel.ConditionalFormattingRule
import org.apache.poi.ss.usermodel.DataFormat
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.FormulaEvaluator
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.PatternFormatting
import org.apache.poi.ss.usermodel.PrintSetup
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.SheetConditionalFormatting
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.text.SimpleDateFormat

@Transactional(readOnly = true)
class GunlukOdemeTahsilatController {
	static responseFormats = ['json', 'xml']

    def yevmiyeService

    def poi(YevmiyeQueryCommand filterQuery){
        def reportConfig = Report.findByReportId('gunluk_odeme_tahsilat')
        if(!filterQuery.gmkods){
            filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
        }
        if(!filterQuery.aciklamaExcludes){
            filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
        }
        if(!filterQuery.serinoExcludes){
            filterQuery.serinoExcludes = reportConfig.config('')
        }

        def tekinVeyaEmirStyle = [:]

        def results = yevmiyeService.search(filterQuery).items

        Workbook wb = new XSSFWorkbook()
        Sheet sheet = wb.createSheet('GUNLUK_ODEME_TAHSILAT')
        sheet.printSetup.landscape = true
        sheet.printSetup.paperSize = PrintSetup.A3_PAPERSIZE
        sheet.printGridlines = false
        sheet.autobreaks = false
        sheet.setColumnWidth(0, 100)
        sheet.setDefaultRowHeight((short)300)

        Font boldFont = wb.createFont()
        boldFont.boldweight = Font.BOLDWEIGHT_BOLD

        Font fontRed = wb.createFont()
        fontRed.color = Font.COLOR_RED

        Font fontRedBold = wb.createFont()
        fontRedBold.color = Font.COLOR_RED
        fontRedBold.boldweight = Font.BOLDWEIGHT_BOLD


        Font boldFontRed = wb.createFont()
        boldFontRed.boldweight = Font.BOLDWEIGHT_BOLD
        boldFontRed.color = Font.COLOR_RED

        DataFormat df = wb.createDataFormat()
        def tutarFormat = df.getFormat('#,##0.00')

        final DEFAULT_BORDER = CellStyle.BORDER_MEDIUM

        CellStyle csDefault = wb.createCellStyle()
//        csDefault.setFillForegroundColor IndexedColors.WHITE.index
//        csDefault.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csDefaultFgYellow = wb.createCellStyle()
        csDefaultFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csDefaultFgYellow.setFillPattern PatternFormatting.SOLID_FOREGROUND

        tekinVeyaEmirStyle[csDefault] = csDefaultFgYellow


        CellStyle csDate = wb.createCellStyle()
        csDate.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
//        csDate.setFillForegroundColor IndexedColors.WHITE.index
//        csDate.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csDateFgYellow = wb.createCellStyle()
        csDateFgYellow.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
        csDateFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csDateFgYellow.setFillPattern PatternFormatting.SOLID_FOREGROUND

        tekinVeyaEmirStyle[csDate] = csDateFgYellow

        CellStyle csM2 = wb.createCellStyle()
        csM2.dataFormat = tutarFormat
        csM2.alignment = CellStyle.ALIGN_LEFT
//        csM2.setFillForegroundColor IndexedColors.WHITE.index
//        csM2.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csM2FgYellow = wb.createCellStyle()
        csM2FgYellow.dataFormat = tutarFormat
        csM2FgYellow.alignment = CellStyle.ALIGN_LEFT
        csM2FgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csM2FgYellow.setFillPattern PatternFormatting.SOLID_FOREGROUND

        tekinVeyaEmirStyle[csM2] = csM2FgYellow

        CellStyle csDateRed = wb.createCellStyle()
        csDateRed.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
        csDateRed.font = fontRed
//        csDateRed.setFillForegroundColor IndexedColors.WHITE.index
//        csDateRed.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csDateRedFgYellow = wb.createCellStyle()
        csDateRedFgYellow.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
        csDateRedFgYellow.font = fontRed
        csDateRedFgYellow.setFillForegroundColor IndexedColors.GREEN.index
        csDateRedFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csDateRed] = csDateRedFgYellow

        CellStyle csLeftBorder = wb.createCellStyle()
        csLeftBorder.borderLeft = DEFAULT_BORDER
//        csLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csLeftBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csLeftBorderFgYellow = wb.createCellStyle()
        csLeftBorderFgYellow.borderLeft = DEFAULT_BORDER
        csLeftBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csLeftBorderFgYellow.setFillPattern PatternFormatting.SOLID_FOREGROUND

        tekinVeyaEmirStyle[csLeftBorder] = csLeftBorderFgYellow

        CellStyle csRightBorder = wb.createCellStyle()
        csRightBorder.borderRight = DEFAULT_BORDER
//        csRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csRightBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csRightBorderFgYellow = wb.createCellStyle()
        csRightBorderFgYellow.borderRight = DEFAULT_BORDER
        csRightBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csRightBorderFgYellow.setFillPattern PatternFormatting.SOLID_FOREGROUND

        tekinVeyaEmirStyle[csRightBorder] = csRightBorderFgYellow

        CellStyle csTopLeftBorder = wb.createCellStyle()
        csTopLeftBorder.borderLeft = DEFAULT_BORDER
        csTopLeftBorder.borderTop = DEFAULT_BORDER
//        csTopLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csTopLeftBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csTopLeftBorderFgYellow = wb.createCellStyle()
        csTopLeftBorderFgYellow.borderLeft = DEFAULT_BORDER
        csTopLeftBorderFgYellow.borderTop = DEFAULT_BORDER
        csTopLeftBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csTopLeftBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csTopLeftBorder] = csTopLeftBorderFgYellow

        CellStyle csBottomLeftBorder = wb.createCellStyle()
        csBottomLeftBorder.borderLeft = DEFAULT_BORDER
        csBottomLeftBorder.borderBottom = DEFAULT_BORDER
//        csBottomLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csBottomLeftBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBottomLeftBorderFgYellow = wb.createCellStyle()
        csBottomLeftBorderFgYellow.borderLeft = DEFAULT_BORDER
        csBottomLeftBorderFgYellow.borderBottom = DEFAULT_BORDER
        csBottomLeftBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBottomLeftBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBottomLeftBorder] = csBottomLeftBorderFgYellow

        CellStyle csTopRightBorder = wb.createCellStyle()
        csTopRightBorder.borderRight = DEFAULT_BORDER
        csTopRightBorder.borderTop = DEFAULT_BORDER
//        csTopRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csTopRightBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csTopRightBorderFgYellow = wb.createCellStyle()
        csTopRightBorderFgYellow.borderRight = DEFAULT_BORDER
        csTopRightBorderFgYellow.borderTop = DEFAULT_BORDER
        csTopRightBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csTopRightBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csTopRightBorder] = csTopRightBorderFgYellow

        CellStyle csBottomBorder = wb.createCellStyle()
        csBottomBorder.borderBottom = DEFAULT_BORDER
//        csBottomBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csBottomBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBottomBorderFgYellow = wb.createCellStyle()
        csBottomBorderFgYellow.borderBottom = DEFAULT_BORDER
        csBottomBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBottomBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBottomBorder] = csBottomBorderFgYellow

        CellStyle csTopBorder = wb.createCellStyle()
        csTopBorder.borderTop = DEFAULT_BORDER
//        csTopBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csTopBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csTopBorderFgYellow = wb.createCellStyle()
        csTopBorderFgYellow.borderTop = DEFAULT_BORDER
        csTopBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csTopBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csTopBorder] = csTopBorderFgYellow

        CellStyle csBottomRightBorder = wb.createCellStyle()
        csBottomRightBorder.borderRight = DEFAULT_BORDER
        csBottomRightBorder.borderBottom = DEFAULT_BORDER
//        csBottomRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//        csBottomRightBorder.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBottomRightBorderFgYellow = wb.createCellStyle()
        csBottomRightBorderFgYellow.borderRight = DEFAULT_BORDER
        csBottomRightBorderFgYellow.borderBottom = DEFAULT_BORDER
        csBottomRightBorderFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBottomRightBorderFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBottomRightBorder] = csBottomRightBorderFgYellow

        CellStyle csBold = wb.createCellStyle()
        csBold.font = boldFont
//        csBold.setFillForegroundColor IndexedColors.WHITE.index
//        csBold.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBoldFgYellow = wb.createCellStyle()
        csBoldFgYellow.font = boldFont
        csBoldFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBoldFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBold] = csBoldFgYellow

        CellStyle csBoldAlignLeft = wb.createCellStyle()
        csBoldAlignLeft.font = boldFont
        csBoldAlignLeft.alignment = CellStyle.ALIGN_LEFT
//        csBoldAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//        csBoldAlignLeft.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBoldAlignLeftFgYellow = wb.createCellStyle()
        csBoldAlignLeftFgYellow.font = boldFont
        csBoldAlignLeftFgYellow.alignment = CellStyle.ALIGN_LEFT
        csBoldAlignLeftFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBoldAlignLeftFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBoldAlignLeft] = csBoldAlignLeftFgYellow

        CellStyle csAlignLeft = wb.createCellStyle()
        csAlignLeft.alignment = CellStyle.ALIGN_LEFT
//        csAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//        csAlignLeft.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csAlignLeftFgYellow = wb.createCellStyle()
        csAlignLeftFgYellow.alignment = CellStyle.ALIGN_LEFT
        csAlignLeftFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csAlignLeftFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csAlignLeft] = csAlignLeftFgYellow

        CellStyle csBoldAlignRight = wb.createCellStyle()
        csBoldAlignRight.font = boldFont
        csBoldAlignRight.alignment = CellStyle.ALIGN_RIGHT
//        csBoldAlignRight.setFillForegroundColor IndexedColors.WHITE.index
//        csBoldAlignRight.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBoldAlignRightFgYellow = wb.createCellStyle()
        csBoldAlignRightFgYellow.font = boldFont
        csBoldAlignRightFgYellow.alignment = CellStyle.ALIGN_RIGHT
        csBoldAlignRightFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBoldAlignRightFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBoldAlignRight] = csBoldAlignRightFgYellow

        CellStyle csTutar = wb.createCellStyle()
        csTutar.dataFormat = tutarFormat
//        csTutar.setFillForegroundColor IndexedColors.WHITE.index
//        csTutar.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csTutarFgYellow = wb.createCellStyle()
        csTutarFgYellow.dataFormat = tutarFormat
        csTutarFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csTutarFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csTutar] = csTutarFgYellow

        CellStyle csTutarRed = wb.createCellStyle()
        csTutarRed.dataFormat = tutarFormat
        csTutarRed.font = fontRed

        CellStyle csTutarRedBold = wb.createCellStyle()
        csTutarRedBold.dataFormat = tutarFormat
        csTutarRedBold.font = fontRedBold

//        csTutarRed.setFillForegroundColor IndexedColors.WHITE.index
//        csTutarRed.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csTutarRedFgYellow = wb.createCellStyle()
        csTutarRedFgYellow.dataFormat = tutarFormat
        csTutarRedFgYellow.font = fontRed
        csTutarRedFgYellow.setFillForegroundColor IndexedColors.GREEN.index
        csTutarRedFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csTutarRed] = csTutarRedFgYellow

        CellStyle csBoldTutar = wb.createCellStyle()
        csBoldTutar.font = boldFont
        csBoldTutar.dataFormat = tutarFormat
//        csBoldTutar.setFillForegroundColor IndexedColors.WHITE.index
//        csBoldTutar.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBoldTutarFgYellow = wb.createCellStyle()
        csBoldTutarFgYellow.font = boldFont
        csBoldTutarFgYellow.dataFormat = tutarFormat
        csBoldTutarFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBoldTutarFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBoldTutar] = csBoldTutarFgYellow

        CellStyle csBoldTutarAlignLeft = wb.createCellStyle()
        csBoldTutarAlignLeft.font = boldFont
        csBoldTutarAlignLeft.dataFormat = tutarFormat
        csBoldTutarAlignLeft.alignment = CellStyle.ALIGN_LEFT
//        csBoldTutarAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//        csBoldTutarAlignLeft.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        CellStyle csBoldTutarAlignLeftFgYellow = wb.createCellStyle()
        csBoldTutarAlignLeftFgYellow.font = boldFont
        csBoldTutarAlignLeftFgYellow.dataFormat = tutarFormat
        csBoldTutarAlignLeftFgYellow.alignment = CellStyle.ALIGN_LEFT
        csBoldTutarAlignLeftFgYellow.setFillForegroundColor IndexedColors.YELLOW.index
        csBoldTutarAlignLeftFgYellow.setFillPattern(PatternFormatting.SOLID_FOREGROUND )

        tekinVeyaEmirStyle[csBoldTutarAlignLeft] = csBoldTutarAlignLeftFgYellow

        CellStyle rowStyle = wb.createCellStyle()
        rowStyle.setFillForegroundColor IndexedColors.LIGHT_CORNFLOWER_BLUE.index
        rowStyle.setFillPattern PatternFormatting.BIG_SPOTS


        def kodMap = [
                '100 01': 'KASA',
                '102 01 003': 'İŞBANK',
                '102 01 004': 'ŞEKERBANK',
                '102 01 005': 'ŞEKERBANK EK HES',
        ]

        def turMap = [
                '100 01': '',
                '102 01 003': 'EFT',
                '102 01 004': 'EFT',
                '102 01 005': 'EFT',
        ]

        def startRow = 5
        def endRow
        def currentRow = startRow
        def bakiye = 0
        
        def fields =            ['firma', 'tarih', 'masrafYeri', 'tur', 'aciklama', 'giderYeri', 'aciklama2', 'tahsilat', 'odeme', 'bakiye']
        def fieldValClosure =   [{}]

        def donemSdf = new SimpleDateFormat('MMMM yyyy', new Locale('tr', 'TR'))

        def rowsPerDonem = [:]

        def currentDonem //= donemSdf.format(results.first().fistar)
        def currentDonemTahsilat = 0
        def currentDonemOdeme = 0
        def currentBakiye = 0

        def tahsilatColumn
        def odemeColumn
        def bakiyeColumn

        results.eachWithIndex { Yevmiye y, int idx ->
            def currentColumn = 0

            currentDonem = donemSdf.format(y.fistar)
            if(!rowsPerDonem[currentDonem]) rowsPerDonem[currentDonem] = []

            Row row = sheet.createRow(currentRow)
            Cell firmaCell = row.createCell(currentColumn)
            firmaCell.cellStyle = csDefault
            firmaCell.setCellValue firma(y.masrafmerkezi)
            checkStyle(y, firmaCell, tekinVeyaEmirStyle)

            currentColumn++

            Cell tarihCell = row.createCell(currentColumn)
            tarihCell.cellStyle = csDate
            tarihCell.setCellValue y.fistar
            checkStyle(y, tarihCell, tekinVeyaEmirStyle)
            currentColumn++

            Cell masrafYeriCell = row.createCell(currentColumn)
            masrafYeriCell.cellStyle = csDefault
            masrafYeriCell.setCellValue kodMap[y.gmkod]
            checkStyle(y, masrafYeriCell, tekinVeyaEmirStyle)
            currentColumn++

            Cell turCell = row.createCell(currentColumn)
            turCell.cellStyle = csDefault
            turCell.setCellValue turMap[y.gmkod]
            checkStyle(y, turCell, tekinVeyaEmirStyle)
            currentColumn++

            Cell aciklamaCell = row.createCell(currentColumn)
            aciklamaCell.cellStyle = csDefault
//            aciklamaCell.setCellValue '?'//y.aciklama
            checkStyle(y, aciklamaCell, tekinVeyaEmirStyle)
            currentColumn++

            Cell giderYeriCell = row.createCell(currentColumn)
            giderYeriCell.cellStyle = csDefault
            giderYeriCell.setCellValue giderYeri(y)//'?' //turMap[y.gmkod]
            checkStyle(y, giderYeriCell, tekinVeyaEmirStyle)
            currentColumn++

            Cell aciklama2Cell = row.createCell(currentColumn)
            aciklama2Cell.cellStyle = csDefault
            aciklama2Cell.setCellValue y.aciklama
            checkStyle(y, aciklama2Cell, tekinVeyaEmirStyle)
            currentColumn++

            Cell tahsilatCell = row.createCell(currentColumn)
            tahsilatCell.cellStyle = csTutar
            tahsilatCell.setCellValue y.borclu
            checkStyle(y, tahsilatCell, tekinVeyaEmirStyle)
            tahsilatColumn = currentColumn
            currentColumn++
            
            Cell odemeCell = row.createCell(currentColumn)
            odemeCell.cellStyle = csTutar
            odemeCell.setCellValue y.alacakli
            checkStyle(y, odemeCell, tekinVeyaEmirStyle)
            odemeColumn = currentColumn
            currentColumn++

            bakiye += y.borclu ?: 0
            bakiye -= y.alacakli ?: 0

            Cell bakiyeCell = row.createCell(currentColumn)
            bakiyeCell.cellStyle = csTutar
            bakiyeColumn = currentColumn


            String odemeCellRef = "${CellReference.convertNumToColString(odemeColumn)}${currentRow+1}"
            String tahsilatCellRef = "${CellReference.convertNumToColString(tahsilatColumn)}${currentRow+1}"
            if(idx == 0){
                bakiyeCell.setCellValue ( (y.borclu?: 0) - (y.alacakli ?: 0) )
            }
            else {
                String previousBakiyeCellRef = "${CellReference.convertNumToColString(bakiyeColumn)}${currentRow}"
                bakiyeCell.setCellFormula("$tahsilatCellRef - $odemeCellRef + $previousBakiyeCellRef")
            }
            //checkStyle(y, bakiyeCell, tekinVeyaEmirStyle)


            currentColumn++

            rowsPerDonem[currentDonem] << row

            currentRow++

            //TODO: HSSFConditionalFormattingRule
            //TODO: HSSFConditionalFormatting
            //TODO: HSSFSheetConditionalFormatting


//            row.setRowStyle(rowStyle)
        }

        endRow = currentRow




        for(Map.Entry donemRows : rowsPerDonem){
            String donem = donemRows.key
            List<Row> rows = donemRows.value

            def firstRow = rows.first().getRowNum()
            def lastRow = rows.last().getRowNum()

            String odemeCellFirstRef = "${CellReference.convertNumToColString(odemeColumn)}${firstRow+1}"
            String odemeCellLastRef = "${CellReference.convertNumToColString(odemeColumn)}${lastRow+1}"
            String tahsilatCellFirstRef = "${CellReference.convertNumToColString(tahsilatColumn)}${firstRow+1}"
            String tahsilatCellLastRef = "${CellReference.convertNumToColString(tahsilatColumn)}${lastRow+1}"
            String bakiyeCellLastRef = "${CellReference.convertNumToColString(bakiyeColumn)}${lastRow+1}"

            String oncekiDonemBakiyeCellLastRef

            def oncekiDonem = donemSdf.format(donemSdf.parse(donem).minus(2))
            def sonrakiDonem = donemSdf.format(donemSdf.parse(donem).plus(32))

            Row topBorderRow = sheet.getRow(rowsPerDonem[oncekiDonem] ? lastRow - 7 : lastRow -6)
            if(topBorderRow) {
                Cell br1 = topBorderRow.createCell(bakiyeColumn+1)
                Cell br2 = topBorderRow.createCell(bakiyeColumn+2)
                br1.cellStyle = wb.createCellStyle()
                br1.cellStyle.borderBottom = DEFAULT_BORDER
                br2.cellStyle = br1.cellStyle
            }

            if(rowsPerDonem[oncekiDonem]) {
                oncekiDonemBakiyeCellLastRef = "${CellReference.convertNumToColString(bakiyeColumn)}${rowsPerDonem[oncekiDonem].last().getRowNum()+1}"

                Row oncekiDonemDevirBakiye = sheet.getRow(lastRow - 6)
                Cell oncekiDonemDevirBakiyeC1 = oncekiDonemDevirBakiye.createCell(bakiyeColumn + 1)
                oncekiDonemDevirBakiyeC1.setCellValue "${oncekiDonem.toUpperCase()} DEVİR"
                oncekiDonemDevirBakiyeC1.cellStyle.font = boldFontRed

                Cell oncekiDonemDevirBakiyeC2 =oncekiDonemDevirBakiye.createCell(bakiyeColumn + 2)
                oncekiDonemDevirBakiyeC2.setCellFormula "${oncekiDonemBakiyeCellLastRef}"
                oncekiDonemDevirBakiyeC2.cellStyle = csTutarRedBold
            }

            Row donemTahsilat = sheet.getRow(lastRow -5)
            Cell donemTahsilatCell1 = donemTahsilat.createCell(bakiyeColumn+1)
            donemTahsilatCell1.setCellValue "${donem.toUpperCase()} TAHSİLAT"
            donemTahsilatCell1.cellStyle.font = boldFontRed
            Cell donemTahsilatCell2 = donemTahsilat.createCell(bakiyeColumn+2)
            donemTahsilatCell2.cellStyle = csTutarRedBold
            donemTahsilatCell2.setCellFormula("SUM($tahsilatCellFirstRef:$tahsilatCellLastRef)")


            Row donemHarcama = sheet.getRow(lastRow -4)
            Cell donemHarcamaCell1 = donemHarcama.createCell(bakiyeColumn+1)
            donemHarcamaCell1.setCellValue "${donem.toUpperCase()} HARCAMA"
            donemHarcamaCell1.cellStyle.font = boldFontRed
            Cell donemHarcamaCell2 = donemHarcama.createCell(bakiyeColumn+2)
            donemHarcamaCell2.cellStyle = csTutarRedBold
            donemHarcamaCell2.setCellFormula("SUM($odemeCellFirstRef:$odemeCellLastRef)")

            Row sonrakiDonemDevir = sheet.getRow(lastRow -3)
            Cell donemDevirCell1 = sonrakiDonemDevir.createCell(bakiyeColumn+1)
            donemDevirCell1.setCellValue "${sonrakiDonem.toUpperCase()} DEVİR"
            donemDevirCell1.cellStyle.font = boldFontRed
            Cell donemDevirCell2 = sonrakiDonemDevir.createCell(bakiyeColumn+2)
            donemDevirCell2.cellStyle = csTutarRedBold
            donemDevirCell2.setCellFormula("$bakiyeCellLastRef")

            Row bottomBorderRow = sheet.getRow(lastRow+1)
            if(bottomBorderRow) {
                Cell br1 = bottomBorderRow.createCell(bakiyeColumn+1)
                Cell br2 = bottomBorderRow.createCell(bakiyeColumn+2)
                br1.cellStyle = wb.createCellStyle()
                br1.cellStyle.borderTop = DEFAULT_BORDER
                br2.cellStyle = br1.cellStyle
            }

        }


        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator()
        for (Row r : sheet) {
            for (Cell c : r) {
                if (c.getCellType() == Cell.CELL_TYPE_FORMULA) {
                    evaluator.evaluateFormulaCell(c)
                }
            }
        }
        //TODO: The $A2 component creates a relative address, which updates with each row: G4, G5, G6, and so on

        sheet.sheetConditionalFormatting.createConditionalFormattingRule(ComparisonOperator.EQUAL, "\$${CellReference.convertNumToColString(0)}2")
        SheetConditionalFormatting sheetCF = sheet.sheetConditionalFormatting

        ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule('OR($A6="TEKİN",$A6="EMİR")')
        PatternFormatting fill1 = rule1.createPatternFormatting()
        BorderFormatting brd1 = rule1.createBorderFormatting()
        brd1.borderTop = BorderFormatting.BORDER_THIN
        brd1.borderBottom = BorderFormatting.BORDER_THIN
//        fill1.setFillForegroundColor IndexedColors.YELLOW.index
        fill1.setFillPattern PatternFormatting.SOLID_FOREGROUND
        fill1.setFillBackgroundColor IndexedColors.YELLOW.index
//
        ConditionalFormattingRule rule2 = sheetCF.createConditionalFormattingRule('NOT(OR($A6="TEKİN",$A6="EMİR"))')
        PatternFormatting fill2 = rule2.createPatternFormatting()
        BorderFormatting brd2 = rule2.createBorderFormatting()
        brd2.borderTop = BorderFormatting.BORDER_THIN
        brd2.borderBottom = BorderFormatting.BORDER_THIN
//        fill1.setFillForegroundColor IndexedColors.YELLOW.index
        fill2.setFillPattern PatternFormatting.SOLID_FOREGROUND
        fill2.setFillBackgroundColor IndexedColors.LIGHT_TURQUOISE.index


        ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule('1')
        PatternFormatting fill3 = rule3.createPatternFormatting()
//        fill1.setFillForegroundColor IndexedColors.YELLOW.index
        fill3.setFillPattern PatternFormatting.SOLID_FOREGROUND
        fill3.setFillBackgroundColor IndexedColors.LIGHT_TURQUOISE.index

//
//        ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule('NOT(IS_NUMBER(FIND($A6,"EMİR")))')
//        PatternFormatting fill3 = rule3.createPatternFormatting()
////        fill1.setFillForegroundColor IndexedColors.YELLOW.index
//        fill3.setFillPattern PatternFormatting.SOLID_FOREGROUND
//        fill3.setFillBackgroundColor IndexedColors.LIGHT_TURQUOISE.index


//        ConditionalFormattingRule rule2= sheetCF.createConditionalFormattingRule(ComparisonOperator.GT,'100')
//        ConditionalFormattingRule rule2= sheetCF.createConditionalFormattingRule('$I1>100')
//        PatternFormatting fill2 = rule2.createPatternFormatting()
//        fill2.setFillForegroundColor IndexedColors.BLUE.index
//        fill2.setFillPattern PatternFormatting.SOLID_FOREGROUND
//        fill2.setFillBackgroundColor IndexedColors.LIGHT_BLUE.index

//        CellRangeAddress[] regions = [new CellRangeAddress(startRow, endRow, tahsilatColumn, odemeColumn)]
        CellRangeAddress[] regions = [new CellRangeAddress(startRow, endRow, 0, bakiyeColumn)]
        CellRangeAddress[] regions2 = [new CellRangeAddress(startRow, endRow, 0, bakiyeColumn)]
        CellRangeAddress[] regions3 = [new CellRangeAddress(startRow, endRow, bakiyeColumn+1, bakiyeColumn+2)]

        sheetCF.addConditionalFormatting(regions, rule1)
        sheetCF.addConditionalFormatting(regions2, rule2)
        sheetCF.addConditionalFormatting(regions3, rule3)
//        sheetCF.addConditionalFormatting(regions, rule1, rule2)
//        sheetCF.addConditionalFormatting(regions, rule2)


        (0..15).each { int col ->
            sheet.autoSizeColumn(col)
        }

        response.setContentType 'application/vnd.ms-excel'
        //def reportBytes = wb.bytes
        //response.setContentLength reportBytes.length
        response.setCharacterEncoding "UTF-8"
        response.setHeader "Content-Disposition", "attachment;filename=gunluk_odeme_tahsilat.xlsx"

//        response.outputStream << reportBytes
        wb.write response.outputStream
        response.outputStream.flush()
        response.outputStream.close()
        return null
    }

    private def checkStyle(Yevmiye y, Cell cell, Map tekinVeyaEmirStyle){
//        if(firma(y.masrafmerkezi) in ['TEKİN','EMİR']){
//            cell.cellStyle = tekinVeyaEmirStyle[cell.cellStyle] ?: cell.cellStyle
//        }
    }

    private def firma(masrafmerkezi) {
        def val = 'ORTAKLIK'
        if (masrafmerkezi == '4') {
            val = 'EMİR'
        }
        else if (masrafmerkezi == '5') {
            val = 'TEKİN'
        }else if(!masrafmerkezi){
            val = ''
        }
        return val
    }

    private def firmaColor(masrafmerkezi){
        def firma = firma(masrafmerkezi)

        if(firma in ['EMİR','TEKİN']){
            return IndexedColors.YELLOW.index
        }
    }

    private def giderYeri(Yevmiye yevmiye){
        def val = ''
        if(yevmiye.alacakli) {
            val = 'İNŞAAT'
            if (yevmiye.masrafmerkezi in ['4', '5']) {
                val = 'SRM-MHS'
            } else if (yevmiye.masrafmerkezi == '6' && yevmiye.gmkod == '102 02 001') {
                val = 'FAİZ'
            }
//            else if(! (yevmiye.masrafmerkezi in [4,5]) ){
//                val = 'İNŞAAT'
//            }
        }
        else if(yevmiye.borclu) {
            if (yevmiye.masrafmerkezi in ['4', '5']) {
                val = 'SRM-TAH'
            }
        }
        else {
            val = ''
        }
        return val
    }

    def index() { }
}

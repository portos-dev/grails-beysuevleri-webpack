package beysuevleri.grails.webpack

import grails.rest.RestfulController
import groovy.transform.ToString

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class HesplanController extends RestfulController{
    static allowedMethods = [save: "POST", map: ['GET', 'POST'],update: ["PUT", "POST"], patch: "PATCH", delete: "DELETE"]

    def hesplanService

    HesplanController(){
        super(Hesplan)
    }

//    static responseFormats = ['json', 'xml']
//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Hesplan.list(params), model:[hesplanCount: Hesplan.count()]
    }

    def query(HesplanQueryCommand filterQuery){
        println filterQuery
        respond hesplanService.search(filterQuery)
    }

    def map(){
        def map = [:]
        Hesplan.list().
                each{
            Hesplan h -> map.put(h.kod, h.aciklama) }
        respond map
    }

    def secilenPlanlar(HesplanQueryCommand cmd){
        println cmd
        respond hesplanService.search(cmd)
    }

    def queryDefault

    def show(Hesplan hesplan) {
        respond hesplan
    }

    @Transactional
    def save(Hesplan hesplan) {
        if (hesplan == null) {
            transactionStatus.setRollbackOnly()
            render status: NOT_FOUND
            return
        }

        if (hesplan.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond hesplan.errors, view:'create'
            return
        }

        hesplan.save flush:true

        respond hesplan, [status: CREATED, view:"show"]
    }

    @Transactional
    def update(Hesplan hesplan) {
        if (hesplan == null) {
            transactionStatus.setRollbackOnly()
            render status: NOT_FOUND
            return
        }

        if (hesplan.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond hesplan.errors, view:'edit'
            return
        }

        hesplan.save flush:true

        respond hesplan, [status: OK, view:"show"]
    }

    @Transactional
    def delete(Hesplan hesplan) {

        if (hesplan == null) {
            transactionStatus.setRollbackOnly()
            render status: NOT_FOUND
            return
        }

        hesplan.delete flush:true

        render status: NO_CONTENT
    }
}

@ToString(includePackage = false, includeNames = true, includeFields = true,
        ignoreNulls = true, includes = ['kod','kodLike'])
class HesplanQueryCommand{
//    @BindingFormat('dd-MM-yyyy')
//    Date baslangicTarihi
//    @BindingFormat('dd-MM-yyyy')
//    Date bitisTarihi
    String kod
    String kodLike
    List<String> kodLikes
    List<String> kods/* = [
            '100 01', // MERKEZ KASA

//            '102', //BANKALAR

            '102 01', //VADESIZ HESAPLAR
//            [kod:'102 01 001'] //AKBANK ignore
//            [kod:'102 01 002'] //TEB ignore
             '102 01 003', //ISBANK
            '102 01 004', // ŞEKERBANK
            '102 01 005', // ŞEKERBANK EK HESAP

//            [kod:'102 02'], //VADELI HESAPLAR
            '102 02 001', //ŞEKERBANK VADELİ HESAP

    ]*/

    Integer max
    Integer offset
    String sort
    String order

    static boolean defaultNullable() {
        true
    }
    static constraints = {
        kod(blank: true, minSize: 3)
        kodLike(blank: true, minSize: 3)
        max(nullable: true)
        offset(nullable: true)
        sort(nullable: true, blank: true)
        order(nullable: true, blank:true)
    }
}



package beysuevleri.grails.webpack

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/hesplan"(resources: "hesplan"){
            "/children"(controller: 'hesplan', action:'children')
            "/map"(controller: 'hesplan', action:'map')
        }

        "/yevmiye"(resources: "yevmiye"){
            "/logYevs"(controller: 'yevmiye', action:'logYevs')
        }

        "/logyev"(resources: "logyev"){
            //"/logYevs"(controller: 'logYev', action:'logYevs')
        }

        "/"(view: '/index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}

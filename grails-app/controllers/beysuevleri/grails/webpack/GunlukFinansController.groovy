package beysuevleri.grails.webpack

import grails.databinding.BindingFormat
import grails.rest.*
import grails.converters.*
import groovy.transform.ToString
import tr.com.portos.grails.rest.PagingController

class GunlukFinansController extends RestfulController/*extends PagingController*/{
	static responseFormats = ['json', 'xml']

    GunlukFinansController(){
        super(Yevmiye)
    }

    def gunlukFinansService
	
//    def index(Integer max) {
//        params.max = Math.min(max ?: 10, 100)
//        def detail = params.detail ?: "complete"
//        println 'index'
//       respond Yevmiye.executeQuery('select yevmiye from Yevmiye yevmiye ', [max:20]),
//               [
//                       yevmiyeCount: Yevmiye.executeQuery('select count(yevmiye) from Yevmiye yevmiye '),
//                       max: params.max,
//                       offset: params.offset
//               ]
//
//    }

    def query(GunlukFinansQueryCommand cmd){
        println cmd
        respond(gunlukFinansService.search(cmd))
    }
}


@ToString(includePackage = false, includeNames = true, includeFields = true,
        ignoreNulls = true, includes = ['gmkod','baslangicTarihi','bitisTarihi', 'refno', 'refno2'])
class GunlukFinansQueryCommand{
    @BindingFormat('dd-MM-yyyy')
    Date baslangicTarihi
    @BindingFormat('dd-MM-yyyy')
    Date bitisTarihi
    String gmkod
    Long refno
    Long refno2

    static boolean defaultNullable() {
        true
    }
    static constraints = {
        baslangicTarihi(blank:true)
        bitisTarihi(blank:true)
        gmkod(blank: true, minSize: 3)
        refno(nullable: true)
        refno2(nullable: true)
    }
}

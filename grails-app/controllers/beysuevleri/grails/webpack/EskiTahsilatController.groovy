package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import org.apache.poi.ss.usermodel.BorderFormatting
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.ConditionalFormattingRule
import org.apache.poi.ss.usermodel.DataFormat
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.FontFormatting
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.PatternFormatting
import org.apache.poi.ss.usermodel.PrintSetup
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.SheetConditionalFormatting
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

@Transactional(readOnly = true)
class EskiTahsilatController {
	static responseFormats = ['json', 'xml']

    def poi(YevmiyeQueryCommand filterQuery) {

        List<DetayliTahsilat> detayliTahsilatResults = []

        Tenants.withId("ongoru") {
            def reportConfig = Report.findByReportId('detayli_tahsilat')
            if (!filterQuery.gmkods) {
                filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
            }
            if (!filterQuery.gmkodLikes) {
                filterQuery.gmkodLikes = reportConfig.config('yevmiye.gmkod.likes')
            }
            if (!filterQuery.aciklamaExcludes) {
                filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
            }

//            def reportConfig = Report.findByReportId('detayli_tahsilat_beysu_1')
            //TODO: def ongoruHesplanBaslangicTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.min')
            //TODO: def ongoruHesplanBitisTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.max')
            def ongoruHesplanGmkodLikes = reportConfig.config('yevmiye.ongoru.hesplan.gmkod.likes')
            def ongoruHesplanPrefix = reportConfig.config('yevmiye.ongoru.hesplan.prefix')
            String ongoruHesplanSuffixDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.suffix.dateFormat')
            def ongoruMusHesplanRefField = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.field')
            def ongoruMusHesplanRefPrefix = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix')
            def ongoruOdemePlanTarihField = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.field')
            def ongoruOdemePlanTarihDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat')

            def tahsilatMusHesplanOdemeTarihField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tarih.field')
            def tahsilatMusHesplanOdemeTutarField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tutar.field')
            def ongoruHesplanBorcTutarField = reportConfig.config('yevmiye.ongoru.hesplan.borc.tutar.field')

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def ongoruOdemePlanTarihSdf = new SimpleDateFormat(ongoruOdemePlanTarihDateFormat)


            def query = "select tahsilat from Yevmiye tahsilat " +
                    " where gmkod like '${ongoruMusHesplanRefPrefix} %' " +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null" +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTarihField} is not null" +
                    //" and musteriHesplan.kod = tahsilat.gmkod "
                    " order by gmkod, ${tahsilatMusHesplanOdemeTarihField}"

            def musteriHesapPlaniVeTahsilatlar = //Yevmiye.executeQuery(query)


                    Yevmiye.where {
                        gmkod =~ "${ongoruMusHesplanRefPrefix} %"
                    }.join('hesplan')
                            .list(sort: 'gmkod')


            def hesplanYevmiye = [:]
            def hesplanOdemePlani = [:]
            def hesplanTahsilat = [:]

//            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Object[] result ->
            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Yevmiye y ->

                if (y.alacakli) {
                    if (y.evrakno) {
                        try {
                            y.evraktarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.sonOdemeTarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.isOdemePlani = true
                            y.kalanTutar = y.alacakli
                            y.tutar = y.alacakli
                            y.index = 0
                            hesplanOdemePlani[y.hesplan] ? hesplanOdemePlani[y.hesplan] << y : (hesplanOdemePlani[y.hesplan] = [y])
                        } catch (Exception e) {
                            log.error y.toString()
                            log.error "gecersiz odeme plani tarihi: ${y.evrakno} "
                            y.evrakno = "??${y.evrakno}??"
                        }
                    }
                } else if (y.borclu) {
                    y.isTahsilat = true
                    y.kalanTutar = y.borclu
                    y.tutar = y.borclu
                    y.odemeTarihi = y.evraktarihi ?: y.fistar
                    y.index = 1 //siralamada tahsilatlar odeme planlarindan sonra listelensin
                    hesplanTahsilat[y.hesplan] ? hesplanTahsilat[y.hesplan] << y : (hesplanTahsilat[y.hesplan] = [y])
                }

                println y.alacakli ? "plan: $y" : "tahsilat: $y"

                hesplanYevmiye[y.hesplan] ? hesplanYevmiye[y.hesplan] << y : (hesplanYevmiye[y.hesplan] = [y])

//                Yevmiye y = result[0]
//                Hesplan h = result[1]
//                def retVal = new Expando(y)
                // y.metaClass.hesplanAciklama = h.aciklama
                // y.metaClass.toplamBorc = h.topb
                println y.alacakli ? "plan: $y" : "tahsilat: $y"
                y
            }

            def hesplanSonuc = [:]

            def totalRows = 0

            def odendiStyleMap = [:]

            double tahsilEdilecekSenetToplami = 0

            hesplanYevmiye.each {
                Hesplan hesplan = it.key
                List<Yevmiye> islenmemisOdemePlanlari = (hesplanOdemePlani[hesplan] ?: []).sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisOdemePlanlari = []
                //Iterable<Yevmiye> tahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmemisTahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisTahsilatlar = []

                def toplamAlacak = hesplan.topa
                def kalanAlacak = toplamAlacak
                def kalanAlacakPlan = toplamAlacak

                boolean odeme = false
                boolean borc = false

                println "${hesplan.aciklama} "
                println "Toplam alacak: ${toplamAlacak}"

                hesplanSonuc[hesplan] = []

                int loopGuard = 0

                while (islenmemisOdemePlanlari) {
                    Yevmiye odemePlani = islenmemisOdemePlanlari.first()
                    kalanAlacakPlan -= odemePlani.tutar
                    odemePlani.kalanToplamBakiyePlan = kalanAlacakPlan

                    while (islenmemisTahsilatlar && !odemePlani.odendi) {
                        Yevmiye tahsilat = islenmemisTahsilatlar.first()
                        // def tahsilEdilen //odemePlanindaki kalan tutar ve tahsilattan eslestirilen  kalan tutar

                        //toplamAlacak -= tahsilat.

                        if (odemePlani.kalanTutar == tahsilat.kalanTutar) {
                            //tahsilEdilen = odemePlani.kalanTutar
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar = 0
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat

                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        } else if (odemePlani.kalanTutar < tahsilat.kalanTutar) {
                            //odeme borctan fazla, odemeden eslestirilen taksiti indir,
                            kalanAlacak -= odemePlani.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            tahsilat.kalanTutar -= odemePlani.kalanTutar
                            odemePlani.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()

                            //odeme borctan yuksek ise, odemeden bu taksiti dusup odemenin kalani ile devam et,
                            // bakiye eksi cikarsa odeme planinda eksik oldugu anlasilir
                            //kalan odemeyi tekrar islemek uzere tut

                        } else if (odemePlani.kalanTutar > tahsilat.kalanTutar) {
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar -= tahsilat.kalanTutar
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = false
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()


                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        } else {
                            println "cannot handle odemeplani: $odemePlani"
                            println "cannot handle tahsilat: $tahsilat"
                            break

                        }
                    }

                    islenmisTahsilatlar.sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }

                    islenmemisOdemePlanlari.remove(odemePlani)
                    islenmisOdemePlanlari << odemePlani

                    loopGuard++
                    if (loopGuard > 10000) {
                        println "LOOPGUARD: ${odemePlani}"
                        println "LOOPGUARD: ${loopGuard}, breaking possibly infinite loop"
                    }
                }

                def hesplanKalanBakiye = hesplan.topa - (hesplan.topb ?: 0 )
                tahsilEdilecekSenetToplami += hesplanKalanBakiye

                hesplanSonuc[hesplan] = [islenmisOdemePlanlari  : islenmisOdemePlanlari,
                                         islenmemisOdemePlanlari: islenmemisOdemePlanlari, islenmisTahsilatlar: islenmisTahsilatlar,
                                         islenmemisTahsilatlar  : islenmemisTahsilatlar,
                                         kalanBakiye: hesplanKalanBakiye
                ]

                detayliTahsilatResults << new DetayliTahsilat(hesplan: hesplan,
                        islenmisOdemePlanlari: islenmisOdemePlanlari,
                        islenmemisOdemePlanlari: islenmemisOdemePlanlari,
                        islenmisTahsilatlar: islenmisTahsilatlar,
                        islenmemisTahsilatlar: islenmemisTahsilatlar)

                totalRows += (9 + islenmisOdemePlanlari.size())

            }


            Workbook wb = new XSSFWorkbook()
            Sheet sheet = wb.createSheet('ESKI_TAHSILAT')
            sheet.printSetup.landscape = true
            sheet.printSetup.paperSize = PrintSetup.A3_PAPERSIZE
            sheet.printGridlines = false
            sheet.autobreaks = false

            Font boldFont = wb.createFont()
            boldFont.bold = true

            Font fontRed = wb.createFont()
            fontRed.color = Font.COLOR_RED

            Font boldFontRed = wb.createFont()
            boldFontRed.bold = true
            boldFontRed.color = Font.COLOR_RED

            DataFormat df = wb.createDataFormat()
            def tutarFormat = df.getFormat('#,##0.00')

            final DEFAULT_BORDER = BorderStyle.MEDIUM

            CellStyle csDefault = wb.createCellStyle()
//            csDefault.setFillForegroundColor IndexedColors.WHITE.index
//            csDefault.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csDefaultFgGreen = wb.createCellStyle()
            csDefaultFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csDefaultFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND


            CellStyle csDate = wb.createCellStyle()
            csDate.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
//            csDate.setFillForegroundColor IndexedColors.WHITE.index
//            csDate.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csDateFgGreen = wb.createCellStyle()
            csDateFgGreen.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csDateFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csDate] = csDateFgGreen

            CellStyle csM2 = wb.createCellStyle()
            csM2.dataFormat = tutarFormat
            csM2.alignment = HorizontalAlignment.LEFT
//            csM2.setFillForegroundColor IndexedColors.WHITE.index
//            csM2.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csM2FgGreen = wb.createCellStyle()
            csM2FgGreen.dataFormat = tutarFormat
            csM2FgGreen.alignment = HorizontalAlignment.LEFT
            csM2FgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csM2FgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csM2] = csM2FgGreen

            CellStyle csDateRed = wb.createCellStyle()
            csDateRed.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateRed.font = fontRed
//            csDateRed.setFillForegroundColor IndexedColors.WHITE.index
//            csDateRed.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csDateRedFgGreen = wb.createCellStyle()
            csDateRedFgGreen.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateRedFgGreen.font = fontRed
            csDateRedFgGreen.setFillForegroundColor IndexedColors.GREEN.index
            csDateRedFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csDateRed] = csDateRedFgGreen

            CellStyle csLeftBorder = wb.createCellStyle()
            csLeftBorder.borderLeft = DEFAULT_BORDER
//            csLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csLeftBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )
//            csLeftBorder.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
//            csLeftBorder.setFillBackgroundColor IndexedColors.LIGHT_GREEN.index
//            csLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csLeftBorderFgGreen = wb.createCellStyle()
            csLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csLeftBorder] = csLeftBorderFgGreen

            CellStyle csRightBorder = wb.createCellStyle()
            csRightBorder.borderRight = DEFAULT_BORDER
//            csRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csRightBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csRightBorderFgGreen = wb.createCellStyle()
            csRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csRightBorder] = csRightBorderFgGreen

            CellStyle csTopLeftBorder = wb.createCellStyle()
            csTopLeftBorder.borderLeft = DEFAULT_BORDER
            csTopLeftBorder.borderTop = DEFAULT_BORDER
//            csTopLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csTopLeftBorderFgGreen = wb.createCellStyle()
            csTopLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csTopLeftBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopLeftBorder] = csTopLeftBorderFgGreen

            CellStyle csBottomLeftBorder = wb.createCellStyle()
            csBottomLeftBorder.borderLeft = DEFAULT_BORDER
            csBottomLeftBorder.borderBottom = DEFAULT_BORDER
//            csBottomLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomLeftBorderFgGreen = wb.createCellStyle()
            csBottomLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csBottomLeftBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomLeftBorder] = csBottomLeftBorderFgGreen

            CellStyle csTopRightBorder = wb.createCellStyle()
            csTopRightBorder.borderRight = DEFAULT_BORDER
            csTopRightBorder.borderTop = DEFAULT_BORDER
//            csTopRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopRightBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csTopRightBorderFgGreen = wb.createCellStyle()
            csTopRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csTopRightBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopRightBorder] = csTopRightBorderFgGreen

            CellStyle csBottomBorder = wb.createCellStyle()
            csBottomBorder.borderBottom = DEFAULT_BORDER
//            csBottomBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomBorderFgGreen = wb.createCellStyle()
            csBottomBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomBorder] = csBottomBorderFgGreen

            CellStyle csTopBorder = wb.createCellStyle()
            csTopBorder.borderTop = DEFAULT_BORDER
//            csTopBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTopBorderFgGreen = wb.createCellStyle()
            csTopBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopBorder] = csTopBorderFgGreen

            CellStyle csBottomRightBorder = wb.createCellStyle()
            csBottomRightBorder.borderRight = DEFAULT_BORDER
            csBottomRightBorder.borderBottom = DEFAULT_BORDER
//            csBottomRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomRightBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomRightBorderFgGreen = wb.createCellStyle()
            csBottomRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csBottomRightBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomRightBorder] = csBottomRightBorderFgGreen

            CellStyle csBold = wb.createCellStyle()
            csBold.font = boldFont
//            csBold.setFillForegroundColor IndexedColors.WHITE.index
//            csBold.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldFgGreen = wb.createCellStyle()
            csBoldFgGreen.font = boldFont
            csBoldFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBold] = csBoldFgGreen

            CellStyle csBoldAlignLeft = wb.createCellStyle()
            csBoldAlignLeft.font = boldFont
            csBoldAlignLeft.alignment = HorizontalAlignment.LEFT
//            csBoldAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldAlignLeftFgGreen = wb.createCellStyle()
            csBoldAlignLeftFgGreen.font = boldFont
            csBoldAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csBoldAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldAlignLeft] = csBoldAlignLeftFgGreen

            CellStyle csAlignLeft = wb.createCellStyle()
            csAlignLeft.alignment = HorizontalAlignment.LEFT
//            csAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csAlignLeftFgGreen = wb.createCellStyle()
            csAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csAlignLeft] = csAlignLeftFgGreen

            CellStyle csBoldAlignRight = wb.createCellStyle()
            csBoldAlignRight.font = boldFont
            csBoldAlignRight.alignment = HorizontalAlignment.RIGHT
//            csBoldAlignRight.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldAlignRight.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldAlignRightFgGreen = wb.createCellStyle()
            csBoldAlignRightFgGreen.font = boldFont
            csBoldAlignRightFgGreen.alignment = HorizontalAlignment.RIGHT
            csBoldAlignRightFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldAlignRightFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldAlignRight] = csBoldAlignRightFgGreen

            CellStyle csTutar = wb.createCellStyle()
            csTutar.dataFormat = tutarFormat
            csTutar.alignment = HorizontalAlignment.RIGHT
//            csTutar.setFillForegroundColor IndexedColors.WHITE.index
//            csTutar.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTutarFgGreen = wb.createCellStyle()
            csTutarFgGreen.dataFormat = tutarFormat
            csTutarFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTutarFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTutar] = csTutarFgGreen

            CellStyle csTutarRed = wb.createCellStyle()
            csTutarRed.dataFormat = tutarFormat
            csTutarRed.font = fontRed
//            csTutarRed.setFillForegroundColor IndexedColors.WHITE.index
//            csTutarRed.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTutarRedFgGreen = wb.createCellStyle()
            csTutarRedFgGreen.dataFormat = tutarFormat
            csTutarRedFgGreen.font = fontRed
            csTutarRedFgGreen.setFillForegroundColor IndexedColors.GREEN.index
            csTutarRedFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTutarRed] = csTutarRedFgGreen

            CellStyle csBoldTutar = wb.createCellStyle()
            csBoldTutar.font = boldFont
            csBoldTutar.dataFormat = tutarFormat
//            csBoldTutar.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldTutar.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldTutarFgGreen = wb.createCellStyle()
            csBoldTutarFgGreen.font = boldFont
            csBoldTutarFgGreen.dataFormat = tutarFormat
            csBoldTutarFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldTutarFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldTutar] = csBoldTutarFgGreen

            CellStyle csBoldTutarAlignLeft = wb.createCellStyle()
            csBoldTutarAlignLeft.font = boldFont
            csBoldTutarAlignLeft.dataFormat = tutarFormat
            csBoldTutarAlignLeft.alignment = HorizontalAlignment.LEFT
//            csBoldTutarAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldTutarAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldTutarAlignLeftFgGreen = wb.createCellStyle()
            csBoldTutarAlignLeftFgGreen.font = boldFont
            csBoldTutarAlignLeftFgGreen.dataFormat = tutarFormat
            csBoldTutarAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csBoldTutarAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldTutarAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldTutarAlignLeft] = csBoldTutarAlignLeftFgGreen


            int startRow = 3
            int endRow
            int cellsPerBucket = 6
            int currentBucket = 0
            int currentRow = startRow
            int currentColumn = 0

            int musteriColumn
            int satisBedeliColumn
            int m2Column
            int ortM2Column

            Pattern m2veOrtM2Regex = ~/(.*)\((.*[,]?.*[.]?.*)M2---(.*[,]?.*[.]?.*)\)/
            Pattern bbRegex = ~/120 (.*)-(.*)/


            def noOfBuckets = 4


            sheet.setColumnWidth(0, 100)
            sheet.setDefaultRowHeight((short)300)


            Row headerRow = sheet.getRow(currentRow-1) ?: sheet.createRow(currentRow-1)


            Cell bbTipHeader = headerRow.createCell(currentColumn)
            bbTipHeader.cellValue = "DAİRE TİPİ"
            bbTipHeader.cellStyle = csBold
            currentColumn++
            Cell bbBlokHeader = headerRow.createCell(currentColumn)
            bbBlokHeader.cellValue = "BLOK NO"
            bbBlokHeader.cellStyle = csBold
            currentColumn++
            Cell bbNoHeader = headerRow.createCell(currentColumn)
            bbNoHeader.cellValue = "DAİRE NO"
            bbNoHeader.cellStyle = csBold
            currentColumn++
            Cell bbM2Header = headerRow.createCell(currentColumn)
            bbM2Header.cellValue = "DAİRE M2"
            bbM2Header.cellStyle = csBold
            m2Column = currentColumn
            currentColumn++
            Cell ortM2Header = headerRow.createCell(currentColumn)
            ortM2Header.cellValue = "ORT M2"
            ortM2Header.cellStyle = csBold
            ortM2Column = currentColumn
            currentColumn++
            Cell musteriHeader = headerRow.createCell(currentColumn)
            musteriHeader.cellValue = "ALICI"
            musteriHeader.cellStyle = csBold
            musteriColumn = currentColumn
            currentColumn++
            Cell satisBedeliHeader = headerRow.createCell(currentColumn)
            satisBedeliHeader.cellValue = "SATIŞ BEDELİ"
            satisBedeliHeader.cellStyle = csBold
            satisBedeliColumn = currentColumn
            currentColumn++
            Cell bakiyeHeader = headerRow.createCell(currentColumn)
            bakiyeHeader.cellValue = 'TAHSİL EDİLEN'
            bakiyeHeader.cellStyle = csBoldAlignRight
            currentColumn++
            Cell kalanAlacakHeader = headerRow.createCell(currentColumn)
            kalanAlacakHeader.cellValue = 'KALAN ALACAK'
            kalanAlacakHeader.cellStyle = csBoldAlignRight
            currentColumn++

            //currentColumn = 0

            def tahsilatRanges = []
            def bbM2Cells = []
            Map<String,Double> donemAlacaklari = [:]

            def aySdf = new SimpleDateFormat('MMMM yyyy', new Locale('tr', 'TR'))
            def yilSdf = new SimpleDateFormat('yyyy', new Locale('tr', 'TR'))

            def donemler = []

            def aylar = []

            detayliTahsilatResults.islenmisOdemePlanlari.collectMany {
                it.evraktarihi
            }.sort{it}.each {
//                Yevmiye y ->
                    def ay = aySdf.parse(aySdf.format(it))
                    if (!(ay in aylar)) {
                        aylar << ay
                    }
            }

//            def aylar = detayliTahsilatResults.collectMany {
//                [
//                        aySdf.parse(aySdf.format(it.islenmisOdemePlanlari.evraktarihi.min())),
//                        aySdf.parse(aySdf.format(it.islenmisOdemePlanlari.evraktarihi.max())),
//                ]
//            }.toSet().toArray().sort()
//                    .collect{
//                aySdf.format(it)
//            }

            /*aylar.each { Date d ->
                donemler << aySdf.format(d)
                if(d[Calendar.MONTH] == 11){ // ARALIK
                    donemler << yilSdf.format(d)
                }
            }*/
            aylar.each { Date d ->

                if(d[Calendar.MONTH] >= new Date()[Calendar.MONTH]) {
                    def ay = aySdf.format(d)
                    def yil = yilSdf.format(d)
                    if (d[Calendar.YEAR] == new Date()[Calendar.YEAR]) {
                        if(!(ay in donemler))
                            donemler << ay
                    } else {
                        if(!(yil in donemler))
                            donemler << yil
                    }
                }
            }


            //tarih sirali olmali donemler
            int donemlerStartColumn = currentColumn
            donemler.each { String donem ->
                Cell header = headerRow.createCell(currentColumn)
                header.cellValue = donem.toUpperCase(new Locale('tr', 'TR'))
                header.cellStyle = csBold
                currentColumn++
            }
            int donemlerEndColumn = currentColumn-1

            def firstColumn
            def lastColumn

            detayliTahsilatResults/*.sort{
                a,b -> a.islenmisOdemePlanlari.size() <=> b.islenmisOdemePlanlari.size()
            }*/.each { DetayliTahsilat dt ->

                def hesplan = dt.hesplan

                Matcher matcher = m2veOrtM2Regex.matcher(hesplan.aciklama)
                Matcher matcher2 = bbRegex.matcher(hesplan.kod)

                currentColumn = 0
                def musteriVal = '?'
                def m2Val = 0
                def ortM2Val = 0
                def bbBlokVal = '?'
                def bbNoVal = '?'

                firstColumn = currentColumn

                if (matcher.find()) {
                    musteriVal = matcher.group(1).trim()
                    m2Val = matcher.group(2).trim().replace('.', '').replace(',', '').toDouble()/100
                    ortM2Val = matcher.group(3).trim().replace('.', '').replace(',', '').toDouble()/100
                }
                if(matcher2.find()){
                    bbBlokVal = matcher2.group(1).trim()
                    bbNoVal = matcher2.group(2).trim()
                }

                Row row = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)



                Cell bbTip = row.createCell(currentColumn)
                bbTip.cellValue = "?"
                bbTip.cellStyle = csAlignLeft
                currentColumn++

                Cell bbBlok = row.createCell(currentColumn)
                bbBlok.cellValue = bbBlokVal
                bbBlok.cellStyle = csAlignLeft
                currentColumn++

                Cell bbNo = row.createCell(currentColumn)
                bbNo.cellValue = bbNoVal
                bbNo.cellStyle = csAlignLeft
                currentColumn++

                Cell bbM2 = row.createCell(currentColumn)
//                bbM2.cellType = Cell.CELL_TYPE_NUMERIC
                bbM2.cellValue = m2Val
                bbM2.cellStyle = csM2
                bbM2Cells << bbM2
                currentColumn++

                Cell ortM2 = row.createCell(currentColumn)
                ortM2.cellValue = ortM2Val
                ortM2.cellStyle = csM2
                currentColumn++

                Cell musteri = row.createCell(currentColumn)
                musteri.cellValue = musteriVal//dt.hesplan.aciklama
                musteri.cellStyle = csAlignLeft
                currentColumn++

                Cell satisBedeli = row.createCell(currentColumn)
                satisBedeli.cellValue = dt.hesplan.topa
                satisBedeli.cellStyle = csTutar
                currentColumn++

                Cell tahsilEdilen = row.createCell(currentColumn)
                tahsilEdilen.cellValue = dt.hesplan.topb
                tahsilEdilen.cellStyle = csTutar
                currentColumn++

                Cell kalanAlacak = row.createCell(currentColumn)
                kalanAlacak.cellValue = dt.hesplan.topa - (dt.hesplan.topb ?: 0)
                kalanAlacak.cellStyle = csTutar
                currentColumn++

                //TODO bastaki ve sonrdaki fazla olan border cell ler icin sheet.setColumnWidth(1) ile kucult

                //tahsilatRanges << new CellRangeAddress(currentRow, currentRow+dt.islenmisOdemePlanlari.size(), column1, column4)

                String currentYil = yilSdf.format(new Date())
                donemAlacaklari = [:]
                dt.islenmisOdemePlanlari.each { Yevmiye y ->
                    String ay = aySdf.format(y.evraktarihi)
                    String yil = yilSdf.format(y.evraktarihi)
                    if (yil == currentYil) {
                        if (!donemAlacaklari[ay]) {
                            donemAlacaklari[ay] = 0
                        }
                        donemAlacaklari[ay] += y.kalanTutar
                    } else {
                        if (!donemAlacaklari[yil]) {
                            donemAlacaklari[yil] = 0
                        }
                        donemAlacaklari[yil] += y.kalanTutar
                    }

                }

                //tarih sirali olmali donemler
                donemler.each { String donem ->
                    Cell donemAlacak = row.createCell(currentColumn)
                    donemAlacak.cellStyle = csTutar
                    donemAlacak.cellValue = donemAlacaklari[donem] ?: null
                    lastColumn = currentColumn
                    currentColumn++
                }

                endRow = currentRow

                currentRow++
            }

            Row toplamlarRow =  sheet.createRow(currentRow)
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 0, musteriColumn))
            Cell toplamlarHeader = toplamlarRow.createCell(0)
            toplamlarHeader.cellValue = "TOPLAMLAR"
            toplamlarHeader.cellStyle = csBoldAlignRight

            //diger columnlarin her biri icin asagida range sumlar icin cell olustur
            int toplamlarStartCol = musteriColumn+1
            int toplamlarEndCol = donemlerEndColumn
            (toplamlarStartCol..toplamlarEndCol).each {
                int col ->
                    CellRangeAddress sumRange= new CellRangeAddress(startRow,endRow, col,col)
                    Cell toplamCell = toplamlarRow.createCell(col)
                    toplamCell.cellStyle = csBoldTutar
                    toplamCell.cellFormula = "SUM(${sumRange.formatAsString()})"
            }

            currentRow++

            Row toplamSatisBedeliRow = sheet.createRow(currentRow)
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 0, 3))
            Cell toplamSatisBedeliHeader = toplamSatisBedeliRow.createCell(0)
            toplamSatisBedeliHeader.cellStyle = csBoldAlignRight
            toplamSatisBedeliHeader.cellValue = 'TOPLAM SATIŞ BEDELİ'
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 4, 5))
            Cell toplamSatisBedeli = toplamSatisBedeliRow.createCell(4)
            toplamSatisBedeli.cellStyle = csBoldTutarAlignLeft
            toplamSatisBedeli.cellFormula = new CellReference(toplamSatisBedeliRow.getRowNum()-1, satisBedeliColumn).formatAsString()

            currentRow++

            Row toplamM2Row = sheet.createRow(currentRow)
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 0, 3))
            Cell toplamM2Header = toplamM2Row.createCell(0)
            toplamM2Header.cellStyle = csBoldAlignRight
            toplamM2Header.cellValue = 'TOPLAM SATIŞ M2'
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 4, 5))
            Cell toplamM2 = toplamM2Row.createCell(4)
            toplamM2.cellStyle = csBoldTutarAlignLeft
            CellRangeAddress m2SumRange= new CellRangeAddress(startRow,endRow, m2Column,m2Column)
            toplamM2.cellFormula = "SUM(${m2SumRange.formatAsString()})"

            currentRow++

            Row ortM2Row = sheet.createRow(currentRow)
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 0, 3))
            Cell ortM2HeaderCell = ortM2Row.createCell(0)
            ortM2HeaderCell.cellStyle = csBoldAlignRight
            ortM2HeaderCell.cellValue = 'ORTALAMA DEĞER M2'
            sheet.addMergedRegion(new CellRangeAddress(currentRow, currentRow, 4, 5))
            Cell ortM2Cell = ortM2Row.createCell(4)
            ortM2Cell.cellStyle = csBoldTutarAlignLeft

            ortM2Cell.cellFormula = "${toplamSatisBedeli.getReference()}/${toplamM2.getReference()}"









//            currentColumn = 0
//
//            Row musteriSummary = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
//            Cell musteriSummaryCell = musteriSummary.createCell(currentColumn+2)
//            musteriSummaryCell.setCellValue "${detayliTahsilatResults.size()} ADET MÜŞTERİ"
//            musteriSummaryCell.cellStyle = wb.createCellStyle()
//            musteriSummaryCell.cellStyle.font = boldFont
//
//            currentRow++
//
//            Row senetSummary = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
//
//            Cell senetSummaryHeaderCell = senetSummary.createCell(currentColumn+2)
//            senetSummaryHeaderCell.setCellValue "TAHSİL EDİLECEK SENET TOPLAMI"
//            senetSummaryHeaderCell.cellStyle = wb.createCellStyle()
//            senetSummaryHeaderCell.cellStyle.font = boldFont
//            senetSummaryHeaderCell.cellStyle.fillForegroundColor = IndexedColors.LIGHT_YELLOW.index
//            senetSummaryHeaderCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
//
////            Cell middleCell = senetSummary.createCell(currentColumn+3)
////            middleCell.cellStyle = wb.createCellStyle()
////            middleCell.cellStyle.fillForegroundColor = IndexedColors.LIGHT_YELLOW.index
////            middleCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
//
//
//            Cell senetSummaryCell = senetSummary.createCell(currentColumn+3)
//            senetSummaryCell.setCellValue tahsilEdilecekSenetToplami
//            senetSummaryCell.cellStyle = wb.createCellStyle()
//            senetSummaryCell.cellStyle.font = boldFont
//            senetSummaryCell.cellStyle.dataFormat = tutarFormat
//            senetSummaryCell.cellStyle.fillBackgroundColor = IndexedColors.LIGHT_YELLOW.index
//            senetSummaryCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND

            CellRangeAddress headerRange = new CellRangeAddress(headerRow.getRowNum(), headerRow.getRowNum(), firstColumn, lastColumn)
            sheet.setAutoFilter(headerRange)


            CellRangeAddress[] donemlerDataRange = [new CellRangeAddress(startRow, endRow, donemlerStartColumn, donemlerEndColumn)]
            CellRangeAddress[] dataRange = [new CellRangeAddress(startRow, endRow, firstColumn, lastColumn)]


            SheetConditionalFormatting sheetCF = sheet.sheetConditionalFormatting

            ConditionalFormattingRule borderRule = sheetCF.createConditionalFormattingRule('MOD(ROW(),2)=0')
            BorderFormatting brdFmt = borderRule.createBorderFormatting()
            brdFmt.borderTop = BorderFormatting.BORDER_THIN
            brdFmt.borderBottom = BorderFormatting.BORDER_THIN

            sheetCF.addConditionalFormatting(dataRange, borderRule)

            ConditionalFormattingRule oddRule = sheetCF.createConditionalFormattingRule('MOD(ROW(),2)=1')
            PatternFormatting patternFmt = oddRule.createPatternFormatting()
            patternFmt.fillBackgroundColor = IndexedColors.LIGHT_YELLOW.index
            patternFmt.fillPattern = PatternFormatting.SOLID_FOREGROUND

            sheetCF.addConditionalFormatting(dataRange, oddRule)

            ConditionalFormattingRule evenRule = sheetCF.createConditionalFormattingRule('MOD(ROW(),2)=0')
            PatternFormatting patternFmt2 = evenRule.createPatternFormatting()
            patternFmt2.fillBackgroundColor = IndexedColors.WHITE.index
            patternFmt2.fillPattern = PatternFormatting.SOLID_FOREGROUND

            sheetCF.addConditionalFormatting(dataRange, evenRule)

//            CellRangeAddress[] allDetayRange = [new CellRangeAddress(startRow, sheet.getLastRowNum(), 0, noOfBuckets*cellsPerBucket)]

            //tahsilat ile planlanan odeme tutari ayni degilse taksit satirini kirmizi fontla isaretle
            /*ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule('NOT($E14=$F14)')
            FontFormatting fontFmt = rule1.createFontFormatting()
            fontFmt.setFontColorIndex IndexedColors.RED.index
            PatternFormatting patternFmt1 = rule1.createPatternFormatting()
            patternFmt1.fillBackgroundColor = IndexedColors.WHITE.index
            patternFmt1.fillPattern = PatternFormatting.SOLID_FOREGROUND

            sheetCF.addConditionalFormatting(tahsilatRanges.toArray(new CellRangeAddress[0]), rule1)*/


            //tahsilat planinda son bakiye 0 degilse planda eksik taksit bulunuyor kalan son bakiyeyi veya tum alani kirmizi ile isaretle
            tahsilatRanges.eachWithIndex { CellRangeAddress range, int i ->
                def lastTahsilatTarihCellRef = "${CellReference.convertNumToColString(range.firstColumn)}${range.lastRow}"
                def lastTahsilatBakiyeCellRef = "${CellReference.convertNumToColString(range.lastColumn)}${range.lastRow}"
                ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule("\$$lastTahsilatBakiyeCellRef > 0")
                FontFormatting fontFmt3 = rule3.createFontFormatting()
                fontFmt3.setFontColorIndex IndexedColors.RED.index
                fontFmt3.setFontStyle(false, true)
                PatternFormatting patternFmt3 = rule3.createPatternFormatting()
                patternFmt3.fillForegroundColor = IndexedColors.RED.index
                patternFmt3.fillPattern = PatternFormatting.SOLID_FOREGROUND

                CellRangeAddress[] ranges = [CellRangeAddress.valueOf("$lastTahsilatTarihCellRef:$lastTahsilatBakiyeCellRef")]
                sheetCF.addConditionalFormatting(ranges, rule3)
            }

            def toplamM2Formula = "SUM(" + (bbM2Cells.collect {Cell m2Cell ->
                CellReference ref = new CellReference(m2Cell.getRow().getRowNum(), m2Cell.getColumnIndex())
                ref.formatAsString()
            }.join(',')) + ")"


            (0..noOfBuckets*cellsPerBucket).each { int col ->
                sheet.autoSizeColumn(col)
            }


            response.setContentType 'application/vnd.ms-excel'
            //def reportBytes = wb.bytes
            //response.setContentLength reportBytes.length
            response.setCharacterEncoding "UTF-8"
            response.setHeader "Content-Disposition", "attachment;filename=eski_tahsilat.xlsx"

//            response.outputStream << reportBytes
            wb.write response.outputStream
            response.outputStream.flush()
            response.outputStream.close()
            return null
        }
    }
}

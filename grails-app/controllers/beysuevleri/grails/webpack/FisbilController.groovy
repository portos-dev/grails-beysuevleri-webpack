package beysuevleri.grails.webpack

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FisbilController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Fisbil.list(params), model:[fisbilCount: Fisbil.count()]
    }

    def show(Fisbil fisbil) {
        respond fisbil
    }

    def create() {
        respond new Fisbil(params)
    }

    @Transactional
    def save(Fisbil fisbil) {
        if (fisbil == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (fisbil.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond fisbil.errors, view:'create'
            return
        }

        fisbil.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'fisbil.label', default: 'Fisbil'), fisbil.id])
                redirect fisbil
            }
            '*' { respond fisbil, [status: CREATED] }
        }
    }

    def edit(Fisbil fisbil) {
        respond fisbil
    }

    @Transactional
    def update(Fisbil fisbil) {
        if (fisbil == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (fisbil.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond fisbil.errors, view:'edit'
            return
        }

        fisbil.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'fisbil.label', default: 'Fisbil'), fisbil.id])
                redirect fisbil
            }
            '*'{ respond fisbil, [status: OK] }
        }
    }

    @Transactional
    def delete(Fisbil fisbil) {

        if (fisbil == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        fisbil.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'fisbil.label', default: 'Fisbil'), fisbil.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'fisbil.label', default: 'Fisbil'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

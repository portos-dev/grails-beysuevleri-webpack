package beysuevleri.grails.webpack

import grails.transaction.Transactional

@Transactional(readOnly = true)
class ReportController {
	static responseFormats = ['json', 'xml'] //TODO byte[] ?

    def reportService

    def index() { }

    def gunlukTablo(YevmiyeQueryCommand filterQuery){
        def reportConfig = Report.findByReportId('gunluk_odeme_tahsilat')
        if(!filterQuery.gmkods){
            filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
        }
        if(!filterQuery.aciklamaExcludes){
            filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
        }

        if(!filterQuery.serinoExcludes){
            filterQuery.serinoExcludes = reportConfig.config('')
        }
        response.setContentType  'application/pdf'
        def reportBytes = reportService.index(filterQuery)
        response.setContentLength reportBytes.length
        response.setCharacterEncoding "UTF-8"
        response.setHeader  "Content-disposition", "attachment;filename=tablo.pdf"

        response.outputStream << reportBytes
        response.outputStream.flush()
        response.outputStream.close()
//        respond reportBytes


    }

    def detayliTahsilat(YevmiyeQueryCommand filterQuery){
        def reportConfig = Report.findByReportId('detayli_tahsilat')
        if(!filterQuery.gmkods){
            filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
        }
        if(!filterQuery.gmkodLikes){
            filterQuery.gmkodLikes = reportConfig.config('yevmiye.gmkod.likes')
        }
        if(!filterQuery.aciklamaExcludes){
            filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
        }
        response.setContentType  'application/pdf'
        def reportBytes = reportService.detayliTahsilat(filterQuery)
        response.setContentLength reportBytes.length
        response.setCharacterEncoding "UTF-8"
        response.setHeader  "Content-disposition", "attachment;filename=detayliTahsilat.pdf"

        response.outputStream << reportBytes
        response.outputStream.flush()
        response.outputStream.close()
//        respond reportBytes


    }

    def detayliTahsilatDefaultDataOnly(){
        def datasource = reportService.detayliTahsilatPredefinedMusteriHesplanOnly(true)
        respond datasource
    }

    def detayliTahsilatDefault(/*TODO: specific report config param*/){
        response.setContentType  'application/pdf'
//        def reportBytes = reportService.detayliTahsilatPredefined()
        def reportBytes = reportService.detayliTahsilatPredefinedMusteriHesplanOnly()
        response.setContentLength reportBytes.length
        response.setCharacterEncoding "UTF-8"
        response.setHeader  "Content-disposition", "attachment;filename=detayliTahsilat.pdf"

        response.outputStream << reportBytes
        response.outputStream.flush()
        response.outputStream.close()
//        respond reportBytes


    }
}

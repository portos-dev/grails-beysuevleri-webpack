package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import org.apache.poi.ss.usermodel.BorderFormatting
import org.apache.poi.ss.usermodel.BorderStyle
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.ConditionalFormattingRule
import org.apache.poi.ss.usermodel.DataFormat
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.FontFormatting
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.PatternFormatting
import org.apache.poi.ss.usermodel.PrintSetup
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.SheetConditionalFormatting
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.text.SimpleDateFormat
import java.util.regex.Matcher
import java.util.regex.Pattern

@Transactional(readOnly = true)
class DetayliTahsilatController {
	static responseFormats = ['json', 'xml']
	
    def index(YevmiyeQueryCommand filterQuery) {

        def detayliTahsilatResults = []

        Tenants.withId("ongoru") {
            def reportConfig = Report.findByReportId('detayli_tahsilat')
            if(!filterQuery.gmkods){
                filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
            }
            if(!filterQuery.gmkodLikes){
                filterQuery.gmkodLikes = reportConfig.config('yevmiye.gmkod.likes')
            }
            if(!filterQuery.aciklamaExcludes){
                filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
            }

//            def reportConfig = Report.findByReportId('detayli_tahsilat_beysu_1')
            //TODO: def ongoruHesplanBaslangicTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.min')
            //TODO: def ongoruHesplanBitisTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.max')
            def ongoruHesplanGmkodLikes = reportConfig.config('yevmiye.ongoru.hesplan.gmkod.likes')
            def ongoruHesplanPrefix = reportConfig.config('yevmiye.ongoru.hesplan.prefix')
            String ongoruHesplanSuffixDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.suffix.dateFormat')
            def ongoruMusHesplanRefField = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.field')
            def ongoruMusHesplanRefPrefix = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix')
            def ongoruOdemePlanTarihField = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.field')
            def ongoruOdemePlanTarihDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat')

            def tahsilatMusHesplanOdemeTarihField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tarih.field')
            def tahsilatMusHesplanOdemeTutarField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tutar.field')
            def ongoruHesplanBorcTutarField = reportConfig.config('yevmiye.ongoru.hesplan.borc.tutar.field')

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def ongoruOdemePlanTarihSdf = new SimpleDateFormat(ongoruOdemePlanTarihDateFormat)


            def query = "select tahsilat from Yevmiye tahsilat " +
                    " where gmkod like '${ongoruMusHesplanRefPrefix} %' " +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null" +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTarihField} is not null" +
                    //" and musteriHesplan.kod = tahsilat.gmkod "
                    " order by gmkod, ${tahsilatMusHesplanOdemeTarihField}"

            def musteriHesapPlaniVeTahsilatlar = //Yevmiye.executeQuery(query)


                    Yevmiye.where {
                        gmkod =~ "${ongoruMusHesplanRefPrefix} %"
                    }.join('hesplan')
                            .list(sort: 'gmkod')

            double tahsilEdilecekSenetToplami = 0
            def hesplanYevmiye = [:]
            def hesplanOdemePlani = [:]
            def hesplanTahsilat = [:]

//            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Object[] result ->
            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Yevmiye y ->

                if(y.alacakli) {
                    if(y.evrakno) {
                        try {
                            y.evraktarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.sonOdemeTarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.isOdemePlani = true
                            y.kalanTutar = y.alacakli
                            y.tutar = y.alacakli
                            y.index = 0
                            hesplanOdemePlani[y.hesplan] ? hesplanOdemePlani[y.hesplan] << y : (hesplanOdemePlani[y.hesplan] = [y])
                        }catch(Exception e){
                            log.error y.toString()
                            log.error "gecersiz odeme plani tarihi: ${y.evrakno} "
                            y.evrakno = "??${y.evrakno}??"
                        }
                    }
                }
                else if(y.borclu){
                    y.isTahsilat = true
                    y.kalanTutar = y.borclu
                    y.tutar = y.borclu
                    y.odemeTarihi = y.evraktarihi ?: y.fistar
                    y.index = 1 //siralamada tahsilatlar odeme planlarindan sonra listelensin
                    hesplanTahsilat[y.hesplan] ? hesplanTahsilat[y.hesplan] << y : (hesplanTahsilat[y.hesplan] = [y])
                }

                println y.alacakli ? "plan: $y" : "tahsilat: $y"

                hesplanYevmiye[y.hesplan] ? hesplanYevmiye[y.hesplan] << y : (hesplanYevmiye[y.hesplan] = [y])

//                Yevmiye y = result[0]
//                Hesplan h = result[1]
//                def retVal = new Expando(y)
                // y.metaClass.hesplanAciklama = h.aciklama
                // y.metaClass.toplamBorc = h.topb
                println y.alacakli ? "plan: $y" : "tahsilat: $y"
                y
            }

            def hesplanSonuc = [:]



            hesplanYevmiye.each {
                Hesplan hesplan = it.key
                List<Yevmiye> islenmemisOdemePlanlari = (hesplanOdemePlani[hesplan] ?: []).sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisOdemePlanlari = []
                //Iterable<Yevmiye> tahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmemisTahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisTahsilatlar = []

                def toplamAlacak = hesplan.topa
                def kalanAlacak = toplamAlacak
                def kalanAlacakPlan = toplamAlacak

                boolean odeme = false
                boolean borc = false

                println "${hesplan.aciklama} "
                println "Toplam alacak: ${toplamAlacak}"

                hesplanSonuc[hesplan] = []

                int loopGuard = 0

                while(islenmemisOdemePlanlari){
                    Yevmiye odemePlani = islenmemisOdemePlanlari.first()
                    kalanAlacakPlan -= odemePlani.tutar
                    odemePlani.kalanToplamBakiyePlan = kalanAlacakPlan

                    while(islenmemisTahsilatlar && !odemePlani.odendi) {
                        Yevmiye tahsilat = islenmemisTahsilatlar.first()
                       // def tahsilEdilen //odemePlanindaki kalan tutar ve tahsilattan eslestirilen  kalan tutar

                        //toplamAlacak -= tahsilat.

                        if(odemePlani.kalanTutar == tahsilat.kalanTutar){
                            //tahsilEdilen = odemePlani.kalanTutar
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar = 0
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat

                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        }
                        else if (odemePlani.kalanTutar < tahsilat.kalanTutar) {
                            //odeme borctan fazla, odemeden eslestirilen taksiti indir,
                            kalanAlacak -= odemePlani.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            tahsilat.kalanTutar -= odemePlani.kalanTutar
                            odemePlani.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()

                            //odeme borctan yuksek ise, odemeden bu taksiti dusup odemenin kalani ile devam et,
                            // bakiye eksi cikarsa odeme planinda eksik oldugu anlasilir
                            //kalan odemeyi tekrar islemek uzere tut

                        }
                        else if(odemePlani.kalanTutar > tahsilat.kalanTutar) {
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar -= tahsilat.kalanTutar
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = false
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()


                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        }
                        else {
                            println "cannot handle odemeplani: $odemePlani"
                            println "cannot handle tahsilat: $tahsilat"
                            break

                        }
                    }

                    islenmisTahsilatlar.sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }

                    islenmemisOdemePlanlari.remove(odemePlani)
                    islenmisOdemePlanlari << odemePlani

                    loopGuard++
                    if(loopGuard > 10000){
                        println "LOOPGUARD: ${odemePlani}"
                        println "LOOPGUARD: ${loopGuard}, breaking possibly infinite loop"
                    }
                }

                hesplanSonuc[hesplan] = [islenmisOdemePlanlari: islenmisOdemePlanlari,
                                         islenmemisOdemePlanlari: islenmemisOdemePlanlari, islenmisTahsilatlar : islenmisTahsilatlar,
                                         islenmemisTahsilatlar: islenmemisTahsilatlar]

                def hesplanKalanBakiye = hesplan.topa - (hesplan.topb ?: 0)
                tahsilEdilecekSenetToplami += hesplanKalanBakiye

                detayliTahsilatResults << new DetayliTahsilat(hesplan: hesplan,
                                                            islenmisOdemePlanlari:  islenmisOdemePlanlari,
                                                            islenmemisOdemePlanlari: islenmemisOdemePlanlari,
                                                            islenmisTahsilatlar: islenmisTahsilatlar,
                                                            islenmemisTahsilatlar: islenmemisTahsilatlar,
                                                            //kalanBakiye: hesplanKalanBakiye
                )


            }


//            if(!dataOnly) {
//
//                return runReport('/reports/musteri_tahsilat.jrxml', new JRBeanCollectionDataSource(datasource))
//            }
//            else{
//                return datasource
//            }
//            hesplanYevmiye
            respond detayliTahsilatResults
        }
    }

    def poi(YevmiyeQueryCommand filterQuery) {

        def detayliTahsilatResults = []

        Tenants.withId("ongoru") {
            def reportConfig = Report.findByReportId('detayli_tahsilat')
            if (!filterQuery.gmkods) {
                filterQuery.gmkods = reportConfig.config('yevmiye.gmkod.includes')
            }
            if (!filterQuery.gmkodLikes) {
                filterQuery.gmkodLikes = reportConfig.config('yevmiye.gmkod.likes')
            }
            if (!filterQuery.aciklamaExcludes) {
                filterQuery.aciklamaExcludes = reportConfig.config('yevmiye.aciklama.excludes')
            }

//            def reportConfig = Report.findByReportId('detayli_tahsilat_beysu_1')
            //TODO: def ongoruHesplanBaslangicTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.min')
            //TODO: def ongoruHesplanBitisTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.max')
            def ongoruHesplanGmkodLikes = reportConfig.config('yevmiye.ongoru.hesplan.gmkod.likes')
            def ongoruHesplanPrefix = reportConfig.config('yevmiye.ongoru.hesplan.prefix')
            String ongoruHesplanSuffixDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.suffix.dateFormat')
            def ongoruMusHesplanRefField = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.field')
            def ongoruMusHesplanRefPrefix = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix')
            def ongoruOdemePlanTarihField = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.field')
            def ongoruOdemePlanTarihDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat')

            def tahsilatMusHesplanOdemeTarihField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tarih.field')
            def tahsilatMusHesplanOdemeTutarField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tutar.field')
            def ongoruHesplanBorcTutarField = reportConfig.config('yevmiye.ongoru.hesplan.borc.tutar.field')

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def ongoruOdemePlanTarihSdf = new SimpleDateFormat(ongoruOdemePlanTarihDateFormat)


            def query = "select tahsilat from Yevmiye tahsilat " +
                    " where gmkod like '${ongoruMusHesplanRefPrefix} %' " +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null" +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTarihField} is not null" +
                    //" and musteriHesplan.kod = tahsilat.gmkod "
                    " order by gmkod, ${tahsilatMusHesplanOdemeTarihField}"

            def musteriHesapPlaniVeTahsilatlar = //Yevmiye.executeQuery(query)


                    Yevmiye.where {
                        gmkod =~ "${ongoruMusHesplanRefPrefix} %"
                    }.join('hesplan')
                            .list(sort: 'gmkod')


            def hesplanYevmiye = [:]
            def hesplanOdemePlani = [:]
            def hesplanTahsilat = [:]

//            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Object[] result ->
            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Yevmiye y ->

                if (y.alacakli) {
                    if (y.evrakno) {
                        try {
                            y.evraktarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.sonOdemeTarihi = ongoruOdemePlanTarihSdf.parse(y.evrakno)
                            y.isOdemePlani = true
                            y.kalanTutar = y.alacakli
                            y.tutar = y.alacakli
                            y.index = 0
                            hesplanOdemePlani[y.hesplan] ? hesplanOdemePlani[y.hesplan] << y : (hesplanOdemePlani[y.hesplan] = [y])
                        } catch (Exception e) {
                            log.error y.toString()
                            log.error "gecersiz odeme plani tarihi: ${y.evrakno} "
                            y.evrakno = "??${y.evrakno}??"
                        }
                    }
                } else if (y.borclu) {
                    y.isTahsilat = true
                    y.kalanTutar = y.borclu
                    y.tutar = y.borclu
                    y.odemeTarihi = y.evraktarihi ?: y.fistar
                    y.index = 1 //siralamada tahsilatlar odeme planlarindan sonra listelensin
                    hesplanTahsilat[y.hesplan] ? hesplanTahsilat[y.hesplan] << y : (hesplanTahsilat[y.hesplan] = [y])
                }

                println y.alacakli ? "plan: $y" : "tahsilat: $y"

                hesplanYevmiye[y.hesplan] ? hesplanYevmiye[y.hesplan] << y : (hesplanYevmiye[y.hesplan] = [y])

//                Yevmiye y = result[0]
//                Hesplan h = result[1]
//                def retVal = new Expando(y)
                // y.metaClass.hesplanAciklama = h.aciklama
                // y.metaClass.toplamBorc = h.topb
                println y.alacakli ? "plan: $y" : "tahsilat: $y"
                y
            }

            def hesplanSonuc = [:]

            def totalRows = 0

            def odendiStyleMap = [:]

            double tahsilEdilecekSenetToplami = 0

            hesplanYevmiye.each {
                Hesplan hesplan = it.key
                List<Yevmiye> islenmemisOdemePlanlari = (hesplanOdemePlani[hesplan] ?: []).sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisOdemePlanlari = []
                //Iterable<Yevmiye> tahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort{ a,b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmemisTahsilatlar = (hesplanTahsilat[hesplan] ?: []).sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }
                List<Yevmiye> islenmisTahsilatlar = []

                def toplamAlacak = hesplan.topa
                def kalanAlacak = toplamAlacak
                def kalanAlacakPlan = toplamAlacak

                boolean odeme = false
                boolean borc = false

                println "${hesplan.aciklama} "
                println "Toplam alacak: ${toplamAlacak}"

                hesplanSonuc[hesplan] = []

                int loopGuard = 0

                while (islenmemisOdemePlanlari) {
                    Yevmiye odemePlani = islenmemisOdemePlanlari.first()
                    kalanAlacakPlan -= odemePlani.tutar
                    odemePlani.kalanToplamBakiyePlan = kalanAlacakPlan

                    while (islenmemisTahsilatlar && !odemePlani.odendi) {
                        Yevmiye tahsilat = islenmemisTahsilatlar.first()
                        // def tahsilEdilen //odemePlanindaki kalan tutar ve tahsilattan eslestirilen  kalan tutar

                        //toplamAlacak -= tahsilat.

                        if (odemePlani.kalanTutar == tahsilat.kalanTutar) {
                            //tahsilEdilen = odemePlani.kalanTutar
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar = 0
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat

                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        } else if (odemePlani.kalanTutar < tahsilat.kalanTutar) {
                            //odeme borctan fazla, odemeden eslestirilen taksiti indir,
                            kalanAlacak -= odemePlani.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            tahsilat.kalanTutar -= odemePlani.kalanTutar
                            odemePlani.kalanTutar = 0

                            odemePlani.odendi = true
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()

                            //odeme borctan yuksek ise, odemeden bu taksiti dusup odemenin kalani ile devam et,
                            // bakiye eksi cikarsa odeme planinda eksik oldugu anlasilir
                            //kalan odemeyi tekrar islemek uzere tut

                        } else if (odemePlani.kalanTutar > tahsilat.kalanTutar) {
                            kalanAlacak -= tahsilat.kalanTutar
                            odemePlani.kalanToplamBakiye = kalanAlacak

                            odemePlani.kalanTutar -= tahsilat.kalanTutar
                            tahsilat.kalanTutar = 0

                            odemePlani.odendi = false
                            odemePlani.odemeTarihi = tahsilat.odemeTarihi
                            odemePlani.odemeler << tahsilat.clone()


                            islenmemisTahsilatlar.remove(tahsilat)
                            islenmisTahsilatlar << tahsilat
                        } else {
                            println "cannot handle odemeplani: $odemePlani"
                            println "cannot handle tahsilat: $tahsilat"
                            break

                        }
                    }

                    islenmisTahsilatlar.sort { a, b -> a.evraktarihi <=> b.evraktarihi ?: a.index <=> b.index }

                    islenmemisOdemePlanlari.remove(odemePlani)
                    islenmisOdemePlanlari << odemePlani

                    loopGuard++
                    if (loopGuard > 10000) {
                        println "LOOPGUARD: ${odemePlani}"
                        println "LOOPGUARD: ${loopGuard}, breaking possibly infinite loop"
                    }
                }

                def hesplanKalanBakiye = hesplan.topa - (hesplan.topb ?: 0 )
                tahsilEdilecekSenetToplami += hesplanKalanBakiye

                hesplanSonuc[hesplan] = [islenmisOdemePlanlari  : islenmisOdemePlanlari,
                                         islenmemisOdemePlanlari: islenmemisOdemePlanlari, islenmisTahsilatlar: islenmisTahsilatlar,
                                         islenmemisTahsilatlar  : islenmemisTahsilatlar,
                                         kalanBakiye: hesplanKalanBakiye
                ]

                detayliTahsilatResults << new DetayliTahsilat(hesplan: hesplan,
                        islenmisOdemePlanlari: islenmisOdemePlanlari,
                        islenmemisOdemePlanlari: islenmemisOdemePlanlari,
                        islenmisTahsilatlar: islenmisTahsilatlar,
                        islenmemisTahsilatlar: islenmemisTahsilatlar)

                totalRows += (9 + islenmisOdemePlanlari.size())

            }


            Workbook wb = new XSSFWorkbook()
            Sheet sheet = wb.createSheet('DETAYLI_TAHSILAT')
            sheet.printSetup.landscape = true
            sheet.printSetup.paperSize = PrintSetup.A3_PAPERSIZE
            sheet.printGridlines = false
            sheet.autobreaks = false

            Font boldFont = wb.createFont()
            boldFont.bold = true

            Font fontRed = wb.createFont()
            fontRed.color = Font.COLOR_RED

            Font boldFontRed = wb.createFont()
            boldFontRed.bold = true
            boldFontRed.color = Font.COLOR_RED

            DataFormat df = wb.createDataFormat()
            def tutarFormat = df.getFormat('#,##0.00')

            final DEFAULT_BORDER = BorderStyle.MEDIUM

            CellStyle csDefault = wb.createCellStyle()
//            csDefault.setFillForegroundColor IndexedColors.WHITE.index
//            csDefault.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csDefaultFgGreen = wb.createCellStyle()
            csDefaultFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csDefaultFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND


            CellStyle csDate = wb.createCellStyle()
            csDate.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
//            csDate.setFillForegroundColor IndexedColors.WHITE.index
//            csDate.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csDateFgGreen = wb.createCellStyle()
            csDateFgGreen.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csDateFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csDate] = csDateFgGreen

            CellStyle csM2 = wb.createCellStyle()
            csM2.dataFormat = tutarFormat
            csM2.alignment = HorizontalAlignment.LEFT
//            csM2.setFillForegroundColor IndexedColors.WHITE.index
//            csM2.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csM2FgGreen = wb.createCellStyle()
            csM2FgGreen.dataFormat = tutarFormat
            csM2FgGreen.alignment = HorizontalAlignment.LEFT
            csM2FgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csM2FgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csM2] = csM2FgGreen

            CellStyle csDateRed = wb.createCellStyle()
            csDateRed.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateRed.font = fontRed
//            csDateRed.setFillForegroundColor IndexedColors.WHITE.index
//            csDateRed.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csDateRedFgGreen = wb.createCellStyle()
            csDateRedFgGreen.dataFormat = wb.createDataFormat().getFormat('dd/mm/yyyy')
            csDateRedFgGreen.font = fontRed
            csDateRedFgGreen.setFillForegroundColor IndexedColors.GREEN.index
            csDateRedFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csDateRed] = csDateRedFgGreen

            CellStyle csLeftBorder = wb.createCellStyle()
            csLeftBorder.borderLeft = DEFAULT_BORDER
//            csLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csLeftBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )
//            csLeftBorder.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
//            csLeftBorder.setFillBackgroundColor IndexedColors.LIGHT_GREEN.index
//            csLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csLeftBorderFgGreen = wb.createCellStyle()
            csLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csLeftBorder] = csLeftBorderFgGreen

            CellStyle csRightBorder = wb.createCellStyle()
            csRightBorder.borderRight = DEFAULT_BORDER
//            csRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csRightBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csRightBorderFgGreen = wb.createCellStyle()
            csRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csRightBorder] = csRightBorderFgGreen

            CellStyle csTopLeftBorder = wb.createCellStyle()
            csTopLeftBorder.borderLeft = DEFAULT_BORDER
            csTopLeftBorder.borderTop = DEFAULT_BORDER
//            csTopLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csTopLeftBorderFgGreen = wb.createCellStyle()
            csTopLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csTopLeftBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopLeftBorder] = csTopLeftBorderFgGreen

            CellStyle csBottomLeftBorder = wb.createCellStyle()
            csBottomLeftBorder.borderLeft = DEFAULT_BORDER
            csBottomLeftBorder.borderBottom = DEFAULT_BORDER
//            csBottomLeftBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomLeftBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomLeftBorderFgGreen = wb.createCellStyle()
            csBottomLeftBorderFgGreen.borderLeft = DEFAULT_BORDER
            csBottomLeftBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomLeftBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomLeftBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomLeftBorder] = csBottomLeftBorderFgGreen

            CellStyle csTopRightBorder = wb.createCellStyle()
            csTopRightBorder.borderRight = DEFAULT_BORDER
            csTopRightBorder.borderTop = DEFAULT_BORDER
//            csTopRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopRightBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csTopRightBorderFgGreen = wb.createCellStyle()
            csTopRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csTopRightBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopRightBorder] = csTopRightBorderFgGreen

            CellStyle csBottomBorder = wb.createCellStyle()
            csBottomBorder.borderBottom = DEFAULT_BORDER
//            csBottomBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomBorderFgGreen = wb.createCellStyle()
            csBottomBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomBorder] = csBottomBorderFgGreen

            CellStyle csTopBorder = wb.createCellStyle()
            csTopBorder.borderTop = DEFAULT_BORDER
//            csTopBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csTopBorder.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTopBorderFgGreen = wb.createCellStyle()
            csTopBorderFgGreen.borderTop = DEFAULT_BORDER
            csTopBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTopBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTopBorder] = csTopBorderFgGreen

            CellStyle csBottomRightBorder = wb.createCellStyle()
            csBottomRightBorder.borderRight = DEFAULT_BORDER
            csBottomRightBorder.borderBottom = DEFAULT_BORDER
//            csBottomRightBorder.setFillForegroundColor IndexedColors.WHITE.index
//            csBottomRightBorder.setFillPattern FillPatternType.SOLID_FOREGROUND

            CellStyle csBottomRightBorderFgGreen = wb.createCellStyle()
            csBottomRightBorderFgGreen.borderRight = DEFAULT_BORDER
            csBottomRightBorderFgGreen.borderBottom = DEFAULT_BORDER
            csBottomRightBorderFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBottomRightBorderFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBottomRightBorder] = csBottomRightBorderFgGreen

            CellStyle csBold = wb.createCellStyle()
            csBold.font = boldFont
//            csBold.setFillForegroundColor IndexedColors.WHITE.index
//            csBold.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldFgGreen = wb.createCellStyle()
            csBoldFgGreen.font = boldFont
            csBoldFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBold] = csBoldFgGreen

            CellStyle csBoldAlignLeft = wb.createCellStyle()
            csBoldAlignLeft.font = boldFont
            csBoldAlignLeft.alignment = HorizontalAlignment.LEFT
//            csBoldAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldAlignLeftFgGreen = wb.createCellStyle()
            csBoldAlignLeftFgGreen.font = boldFont
            csBoldAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csBoldAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldAlignLeft] = csBoldAlignLeftFgGreen

            CellStyle csAlignLeft = wb.createCellStyle()
            csAlignLeft.alignment = HorizontalAlignment.LEFT
//            csAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csAlignLeftFgGreen = wb.createCellStyle()
            csAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csAlignLeft] = csAlignLeftFgGreen

            CellStyle csBoldAlignRight = wb.createCellStyle()
            csBoldAlignRight.font = boldFont
            csBoldAlignRight.alignment = HorizontalAlignment.RIGHT
//            csBoldAlignRight.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldAlignRight.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldAlignRightFgGreen = wb.createCellStyle()
            csBoldAlignRightFgGreen.font = boldFont
            csBoldAlignRightFgGreen.alignment = HorizontalAlignment.RIGHT
            csBoldAlignRightFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldAlignRightFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldAlignRight] = csBoldAlignRightFgGreen

            CellStyle csTutar = wb.createCellStyle()
            csTutar.dataFormat = tutarFormat
//            csTutar.setFillForegroundColor IndexedColors.WHITE.index
//            csTutar.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTutarFgGreen = wb.createCellStyle()
            csTutarFgGreen.dataFormat = tutarFormat
            csTutarFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csTutarFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTutar] = csTutarFgGreen

            CellStyle csTutarRed = wb.createCellStyle()
            csTutarRed.dataFormat = tutarFormat
            csTutarRed.font = fontRed
//            csTutarRed.setFillForegroundColor IndexedColors.WHITE.index
//            csTutarRed.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csTutarRedFgGreen = wb.createCellStyle()
            csTutarRedFgGreen.dataFormat = tutarFormat
            csTutarRedFgGreen.font = fontRed
            csTutarRedFgGreen.setFillForegroundColor IndexedColors.GREEN.index
            csTutarRedFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csTutarRed] = csTutarRedFgGreen

            CellStyle csBoldTutar = wb.createCellStyle()
            csBoldTutar.font = boldFont
            csBoldTutar.dataFormat = tutarFormat
//            csBoldTutar.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldTutar.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldTutarFgGreen = wb.createCellStyle()
            csBoldTutarFgGreen.font = boldFont
            csBoldTutarFgGreen.dataFormat = tutarFormat
            csBoldTutarFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldTutarFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldTutar] = csBoldTutarFgGreen

            CellStyle csBoldTutarAlignLeft = wb.createCellStyle()
            csBoldTutarAlignLeft.font = boldFont
            csBoldTutarAlignLeft.dataFormat = tutarFormat
            csBoldTutarAlignLeft.alignment = HorizontalAlignment.LEFT
//            csBoldTutarAlignLeft.setFillForegroundColor IndexedColors.WHITE.index
//            csBoldTutarAlignLeft.setFillPattern(FillPatternType.SOLID_FOREGROUND )

            CellStyle csBoldTutarAlignLeftFgGreen = wb.createCellStyle()
            csBoldTutarAlignLeftFgGreen.font = boldFont
            csBoldTutarAlignLeftFgGreen.dataFormat = tutarFormat
            csBoldTutarAlignLeftFgGreen.alignment = HorizontalAlignment.LEFT
            csBoldTutarAlignLeftFgGreen.setFillForegroundColor IndexedColors.LIGHT_GREEN.index
            csBoldTutarAlignLeftFgGreen.setFillPattern FillPatternType.SOLID_FOREGROUND

            odendiStyleMap[csBoldTutarAlignLeft] = csBoldTutarAlignLeftFgGreen

            int rowsPerBucket = totalRows / 4

            //TODO daha dengeli bir bucket distribution?
//            detayliTahsilatResults.eachWithIndex { DetayliTahsilat dt, Integer idx ->
//                def bucket = idx%4
//                buckets[bucket] ? buckets[bucket] << dt : (buckets[bucket] = [dt])
//            }

            int startRow = 3
            int cellsPerBucket = 6
            int currentBucket = 0
            int currentRow = startRow
            List currentRowForBucket = [startRow,startRow,startRow,startRow]

            int rowsPerPage = 60
            int pageRows = 0

            Pattern m2veOrtM2Regex = ~/(.*)\((.*[,]?.*[.]?.*)M2---(.*[,]?.*[.]?.*)\)/
            Pattern bbRegex = ~/120 (.*)-(.*)/

            def maxColumn1 = 0
            def maxColumn2 = 0
            def maxColumn3 = 0

            def detayRangeMap = [:]


            def bucketRangesPerRow = []
            def bucketRow = 0

            def noOfBuckets = 4


            sheet.setColumnWidth(0, 100)
            sheet.setDefaultRowHeight((short)300)

            def tahsilatRanges = []
            def bbM2Cells = []

            detayliTahsilatResults.sort{
                a,b -> a.islenmisOdemePlanlari.size() <=> b.islenmisOdemePlanlari.size()
            }.each { DetayliTahsilat dt ->
//                currentRow = currentRowForBucket[currentBucket]
                currentRow = currentRowForBucket[currentBucket]
                def detayFirstRow = currentRow


                Matcher matcher = m2veOrtM2Regex.matcher(dt.hesplan.aciklama)
                Matcher matcher2 = bbRegex.matcher(dt.hesplan.kod)

                def musteriVal = '?'
                def m2Val = 0
                def ortM2Val = 0
                def bbBlokVal = '?'
                def bbNoVal = '?'

                def firstColumn = currentBucket * cellsPerBucket + 1//(0,6,12,18)
                def lastColumn = firstColumn + 5
                def column1 = firstColumn + 1
                def column2 = firstColumn + 2
                def column3 = firstColumn + 3
                def column4 = firstColumn + 4

                sheet.setColumnWidth(firstColumn,100)
                sheet.autoSizeColumn(column1)
                sheet.autoSizeColumn(column2)
                sheet.autoSizeColumn(column3)
                sheet.autoSizeColumn(column4)
                sheet.setColumnWidth(lastColumn,100)


                if (matcher.find()) {
                    musteriVal = matcher.group(1).trim()
                    m2Val = matcher.group(2).trim().replace('.', '').replace(',', '').toDouble()/100
                    ortM2Val = matcher.group(3).trim().replace('.', '').replace(',', '').toDouble()/100
                }
                if(matcher2.find()){
                    bbBlokVal = matcher2.group(1).trim()
                    bbNoVal = matcher2.group(2).trim()
                }

                Row topRow = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                topRow.createCell(firstColumn)//.cellStyle = csTopLeftBorder
                topRow.createCell(column1)//.cellStyle = csTopBorder
                topRow.createCell(column2)//.cellStyle = csTopBorder
                topRow.createCell(column3)//.cellStyle = csTopBorder
                topRow.createCell(column4)//.cellStyle = csTopBorder
                topRow.createCell(lastColumn)//.cellStyle = csTopRightBorder

                currentRow++

                Row row = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                row.createCell(firstColumn).cellStyle = csBold
                row.createCell(lastColumn).cellStyle = csBold


                Cell musteriHeader = row.createCell(column1)
                musteriHeader.cellValue = "MÜŞTERİ"
                musteriHeader.cellStyle = csBold
                Cell musteri = row.createCell(column2)
                musteri.cellValue = musteriVal//dt.hesplan.aciklama
                musteri.cellStyle = csBoldAlignLeft

                currentRow++

                Row bbTipRow = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                bbTipRow.createCell(firstColumn).cellStyle = csBold
                bbTipRow.createCell(lastColumn).cellStyle = csBold

                Cell bbTipHeader = bbTipRow.createCell(column1)
                bbTipHeader.cellValue = "BB TİP"
                bbTipHeader.cellStyle = csBold
                Cell bbTip = bbTipRow.createCell(column2)
                bbTip.cellValue = "?"
                bbTip.cellStyle = csAlignLeft


                currentRow++

                Row bbBlokRow = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                bbBlokRow.createCell(firstColumn).cellStyle = csBold
                bbBlokRow.createCell(lastColumn).cellStyle = csBold

                Cell bbBlokHeader = bbBlokRow.createCell(column1)
                bbBlokHeader.cellValue = "BB BLOK"
                bbBlokHeader.cellStyle = csBold
                Cell bbBlok = bbBlokRow.createCell(column2)
                bbBlok.cellValue = bbBlokVal
                bbBlok.cellStyle = csAlignLeft

                currentRow++

                Row bbNoRow = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                bbNoRow.createCell(firstColumn).cellStyle = csBold
                bbNoRow.createCell(lastColumn).cellStyle = csBold

                Cell bbNoHeader = bbNoRow.createCell(column1)
                bbNoHeader.cellValue = "BB NO"
                bbNoHeader.cellStyle = csBold
                Cell bbNo = bbNoRow.createCell(column2)
                bbNo.cellValue = bbNoVal
                bbNo.cellStyle = csAlignLeft

                currentRow++

                Row bbM2Row = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                def firstCell = bbM2Row.createCell(firstColumn)
                firstCell.cellStyle = csBold
                def lastCell = bbM2Row.createCell(lastColumn)
                lastCell.cellStyle = csBold

                Cell bbM2Header = bbM2Row.createCell(column1)
                bbM2Header.cellValue = "BB M2"
                bbM2Header.cellStyle = csBold
                Cell bbM2 = bbM2Row.createCell(column2)
//                bbM2.cellType = Cell.CELL_TYPE_NUMERIC
                bbM2.cellValue = m2Val
                bbM2.cellStyle = csM2

                bbM2Cells << bbM2

                currentRow++

                Row ortM2Row = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                ortM2Row.createCell(firstColumn).cellStyle = csBold
                ortM2Row.createCell(lastColumn).cellStyle = csBold

                Cell ortM2Header = ortM2Row.createCell(column1)
                ortM2Header.cellValue = "ORT M2"
                ortM2Header.cellStyle = csBold
                Cell ortM2 = ortM2Row.createCell(column2)
//                ortM2.cellType = Cell.CELL_TYPE_NUMERIC
                ortM2.cellValue = ortM2Val
                ortM2.cellStyle = csM2

                currentRow++

                Row row2 = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                row2.createCell(firstColumn).cellStyle = csBold
                row2.createCell(lastColumn).cellStyle = csBold

                Cell satisBedeliHeader = row2.createCell(column1)
                satisBedeliHeader.cellValue = "SATIŞ BEDELİ"
                satisBedeliHeader.cellStyle = csBold
                Cell satisBedeli = row2.createCell(column2)
                satisBedeli.cellValue = dt.hesplan.topa
                satisBedeli.cellStyle = csBoldTutarAlignLeft

                currentRow++

                Row row3 = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                row3.createCell(firstColumn).cellStyle = csBold
                row3.createCell(lastColumn).cellStyle = csBold

                Cell bakiyeHeader = row3.createCell(column4)
                bakiyeHeader.cellValue = 'BAKİYE'
                bakiyeHeader.cellStyle = csBoldAlignRight

                currentRow++

                Row row4 = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                row4.createCell(firstColumn).cellStyle = csBold
                row4.createCell(lastColumn).cellStyle = csBold

                Cell tahsilatTarihiHeader = row4.createCell(column1)
                tahsilatTarihiHeader.cellValue = 'TAHSİL TARİHİ'
                tahsilatTarihiHeader.cellStyle = csBoldAlignRight

                row4.createCell(column3).cellValue = "KALAN BAKİYE"


                Cell kalanBakiye = row4.createCell(column4)
                kalanBakiye.cellValue = dt.hesplan.topa
                kalanBakiye.cellStyle = csBoldTutar

                currentRow++

                //TODO bastaki ve sonrdaki fazla olan border cell ler icin sheet.setColumnWidth(1) ile kucult

                tahsilatRanges << new CellRangeAddress(currentRow, currentRow+dt.islenmisOdemePlanlari.size(), column1, column4)

                dt.islenmisOdemePlanlari.each { Yevmiye y ->
                    Row r = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                    r.createCell(firstColumn).cellStyle = csBold
                    r.createCell(lastColumn).cellStyle = csBold

                    Cell c1 = r.createCell(column1)
                    c1.setCellValue(y.evraktarihi)
                    c1.cellStyle = !y.odendi ? csDateRed : csDate

                    def c2 = r.createCell(column2)
                    c2.setCellValue(y.tutar)
                    c2.cellStyle = !y.odendi ? csTutarRed : csTutar

                    r.createCell(column3).cellValue = y.kalanToplamBakiye

                    sheet.setColumnHidden column3, true

                    def c3 = r.createCell(column4)
                    c3.setCellValue(y.kalanToplamBakiyePlan)
                    c3.cellStyle = !y.odendi ? csTutarRed : csTutar

                    currentRow++

                }

               /* def lastRow = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
                lastRow.createCell(firstColumn).cellStyle = csBottomLeftBorder
                lastRow.createCell(column1).cellStyle = csBottomBorder
                lastRow.createCell(column2).cellStyle = csBottomBorder
                lastRow.createCell(column3).cellStyle = csBottomBorder
                lastRow.createCell(lastColumn).cellStyle = csBottomRightBorder*/

                def detayLastRow = currentRow

//                currentRow++

                /*if (currentRow - startRow > rowsPerBucket) {
                    currentRow = startRow
                    currentBucket++
                }*/

                //alani belirleyen sınır hucreleri bos olmali, daha sonra bu rangelerden yararlanarak bunlara style verilecek
                CellRangeAddress cellRangeAddress = new CellRangeAddress(detayFirstRow, detayLastRow, firstColumn, lastColumn)

                bucketRangesPerRow[bucketRow] ? (bucketRangesPerRow[bucketRow] << cellRangeAddress) : (bucketRangesPerRow[bucketRow] = [cellRangeAddress])

                detayRangeMap.put(dt, cellRangeAddress)


                currentRowForBucket[currentBucket] = currentRow + 3 //Math.max(currentRow, currentRowForBucket.max())
                currentBucket = (currentBucket + 1) % noOfBuckets
                if(currentBucket == 0){
                    bucketRow ++
                    //reset all next 4 bucket to start at the same row
                    (0..3).each { int bucket ->
                        currentRowForBucket[bucket] = currentRowForBucket.max()
                    }
                }
            }

            def startRows = [] as Set

            bucketRangesPerRow.each { List<CellRangeAddress> bucketCellRanges ->
                def maxBucketRow = bucketCellRanges.sort{ a,b -> a.lastRow <=> b.lastRow}.last().lastRow
                bucketCellRanges.each {
                    it.lastRow = maxBucketRow
                }
            }
//            currentRow = bucketRangesPerRow.last().sort{ a,b -> a.lastRow <=> b.lastRow}.last().lastRow + 3
//            println currentRow

            currentRow = currentRowForBucket[currentBucket]
            def currentColumn = currentBucket * cellsPerBucket

            Row musteriSummary = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)
            Cell musteriSummaryCell = musteriSummary.createCell(currentColumn+2)
            musteriSummaryCell.setCellValue "${detayliTahsilatResults.size()} ADET MÜŞTERİ"
            musteriSummaryCell.cellStyle = wb.createCellStyle()
            musteriSummaryCell.cellStyle.font = boldFont

            currentRow++

            Row senetSummary = sheet.getRow(currentRow) ?: sheet.createRow(currentRow)

            Cell senetSummaryHeaderCell = senetSummary.createCell(currentColumn+2)
            senetSummaryHeaderCell.setCellValue "TAHSİL EDİLECEK SENET TOPLAMI"
            senetSummaryHeaderCell.cellStyle = wb.createCellStyle()
            senetSummaryHeaderCell.cellStyle.font = boldFont
            senetSummaryHeaderCell.cellStyle.fillForegroundColor = IndexedColors.LIGHT_YELLOW.index
            senetSummaryHeaderCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND

//            Cell middleCell = senetSummary.createCell(currentColumn+3)
//            middleCell.cellStyle = wb.createCellStyle()
//            middleCell.cellStyle.fillForegroundColor = IndexedColors.LIGHT_YELLOW.index
//            middleCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND


            Cell senetSummaryCell = senetSummary.createCell(currentColumn+3)
            senetSummaryCell.setCellValue tahsilEdilecekSenetToplami
            senetSummaryCell.cellStyle = wb.createCellStyle()
            senetSummaryCell.cellStyle.font = boldFont
            senetSummaryCell.cellStyle.dataFormat = tutarFormat
            senetSummaryCell.cellStyle.fillBackgroundColor = IndexedColors.LIGHT_YELLOW.index
            senetSummaryCell.cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND




            for(Map.Entry entry in detayRangeMap){
                DetayliTahsilat dt = entry.key
                CellRangeAddress region = entry.value

                startRows << region.firstRow

                def odendi = dt.hesplan.topa == dt.hesplan.topb

                for(int i = region.firstRow; i <= region.lastRow; i++){
                    Row row = sheet.getRow(i) ?: sheet.createRow(i)
                    row.height
                    for(int j = region.firstColumn; j <= region.lastColumn; j++){
                        Cell cell = row.getCell(j) ?: row.createCell(j)

                        if(i == region.firstRow){
                            if(j == region.firstColumn) {
                                cell.cellStyle = /*odendi ? csTopLeftBorderFgGreen :*/ csTopLeftBorder
                            }
                            else if (j == region.lastColumn){
                                cell.cellStyle = /*odendi ? csTopRightBorderFgGreen :*/ csTopRightBorder
                            }
                            else {
                                cell.cellStyle = /*odendi ? csTopBorderFgGreen :*/ csTopBorder
                            }
                        }
                        else if(i == region.lastRow){
                            if(j == region.firstColumn) {
                                cell.cellStyle = /*odendi ? csBottomLeftBorderFgGreen :*/ csBottomLeftBorder
                            }
                            else if (j == region.lastColumn){
                                cell.cellStyle = /*odendi ? csBottomRightBorderFgGreen :*/ csBottomRightBorder
                            }
                            else {
                                cell.cellStyle = /*odendi ? csBottomBorderFgGreen :*/ csBottomBorder
                            }
                        }
                        else{
                            if(j == region.firstColumn) {
                                cell.cellStyle = /*odendi ? csLeftBorderFgGreen :*/ csLeftBorder
                            }
                            else if (j == region.lastColumn){
                                cell.cellStyle = /*odendi ? csRightBorderFgGreen :*/ csRightBorder
                            }
                            else {
                              //  cell.cellStyle = odendi ? (odendiStyleMap[cell.cellStyle] ?: (getCellValue(cell) != null ? cell.cellStyle : csDefaultFgGreen)) : (getCellValue(cell) != null ? cell.cellStyle : csDefault)
                            }
                        }
                    }
                }
            }

            def lastBreak = 0
            for(int i =0; i < startRows.size(); i++){
                if(startRows[i] - lastBreak >= rowsPerPage){
                    lastBreak = startRows[i-1] -2
                    sheet.setRowBreak(lastBreak) //above next row
                }
                if(i == startRows.size() - 1){
                    lastBreak = startRows[i] -2
                    sheet.setRowBreak(lastBreak)
                }
            }

            SheetConditionalFormatting sheetCF = sheet.sheetConditionalFormatting

            ConditionalFormattingRule rule2 = sheetCF.createConditionalFormattingRule('1')
            PatternFormatting patternFmt = rule2.createPatternFormatting()
            patternFmt.fillBackgroundColor = IndexedColors.WHITE.index
            patternFmt.fillPattern = PatternFormatting.SOLID_FOREGROUND

            CellRangeAddress[] allDetayRange = [new CellRangeAddress(startRow, sheet.getLastRowNum(), 0, noOfBuckets*cellsPerBucket)]

            sheetCF.addConditionalFormatting(allDetayRange, rule2)


            //tahsilat ile planlanan odeme tutari ayni degilse taksit satirini kirmizi fontla isaretle
            ConditionalFormattingRule rule1 = sheetCF.createConditionalFormattingRule('NOT($E14=$F14)')
            FontFormatting fontFmt = rule1.createFontFormatting()
            fontFmt.setFontColorIndex IndexedColors.RED.index
            PatternFormatting patternFmt1 = rule1.createPatternFormatting()
            patternFmt1.fillBackgroundColor = IndexedColors.WHITE.index
            patternFmt1.fillPattern = PatternFormatting.SOLID_FOREGROUND

            sheetCF.addConditionalFormatting(tahsilatRanges.toArray(new CellRangeAddress[0]), rule1)


            //tahsilat planinda son bakiye 0 degilse planda eksik taksit bulunuyor kalan son bakiyeyi veya tum alani kirmizi ile isaretle
            tahsilatRanges.eachWithIndex { CellRangeAddress range, int i ->
                def lastTahsilatTarihCellRef = "${CellReference.convertNumToColString(range.firstColumn)}${range.lastRow}"
                def lastTahsilatBakiyeCellRef = "${CellReference.convertNumToColString(range.lastColumn)}${range.lastRow}"
                ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule("\$$lastTahsilatBakiyeCellRef > 0")
                FontFormatting fontFmt3 = rule3.createFontFormatting()
                fontFmt3.setFontColorIndex IndexedColors.RED.index
                fontFmt3.setFontStyle(false, true)
                PatternFormatting patternFmt3 = rule3.createPatternFormatting()
                patternFmt3.fillForegroundColor = IndexedColors.RED.index
                patternFmt3.fillPattern = PatternFormatting.SOLID_FOREGROUND

                CellRangeAddress[] ranges = [CellRangeAddress.valueOf("$lastTahsilatTarihCellRef:$lastTahsilatBakiyeCellRef")]
                sheetCF.addConditionalFormatting(ranges, rule3)
            }

            def toplamM2Formula = "SUM(" + (bbM2Cells.collect {Cell m2Cell ->
                CellReference ref = new CellReference(m2Cell.getRow().getRowNum(), m2Cell.getColumnIndex())
                ref.formatAsString()
            }.join(',')) + ")"


            (0..noOfBuckets*cellsPerBucket).each { int col ->
                sheet.autoSizeColumn(col)
            }


            response.setContentType 'application/vnd.ms-excel'
            //def reportBytes = wb.bytes
            //response.setContentLength reportBytes.length
            response.setCharacterEncoding "UTF-8"
            response.setHeader "Content-Disposition", "attachment;filename=detayliTahsilat.xlsx"

//            response.outputStream << reportBytes
            wb.write response.outputStream
            response.outputStream.flush()
            response.outputStream.close()
            return null
        }
    }

    private def getCellValue(Cell cell){
        def val
        switch (cell.cellType){
            case Cell.CELL_TYPE_BLANK:
                val = null
                break
            case Cell.CELL_TYPE_NUMERIC:
                val = cell.numericCellValue
                break
            case Cell.CELL_TYPE_STRING:
                val = cell.stringCellValue
                break
            default:
                val = null
        }
    }
}

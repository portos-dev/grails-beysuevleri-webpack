package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.transaction.Transactional

@Transactional(readOnly = true)
class HesplanService {

    def search(HesplanQueryCommand filterQuery) {

        def queryParams = [:]
        def queryCriteria = []

        def params = [:]
        params.max = filterQuery.max ?: 25
        params.offset = filterQuery.offset ?: 0
//        params.sort = filterQuery.sort ?: ''
//        params.order = filterQuery.order ?: ''


        /* def criteria = Yevmiye.where {*/
        if (filterQuery.kod) {
            queryParams['gmkod'] = filterQuery.kod
            queryCriteria << 'kod = :gmkod'
        }
        if(filterQuery.kods){
            queryParams['gmkods'] = filterQuery.kods
            queryCriteria << 'kod in :gmkods'
        }
        if(filterQuery.kodLike){
            queryParams['gmkodLike'] = filterQuery.kodLike
            queryCriteria << 'kod like :gmkodLike'
        }

        if(filterQuery.kodLikes){
            def orCriteria = []
            filterQuery.kodLikes.eachWithIndex {
                String kodLike, Integer index ->
                    queryParams["gmkodLike${index}"] = kodLike
                    orCriteria << "kod like :gmkodLike${index}"
            }
            queryCriteria << "(${orCriteria.join(" or ")})"
        }

        def query = " from Hesplan hesplan "
//        def finalQuery = "select hesplan ${query} where ${queryCriteria.join(' and ')} order by $params.sort $params.order"
        def finalQuery = "select hesplan ${query} where ${queryCriteria.join(' and ')} "
        def countQuery = "select count (*) ${query} where ${queryCriteria.join(' and ')}"
        def results = Hesplan.executeQuery(finalQuery, queryParams, params )
        Long count = Hesplan.executeQuery(countQuery, queryParams)[0]

        ([items: results,
          count: count.toInteger(),
          max: params.max,
          offset: params.offset,
          sort: params.sort,
          order: params.order,
        ])
    }
}

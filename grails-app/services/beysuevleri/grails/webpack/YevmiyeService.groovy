package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.transaction.Transactional

@Transactional(readOnly = true)
class YevmiyeService {

    def search(YevmiyeQueryCommand filterQuery) {

        Tenants.withCurrent { // TODO  bu methodun cagrildigi yerde zaten ilgili tenant seciliyor, bu tenant bilgisi gereksiz mi?

            println filterQuery

            def queryParams = [:]
            def queryCriteria = []

            def dateField = 'evraktarihi' //'fistar'  ?


            def params = [:]
            params.max = filterQuery.max ?: 25
            params.offset = filterQuery.offset ?: 0
            params.sort = filterQuery.sort ?: dateField
            params.order = filterQuery.order ?: 'asc'

            /* def criteria = Yevmiye.where {*/
            if (filterQuery.gmkod) {
                queryParams['gmkod'] = filterQuery.gmkod
                queryCriteria << 'gmkod = :gmkod'
            }
            if (filterQuery.gmkods) {
                queryParams['gmkods'] = filterQuery.gmkods
                queryCriteria << 'gmkod in :gmkods'
            }
            if (filterQuery.gmkodLike) {
                queryParams['gmkodLike'] = filterQuery.gmkodLike
                queryCriteria << 'gmkod like :gmkodLike'
            }

            if (filterQuery.gmkodLikes) {
                def orCriteria = []
                filterQuery.gmkodLikes.eachWithIndex {
                    String kodLike, Integer index ->
                        queryParams["gmkodLike${index}"] = kodLike
                        orCriteria << "gmkod like :gmkodLike${index}"
                }
                queryCriteria << "(${orCriteria.join(" or ")})"
            }

            if (filterQuery.baslangicTarihi) {
//                fistar >= filterQuery.baslangicTarihi
                queryParams['baslangicTarihi'] = filterQuery.baslangicTarihi
                queryCriteria << "${dateField} >= :baslangicTarihi"
            }
            if (filterQuery.bitisTarihi) {
//                fistar <= filterQuery.baslangicTarihi
                queryParams['bitisTarihi'] = filterQuery.bitisTarihi
                queryCriteria << "${dateField}  <= :bitisTarihi"
            }

            if (filterQuery.aciklamaExcludes) {
                filterQuery.aciklamaExcludes.each {
                    String exc ->
//                        aciklama ==~ exc
//                        queryParams
                        queryCriteria << "aciklama not like '${exc}'"
                }
            }

            if (filterQuery.serinoExcludes) {
                filterQuery.serinoExcludes.each {
                    String exc ->
                        queryCriteria << "serino != '${exc}'"
                }
            }

            if (filterQuery.aciklamaLike) {
                queryCriteria << "lower(aciklama) like lower('%$filterQuery.aciklamaLike%')"
            }

//            aciklama ==~ filterQuery.excludes.first()
//            aciklama =~ filterQuery.excludes.last()
            /*   }*/

            def query = " from Yevmiye yevmiye "
            def finalQuery = "select yevmiye ${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')} order by $params.sort $params.order"
            def countQuery = "select count (*) ${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')}"
            def results = Yevmiye.executeQuery(finalQuery, queryParams, params)
            Long count = Yevmiye.executeQuery(countQuery, queryParams)[0]

            ([items : results,
              count : count.toInteger(),
              max   : params.max,
              offset: params.offset,
              sort  : params.sort,
              order : params.order,
            ])

        }
    }
}

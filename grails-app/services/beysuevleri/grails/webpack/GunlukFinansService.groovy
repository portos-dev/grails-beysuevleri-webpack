package beysuevleri.grails.webpack

import grails.transaction.Transactional

@Transactional
class GunlukFinansService {

    def search(GunlukFinansQueryCommand cmd, params = [:]) {

        params.max = params?.max ? params.max : 10
        params.sort = params?.sort ? params.sort : 'fistar'
        params.order = params?.order ? params.order : 'asc'

        Yevmiye.where {
            if (cmd.gmkod) {
                gmkod == cmd.gmkod
            }
//            if(cmd.baslangicTarihi && cmd.bitisTarihi){
//                //TODO between ?
//            }
            if(cmd.baslangicTarihi){
                fistar >= cmd.baslangicTarihi
            }
            if(cmd.bitisTarihi){
                fistar <= cmd.baslangicTarihi
            }
        }.list(params)

    }
}

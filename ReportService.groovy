package beysuevleri.grails.webpack

import grails.gorm.multitenancy.Tenants
import grails.transaction.Transactional
import net.sf.jasperreports.engine.JRDataSource
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource

import java.beans.Transient
import java.text.SimpleDateFormat

@Transactional
class ReportService {

    def hesplanService //TODO need another for ongoru service?
    def yevmiyeService
    def yevmiyeOngoruService

    def index(YevmiyeQueryCommand cmd){

        def monthDateFormat = new SimpleDateFormat('MM-yyyy')
        def data = yevmiyeService.search(cmd)
        def hesplanMap = [:]
        Hesplan.list().
                each{
                    Hesplan h -> hesplanMap.put(h.kod, h.aciklama) }


        def joinData = []

        data.items.each {Yevmiye y ->
            def hesplanItems = y.gmkod.split(' ').toList()
//            def depth = hesplanItems.size()
//            def parentGmkod = hesplanItems.subList(0,Math.max(1,depth-2)).join(" ")
            def parentGmkod = y.gmkod.substring(0,y.gmkod.lastIndexOf(" "))
            def rootGmkod = hesplanItems[0]
            //TODO: parent hesplan ve rapor goruntulemede
            //TODO: kullanilacak text degerleri bir config tabvle ile kontrol edilebilmeli
            //FIXME: yilbasindan itibaren cekilen raporda acilis fisleri oldugu icin bakiye hesaplanabiliyor aksi durumda rapor alinan tarihlerden
            //FIXME: bir gun onceki bakiyenin hesaplanmasi ve parametre olarak rapora girilmesi gerekir
            def hesplan = hesplanMap[y.gmkod]
            def parentHesplan = hesplanMap[parentGmkod]
            def rootHesplan = hesplanMap[rootGmkod]
            joinData << [gmkod: y.gmkod,
                        hesplan: hesplan,
                        parentHesplan: parentHesplan,
                        rootHesplan: rootHesplan,
                        month: monthDateFormat.format(y.evraktarihi),
                         aciklama: y.aciklama,
                         fistar: y.fistar,
                         evraktarihi: y.evraktarihi,
                         borclu: y.borclu,
                         alacakli: y.alacakli]
        }

        def params = [:]
        params['REPORT_LOCALE'] = new Locale('tr','TR')

        String reportname = '/reports/gunluk_odeme_tahsilat.jrxml'
        String jasperFileName = 'test.jasper'

        String subReportName = '/reports/gunluk_odeme_tahsilat_aylik_ozet_subreport.jrxml'

        InputStream reportInputStream = this.class.getResourceAsStream(reportname)
        InputStream subReportInputStream = this.class.getResourceAsStream(subReportName)

//        JasperCompileManager.compileReportToFile(reportname, jasperFileName)
        def report = JasperCompileManager.compileReport(reportInputStream)
        def subReport = JasperCompileManager.compileReport(subReportInputStream)

        params['SUB_REPORT'] = subReport

        JasperPrint print = JasperFillManager.fillReport(report, params,
//                new JRBeanCollectionDataSource(data.items))
                new JRMapCollectionDataSource(joinData))

        JasperExportManager.exportReportToPdf(print)
    }




    def detayliTahsilatPredefinedMusteriHesplanOnly(Boolean dataOnly){
        Tenants.withId("ongoru") {
            def reportConfig = Report.findByReportId('detayli_tahsilat')
//            def reportConfig = Report.findByReportId('detayli_tahsilat_beysu_1')
            //TODO: def ongoruHesplanBaslangicTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.min')
            //TODO: def ongoruHesplanBitisTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.max')
            def ongoruHesplanGmkodLikes = reportConfig.config('yevmiye.ongoru.hesplan.gmkod.likes')
            def ongoruHesplanPrefix = reportConfig.config('yevmiye.ongoru.hesplan.prefix')
            String ongoruHesplanSuffixDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.suffix.dateFormat')
            def ongoruMusHesplanRefField = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.field')
            def ongoruMusHesplanRefPrefix = reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix')
            def ongoruOdemePlanTarihField = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.field')
            def ongoruOdemePlanTarihDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat')

            def tahsilatMusHesplanOdemeTarihField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tarih.field')
            def tahsilatMusHesplanOdemeTutarField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tutar.field')
            def ongoruHesplanBorcTutarField = reportConfig.config('yevmiye.ongoru.hesplan.borc.tutar.field')

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def ongoruOdemePlanTarihSdf = new SimpleDateFormat(ongoruOdemePlanTarihDateFormat)


//            def musteriHesapPlaniVeOdemeleri = Yevmiye.where {
//                gmkod =~ '120 %'
//            }.list(sort: 'gmkod,evraktarihi' )

//            def query = "select tahsilat, musteriHesplan from Yevmiye tahsilat, Hesplan musteriHesplan " +
            def query = "select tahsilat from Yevmiye tahsilat " +
                    " where gmkod like '${ongoruMusHesplanRefPrefix} %' " +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null" +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTarihField} is not null" +
                    //" and musteriHesplan.kod = tahsilat.gmkod "
                    " order by gmkod, ${tahsilatMusHesplanOdemeTarihField}"

            def musteriHesapPlaniVeTahsilatlar = //Yevmiye.executeQuery(query)


            Yevmiye.where {
                gmkod =~ "${ongoruMusHesplanRefPrefix} %"
            }.join('hesplan')
            .list()

//            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Object[] result ->
            def datasource = musteriHesapPlaniVeTahsilatlar.collect { Yevmiye y ->
//                Yevmiye y = result[0]
//                Hesplan h = result[1]
//                def retVal = new Expando(y)
               // y.metaClass.hesplanAciklama = h.aciklama
               // y.metaClass.toplamBorc = h.topb
                println y.alacakli ? "plan: $y" : "tahsilat: $y"
                y
            }
            if(!dataOnly) {

                return runReport('/reports/musteri_tahsilat.jrxml', new JRBeanCollectionDataSource(datasource))
            }
            else{
                return datasource
            }
        }
    }






    //TODO: detayli tahsilat raporu icin birden fazla predefined config olabilir ve bunlar bir listeden secilip ilgili predefined value icin rapor alinabilir olmali
    def detayliTahsilatPredefined(){
        Tenants.withId("ongoru"){
            def reportConfig = Report.findByReportId('detayli_tahsilat')
//            def reportConfig = Report.findByReportId('detayli_tahsilat_beysu_1')
            //TODO: def ongoruHesplanBaslangicTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.min')
            //TODO: def ongoruHesplanBitisTarihi = reportConfig.config('yevmiye.ongoru.hesplan.evraktarihi.max')
            def ongoruHesplanGmkodLikes = reportConfig.config('yevmiye.ongoru.hesplan.gmkod.likes')
            def ongoruHesplanPrefix = reportConfig.config('yevmiye.ongoru.hesplan.prefix')
            String ongoruHesplanSuffixDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.suffix.dateFormat')
            def ongoruMusHesplanRefField =       reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.field')
            def ongoruMusHesplanRefPrefix =      reportConfig.config('yevmiye.ongoru.hesplan.musteri.hesplan.ref.prefix')
            def ongoruOdemePlanTarihField =      reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.field')
            def ongoruOdemePlanTarihDateFormat = reportConfig.config('yevmiye.ongoru.hesplan.odeme.plan.tarih.dateFormat')

            def tahsilatMusHesplanOdemeTarihField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tarih.field')
            def tahsilatMusHesplanOdemeTutarField = reportConfig.config('yevmiye.tahsilat.hesplan.odeme.tutar.field')

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def ongoruOdemePlanTarihSdf = new SimpleDateFormat(ongoruOdemePlanTarihDateFormat)

            def queryParams = [:]
            def queryCriteria = []

            if (ongoruHesplanGmkodLikes) {
                def orCriteria = []
                ongoruHesplanGmkodLikes.eachWithIndex {
                    String kodLike, Integer index ->
                        queryParams["gmkodLike${index}"] = kodLike
                        orCriteria << "yevmiye.gmkod like :gmkodLike${index}"
                }
                queryCriteria << "(${orCriteria.join(" or ")})"
            }
            queryCriteria << "yevmiye.${ongoruMusHesplanRefField} is not null" //stk bos olmamali
            queryCriteria << "yevmiye.${ongoruOdemePlanTarihField} is not null" //evrak no bos olmamali
            queryCriteria << "yevmiyeHesplan.kod = yevmiye.gmkod"
//            queryCriteria << "tahsilatHesplan.kod = concat('${ongoruMusHesplanRefPrefix}', ' ' , yevmiye.${ongoruMusHesplanRefField})"

//            def query = " from Yevmiye yevmiye, Hesplan yevmiyeHesplan, Hesplan tahsilatHesplan "//TODO: tah hesplan?
            def query = " from Yevmiye yevmiye, Hesplan yevmiyeHesplan "//TODO: tah hesplan?
            def finalQuery =
                    //"select new map( yevmiye.gmkod as yevGmkod, yevmiye.aciklama as yevAciklama, " +
                    //"yevmiye.borclu as yevBorclu,yevmiye.alacakli as yevAlacakli, yevmiye.evrakno as yevEvrakno, " +
//                    " musteriHesplan as yevMusteriHesplan) " +
                    //"select yevmiye, yevmiyeHesplan, tahsilatHesplan " +
                    "select new map(" +
                            " yevmiye.gmkod as yevGmkod, yevmiye.aciklama as yevAciklama, yevmiye.borclu as yevBorclu,yevmiye.alacakli as yevAlacakli," +
                            " yevmiye.${ongoruOdemePlanTarihField} as yevSonOdemeTarihi," +
                            " yevmiyeHesplan.aciklama as yevHesplanAciklama, yevmiyeHesplan.kod as yevHesplanKod," +
                            " concat('${ongoruMusHesplanRefPrefix} ', yevmiye.${ongoruMusHesplanRefField}) as tahHesplanKod)" +

//                            " tahsilatHesplan.aciklama as tahHesplanAciklama, tahsilatHesplan.kod as tahHesplanKod)" +
                    "${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')} " +
                    "order by  tahHesplanKod, yevHesplanKod"
            def countQuery = "select count (*) ${query} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')}"
            def odemePlanlari = Yevmiye.executeQuery(finalQuery, queryParams/*, params*/)/*.collect{ Map result ->
                result.put('yevSonOdemeTarihi', ongoruOdemePlanTarihSdf.parse(result["yevSonOdemeTarihi"]))
                result
            }*/



            def tahsilatQuery = "select tahsilat from Yevmiye tahsilat " +
                    " where tahsilat.gmkod = :gmkod " +
                    " and month(tahsilat.evraktarihi) = :month" +
                    " and year(tahsilat.evraktarihi) = :year" +
                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null"
            odemePlanlari.each { Map m ->
                def musteriHesplanKod = m['tahHesplanKod']
                def sonOdemeTarihi = ongoruOdemePlanTarihSdf.parse(m['yevSonOdemeTarihi'])
                //def sonOdemeTarihi = m['yevSonOdemeTarihi']
                def month = sonOdemeTarihi[Calendar.MONTH]+1
                def year = sonOdemeTarihi[Calendar.YEAR]
                def odemeler = Yevmiye.executeQuery(tahsilatQuery, [gmkod: musteriHesplanKod, month: month, year: year])
                if(odemeler) {
                    println m
                    println odemeler
                    m.odemeler = odemeler
                }
                //tahsilat.gmkod  = m['tahHesplanKod']
            }

            def tumTahsilatlarQuery = "select tahsilat from Yevmiye tahsilat " +
                    " where tahsilat.gmkod like '${ongoruMusHesplanRefPrefix} %' " +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null" +
//                    " and tahsilat.${tahsilatMusHesplanOdemeTarihField} is not null" +
                    " order by gmkod, ${tahsilatMusHesplanOdemeTarihField}"

            def tumTahsilatlar = Yevmiye.executeQuery(tumTahsilatlarQuery)

            //TODO: odeme planlari ile tum tahsilatlari yedir


           // queryParams = [:] //reset query params
           // queryCriteria = [] //reset query criteria

            //TODO get from param
            //donem plani hesplanindaki yevmiye ile musteri hesplanindaki yevmiyeleri eslestir
            //odeme yapilmayan odeme
            queryCriteria << "tahsilat.gmkod  = concat('${ongoruMusHesplanRefPrefix}', ' ' , yevmiye.${ongoruMusHesplanRefField})"
            //TODO: 121 17 06 hesplanindaki 06 2017 odeme plani yevmiyeleri ile bu donemde yapilan musteri hesplanindaki odemeleri eslestir
            def mySqlDateFormat = ongoruHesplanSuffixDateFormat
            if(ongoruHesplanSuffixDateFormat.contains("MM")){
                mySqlDateFormat =mySqlDateFormat.replace('MM', '%m')
            }
            if(ongoruHesplanSuffixDateFormat.contains("yyyy")){
                mySqlDateFormat = mySqlDateFormat.replace('yyyy', '%Y')
            }
            else if(ongoruHesplanSuffixDateFormat.contains("yy")){
                mySqlDateFormat = mySqlDateFormat.replace('yy', '%y')
            }



//            else if("M" in ongoruHesplanSuffixDateFormat){
//                mySqlDateFormat.replace('M', '%y')
//            }

//            queryCriteria << "yevmiye.gmkod = concat('${ongoruHesplanPrefix}', date_to_string(tahsilat.evraktarihi, '${ongoruHesplanSuffixDateFormat}'))"
            queryCriteria << "yevmiye.gmkod = concat('${ongoruHesplanPrefix}',' ', DATE_FORMAT(tahsilat.evraktarihi, '${mySqlDateFormat}'))"
         //TODO: mssql icin farkli date format function kullanilmali
//             queryCriteria << "yevmiye.gmkod = concat('${ongoruHesplanPrefix}', ' ', substring(year(tahsilat.evraktarihi), 0,2),' ',  month(tahsilat.evraktarihi))"
            //Bu query yi projection veya benzer bir subquery formati ile yazabilir miyiz?


            queryCriteria << "tahsilatHesplan.kod = tahsilat.gmkod"



            //tahsilatin odeme alani olarak kullanilacak kismi bos olmamali
            queryCriteria << "tahsilat.${tahsilatMusHesplanOdemeTutarField} is not null"

            //TODO: yapilan odemelerle esletirip join et sonuclari tek bir yevmiyede birlestirip borc alacak alanlarini doldurabilir misin bak
            def odemePlaniTahsilatJoinQuery = "from Yevmiye yevmiye, Yevmiye tahsilat, Hesplan yevmiyeHesplan, Hesplan tahsilatHesplan "
//            def finalOdemePlaniTahsilatJoinQuery = "select yevmiye, tahsilat, DATE_FORMAT(tahsilat.evraktarihi, '${mySqlDateFormat}')" +
            def finalOdemePlaniTahsilatJoinQuery = "select new map(" +
                    " yevmiye.gmkod as yevGmkod, yevmiye.aciklama as yevAciklama, yevmiye.borclu as yevBorclu,yevmiye.alacakli as yevAlacakli, " +
                    //bu da ongoru.sonodemetarihi olarak cevrilip alinabilir
                    " yevmiye.evrakno as yevEvrakno, " +
                    " yevmiyeHesplan.aciklama as yevHesplanAciklama, yevmiyeHesplan.kod as yevHesplanKod, " +
                    " tahsilatHesplan.aciklama as tahHesplanAciklama, tahsilatHesplan.kod as tahHesplanKod, " +
                    " tahsilat.gmkod as tahGmkod, tahsilat.aciklama as tahAciklama, tahsilat.evraktarihi as tahEvraktarihi, tahsilat.borclu as tahBorclu, tahsilat.alacakli as tahAlacakli," +
                    " tahsilat.${tahsilatMusHesplanOdemeTutarField} as tahTutar)" +

                    " ${odemePlaniTahsilatJoinQuery} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')} " +
                    "  order by tahsilatHesplan.kod, yevmiyeHesplan.kod, tahsilat.evraktarihi"
            def countOdemePlaniTahsilatJoinQuery = "select count (*) ${odemePlaniTahsilatJoinQuery} ${queryCriteria ? 'where ' : ''} ${queryCriteria.join(' and ')}"

            def joinedResults = Yevmiye.executeQuery(finalOdemePlaniTahsilatJoinQuery, queryParams)
            Long count = Yevmiye.executeQuery(countOdemePlaniTahsilatJoinQuery, queryParams)[0]


            def params = [:]
            params['REPORT_LOCALE'] = new Locale('tr', 'TR')
            params['TAHSILAT_TARIHI_FIELD'] = 'evrakno'//new Locale('tr','TR')//TODO: getfrom report config
            params['TAHSILAT_TARIHI_DATE_FORMAT'] = 'dd.MM.yyyy' //TODO: getfrom report config

            String reportname = '/reports/detayli_tahsilat.jrxml'
            //String jasperFileName = 'test.jasper'

            //String subReportName = '/reports/gunluk_odeme_tahsilat_aylik_ozet_subreport.jrxml'

            InputStream reportInputStream = this.class.getResourceAsStream(reportname)
            //InputStream subReportInputStream = this.class.getResourceAsStream(subReportName)

//        JasperCompileManager.compileReportToFile(reportname, jasperFileName)
            def report = JasperCompileManager.compileReport(reportInputStream)
            //def subReport = JasperCompileManager.compileReport(subReportInputStream)

            //params['SUB_REPORT'] = subReport

            JasperPrint print = JasperFillManager.fillReport(report, params,
//                new JRBeanCollectionDataSource(data.items))
                    new JRMapCollectionDataSource(joinedResults))

            JasperExportManager.exportReportToPdf(print)

        }
    }

    def detayliTahsilat(YevmiyeQueryCommand cmd) {
        Tenants.withId("ongoru") {
            def reportConfig = Report.findByReportId('detayli_tahsilat')
            reportConfig.config()

            def monthDateFormat = new SimpleDateFormat('MM-yyyy')
            def evrakNoFormat = new SimpleDateFormat('dd.MM.yyyy')
            def data = yevmiyeService.search(cmd)
            def hesplanMap = [:]

            hesplanService.search(
                    new HesplanQueryCommand(max: -1, kodLikes: cmd.gmkodLikes, sort: 'kod'))
                    .items.each {
                Hesplan h -> hesplanMap.put(h.kod, h.aciklama)
            }


            def joinData = []

            data.items.each { Yevmiye y ->
                def hesplanItems = y.gmkod.split(' ').toList()
//            def depth = hesplanItems.size()
//            def parentGmkod = hesplanItems.subList(0,Math.max(1,depth-2)).join(" ")
                def parentGmkod = y.gmkod.substring(0, y.gmkod.lastIndexOf(" "))
                def rootGmkod = hesplanItems[0]
                //TODO: parent hesplan ve rapor goruntulemede
                //TODO: kullanilacak text degerleri bir config tabvle ile kontrol edilebilmeli
                //FIXME: yilbasindan itibaren cekilen raporda acilis fisleri oldugu icin bakiye hesaplanabiliyor aksi durumda rapor alinan tarihlerden
                //FIXME: bir gun onceki bakiyenin hesaplanmasi ve parametre olarak rapora girilmesi gerekir
                def hesplan = hesplanMap[y.gmkod]
                def parentHesplan = hesplanMap[parentGmkod]
                def rootHesplan = hesplanMap[rootGmkod]
                println y
                joinData << [gmkod            : y.gmkod,
                             hesplan          : hesplan,
                             parentHesplan    : parentHesplan,
                             rootHesplan      : rootHesplan,
//                         month: y.evrakno, //?.length() == 10 ? monthDateFormat.format(evrakNoFormat.parse(y.evrakno)) : '',
//                         month: monthDateFormat.format(y.evrakno?.length() == 10 ? evrakNoFormat.parse(y.evrakno): new Date()),
//                         month: monthDateFormat.format(evrakNoFormat.parse(y.evrakno)),
                             tahsilatTarihiStr: y.evrakno, //TODO: getfrom report config
                             aciklama         : y.aciklama,
                             fistar           : y.fistar,
                             evraktarihi      : y.evraktarihi,
                             borclu           : y.borclu,
                             alacakli         : y.alacakli]
            }

            def params = [:]
            params['REPORT_LOCALE'] = new Locale('tr', 'TR')
            params['TAHSILAT_TARIHI_FIELD'] = 'evrakno'//new Locale('tr','TR')//TODO: getfrom report config
            params['TAHSILAT_TARIHI_DATE_FORMAT'] = 'dd.MM.yyyy' //TODO: getfrom report config

            String reportname = '/reports/detayli_tahsilat.jrxml'
            //String jasperFileName = 'test.jasper'

            //String subReportName = '/reports/gunluk_odeme_tahsilat_aylik_ozet_subreport.jrxml'

            InputStream reportInputStream = this.class.getResourceAsStream(reportname)
            //InputStream subReportInputStream = this.class.getResourceAsStream(subReportName)

//        JasperCompileManager.compileReportToFile(reportname, jasperFileName)
            def report = JasperCompileManager.compileReport(reportInputStream)
            //def subReport = JasperCompileManager.compileReport(subReportInputStream)

            //params['SUB_REPORT'] = subReport

            JasperPrint print = JasperFillManager.fillReport(report, params,
//                new JRBeanCollectionDataSource(data.items))
                    new JRMapCollectionDataSource(joinData))

            JasperExportManager.exportReportToPdf(print)
        }
    }


    private def runReport(String reportFile, JRDataSource jrDataSource){
        def params = [:]
        params['REPORT_LOCALE'] = new Locale('tr', 'TR')
        params['TAHSILAT_TARIHI_FIELD'] = 'evrakno'//new Locale('tr','TR')//TODO: getfrom report config
        params['TAHSILAT_TARIHI_DATE_FORMAT'] = 'dd.MM.yyyy' //TODO: getfrom report config

        String reportname = reportFile
        //String jasperFileName = 'test.jasper'

        //String subReportName = '/reports/gunluk_odeme_tahsilat_aylik_ozet_subreport.jrxml'

        InputStream reportInputStream = this.class.getResourceAsStream(reportname)
        //InputStream subReportInputStream = this.class.getResourceAsStream(subReportName)

//        JasperCompileManager.compileReportToFile(reportname, jasperFileName)
        def report = JasperCompileManager.compileReport(reportInputStream)
        //def subReport = JasperCompileManager.compileReport(subReportInputStream)

        //params['SUB_REPORT'] = subReport

        JasperPrint print = JasperFillManager.fillReport(report, params,
//                new JRBeanCollectionDataSource(data.items))
                jrDataSource)

        JasperExportManager.exportReportToPdf(print)
    }

}
